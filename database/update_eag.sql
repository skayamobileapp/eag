


ALTER TABLE `student`  ADD `country_code` VARCHAR(50) NULL AFTER `id_type`;


ALTER TABLE `fee_structure_master` ADD `description` VARCHAR(2048) NULL DEFAULT '' AFTER `code`;

ALTER TABLE `scholarship_individual_entry_requirement` ADD `entry_type` VARCHAR(50) NULL AFTER `updated_dt_tm`;

UPDATE `scholarship_individual_entry_requirement` SET `entry_type` = 'ENTRY'

ALTER TABLE `applicant` ADD `profile_pic` VARCHAR(1024) NULL DEFAULT 'default_profile.jpg' AFTER `ig_id`;

ALTER TABLE `student` ADD `profile_pic` VARCHAR(1024) NULL DEFAULT 'default_profile.jpg' AFTER `ig_id`;


ALTER TABLE `file_type` CHANGE `file_type_name` `name` VARCHAR(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE `file_type` ADD `status` INT(2) NULL DEFAULT '0' AFTER `name`, ADD `created_by` INT(20) NULL AFTER `status`, ADD `created_dt_tm` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_by`, ADD `updated_by` INT(20) NULL AFTER `created_dt_tm`, ADD `updated_dt_tm` DATETIME NULL AFTER `updated_by`;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'fileType', 'Setup', 'Document', '1', 'fileType', 'list', '5');

ALTER TABLE `discount_type` ADD `status` INT(2) NULL DEFAULT '0' AFTER `description`, ADD `created_by` INT(20) NULL AFTER `status`, ADD `created_dt_tm` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_by`, ADD `updated_by` INT(20) NULL AFTER `created_dt_tm`, ADD `updated_dt_tm` DATETIME NULL AFTER `updated_by`;

ALTER TABLE `discount` ADD `status` INT(2) NULL DEFAULT '0' AFTER `currency`, ADD `created_by` INT(20) NULL AFTER `status`, ADD `created_dt_tm` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_by`, ADD `updated_by` INT(20) NULL AFTER `created_dt_tm`, ADD `updated_dt_tm` DATETIME NULL AFTER `updated_by`;

CREATE TABLE `program_landscape_learning_mode` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_program_landscape` bigint(20) NOT NULL DEFAULT 0,
  `id_programme` bigint(20) NOT NULL DEFAULT 0,
  `code` varchar(1024) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `mode_of_program` varchar(2048) DEFAULT '',
  `mode_of_study` varchar(2048) DEFAULT '',
  `id_program_type` bigint(20) NOT NULL DEFAULT 0,
  `total_semester` bigint(20) NOT NULL DEFAULT 0,
  `min_cr_hrs` bigint(20) NOT NULL DEFAULT 0,
  `max_cr_hrs` bigint(20) NOT NULL DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(1024) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `event` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(1024) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `start_date` varchar(2048) DEFAULT '',
  `end_date` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Notification', 'Communication', 'Alerts', '1', 'notification', 'list', '3');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Events', 'Communication', 'Alerts', '2', 'events', 'list', '3');

ALTER TABLE `fee_structure_activity` ADD `id_currency` INT(20) NULL DEFAULT '0' AFTER `amount_international`;

ALTER TABLE `apply_change_programme` ADD `by_student` INT(20) NULL DEFAULT '0' AFTER `id_new_program_has_scheme`;

ALTER TABLE `apply_change_scheme` ADD `by_student` INT(20) NULL DEFAULT '0' AFTER `id_new_program_scheme`;

ALTER TABLE `communication_template` ADD `id_university` INT(20) NULL DEFAULT '0' AFTER `message`, ADD `id_education_level` INT(20) NULL DEFAULT '0' AFTER `id_university`;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'New Student Enrollment', 'Setup', 'Student Enrollment', '1', 'enrollment', 'list', '7');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Checklist For StudentVisa', 'Setup', 'Student Enrollment', '2', 'checklistVisa', 'list', '7');

CREATE TABLE `enrollment_setup` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_university` int(20) DEFAULT 0,
  `id_education_level` int(20) DEFAULT 0,
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `checklist_visa_setup` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_university` int(20) DEFAULT 0,
  `id_education_level` int(20) DEFAULT 0,
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
------ Update CMS From Here ------

ALTER TABLE `communication_template_message` ADD `id_university` INT(20) NULL DEFAULT '0' AFTER `message`, ADD `id_education_level` INT(20) NULL DEFAULT '0' AFTER `id_university`



CREATE TABLE `temp_test_booking_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(2048) DEFAULT '',
  `booking_type` varchar(2048) DEFAULT '',
  `id_test` int(20) DEFAULT 0,
  `id_package` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `test_booking_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_test_booking` varchar(2048) DEFAULT '',
  `booking_type` varchar(2048) DEFAULT '',
  `id_test` int(20) DEFAULT 0,
  `id_package` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `temp_sample_booking_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(2048) DEFAULT '',
  `id_sample` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `sample_booking_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_test_booking` varchar(2048) DEFAULT '',
  `id_sample` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;






CREATE TABLE `test_booking` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_patient` int(20) DEFAULT 0,
  `id_staff` int(20) DEFAULT 0,
  `time_slot` varchar(200) DEFAULT '',
  `appointment_date` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




























