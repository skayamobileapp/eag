<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class PartnerUniversityLogin extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('partner_university_login_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
            // echo "Not Login";exit;
        $this->checkPartnerUniversityLoggedIn();
    }
    

    function checkPartnerUniversityLoggedIn()
    {
        $isPartnerUniversityLoggedIn = $this->session->userdata('isPartnerUniversityLoggedIn');
        
        if(!isset($isPartnerUniversityLoggedIn) || $isPartnerUniversityLoggedIn != TRUE)
        {
            // echo "Not Login";exit;
            $this->load->view('partner_university_login');
        }
        else
        {
            // echo "Login";exit;
            
            redirect('/partner_university/student/welcome');
            // redirect('partner_university/scheme/list');
        }
    }


    public function partnerUniLogin()
    {
        $formData = $this->input->post();
        // echo "<Pre>"; print_r($formData);exit;
        // $domain = $this->getDomainName();

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|max_length[32]');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = strtolower($this->security->xss_clean($this->input->post('email')));
            $password = $this->input->post('password');
            
            $result = $this->partner_university_login_model->loginPartnerUniversity($email, $password);
            
            // echo "<Pre>";print_r($result); exit;
            
            if(!empty($result))
            {
                
                    
                $lastLogin = $this->partner_university_login_model->partnerUniversityLastLoginInfo($result->id);

                if($lastLogin == '')
                {
                    $partner_university_login = date('Y-m-d h:i:s');
                }
                else
                {
                    $partner_university_login = $lastLogin->created_dt_tm;
                }

                $sessionArray = array('id_partner_university'=>$result->id,
                                        'partner_university_name'=>$result->name,
                                        'partner_university_code'=>$result->code,
                                        'partner_university_login_id'=>$result->login_id,
                                        'partner_university_last_login'=> $partner_university_login,
                                        'isPartnerUniversityLoggedIn' => TRUE
                                );
        // echo "<Pre>";print_r($sessionArray);exit();

                $this->session->set_userdata($sessionArray);

                unset($sessionArray['id_partner_university'], $sessionArray['isPartnerUniversityLoggedIn'], $sessionArray['partner_university_last_login']);

                $loginInfo = array("id_partner_university"=>$result->id, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());

                $uniqueId = rand(0000000000,9999999999);
                $this->session->set_userdata("my_partner_university_session_id", md5($uniqueId));


                $this->partner_university_login_model->addPartnerUniversityLastLogin($loginInfo);

                // echo "<Pre>"; print_r($this->session->userdata());exit();
                // echo "Login";exit();
                redirect('/partner_university/student/welcome');
                }
            else
            {
                $this->session->set_flashdata('error', 'Login ID or Password Mismatch');
                
                $this->index();
            }
        }
    }
}

?>