<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class InvestmentWithdraw extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('investment_withdraw_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('investment_withdraw.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['registration_number'] = $this->security->xss_clean($this->input->post('registration_number'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['investmentWithdrawList'] = $this->investment_withdraw_model->getInvestmentWithdrawListSearch($formData);
            // echo "<Pre>";print_r($data['investmentRegistrationList']);exit;

            $this->global['pageTitle'] = 'FIMS : List Investment Withdraw';
            $this->loadViews("investment_withdraw/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('investment_withdraw.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $generated_number = $this->investment_withdraw_model->generateInvestmentWithdraw();

                $id_investment_registration = $this->security->xss_clean($this->input->post('id_investment_registration'));


                $detail_data = $this->investment_withdraw_model->getInvestmentRegistration($id_investment_registration);

                // echo "<Pre>";print_r($detail_data);exit();

                // For Investment Registration
                // investment_status Field
                // 1-> Initial Entry - Registered
                // 0-> Added To Withdraw Application
                // 2-> Approved Withdraw Application
                // 3-> Reinvested Application

                $data = array(
                    'id_application' => $detail_data->id_application,
                    'id_application_detail' => $detail_data->id_application_detail,
                    'id_investment_registration' => $id_investment_registration,
                    'reference_number' => $generated_number,
                    'amount' => $detail_data->amount,
                    'status'=> '0',
                    'created_by'=> $user_id
                );
                 // echo "<Pre>";print_r($data);exit();

                $inserted_id = $this->investment_withdraw_model->addInvestmentRegistrationWithdraw($data);

                if($inserted_id)
                {
                 // echo "<Pre>";print_r($inserted_id);exit();

                    $data = array(
                        'investment_status' => 0
                    );

                    $detail_data_update = $this->investment_withdraw_model->updateInvestmentRegistrationForWithdraw($data,$id_investment_registration);
                }


                redirect('/investment/investmentWithdraw/list');


            }

            $data['investmentRegistrationList'] = $this->investment_withdraw_model->investmentRegistrationListForWithdraw();

            // echo "<Pre>";print_r($data);exit();


            $this->global['pageTitle'] = 'FIMS : Add Investment Registration Withdraw';
            $this->loadViews("investment_withdraw/add", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('investment_withdraw.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/investment/investmentWithdraw/approvalList');
            }

            $data['investmentWithdraw'] = $this->investment_withdraw_model->getInvestmentWithdraw($id);

            // echo "<Pre>";print_r($data['investmentWithdraw']);exit();
            
            $this->global['pageTitle'] = 'FIMS : View Investment Registration Withdraw';
            $this->loadViews("investment_withdraw/view", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('investment_withdraw.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/investment/investmentWithdraw/list');
            }

            $data['investmentWithdraw'] = $this->investment_withdraw_model->getInvestmentWithdraw($id);

            // echo "<Pre>";print_r($data['investmentWithdraw']);exit();
            
            $this->global['pageTitle'] = 'FIMS : View Investment Registration Withdraw';
            $this->loadViews("investment_withdraw/edit", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('investment_withdraw.aprovel_list') == 1)
        // if ($this->checkAccess('investment_withdraw.approval') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 

            $resultprint = $this->input->post();

           if($resultprint)
            {
             switch ($resultprint['button'])
             {
                case 'approve':

                     for($i=0;$i<count($resultprint['checkvalue']);$i++)
                        {

                         $id = $resultprint['checkvalue'][$i];
                         // echo "<Pre>";print_r($id);exit();

                         $data = array('status' => 1);
                         
                         $result = $this->investment_withdraw_model->approveInvestmentWithdraw($data,$id,'1');
                        }
                        redirect($_SERVER['HTTP_REFERER']);
                     break;


                case 'reject':

                     for($i=0;$i<count($resultprint['checkvalue']);$i++)
                        {

                         $id = $resultprint['checkvalue'][$i];
                         // echo "<Pre>";print_r($id);exit();

                         $data = array('status' => 2);
                         
                         $result = $this->investment_withdraw_model->approveInvestmentWithdraw($data,$id,'2');
                        }
                        redirect($_SERVER['HTTP_REFERER']);
                     break;


                case 'search':

                  
                     
                     break;
                 
                default:
                     break;
             }
                
            }
            
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['registration_number'] = $this->security->xss_clean($this->input->post('registration_number'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;
            $data['investmentWithdrawList'] = $this->investment_withdraw_model->getInvestmentWithdrawListSearch($formData);



            // echo "<Pre>";print_r($data['investmentWithdrawList']);exit;


            $this->global['pageTitle'] = 'FIMS : Approve Investment Withdraw';
            $this->loadViews("investment_withdraw/approval_list", $this->global, $data, NULL);
        }
    }



    function getRegistrationDetails($id_investment_registration)
    {
        if($id_investment_registration)
        {
       
        $results = $this->investment_withdraw_model->getInvestmentRegistrationDetail($id_investment_registration);
        // echo "<Pre>";print_r($results);exit();
        

        $id = $results->id;
        $duration = $results->duration;
        $duration_type = $results->duration_type;
        $amount = $results->amount;
        $name = $results->name;
        $profit_rate = $results->profit_rate;
        $maturity_date = date("d-m-Y", strtotime($results->maturity_date));
        $effective_date = date("d-m-Y", strtotime($results->effective_date));
        $investment_type = $results->investment_type;
        $investment_institution_code = $results->investment_institution_code;
        $investment_institution_name = $results->investment_institution_name;
        $bank_code = $results->bank_code;
        $bank_name = $results->bank_name;
        $rate_of_interest = $results->rate_of_interest;
        $profit_amount = $results->profit_amount;




        $interest_to_bank = $results->interest_to_bank;
        $interest_day = $results->interest_day;
        $file_upload = $results->file_upload;
        $contact_person_one = $results->contact_person_one;
        $contact_email_one = $results->contact_email_one;
        $contact_person_two = $results->contact_person_two;
        $contact_email_two = $results->contact_email_two;

        $account_code_name = $results->account_code_name;
        $activity_code_name = $results->activity_code_name;
        $department_code_name = $results->department_code_name;
        $fund_code_name = $results->fund_code_name;
        $account_code = $results->account_code;
        $activity_code = $results->activity_code;
        $department_code = $results->department_code;
        $fund_code = $results->fund_code;


            $table="

             <script type='text/javascript'>
                $('select').select2();
            </script>


            <div class='page-title clearfix'>
                <h3>Investment Registration Details</h3>
            </div>
           

        <div class='row'>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Duration <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='amount' name='amount' readonly='readonly' value=' $duration $duration_type'>
                    </div>
            </div>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Profit Rate <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='amount' name='amount' readonly='readonly' value=' $profit_rate'>
                    </div>
            </div>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Investment Type <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='amount' name='amount' readonly='readonly' value=' $investment_type '>
                    </div>
            </div>            

        </div>

        <div class='row'>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Investment Institution <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='amount' name='amount' readonly='readonly' value=' $investment_institution_code - $investment_institution_name '>
                    </div>
            </div>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Investment Bank <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='amount' name='amount' readonly='readonly' value=' $bank_code - $bank_name'>
                    </div>
            </div>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Maturity Date <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='maturity_date' name='maturity_date' readonly='readonly' value='$maturity_date'>
                    </div>
            </div>

        </div>

        <div class='row'>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Investment Amount <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='amount' name='amount' readonly='readonly' value='$amount'>
                    </div>
            </div>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Description For Registration <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='amount' name='amount' readonly='readonly' value='$name'>
                    </div>
            </div>


            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Interest To Bank <span class='error-text'>*</span></label>
                        <input type='number' class='form-control' id='interest_to_bank' name='interest_to_bank' readonly='readonly' value='$interest_to_bank'>
                    </div>
            </div>

        </div>

        <div class='row'>

         <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Effective Date <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='effective_date' name='effective_date' readonly='readonly' value='$effective_date'>
                    </div>
            </div>




            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Contact Person Name <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='contact_person_one' name='contact_person_one' readonly='readonly' value='$contact_person_one'>
                    </div>
            </div>


            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Contact Person Email <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='contact_email_one' name='contact_email_one' readonly='readonly' value='$contact_email_one'>
                    </div>
            </div>

        </div>


        <div class='row'>


            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Rate Of Interest <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='rate_of_interest' name='rate_of_interest' readonly='readonly' value='$rate_of_interest' >
                    </div>
            </div>


            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Contact Person 2 Name <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='contact_person_two' name='contact_person_two' readonly='readonly' value='$contact_person_two'>
                    </div>
            </div>


            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Contact Person 2 Email <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='contact_email_two' name='contact_email_two' readonly='readonly' value='$contact_email_two'>
                    </div>
            </div>

        </div>


        <div class='row'>

        

         <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Interest Day <span class='error-text'>*</span></label>
                        <input type='number' class='form-control' id='interest_day' name='interest_day' readonly='readonly' value='$interest_day'>
                    </div>
            </div>


            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Profit Amount <span class='error-text'>*</span></label>
                        <input type='number' class='form-control' id='profit_amount' name='profit_amount' readonly='readonly' value='$profit_amount'>
                    </div>
            </div>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Account Code <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='account_code' name='account_code' readonly='readonly' value=' $account_code - $account_code_name '>
                    </div>
            </div>


        </div>  


        <div class='row'>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Activity Code <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='activity_code' name='activity_code' readonly='readonly' value=' $activity_code - $activity_code_name '>
                    </div>
            </div>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Department Code <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='department_code' name='department_code' readonly='readonly' value=' $department_code - $department_code_name'>
                    </div>
            </div>

            <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Fund Code <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='fund_code' name='fund_code' readonly='readonly' value='$fund_code - $fund_code_name'>
                    </div>
            </div>

        </div>
            ";


            echo $table;
        }

    }
}
