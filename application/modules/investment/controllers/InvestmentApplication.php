<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class InvestmentApplication extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('investment_application_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('investment_application.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['investmentBankList'] = $this->investment_application_model->getBankList();
            $data['investmentInstitutionList'] = $this->investment_application_model->investmentInstitutionListByStatus('1');

            $formData['id_investment_bank'] = $this->security->xss_clean($this->input->post('id_investment_bank'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['investmentApplicationList'] = $this->investment_application_model->getInvestmentApplicationListSearch($formData);

            $this->global['pageTitle'] = 'FIMS : List Investment Application';
            // echo "<Pre>";print_r($data['investmentApplicationList']);exit;
            $this->loadViews("investment_application/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('investment_application.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                

                $generated_number = $this->investment_application_model->generateInvestmentApplicationNumber();

                $description = $this->security->xss_clean($this->input->post('description'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));
                $id_investment_bank = $this->security->xss_clean($this->input->post('id_investment_bank'));

                $data = array(
                    'application_number' =>$generated_number,
                    'description' =>$description,
                    'date_time' => date("Y-m-d", strtotime($date_time)),
                    'amount' => $amount,
                    'id_investment_bank' => $id_investment_bank,
                    'created_by' => $user_id,
                    'status'=>'0'
                );
             $inserted_id = $this->investment_application_model->addInvestmentApplication($data);


                $temp_details = $this->investment_application_model->getTempDetailsBySession($id_session);
                     // echo "<Pre>";print_r($temp_details);exit;

                 for($i=0;$i<count($temp_details);$i++)
                 {

                        $duration = $temp_details[$i]->duration;
                        $duration_type = $temp_details[$i]->duration_type;
                        $total_amount = $temp_details[$i]->total_amount;
                        $id_institution = $temp_details[$i]->id_institution;
                        $id_investment_type = $temp_details[$i]->id_investment_type;
                        $profit_rate = $temp_details[$i]->profit_rate;
                        $id_bank = $temp_details[$i]->id_bank;
                        $bank_branch = $temp_details[$i]->bank_branch;
                        $maturity_date =  $temp_details[$i]->maturity_date;
                    

                     $detailsData = array(
                        'id_application'=> $inserted_id,
                        'duration'=> $duration,
                        'duration_type'=> $duration_type,
                        'total_amount'=> $total_amount,
                        'id_institution'=> $id_institution,
                        'id_investment_type'=> $id_investment_type,
                        'profit_rate'=> $profit_rate,
                        'id_bank'=> $id_bank,
                        'bank_branch'=> $bank_branch,
                        'maturity_date'=> date("Y-m-d", strtotime($maturity_date)),
                        'status'=> '0',
                        'created_by'=>$user_id

                    );
                     
                    $result = $this->investment_application_model->addNewInvestmentApplicationDetail($detailsData);
                     // echo "<Pre>";print_r($result);exit;
                }

                $this->investment_application_model->deleteTempDetailsBySession($id_session);
                redirect('/investment/investmentApplication/list');


            }
            else
            {
                $this->investment_application_model->deleteTempDetailsBySession($id_session);
            }

            $data['investmentBankList'] = $this->investment_application_model->getBankList();
            $data['bankList'] = $this->investment_application_model->getBankList();
            $data['investmentInstitutionList'] = $this->investment_application_model->investmentInstitutionListByStatus('1');
            $data['investmentTypeList'] = $this->investment_application_model->investmentTypeListByStatus('1');

            // echo "<Pre>";print_r($data);exit();


            $this->global['pageTitle'] = 'FIMS : Add Investment Application';
            $this->loadViews("investment_application/add", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('investment_application.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/investment/investmentApplication/list');
            }
            
            
            $data['investmentApplication'] = $this->investment_application_model->getInvestmentApplication($id);
            $data['iADetails'] = $this->investment_application_model->getInvestmentApplicationDetails($id);

            // echo "<Pre>";print_r($data);exit();
            

            $this->global['pageTitle'] = 'FIMS : View Investment Application';
            $this->loadViews("investment_application/edit", $this->global, $data, NULL);
        }
    }


    function view($id = NULL)
    {
        if ($this->checkAccess('investment_application.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/investment/investmentApplication/approvalList');
            }
            


            if($this->input->post())
            {
             $resultprint = $this->input->post();
             // echo "<Pre>"; print_r($resultprint);exit;

             switch ($resultprint['button'])
             {
                 case 'approve':
                     if(!empty($resultprint['approval']))
                     {
             // echo "<Pre>"; print_r($resultprint);exit;
                         for($i=0;$i<count($resultprint['approval']);$i++)
                            {
                                $id_detail = $resultprint['approval'][$i];
                                $remarks = $resultprint['remarks'][$i];

                                // echo "<Pre>";print_r($id);exit();
                                $data = array(
                                    'status' => 1,
                                    'remarks' => $remarks,
                                );
                                $result = $this->investment_application_model->editInvestmentApplicationDetails($data, $id_detail,$id);
                            }
                            // redirect(['HTTP_REFERER']);
                            redirect('/investment/investmentApplication/view/' . $id);
                      }
                     break;


                     case 'search':
                     
                     break;
                 
                 default:
                     break;
             }
                
            }





            $data['investmentApplication'] = $this->investment_application_model->getInvestmentApplication($id);
            $data['iADetails'] = $this->investment_application_model->getInvestmentApplicationDetails($id);

            // echo "<Pre>";print_r($data['iADetails']);exit();
            

            $this->global['pageTitle'] = 'FIMS : View Investment Application';
            $this->loadViews("investment_application/view", $this->global, $data, NULL);
        }
    }



    function tempadd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        $tempData['maturity_date'] = date("Y-m-d", strtotime($tempData['maturity_date']));
        
        // echo "<Pre>";print_r($tempData);exit();
        // echo "<Pre>";  print_r($tempData);exit();
        // $check = $this->investment_application_model->checkDuplicationOnTempDetails($tempData['id_investment_type'],$tempData['id_session']);
        // if($check)
        // {
        //     echo "<Pre>";print_r($check);exit();
        // }
        // else
        // {
            $inserted_id = $this->investment_application_model->addTempDetails($tempData);
            $data = $this->displaytempdata();
        // }
        
        echo $data;        
    }

    function tempDelete($id) {
        $id_session = $this->session->my_session_id;
        $inserted_id = $this->investment_application_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $details = $this->investment_application_model->getTempInvestmentApplicationDetailsBySession($id_session); 
        if(!empty($details))
        {
        // echo "<Pre>";print_r($details);exit;
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                  <tr>
                  <thead>
                    <th>Sl. No</th>
                    <th>Duration</th>
                    <th>Investment Institution</th>
                    <th>Bank</th>
                    <th>Profit rate</th>
                    <th>Investment Type</th>
                    <th>Maturity Date</th>
                    <th>Investment Amount</th>
                    <th>Action</th>
                    </tr>
                    </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($details);$i++)
                    {
                        $id = $details[$i]->id;
                        $duration = $details[$i]->duration;
                        $duration_type = $details[$i]->duration_type;
                        $total_amount = $details[$i]->total_amount;
                        $profit_rate = $details[$i]->profit_rate;
                        $maturity_date = date("d-m-Y", strtotime($details[$i]->maturity_date));
                        $investment_type = $details[$i]->investment_type;
                        $investment_institution_code = $details[$i]->investment_institution_code;
                        $investment_institution_name = $details[$i]->investment_institution_name;
                        $bank_code = $details[$i]->bank_code;
                        $bank_name = $details[$i]->bank_name;

                        $j=$i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j .</td>
                            <td>$duration  $duration_type</td>
                            <td>$investment_institution_code  $investment_institution_name</td>
                            <td>$bank_code  $bank_name</td>
                            <td>$profit_rate</td>
                            <td>$investment_type</td>
                            <td>$maturity_date</td>
                            <td>$total_amount</td>
                            
                            <td>
                                <span class='btn btn-sm btn-edit' onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                        $total_detail = $total_detail + $total_amount;
                    }

                     $table .= "

                    <tr>
                           
                            <td bgcolor=''></td>
                            <td bgcolor=''></td>
                            <td bgcolor=''></td>
                            <td bgcolor=''></td>
                            <td bgcolor=''></td>
                            <td bgcolor=''></td>
                            <td bgcolor=''><b>Total :</b></td>
                            <td bgcolor=''><input type='hidden' name='total_detail' id='total_detail' value='$total_detail' /><b>$total_detail</b></td>
                            <td bgcolor=''></td>
                            
                            <td bgcolor=''></td>
                        </tr>
                        </tbody>";

                    // <td>
                    //             <span onclick='getTempData($id)'>Edit</a>
                    //         <td>
        $table.= "</table>
        </div>";
        }
        else
        {
            $total_detail='0';
            $table = "
            <input type='hidden' name='total_detail' id='total_detail' value='$total_detail' />
            ";
            $table = "";
        }
        return $table;
    }


    function approvalList()
    {
        if ($this->checkAccess('investment_application_approval.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 

            if($this->input->post())
            {
             $resultprint = $this->input->post();
             // echo "<Pre>"; print_r($resultprint['button']);exit;

             switch ($resultprint['button'])
             {
                 case 'Approve':
                     for($i=0;$i<count($resultprint['approval']);$i++)
                    {

                         $id = $resultprint['approval'][$i];
                         $data = array(
                            'status' => 1,
                        );
                        $result = $this->investment_application_model->editInvestmentApplication($data, $id);


                    }
                        redirect('/investment/investmentApplication/approvalList');
                     break;


                     case 'search':                     
                     break;
                 
                 default:
                     break;
             }
                
            }



           $data['investmentBankList'] = $this->investment_application_model->getBankList();
            $data['investmentInstitutionList'] = $this->investment_application_model->investmentInstitutionListByStatus('1');

            $formData['id_investment_bank'] = $this->security->xss_clean($this->input->post('id_investment_bank'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;

            $data['investmentApplicationList'] = $this->investment_application_model->getInvestmentApplicationListSearch($formData);



            
                // $array = $this->security->xss_clean($this->input->post('checkvalue'));
                // if (!empty($array))
                // {

                //     $result = $this->investment_application_model->editPRList($array);
                //     redirect($_SERVER['HTTP_REFERER']);
                // }


            $this->global['pageTitle'] = 'FIMS : Approve Investment Application';
            $this->loadViews("investment_application/approval_list", $this->global, $data, NULL);
        }
    }


    function getItemBySubCategoryId()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id_sub_category = $tempData['id_sub_category'];
        $id_category = $tempData['id_category'];
        $type = $tempData['type'];
        // echo "<Pre>";print_r($tempData);exit();
        if($type == 'Asset')
        {
            $results = $this->investment_application_model->assetItemBySubCategory($id_category,$id_sub_category);

        }elseif ($type == 'Procurement')
        {
            $results = $this->investment_application_model->procurementItemBySubCategory($id_category,$id_sub_category);            
        }


            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>

            <select name='id_item' id='id_item' class='form-control'>";

            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $code = $results[$i]->code;
            $description = $results[$i]->description;
            $table.="<option value=".$id.">".$code. " - " . $description . "</option>";

            }
            $table.="</select>";

            echo $table;

    }
}
