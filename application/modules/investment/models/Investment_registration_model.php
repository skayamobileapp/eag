<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Investment_registration_model extends CI_Model
{
	function investmentApplicationListForRegistration()
    {
        $this->db->select('*');
        $this->db->from('investment_application');
        $this->db->where('status', '1');
        $this->db->where('is_registered', '0');
        $query = $this->db->get();
        return $query->result();
    }

    function getInvestmentApplicationDetailsForRegistration($id_application)
    {
    	$this->db->select('inad.*, invt.type as investment_type');
        $this->db->from('investment_application_details as inad');
        $this->db->join('investment_type as invt', 'inad.id_investment_type = invt.id');
        $this->db->where('inad.status', '1');
        $this->db->where('inad.is_registered', '0');
        $this->db->where('inad.id_application', $id_application);
        $query = $this->db->get();
        return $query->result();
    }

    function getInvestmentApplicationDetail($id_application_detail)
    {
    	$this->db->select('tpe.*, invt.type as investment_type, invi.code as investment_institution_code, invi.name as investment_institution_name, bnk.code as bank_code, bnk.name as bank_name');
        $this->db->from('investment_application_details as tpe');
        $this->db->join('investment_type as invt', 'tpe.id_investment_type = invt.id');
        $this->db->join('investment_institution as invi', 'tpe.id_institution = invi.id');
        $this->db->join('bank_registration as bnk', 'tpe.id_bank = bnk.id');
        $this->db->where('tpe.id', $id_application_detail);
        $query = $this->db->get();
        return $query->row();
    }


    function getAccountCodeList()
    {
        $this->db->select('*');
        $this->db->from('account_code');
        $this->db->where('status', '1');
        $this->db->where('level', '3');
        $query = $this->db->get();
        return $query->result();
    }

    function getActivityCodeList()
    {
        $this->db->select('*');
        $this->db->from('activity_code');
        $this->db->where('status', '1');
        $this->db->where('level', '3');
        $query = $this->db->get();
        return $query->result();
    }

    function getDepartmentCodeList()
    {
        $this->db->select('*');
        $this->db->from('department_code');
         $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function getFundCodeList()
    {
        $this->db->select('*');
        $this->db->from('fund_code');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function generateInvestmentRegistrationNumber()
    {
    	$year = date('y');
        $Year = date('Y');
            $this->db->select('*');
            $this->db->from('investment_registration');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
            $generated_number = "INVR" .(sprintf("%'06d", $count)). "/" . $Year;
           // echo "<Pre>";print_r($generated_number);exit();
           return $generated_number;
    }

    function addInvestmentRegistration($data)
    {
    	$this->db->trans_start();
        $this->db->insert('investment_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getInvestmentRegistrationListSearch($data)
    {
        $this->db->select('ina.*');
        $this->db->from('investment_registration as ina');
        if ($data['name']!='')
        {
            $likeCriteria = "(ina.name  LIKE '%" . $data['name'] . "%' or ina.registration_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($result);exit();     
         return $result;
    }

    function getInvestmentApplicationDetailById($id)
    {
    	$this->db->select('ina.*');
        $this->db->from('investment_application_details as ina');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function updateInvestmentApplicationDetailById($id_detail,$id_application,$id)
    {
                 // echo "<Pre>";print_r($id_detail);exit();
    	
    	$data = array(
    		'is_registered' => $id,
    	);
    	$this->db->where('id', $id_detail);
        $this->db->update('investment_application_details', $data);

        $is_set = $this->checkInvestmentAppplicationDetailRemaining($id_application);

        if($is_set)
        {

        }
        else
        {
        	$this->updateInvestmentApplicationToRegister($id_application);
        }

        return TRUE;
    }

    function checkInvestmentAppplicationDetailRemaining($id)
    {
    	$this->db->select('ina.*');
        $this->db->from('investment_application_details as ina');
        $this->db->where('id_application', $id);
        $this->db->where('is_registered', '0');
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function updateInvestmentApplicationToRegister($id)
    {
    	$data = array(
    		'is_registered' => 1
    	);
    	$this->db->where('id', $id);
        $this->db->update('investment_application', $data);

        return TRUE;
    }

    function getInvestmentRegistration($id)
    {
        $this->db->select('tpe.*, inva.application_number, invt.type as investment_type, invi.code as investment_institution_code, invi.name as investment_institution_name, bnk.code as bank_code, bnk.name as bank_name, accc.name as account_code_name, actc.name as activity_code_name, depc.name as department_code_name, func.name as fund_code_name');
        $this->db->from('investment_registration as tpe');
        $this->db->join('investment_application as inva', 'tpe.id_application = inva.id');
        $this->db->join('investment_type as invt', 'tpe.id_investment_type = invt.id');
        $this->db->join('investment_institution as invi', 'tpe.id_institution = invi.id');
        $this->db->join('bank_registration as bnk', 'tpe.id_bank = bnk.id');
        $this->db->join('account_code as accc', 'tpe.account_code = accc.code');
        $this->db->join('activity_code as actc', 'tpe.activity_code = actc.code');
        $this->db->join('department_code as depc', 'tpe.department_code = depc.code');
        $this->db->join('fund_code as func', 'tpe.fund_code = func.code');
        $this->db->where('tpe.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getInvestmentRegistrationListSearchForReinvestment($data)
    {
        $this->db->select('ina.*');
        $this->db->from('investment_registration as ina');
        if ($data['name']!='')
        {
            $likeCriteria = "(ina.name  LIKE '%" . $data['name'] . "%' or ina.registration_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $likeCriteria = "(date(ina.maturity_date)  < '" . date('Y-m-d') . "')";
            $this->db->where($likeCriteria);
        $this->db->where('ina.investment_status', '1');
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($result);exit();     
         return $result;
    }

    function reinvest($id)
    {
        $data = array(
            'investment_status' => 3
        );
        $this->db->where('id', $id);
        $this->db->update('investment_registration', $data);

        $data_row = $this->getRegistrationDataRow($id);


        $data_row->is_reinvested = $data_row->id;
        $data_row->investment_status = '1';
        $data_row->registration_number = $this->generateInvestmentRegistrationNumber();
        $data_row->maturity_date =  date('Y-m-d', strtotime('+1 year'));
        $data_row->effective_date =  date('Y-m-d');
        unset($data_row->id);
        // echo "<Pre>";print_r($data_row);exit();


        $inserted_id = $this->addInvestmentRegistration($data_row);

        return TRUE;
    }

    function getRegistrationDataRow($id)
    {
        $this->db->select('*');
        $this->db->from('investment_registration');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;   
    }
}