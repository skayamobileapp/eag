<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Investment Institution</h3>
        </div>
        <form id="form_investment_institution" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Investment Institution Details</h4>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Bank <span class='error-text'>*</span></label>
                        <select name="id_bank" id="id_bank" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($bankList))
                            {
                                foreach ($bankList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code ."-". $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="contact_name" name="contact_name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="contact_number" name="contact_number">
                    </div>
                </div>
            

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address" name="address">
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Country <span class='error-text'>*</span></label>
                        <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>State <span class='error-text'>*</span></label>
                        <span id='view_state'></span>
                    </div>
                </div>
            

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Landmark <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="landmark" name="landmark">
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Branch Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="branch" name="branch">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Zipcode <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode">
                    </div>
                </div>

            </div>


            <div class="row">


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
            
            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>

    function getStateByCountry(id)
    {
        // alert(id);
        $.get("/investment/InvestmentInstitution/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
        });
    }



    $(document).ready(function() {
        $("#form_investment_institution").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                id_bank: {
                    required: true
                },
                contact_name: {
                    required: true
                },
                contact_number: {
                    required: true
                },
                address: {
                    required: true
                },
                id_country: {
                    required: true
                },
                id_state: {
                    required: true
                },
                landmark: {
                    required: true
                },
                branch: {
                    required: true
                },
                city: {
                    required: true
                },
                zipcode: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Institution Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Institution Code Required</p>",
                },
                id_bank: {
                    required: "<p class='error-text'>Select Bank</p>",
                },
                contact_name: {
                    required: "<p class='error-text'>Enter Contact Name</p>",
                },
                contact_number: {
                    required: "<p class='error-text'>Enter Contact Number</p>",
                },
                address: {
                    required: "<p class='error-text'>Enter Address</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                landmark: {
                    required: "<p class='error-text'>Enter Landmark</p>",
                },
                branch: {
                    required: "<p class='error-text'>Enter Branch Name</p>",
                },
                city: {
                    required: "<p class='error-text'>Enter City Name</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Enter Zip Code</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>

<script type='text/javascript'>
     $('select').select2();
 </script>