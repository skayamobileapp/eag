<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Investment Registration</h3>
        </div>

        <br>
        <form id="form_investment_institution" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Investment Registration Details</h4>





            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Investment Institution <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentRegistration->investment_institution_code . " - " . $investmentRegistration->investment_institution_name; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Investment Bank <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentRegistration->bank_code . " - " . $investmentRegistration->bank_name; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Investment Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentRegistration->investment_type; ?>" readonly="readonly">
                    </div>
                </div>

            </div>





            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Registration Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $investmentRegistration->registration_number; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Application Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $investmentRegistration->application_number; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Registration Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $investmentRegistration->name; ?>" readonly="readonly">
                    </div>
                </div>               


            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="contact_name" name="contact_name" value="<?php echo $investmentRegistration->rate_of_interest; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="contact_number" name="contact_number" value="<?php echo $investmentRegistration->contact_person_one; ?>" readonly="readonly">
                    </div>
                </div>
            

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address" name="address" value="<?php echo $investmentRegistration->contact_email_one; ?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person 2 Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="branch" name="branch" value="<?php echo $investmentRegistration->contact_person_two; ?>"  readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Two Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city" value="<?php echo $investmentRegistration->contact_email_two; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Effective Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo date("d-m-Y", strtotime($investmentRegistration->effective_date)); ?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Maturity Date <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode" value="<?php echo date("d-m-Y", strtotime($investmentRegistration->maturity_date)); ?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Profit Rate <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentRegistration->profit_rate; ?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Profit Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentRegistration->profit_amount; ?>" readonly="readonly">
                    </div>
                </div>

            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Interest To Bank <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentRegistration->interest_to_bank; ?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Interest Day <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentRegistration->interest_day; ?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Duration <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentRegistration->duration . " - " . $investmentRegistration->duration_type; ?>" readonly="readonly">
                    </div>
                </div>

            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentRegistration->account_code . " - " . $investmentRegistration->account_code_name; ?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Activity Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentRegistration->activity_code . " - " . $investmentRegistration->activity_code_name; ?>" readonly="readonly">
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentRegistration->department_code . " - " . $investmentRegistration->department_code_name; ?>" readonly="readonly">
                    </div>
                </div>

            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fund Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $investmentRegistration->fund_code . " - " . $investmentRegistration->fund_code_name; ?>" readonly="readonly">
                    </div>
                </div>

            </div>

        </div>






            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>