<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Notification_model extends CI_Model
{
    function notificationList()
    {
        $this->db->select('sp.*');
        $this->db->from('notification as sp');
        $this->db->order_by("sp.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function notificationListSearch($data)
    {
        $this->db->select('sp.*');
        $this->db->from('notification as sp');
        if ($data['name'] != '')
        {
            $likeCriteria = "(sp.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['type'] != '')
        {
            $this->db->where('sp.type', $data['type']);
        }
        $this->db->order_by("sp.name", "ASC");
         $query = $this->db->get();
         $results = $query->result();  
         // echo "<Pre>"; print_r($recepients);exit;
         return $results;
    }

    function getNotification($id)
    {
        $this->db->select('*');
        $this->db->from('notification');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewNotification($data)
    {
        $this->db->trans_start();
        $this->db->insert('notification', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editNotification($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('notification', $data);
        return TRUE;
    }

     function deleteNotification($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id', $id);
       $this->db->delete('notification_recepients');
    }

}

