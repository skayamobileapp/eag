<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Set Form</h3>
      <a href="add" class="btn btn-primary">+ Add Set Form</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">
                

              <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Branch</label>
                    <div class="col-sm-8">
                      <select name="id_branch" id="id_branch" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($branchList)) {
                          foreach ($branchList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_branch']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name ."-".$record->permanent_address;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Semester</label>
                    <div class="col-sm-8">
                      <select name="id_semester" id="id_semester" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($semesterList)) {
                          foreach ($semesterList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_semester']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Program</label>
                    <div class="col-sm-8">
                      <select name="id_program" id="id_program" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programList)) {
                          foreach ($programList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_program']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>



              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Name</th>
            <th>Branch</th>
            <th>Semester</th>
            <th>Program</th>
            <th>Section Tagging</th>
            <th>Status</th>
            <th>Publish</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($setFormList))
          {
            $i=1;
            foreach ($setFormList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->branch_name . " - " . $record->permanent_address ?></td>
                <td><?php echo $record->semester_code . " - " . $record->semester_name ?></td>
                <td><?php echo $record->program_name . " - " . $record->program_code ?></td>
                <td>
                  <?php 
                  foreach ($record->details as $value)
                  {

                    ?>
                   &#8226; <?php echo $value->question_pool; ?><br>
                   <?php
                  } 

                  ?>
                  
                </td>
                <td><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                </td>
                <td><?php if( $record->is_publish == '1')
                {
                  echo "Published";
                }
                elseif( $record->is_publish == '0')
                {
                  echo "Not Published";
                } 
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                  
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>



  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    $('select').select2();
  
    function clearSearchForm()
    {
      window.location.reload();
    }
</script>