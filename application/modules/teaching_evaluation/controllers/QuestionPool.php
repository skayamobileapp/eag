<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class QuestionPool extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('question_pool_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('question_pool.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['questionPoolList'] = $this->question_pool_model->questionPoolListSearch($name);
            $this->global['pageTitle'] = 'Campus Management System : List Question Pool';
            //print_r($subjectDetails);exit;
            $this->loadViews("question_pool/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('question_pool.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'status' => $status
                );
            
                $result = $this->question_pool_model->addNewQuestionPool($data);
                redirect('/teaching_evaluation/questionPool/list');
            }
            
            $this->global['pageTitle'] = 'Campus Management System : Add Question Pool';
            $this->loadViews("question_pool/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('question_pool.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/teaching_evaluation/question_pool/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'status' => $status
                );
                
                $result = $this->question_pool_model->editQuestionPool($data,$id);
                redirect('/teaching_evaluation/questionPool/list');
            }
            $data['questionPool'] = $this->question_pool_model->getQuestionPoolDetails($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Question Pool';
            $this->loadViews("question_pool/edit", $this->global, $data, NULL);
        }
    }
}
