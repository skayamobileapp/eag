<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Set_form_model extends CI_Model
{
    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by('name', 'ASC');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function semesterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('semester');
        $this->db->where('status', $status);
        $this->db->order_by('name', 'ASC');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function organisationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('organisation');
        $this->db->where('status', $status);
        $this->db->order_by('name', 'ASC');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function questionPoolListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('question_pool');
        $this->db->where('status', $status);
        $this->db->order_by('name', 'ASC');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }


    function setFormListSearch($data)
    {
        $this->db->select('sf.*, p.code as program_code, p.name as program_name, o.name as branch_name, o.address1 as permanent_address, s.name as semester_name, s.code as semester_code');
        $this->db->from('set_form as sf');
        $this->db->join('programme as p', 'sf.id_program = p.id');
        $this->db->join('organisation as o', 'sf.id_branch = o.id');
        $this->db->join('semester as s', 'sf.id_semester = s.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_semester'] != '')
        {
            $this->db->where('sf.id_semester', $data['id_semester']);
        }
        if ($data['id_branch'] != '')
        {
            $this->db->where('sf.id_branch', $data['id_branch']);
        }
        if ($data['id_program'] != '')
        {
            $this->db->where('sf.id_program', $data['id_program']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('sf.status', $data['status']);
        }
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();  

         $data = array();
         foreach ($result as $value)
         {
                $value->details = $this->getSetFormHasQuestionPoolByFormId($value->id);
                array_push($data, $value);
         }   
         return $data;
    }

    function getSetFormDetails($id)
    {
        $this->db->select('*');
        $this->db->from('set_form');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    // function getSetFormQuestionPoolList($id_set_form)
    // {
    //     $this->db->select('sfhqp.*, qp.name as question_pool');
    //     $this->db->from('set_form_has_question_pool as sfhqp');
    //     $this->db->join('question_pool as qp', 'sfhqp.id_pool = qp.id');
    //     $this->db->where('sfhqp.id_set_form', $id_set_form);
    //     $query = $this->db->get();
    //     return $query->result();
    // }

    function addNewSetForm($data)
    {
        $this->db->trans_start();
        $this->db->insert('set_form', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewSetFormHasQuestionPool($data)
    {
        $this->db->trans_start();
        $this->db->insert('set_form_has_question_pool', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getSetFormHasQuestionPoolByFormId($id_set_form)
    {
        $this->db->select('sfhqp.*, qp.name as question_pool');
        $this->db->from('set_form_has_question_pool as sfhqp');
        $this->db->join('question_pool as qp', 'sfhqp.id_pool = qp.id');
        $this->db->where('sfhqp.id_set_form', $id_set_form);
        $query = $this->db->get();
        return $query->result();
    }

    function editSetForm($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('set_form', $data);
        return TRUE;
    }
}

