            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="/sponser/user/profile">Edit Profile</a></li>
                            <li><a href="/setup/user/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    <h4>Setup</h4>
                    <ul>
                        <li><a href="/internship/internship/limit">Max. Internship Limit</a></li>
                        <li><a href="/internship/companyType/list">Company Type</a></li>
                        <li><a href="/internship/companyRegistration/list">Company Registration</a></li>
                    </ul>
                    <h4>Internship</h4>
                    <ul>
                        <li><a href="/internship/internshipApplication/list">Application</a></li>
                        <li><a href="/internship/internshipApplication/approvallist">Application Approval</a></li>
                        <!-- <li><a href="/internship/internship/demo">Internship Progress</a></li> -->
                        <li><a href="/internship/internship/demo">Reporting</a></li>
                        <li><a href="/internship/placement/list">Placement Form</a></li>
                    </ul>
                    <!-- <h4>Placements</h4>
                    <ul>
                        <li><a href="/teaching_evaluation/">Placement Form</a></li>
                    </ul> -->
                </div>
            </div>