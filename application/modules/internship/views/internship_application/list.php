<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Internship Application</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Internship Application</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Application Number / Description</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Select Company Type</label>
                    <div class="col-sm-8">
                      <select name="id_company_type" id="id_company_type" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($companyTypeList)) {
                          foreach ($companyTypeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_company_type']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>


              <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Select Company</label>
                    <div class="col-sm-8">
                      <select name="id_company" id="id_company" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($companyRegistrationList)) {
                          foreach ($companyRegistrationList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_company']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->registration_no ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>


                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Select Intake</label>
                    <div class="col-sm-8">
                      <select name="id_intake" id="id_intake" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($intakeList)) {
                          foreach ($intakeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_intake']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Select Program</label>
                    <div class="col-sm-8">
                      <select name="id_program" id="id_program" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programList)) {
                          foreach ($programList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_program']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Application Number</th>
            <th>Intake</th>
            <th>Program</th>
            <th>Student</th>
            <th>Description</th>
            <!-- <th>Duration (Months)</th> -->
            <th>Company Type</th>
            <th>Company </th>
            <th>Start Date</th>
            <th>Status</th>
            <th style="text-align:center; ">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($internshipApplicationList))
          {
            $i=0;
            foreach ($internshipApplicationList as $record) {
          ?>
              <tr>
                 <td><?php echo $i+1;?></td>
                <td><?php echo $record->application_number ?></td>
                <td><?php echo $record->intake ?></td>
                <td><?php echo $record->program_code . " - " . $record->program_name ?></td>
                <td><?php echo $record->nric . " - " . $record->student_name ?></td>
                <td><?php echo $record->description ?></td>
                <!-- <td><?php echo $record->duration ?></td> -->
                <td><?php echo $record->company_type_code . " - " . $record->company_type_name ?></td>
                <td><?php echo $record->registration_no . " - " . $record->company_name ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->from_dt)) ?></td>
                <td class="text-center"><?php 
                    if( $record->is_reporting == '1')
                    {
                      echo "Reported";
                    }
                    elseif( $record->status == '0')
                    {
                      echo "Pending";
                    }
                    elseif( $record->status == '1')
                    {
                      echo "Approved";
                    }elseif( $record->status == '2')
                    {
                      echo "Rejected";
                    }
                    ?></td>
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="View">View</a>
                  <!--  -->
                </td>
              </tr>
          <?php
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    
    $('select').select2();

    function clearSearchForm()
      {
        window.location.reload();
      }
</script>