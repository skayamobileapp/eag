<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Internship Company</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Internship Company Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Registration Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="registration_no" name="registration_no" value="<?php echo $companyRegistration->registration_no; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $companyRegistration->name; ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company Type <span class='error-text'>*</span></label>
                        <select name="id_company_type" id="id_company_type" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($companyTypeList))
                            {
                                foreach ($companyTypeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $companyRegistration->id_company_type)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Person In Charge <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="person_in_charge" name="person_in_charge" value="<?php echo $companyRegistration->person_in_charge; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="email" name="email" value="<?php echo $companyRegistration->email; ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="contact_number" name="contact_number" value="<?php echo $companyRegistration->contact_number; ?>">
                    </div>
                </div>

            </div>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address" name="address" value="<?php echo $companyRegistration->address; ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Country <span class='error-text'>*</span></label>
                        <select name="id_country" id="id_country" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $companyRegistration->id_country)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>State <span class='error-text'>*</span></label>
                        <select name="id_state" id="id_state" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($stateList))
                            {
                                foreach ($stateList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $companyRegistration->id_state)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city" value="<?php echo $companyRegistration->city; ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Zipcode <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $companyRegistration->zipcode; ?>">
                    </div>
                </div>

            <!-- </div>

            <div class="row"> -->

                
                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($companyRegistration->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($companyRegistration->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>               
                
            </div>

        </div>


        </form>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>



        <form id="form_programme_intake" action="" method="post">


            <div class="form-container">
                <h4 class="form-group-title">Program Tagging Company Details</h4>


                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Program <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->name; ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                  
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                    </div>
                </div>
            </div>



        <div class="form-container">
            <h4 class="form-group-title">Program Tagging Company List</h4>


                <div class="row">
                    <div id="view"></div>
                </div>

            </div>
        </form>



            
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    function saveData()
        {

        // if($('#form_programme_intake').valid())
        // {


        var tempPR = {};
        tempPR['id_program'] = $("#id_program").val();
        tempPR['id'] = <?php echo $idCompany;?>;
            $.ajax(
            {
               url: '/internship/companyRegistration/directadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
               }
            });
        // }
    }

    function deleteCompanyHasProgramme(id) {
                    // alert(id);

            $.ajax(
            {
               url: '/internship/companyRegistration/deleteCompanyHasProgramme/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // $("#view").html(result);
                    // var ta = $("#inv-total-amount").val();
                // $("#total_amount").val(ta);
                    // alert(result);
                    window.location.reload();
               }
            });
    }

    function validateDetailsData()
    {
        if($('#form_grade').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam=='')
            {
                alert("Add Program to the Company");
            }
            else
            {
                $('#form_grade').submit();
            }
        }    
    }


    $('select').select2();
    
     $(document).ready(function() {
        saveData();
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 registration_no: {
                    required: true
                },
                 address: {
                    required: true
                },
                 id_country: {
                    required: true
                },
                 id_state: {
                    required: true
                },
                 city: {
                    required: true
                },
                 zipcode: {
                    required: true
                },
                 contact_number: {
                    required: true
                },
                 id_company_type: {
                    required: true
                },
                 person_in_charge: {
                    required: true
                },
                 email: {
                    required: true
                },
                 status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                registration_no: {
                    required: "<p class='error-text'>Registration Number Required</p>",
                },
                address: {
                    required: "<p class='error-text'>Address Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                city: {
                    required: "<p class='error-text'>City Required</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                contact_number: {
                    required: "<p class='error-text'>Contact Number Required</p>",
                },
                id_company_type: {
                    required: "<p class='error-text'>Select Company Type</p>",
                },
                person_in_charge: {
                    required: "<p class='error-text'>Person In Charge Required</p>",
                },
                email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Status Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>