<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Placement extends BaseController
{
    public function __construct()
    {
        // $test = new BaseController();
        // $test->isStudentLoggedIn();
        parent::__construct();
        $this->load->model('placement_model');
        $this->isLoggedIn();
    }

    function list()
    {   
        if ($this->checkAccess('placement_form.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
        
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_qualification'] = $this->security->xss_clean($this->input->post('id_qualification'));
            $formData['status'] = '';
            $data['searchParam'] = $formData;

            $data['placementList'] = $this->placement_model->placementListSearch($formData);
            $data['programList'] = $this->placement_model->programList();
            $data['intakeList'] = $this->placement_model->intakeList();
            $data['qualificationList'] = $this->placement_model->qualificationList();

                     // echo "<Pre>";print_r($data);exit();

            $this->global['pageTitle'] = 'Student Portal : Placement Form';
            $this->loadViews("placement/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {

        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;

        if($this->input->post())
        {
            $company_name = $this->security->xss_clean($this->input->post('company_name'));
            $email = $this->security->xss_clean($this->input->post('email'));
            $phone = $this->security->xss_clean($this->input->post('phone'));
            $company_address = $this->security->xss_clean($this->input->post('company_address'));
            $id_country = $this->security->xss_clean($this->input->post('id_country'));
            $id_state = $this->security->xss_clean($this->input->post('id_state'));
            $city = $this->security->xss_clean($this->input->post('city'));
            $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
            $joining_dt = $this->security->xss_clean($this->input->post('joining_dt'));
            $designation = $this->security->xss_clean($this->input->post('designation'));


            $data = array(
                'company_name' => $company_name,
                'email' => $email,
                'phone' => $phone,
                'id_student' => $id_student,
                'id_intake' => $id_intake,
                'id_program' => $id_program,
                'id_qualification' => $id_qualification,
                'company_address' => $company_address,
                'id_country' => $id_country,
                'id_state' => $id_state,
                'city' => $city,
                'zipcode' => $zipcode,
                'joining_dt' => date('Y-m-d',strtotime($joining_dt)),
                'designation' => $designation,
                'status' => 1
            );

            // $check_limit = $this->placement_model->checkStudentPlacement($data);
            // if($check_limit == 1)
            // {
            //      echo "<Pre>";print_r("Max Application Limit Reached For Active & Pending Internship Application");exit();
            // }            
                 // echo "<Pre>";print_r($check_limit);exit();
            $insert_id = $this->placement_model->addPlacement($data);
            redirect('/internship/placement/list');
        }

        $data['countryList'] = $this->placement_model->countryListByStatus('1');

        $this->global['pageTitle'] = 'Student Portal : Add Placement Form';
        $this->loadViews("placement/add", $this->global, $data, NULL);
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('placement_form.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/internship/placement/list');
            }
            // $data['studentDetails'] = $this->placement_model->getStudentByStudentId($id);
            // $data['projectReportSubmission'] = $this->project_report_submission_model->getProjectReport($id);
            $data['placement'] = $this->placement_model->getPlacement($id);
            $data['studentDetails'] = $this->placement_model->getStudentByStudentId($data['placement']->id_student);
            // echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Student Portal : View Placement Form';
            $this->loadViews("placement/edit", $this->global, $data, NULL);
        }
    }
}

