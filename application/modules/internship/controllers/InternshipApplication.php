<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class InternshipApplication extends BaseController
{
    public function __construct()
    {
        // $test = new BaseController();
        // $test->isStudentLoggedIn();
        parent::__construct();
        $this->load->model('internship_application_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('internship_application.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_company_type'] = $this->security->xss_clean($this->input->post('id_company_type'));
            $formData['id_company'] = $this->security->xss_clean($this->input->post('id_company'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;
            
            $data['internshipApplicationList'] = $this->internship_application_model->getInternshipApplicationListSearch($formData);
            
            $data['companyTypeList'] = $this->internship_application_model->companyTypeListByStatus('1');
            $data['companyRegistrationList'] = $this->internship_application_model->companyRegistrationListByStatus('1');
            $data['intakeList'] = $this->internship_application_model->intakeListByStatus('1');
            $data['programList'] = $this->internship_application_model->programListByStatus('1');



            // $data['internshipApplicationList'] = $this->internship_application_model->getInternshipApplicationListByStudentId($id);
                     // echo "<Pre>";print_r($data);exit();

            $this->global['pageTitle'] = 'Campus Management System : List Internship Application';
            $this->loadViews("internship_application/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('internship_application.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {


        if ($id == null)
        {
            redirect('/internship/internshipApplication/list');
        }
        // $data['studentDetails'] = $this->internship_application_model->getStudentByStudentId($id);
        $data['companyTypeList'] = $this->internship_application_model->companyTypeList();
        $data['companyRegistationList'] = $this->internship_application_model->companyRegistrationList();
        $data['internshipApplication'] = $this->internship_application_model->getInternshipApplication($id);
        $data['studentDetails'] = $this->internship_application_model->getStudentByStudentId($data['internshipApplication']->id_student);
            // echo "<Pre>"; print_r($data['internshipApplication']->id_student);exit;

        $this->global['pageTitle'] = 'Campus Management System : View Internship Application';
        $this->loadViews("internship_application/edit", $this->global, $data, NULL);
        }
    }
    
    function approvalList()
    {
        if ($this->checkAccess('internship_application.approval_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_company_type'] = $this->security->xss_clean($this->input->post('id_company_type'));
            $formData['id_company'] = $this->security->xss_clean($this->input->post('id_company'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;
            
            $data['internshipApplicationList'] = $this->internship_application_model->getInternshipApplicationListSearch($formData);
            
            $data['companyTypeList'] = $this->internship_application_model->companyTypeListByStatus('1');
            $data['companyRegistrationList'] = $this->internship_application_model->companyRegistrationListByStatus('1');
            $data['intakeList'] = $this->internship_application_model->intakeListByStatus('1');
            $data['programList'] = $this->internship_application_model->programListByStatus('1');



            // $data['internshipApplicationList'] = $this->internship_application_model->getInternshipApplicationListByStudentId($id);
                     // echo "<Pre>";print_r($data);exit();

            $this->global['pageTitle'] = 'Campus Management System : List Internship Application';
            $this->loadViews("internship_application/approval_list", $this->global, $data, NULL);
        }
    }


    function view($id = NULL)
    {
        if ($this->checkAccess('internship_application.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {


        if ($id == null)
        {
            redirect('/internship/internshipApplication/list');
        }
        if($this->input->post())
        {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );

                 $result = $this->internship_application_model->editInternshipApplication($data,$id);
                redirect('/internship/InternshipApplication/approvalList');

        }
        // $data['studentDetails'] = $this->internship_application_model->getStudentByStudentId($id);
        $data['companyTypeList'] = $this->internship_application_model->companyTypeList();
        $data['companyRegistationList'] = $this->internship_application_model->companyRegistrationList();
        $data['internshipApplication'] = $this->internship_application_model->getInternshipApplication($id);
        $data['studentDetails'] = $this->internship_application_model->getStudentByStudentId($data['internshipApplication']->id_student);

            // echo "<Pre>"; print_r($data);exit;

        $this->global['pageTitle'] = 'Campus Management System : Approve Internship Application';
        $this->loadViews("internship_application/view", $this->global, $data, NULL);
        }
    }


    function diffDates()
    {
        // // Declare two dates 
        // $start_date = strtotime("2018-06-08"); 
        // $end_date = strtotime("2018-09-01"); 
          
        // // Get the difference and divide into  
        // // total no. seconds 60/60/24 to get  
        // // number of days 
        // echo "Difference between two dates: "
        //     . ($end_date - $start_date)/60/60/24;exit();
    }
}

