<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Course_registration_model extends CI_Model
{

    function getPerogramLandscape($id_intake,$id_programme)
    {
        $this->db->select('DISTINCT(c.id) as id, c.name , c.code');
        $this->db->from('add_course_to_program_landscape as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('course as c', 'a.id_course = c.id');
        $this->db->join('programme_landscape as pl', 'a.id_program_landscape = pl.id');
        $this->db->where('a.id_intake', $id_intake);
        $this->db->where('pl.status', '1');
         $this->db->where('pl.id_programme', $id_programme);
        $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function getCoursePerogramLandscape($id)
    {
        $this->db->select('a.id, b.pre_requisite, c.name as courseName, pl.name as plName, pl.min_total_cr_hrs');

        $this->db->from('courses_from_programme_landscape as a');
        $this->db->join('add_course_to_program_landscape as b', 'a.id_course_programme = b.id');
        $this->db->join('course as c', 'b.id_course = c.id');
        $this->db->join('programme_landscape as pl', 'b.id_program_landscape = pl.id');
        $this->db->where('a.id_course_registration', $id);
        $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function getCourseRegisteredList($id_student,$id_intake,$id_program,$id_qualification){
        $this->db->select('DISTINCT(cr.id_course) as id_course, cr.*, c.name as course_name, c.code as course_code, pl.min_total_cr_hrs, rl.role');
        $this->db->from('course_registration as cr');
        $this->db->join('course as c', 'cr.id_course = c.id');
        $this->db->join('add_course_to_program_landscape as acpl', 'acpl.id_course = c.id');
        $this->db->join('programme_landscape as pl', 'acpl.id_program_landscape = pl.id');
        $this->db->join('roles as rl', 'cr.created_by = rl.id','left');
        $this->db->where('cr.id_student', $id_student);
        $this->db->where('acpl.id_intake', $id_intake);
        $this->db->where('acpl.id_program', $id_program);
        // $this->db->where('acpl.id_qualification', $id_qualification);
         $this->db->where('cr.is_exam_registered', '0');
         $this->db->where('cr.is_bulk_withdraw', '0');
        $query = $this->db->get();
         return $query->result();
    }

    // function getCoursesByProgramNIntakeNStudent($id_intake,$id_programme,$id_student)
    // {
    //      // print_r($id_intake);exit();
    //     $this->db->select('cou.name, cou.name_in_malay, cou.code, acpl.pre_requisite, pl.min_total_cr_hrs');
    //     $this->db->from('course_registration as a');
    //     $this->db->join('course as cou', 'a.id_course = cou.id');
    //     $this->db->join('add_course_to_program_landscape as acpl', 'acpl.id_course = cou.id');
    //     $this->db->join('programme_landscape as pl', 'acpl.id_program_landscape = pl.id');
    //     $this->db->where('pl.id_programme', $id_programme);
    //     $this->db->where('pl.id_intake', $id_intake);
    //     $this->db->where('a.id_programme', $id_programme);
    //     $this->db->where('a.id_intake', $id_intake);
    //     $this->db->where('a.id_student', $id_student);
    //     $this->db->where('a.is_exam_registered', '0');
    //     $this->db->where('a.is_bulk_withdraw', '0');
    //      $query = $this->db->get();
    //      $result = $query->result();
    //      return $result;
    // }

    function deleteCourseRegistration($id_course_registration)
    {
         $this->db->where('id', $id_course_registration);
        $this->db->delete('course_registration');
        return TRUE;
    }

    function addCoureRegistration($data)
    {
        $this->db->trans_start();
        $this->db->insert('course_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getCourseFromExamRegister($id_intake,$id_programme,$id_student)
    {
      $status = '0';
        $this->db->select('DISTINCT(c.id) as id_course, cr.id, in.name as intake_name, c.code as course_code, c.name as course_name, exc.name as exam_center_name, exc.address as exam_center_address');
        $this->db->from('exam_registration as cr');
        $this->db->join('intake as in', 'cr.id_intake = in.id');
        $this->db->join('course as c', 'cr.id_course = c.id');
        $this->db->join('exam_center as exc', 'cr.id_exam_center = exc.id');
        $this->db->where('cr.id_intake', $id_intake);
        $this->db->where('cr.id_student', $id_student);
        $this->db->where('cr.id_programme', $id_programme);
        $this->db->where('cr.is_bulk_withdraw', $status);
        // $this->db->where('cr.is_exam_registered', $status);
        $query = $this->db->get();
          $result = $query->result();
         return $result; 
    }

    function bulkWithdrawList($id_student)
    {
        $this->db->select('DISTINCT(cr.id_course) as id_course, cr.id, cr.reason, in.id as id_intake,  in.name as intake_name, exc.name as exam_center_name, exc.address as exam_center_address, stu.full_name, stu.nric, stu.email_id, p.name as programme_name, p.code as programme_code, p.id as id_programme, c.code as course_code, c.name as course_name');
        $this->db->from('bulk_withdraw as cr');
        $this->db->join('intake as in', 'cr.id_intake = in.id');
        $this->db->join('course as c', 'cr.id_course = c.id');
        $this->db->join('student as stu', 'cr.id_student = stu.id');
        $this->db->join('programme as p', 'cr.id_programme = p.id');
        $this->db->join('exam_center as exc', 'cr.id_exam_center = exc.id');
         $this->db->where('cr.id_student', $id_student);
        $query = $this->db->get();
          $result = $query->result();
         return $result; 
    }

    function addBulkWithdraw($data)
    {
        $this->db->trans_start();
        $this->db->insert('bulk_withdraw', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getExamRegistration($id)
    {
        $this->db->select('*');
        $this->db->from('exam_registration');
         $this->db->where('id', $id);
        $query = $this->db->get();
          $result = $query->row();
         return $result; 
    }

    function updateExamRegistration($data,$id)
    {
      $this->db->where_in('id', $id);
      $this->db->update('exam_registration', $data);
    }

    function getRole($id)
    {
        $this->db->select('*');
        $this->db->from('roles');
        $this->db->where('id', $id);
        $query = $this->db->get();
            // echo "<pre>";print_r($query);die;
        
        return $query->row();
    }
}