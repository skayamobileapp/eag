<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Examination_model extends CI_Model
{

	function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function semesterResultList($id_student)
    {
        $this->db->select('ssr.*, sem.name as semester_name, sem.code as semester_code, stu.full_name as student_name, stu.nric, i.year as intake_year, i.name as intake_name, p.name as programme_name, p.code as programme_code, gra.name as grade');
        $this->db->from('student_semester_result as ssr');
        $this->db->join('student as stu', 'ssr.id_student = stu.id');
        $this->db->join('semester as sem', 'ssr.id_semester = sem.id');
        $this->db->join('intake as i', 'ssr.id_intake = i.id');
        $this->db->join('programme as p', 'ssr.id_program = p.id');
        $this->db->join('grade as gra', 'ssr.grade = gra.id');
        $this->db->where('ssr.id_student', $id_student);
        $this->db->where('ssr.status', '1');
        $this->db->order_by("ssr.id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getMarksEntry($id)
    {
        $this->db->select('ssr.*, sem.name as semester_name, sem.code as semester_code, stu.full_name as student_name, stu.nric, stu.email_id, stu.phone, i.year as intake_year, i.name as intake_name, p.name as programme_name, p.code as programme_code, gra.name as grade');
        $this->db->from('student_semester_result as ssr');
        $this->db->join('student as stu', 'ssr.id_student = stu.id');
        $this->db->join('semester as sem', 'ssr.id_semester = sem.id');
        $this->db->join('intake as i', 'ssr.id_intake = i.id');
        $this->db->join('programme as p', 'ssr.id_program = p.id');
        $this->db->join('grade as gra', 'ssr.grade = gra.id');
        $this->db->where('ssr.id', $id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }
    

    function getMarksEntryDetailsByMasterId($id_master)
    {
         $this->db->select('ssr.*, c.name as course_name, c.code as course_code, gra.name as grade');
        $this->db->from('student_semester_course_details as ssr');
        $this->db->join('course as c', 'ssr.id_course = c.id');
        $this->db->join('grade as gra', 'ssr.grade = gra.id');
        $this->db->where('ssr.id_student_semester_result', $id_master);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
























    

    function getPerogramLandscape($id_intake,$id_programme)
    {
        $this->db->select('DISTINCT(c.id) as id, c.name , c.code');
        $this->db->from('add_course_to_program_landscape as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('course as c', 'a.id_course = c.id');
        $this->db->join('programme_landscape as pl', 'a.id_program_landscape = pl.id');
        $this->db->where('a.id_intake', $id_intake);
         $this->db->where('pl.id_programme', $id_programme);
        $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function getCoursePerogramLandscape($id)
    {
        $this->db->select('a.id, b.pre_requisite, c.name as courseName, pl.name as plName, pl.min_total_cr_hrs');

        $this->db->from('courses_from_programme_landscape as a');
        $this->db->join('add_course_to_program_landscape as b', 'a.id_course_programme = b.id');
        $this->db->join('course as c', 'b.id_course = c.id');
        $this->db->join('programme_landscape as pl', 'b.id_program_landscape = pl.id');
        $this->db->where('a.id_course_registration', $id);
        $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function getCourseRegisteredList($id_student){
        $this->db->select('cr.*, c.name as course_name, c.code as course_code');
        $this->db->from('course_registration as cr');
        $this->db->join('course as c', 'cr.id_course = c.id');
        $this->db->where('cr.id_student', $id_student);
        $query = $this->db->get();
         return $query->result();
    }

    function deleteCourseRegistration($id_course_registration)
    {
         $this->db->where('id', $id_course_registration);
        $this->db->delete('course_registration');
        return TRUE;
    }

    function addCoureRegistration($data)
    {
        $this->db->trans_start();
        $this->db->insert('course_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
}