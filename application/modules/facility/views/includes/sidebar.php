            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="/setup/user/profile">Edit Profile</a></li>
                            <li><a href="/setup/user/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    <h4>Setup</h4>
                    <ul>
                                                <li><a href="/facility/Equipment/list">Equipments</a></li>

                        <li><a href="/facility/roomType/list">Room Type</a></li>
                        <li><a href="/facility/facilityBuildingRegistration/list">Building</a></li>
                        <li><a href="/facility/RoomSetup/list">Room Setup</a></li>
                        <li><a href="/facility/RoomBooking/list">Apply For Booking</a></li>


                        <li><a href="/facility/RoomBooking/bookinglist">Booking Summary</a></li>
                       
                    </ul>
                    
                </div>
            </div>