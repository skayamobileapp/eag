<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Booking of Room List</h3>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Room Name </label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="">
                      </div>
                    </div>
                  </div>


                 

              

                </div>

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Room Name</th>
            <th>Address</th>

            <th>Booking Person</th>
            <th>Booking Person Mobile</th>
            <th>Booking Type</th>
            <th>Booking Start Date</th>
            <th>Booking End Date</th>
            <th>Remarks</th>


          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($roomList))
          {
            $i=1;
            foreach ($roomList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->address ?></td>
                <td><?php echo $record->booking_name ?></td>
                <td><?php echo $record->booking_mobile ?></td>
                <td><?php echo $record->booking_type ?></td>

                <td><?php echo $record->booking_start_date ?></td>
                <td><?php echo $record->booking_end_date ?></td>
                <td><?php echo $record->booking_remarks ?></td>
                
               
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
