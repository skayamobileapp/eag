<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Equipment extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('equipment_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('equipment.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['roomTypeList'] = $this->equipment_model->roomTypeListSearch($name);
            $this->global['pageTitle'] = 'Campus Management System : List Room Type';
            //print_r($subjectDetails);exit;
            $this->loadViews("equipment/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('equipment.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'name_in_malay' => $name_in_malay,
                    'status' => $status,
                    'created_by' => $user_id
                );
            
                $duplicate_row = $this->equipment_model->checkRoomTypeDuplication($data);

                if($duplicate_row)
                {
                    echo "Duplicate Room Type Not Allowed";exit();
                }

                $result = $this->equipment_model->addNewRoomType($data);
                redirect('/facility/Equipment/list');
            }
            
            $this->global['pageTitle'] = 'Campus Management System : Add Room Type';
            $this->loadViews("equipment/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('equipment.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/facility/Equipment/list');
            }
            
            $user_id = $this->session->userId;
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'name_in_malay' => $name_in_malay,
                    'status' => $status,
                    'created_by' => $user_id
                );

                $duplicate_row = $this->equipment_model->checkRoomTypeDuplicationEdit($data,$id);

                if($duplicate_row)
                {
                    echo "Duplicate Room Type Not Allowed";exit();
                }
                
                $result = $this->equipment_model->editRoomType($data,$id);
                redirect('/facility/Equipment/list');
            }
            $data['roomType'] = $this->equipment_model->getRoomType($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Room Type';
            $this->loadViews("equipment/edit", $this->global, $data, NULL);
        }
    }
}
