<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FacilityBuildingRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('facility_building_registration_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('facility_building_registration.list') == 1)
        // if ($this->checkAccess('facility_building_registration.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['contact_name'] = $this->security->xss_clean($this->input->post('contact_name'));
 
            $data['searchParam'] = $formData;

            $data['staffList'] = $this->facility_building_registration_model->staffListByStatus('1');
            $data['hostelRegistrationList'] = $this->facility_building_registration_model->hostelRegistrationListSearch($formData);
               // echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Campus Management System : Hostel Registration List';
            $this->loadViews("facility_building_registration/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('facility_building_registration.add') == 1)
        // if ($this->checkAccess('facility_building_registration.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $formData = $this->input->post();

               // echo "<Pre>"; print_r($formData);exit;
            	$id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $max_capacity = $this->security->xss_clean($this->input->post('max_capacity'));
                $contact_name = $this->security->xss_clean($this->input->post('contact_name'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $landmark = $this->security->xss_clean($this->input->post('landmark'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $status = $this->security->xss_clean($this->input->post('status'));

                
                $data = array(
					'name' => $name,
					'code' => $code,
                    'type' => $type,
                    'contact_name' => $contact_name,
                    'contact_number' => $contact_number,
					'max_capacity' => $max_capacity,
					'address' => $address,
					'landmark' => $landmark,
					'city' => $city,
					'id_state' => $id_state,
					'id_country' => $id_country,
                    'zipcode' => $zipcode,
					'status' => $status,
					'created_by' => $user_id
                );
                $inserted_id = $this->facility_building_registration_model->addNewHostelRegistration($data);
                redirect('/facility/facilityBuildingRegistration/list');
            }
            $data['countryList'] = $this->facility_building_registration_model->countryListByStatus('1');
            $data['staffList'] = $this->facility_building_registration_model->staffListByStatus('1');
               // echo "<Pre>"; print_r($data);exit;

            
            $this->global['pageTitle'] = 'Campus Management System : Add Hostel Registration';
            $this->loadViews("facility_building_registration/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('facility_building_registration.edit') == 1)
        // if ($this->checkAccess('facility_building_registration.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/facility/facilityBuildingRegistration/list');
            }
            if($this->input->post())
            {
                $formData = $this->input->post();

               // echo "<Pre>"; print_r($formData);exit;

	            $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $max_capacity = $this->security->xss_clean($this->input->post('max_capacity'));
                $contact_name = $this->security->xss_clean($this->input->post('contact_name'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $landmark = $this->security->xss_clean($this->input->post('landmark'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $status = $this->security->xss_clean($this->input->post('status'));

                
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'type' => $type,
                    'contact_name' => $contact_name,
                    'contact_number' => $contact_number,
                    'max_capacity' => $max_capacity,
                    'address' => $address,
                    'landmark' => $landmark,
                    'city' => $city,
                    'id_state' => $id_state,
                    'id_country' => $id_country,
                    'zipcode' => $zipcode,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                //print_r($data);exit;
                $result = $this->facility_building_registration_model->editHostelRegistration($data,$id);
                redirect('/facility/facilityBuildingRegistration/list');
            }
            // $data['studentList'] = $this->facility_building_registration_model->studentList();
            $data['hostelRegistration'] = $this->facility_building_registration_model->getHostelRegistration($id);
            $data['countryList'] = $this->facility_building_registration_model->countryListByStatus('1');
            $data['stateList'] = $this->facility_building_registration_model->stateListByStatus('1');
            $data['staffList'] = $this->facility_building_registration_model->staffListByStatus('1');
            
               // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Campus Management System : Edit Hostel Registration';
            $this->loadViews("facility_building_registration/edit", $this->global, $data, NULL);
        }
    }

    function getStateByCountry($id_country)
    {
        $results = $this->facility_building_registration_model->getStateByCountryId($id_country);

        // echo "<Pre>"; print_r($programme_data);exit;
     //    $table="   
     //        <script type='text/javascript'>
     //             $('select').select2();
     //         </script>
     // ";

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_state' id='id_state' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }
}
