<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Room_booking_model extends CI_Model
{
    

    function roomListListSearch($data)
    {
        $this->db->select('fc.*, hr.contact_name,hr.contact_number,hr.address');
        $this->db->from('facility_room as fc');
        $this->db->join('facility_building_registration as hr', 'fc.id_facility_building_registration = hr.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(fc.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
       
        // if ($data['id_intake'] != '')
        // {
        //     $this->db->where('fc.id_intake', $data['id_intake']);
        // }

        $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addbooking($data){
         $this->db->trans_start();
        $this->db->insert('booking', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function bookinglist($searc) {


        $this->db->select('fc.*, hr.contact_name,hr.contact_number,hr.address,b.*');
        $this->db->from('booking as b');
        $this->db->join('facility_room as fc', 'fc.id = b.id_room');
        $this->db->join('facility_building_registration as hr', 'fc.id_facility_building_registration = hr.id');
        
        $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;


    }

    
}