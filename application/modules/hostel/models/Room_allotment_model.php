<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Room_allotment_model extends CI_Model
{
    function studentListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('status', $status);
        $this->db->order_by("full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function hostelListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('hostel_registration');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function hostelList()
    {
        $this->db->select('*');
        $this->db->from('hostel_registration');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function roomTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('hostel_room_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function roomTypeList()
    {
        $this->db->select('*');
        $this->db->from('hostel_room_type');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function buildingList()
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('level', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function buildingListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('level', '1');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function blockListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('level', '1');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }




    function roomAllotmentListSearch($data)
    {
        $this->db->select('ra.*, hstr.code as hostel_code, hstr.name as hostel_name, hrt.name as room_type_name, hrt.code as room_type_code, stu.nric, stu.full_name, hr.code as room_code, hr.name as room_name, hr.max_capacity');
        $this->db->from('room_allotment as ra');
        $this->db->join('hostel_registration as hstr', 'ra.id_hostel = hstr.id');
        $this->db->join('hostel_room as hr', 'ra.id_room = hr.id');
        $this->db->join('hostel_room_type as hrt', 'ra.id_room_type = hrt.id');
        $this->db->join('student as stu', 'ra.id_student = stu.id');
        // if ($data['id_student'] != '')
        // {
        //     $this->db->where('fc.id_student', $data['id_student']);
        // }
        if ($data['id_hostel'] != '')
        {
            $this->db->where('fc.id_hostel', $data['id_hostel']);
        }
        $this->db->order_by("hr.name", "ASC");
         $query = $this->db->get();
         $results = $query->result();
         $allotments =  array();
         foreach ($results as $result)
         {
            // $result->student_room_status = 'Vacated';
            $filled_count = $this->getRoomAllotmentDetailsByRoomId($result->id_room);
            // if($filled_count > 0)
            // {
            //     $result->student_room_status = 'Active';
            // }
            $result->filled_count = $filled_count;
            $result->vacant_count = $result->max_capacity - $filled_count;
            array_push($allotments, $result);
         }
         // echo "<Pre>";print_r($result);exit();     
         return $allotments;
    }

    function roomAllotmentListSearchActive($data)
    {
        $this->db->select('ra.*, hstr.code as hostel_code, hstr.name as hostel_name, hrt.name as room_type_name, hrt.code as room_type_code, stu.nric, stu.full_name, hr.code as room_code, hr.name as room_name, hr.max_capacity');
        $this->db->from('room_allotment as ra');
        $this->db->join('hostel_registration as hstr', 'ra.id_hostel = hstr.id');
        $this->db->join('hostel_room as hr', 'ra.id_room = hr.id');
        $this->db->join('hostel_room_type as hrt', 'ra.id_room_type = hrt.id');
        $this->db->join('student as stu', 'ra.id_student = stu.id');
        // if ($data['id_student'] != '')
        // {
        //     $this->db->where('fc.id_student', $data['id_student']);
        // }
        if ($data['id_hostel'] != '')
        {
            $this->db->where('fc.id_hostel', $data['id_hostel']);
        }
        $this->db->order_by("hr.name", "ASC");
         $query = $this->db->get();
         $results = $query->result();
         $allotments =  array();
         foreach ($results as $result)
         {
            // $result->student_room_status = 'Vacated';
            $filled_count = $this->getRoomAllotmentDetailsByRoomId($result->id_room);
            // if($filled_count > 0)
            // {
            //     $result->student_room_status = 'Active';
            // }
            $result->filled_count = $filled_count;
            $result->vacant_count = $result->max_capacity - $filled_count;
            if($result->to_dt > date('Y-m-d'))
            {
                array_push($allotments, $result);
            }
         }
         // echo "<Pre>";print_r($result);exit();     
         return $allotments;
    }

    function getIntakeListByProgramme($id_programme)
    {
        $this->db->select('DISTINCT(ihp.id_intake) as id_intake, ihp.*, in.name as intake_name');
        $this->db->from('intake_has_programme as ihp');
        $this->db->join('intake as in', 'ihp.id_intake = in.id');
        $this->db->where('ihp.id_programme', $id_programme);
        $query = $this->db->get();
         return $query->result();
    }

    function getStudentByProgNIntake($data)
    {
        $this->db->select('DISTINCT(er.id) as id, er.*');
        $this->db->from('student as er');
        if ($data['id_programme'] != '')
        {
            $this->db->where('er.id_program', $data['id_programme']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('er.id_intake', $data['id_intake']);
        }
        if($data['id_degree_type'] != '')
        {
            $this->db->where('er.id_degree_type', $data['id_degree_type']);

        }
        $this->db->where('er.applicant_status', 'Approved');
        $this->db->where('er.is_hostel', '1');
        $this->db->order_by("er.full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getPerogramLandscape($id_intake,$id_programme)
    {
        $this->db->select('c.id, a.pre_requisite, in.name as intakeName, c.name as courseName, pl.name as plName, pl.min_total_cr_hrs');
        $this->db->from('add_course_to_program_landscape as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('course as c', 'a.id_course = c.id');
        $this->db->join('programme_landscape as pl', 'a.id_program_landscape = pl.id');
        $this->db->where('a.id_intake', $id_intake);
         $this->db->where('pl.id_programme', $id_programme);
        $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

     function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.code as programme_code, p.name as programme_name, i.id as id_intake, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function qualificationList()
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getHostelRegistrationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('hostel_registration');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getRoomTypeListByStatus($status)
    {
         $this->db->select('*');
        $this->db->from('hostel_room_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getBuildingListByHostelId($id_hostel)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('level', '1');
        $this->db->where('id_hostel', $id_hostel);
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function getHostelRoomByData($data)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        if($data['level'] != '')
        {
            $this->db->where('level', $data['level']);
        }
         if($data['id_building'] != '')
        {
            $this->db->where('id_parent', $data['id_building']);
        }
        if($data['id_hostel'] != '')
        {
            $this->db->where('id_hostel', $data['id_hostel']);
        }
            $this->db->where('status', '1');
        $this->db->order_by("id", "ASC");
         // print_r($db->query);exit();     
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getRoomByBlockNHostel($data)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        if($data['level'] != '')
        {
            $this->db->where('level', $data['level']);
        }
         if($data['id_block'] != '')
        {
            $this->db->where('id_parent', $data['id_block']);
        }
        if($data['id_hostel'] != '')
        {
            $this->db->where('id_hostel', $data['id_hostel']);
        }
            $this->db->where('status', '1');
        $this->db->order_by("id", "ASC");
         // print_r($db->query);exit();     
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }


    function getRoomDetails($data)
    {
        $this->db->select('hr.*, hrt.id as id_room_type, hrt.name as room_type_name, hrt.code as room_type_code');
        $this->db->from('hostel_room as hr');
        $this->db->join('hostel_room_type as hrt', 'hr.id_room_type = hrt.id'); 
        if($data['level'] != '')
        {
            $this->db->where('hr.level', $data['level']);
        }
         if($data['id_room'] != '')
        {
            $this->db->where('hr.id', $data['id_room']);
        }
        if($data['id_hostel'] != '')
        {
            $this->db->where('hr.id_hostel', $data['id_hostel']);
        }
            $this->db->where('hr.status', '1');
        // $this->db->order_by("id", "ASC");
         // print_r($db->query);exit();     
         $query = $this->db->get();
         $result = $query->row(); 

         $filled_count = $this->getRoomAllotmentDetailsByRoomId($data['id_room']); 

         $result->filled_seats = $filled_count;
         return $result;
    }


    function getRoomAllotmentDetailsByRoomId($id_room)
    {
        $this->db->select('hr.*');
        $this->db->from('room_allotment as hr');

        $this->db->where('hr.from_dt <', date('Y-m-d'));
        $this->db->where('hr.to_dt >', date('Y-m-d'));
        $this->db->where('hr.id_room', $id_room);
        $query = $this->db->get();
        $count = $query->num_rows();

        return $count;



        // if($data['level'] != '')
        // {
        //     $this->db->where('hr.level', $data['level']);
        // }
        //  if($data['id_room'] != '')
        // {
        //     $this->db->where('hr.id', $data['id_room']);
        // }
        // if($data['id_hostel'] != '')
        // {
        //     $this->db->where('hr.id_hostel', $data['id_hostel']);
        // }
        //     $this->db->where('hr.status', '1');
        // // $this->db->order_by("id", "ASC");
        //  // print_r($db->query);exit();     
        //  $query = $this->db->get();
        //  $result = $query->row(); 

        //  $this->getAllotmentDetails($data['id_room']);  
        //  return $result;
    }

    function addNewRoomAllotment($data)
    {
        $this->db->trans_start();
        $this->db->insert('room_allotment', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getRoomAllotmentDetailsByStudentId($id_student)
    {
        $this->db->select('hr.*');
        $this->db->from('room_allotment as hr');

        $this->db->where('hr.from_dt <', date('Y-m-d'));
        $this->db->where('hr.to_dt >', date('Y-m-d'));
        $this->db->where('hr.id_student', $id_student);
        $query = $this->db->get();
        $count = $query->num_rows();

        return $count;
    }

    function getRoomAllotmentByStud($id_student)
    {
        $this->db->select('hr.*, hstr.name as room_name, hstr.code as room_code, hstrt.code as room_type_code, hstrt.name as room_type_name');
        $this->db->from('room_allotment as hr');
        $this->db->join('hostel_room as hstr', 'hr.id_room = hstr.id');
        $this->db->join('hostel_room_type as hstrt', 'hr.id_room_type = hstrt.id');
        $this->db->where('hr.from_dt <', date('Y-m-d'));
        $this->db->where('hr.to_dt >', date('Y-m-d'));
        $this->db->where('hr.id_student', $id_student);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

     function getRoomAllotment($id)
    {
        $this->db->select('*');
        $this->db->from('room_allotment');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $data = $query->row();
        $data->filled_count = $this->getRoomAllotmentDetailsByRoomId($data->id_room);
        return $data;
    }

    function getRoomByRoomId($id_room)
    {
        $this->db->select('hr.*, hrt.id as id_room_type, hrt.name as room_type_name, hrt.code as room_type_code, hstr.name as hostel_name, hstr.code as hostel_code');
        $this->db->from('hostel_room as hr');
        $this->db->join('hostel_room_type as hrt', 'hr.id_room_type = hrt.id');
        $this->db->join('hostel_registration as hstr', 'hr.id_hostel = hstr.id');
        $this->db->where('hr.id', $id_room);
        $query = $this->db->get();
        return $query->row();

    }


























   
    
    

    function editTaxCode($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('room_allotment', $data);
        return TRUE;
    }
}

