<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <form id="form_grade" action="" method="post">

            <div class="page-title clearfix">
            <h3>Hostel Details</h3>
            </div>
            <div class="form-container">
                <h4 class="form-group-title">Hostel Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Hostel Name :</dt>
                                <dd><?php echo ucwords($hostelDetails->name);?></dd>
                            </dl>
                            <dl>
                                <dt>Staff Incharge :</dt>
                                <dd><?php echo $hostelDetails->ic_no . " - " . $hostelDetails->staff_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Hostel Address :</dt>
                                <dd>
                                    <?php echo $hostelDetails->address ?></dd>
                            </dl> 
                            <dl>
                                <dt>Hostel City :</dt>
                                <dd><?php echo $hostelDetails->city ?></dd>
                            </dl>  
                                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Hostel Code :</dt>
                                <dd><?php echo $hostelDetails->code ?></dd>
                            </dl>
                            <dl>
                                <dt>Contact Number :</dt>
                                <dd><?php echo $hostelDetails->contact_number ?></dd>
                            </dl>  
                            <dl>
                                <dt>Hostel Landmark :</dt>
                                <dd><?php echo $hostelDetails->landmark; ?></dd>
                            </dl>
                            <dl>
                                <dt>Hostel State :</dt>
                                <dd><?php echo $hostelDetails->state; ?></dd>
                            </dl>
                            
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-container">
                <h4 class="form-group-title">Building Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Building Name :</dt>
                                <dd><?php echo ucwords($hostelRoomBuilding->name);?></dd>
                            </dl>   
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Building Code :</dt>
                                <dd><?php echo $hostelRoomBuilding->short_code ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>

            <br>


            <div class="page-title clearfix">
            <h3>Add Block / Level</h3>
            </div>
            <div class="form-container">
                <h4 class="form-group-title">Block / Level Details</h4>
                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="code" name="code">
                            <input type="hidden" class="form-control" id="code1" name="code1" value="<?php echo $hostelRoomBuilding->code ?>">
                        </div>
                    </div>
                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>


                    <!--  <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div> -->
                    
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="<?php echo '../../add/' . $hostelRoomBuilding->id_hostel?>" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>

        <br>


        <div class="form-container">
                <h4 class="form-group-title">Block / Level List</h4>

             <div class="custom-table">
                <table class="table">
                    <thead>
                        <tr>
                        <th>Sl. No</th>
                        <th>Builiding CODE</th>
                         <th>CODE</th>
                         <th>Name</th>
                         <th style="text-align:center; ">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php
                     $total = 0;
                      for($i=0;$i<count($hostelRoomList);$i++)
                     { ?>
                        <tr>
                        <td><?php echo $i+1;?></td>
                        <td><?php echo $hostelRoomBuilding->short_code;?></td>
                        <td><?php echo $hostelRoomList[$i]->short_code;?></td>
                        <td><?php echo $hostelRoomList[$i]->name;?></td>
                        <td style="text-align: center;">
                            <a class="btn btn-sm btn-edit" href="<?php echo '../../level3/' . $hostelRoomList[$i]->id . '/'.$hostelRoomList[$i]->id_parent. '/'.$hostelRoomList[$i]->id_hostel; ?>" title="Edit">Add/View</a>

                        <a class="btn btn-sm btn-edit" href="<?php echo '../../level2Edit/' . $hostelRoomList[$i]->id. '/' . $hostelRoomList[$i]->id_parent . '/'. $hostelDetails->id; ?>" title="Edit">Edit</a>
                        </td>
                         </tr>
                      <?php 
                  } 
                  ?>
                    </tbody>
                </table>
            </div>
        </div>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 code: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
