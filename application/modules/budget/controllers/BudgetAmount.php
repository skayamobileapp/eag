<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class BudgetAmount extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('budget_amount_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('budget_amount.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['financialYearList'] = $this->budget_amount_model->financialYearListByStatus('1');
            $data['departmentList'] = $this->budget_amount_model->departmentListByStatus('1');
            $data['budgetYearList'] = $this->budget_amount_model->budgetYearListByStatus('1');
            $data['departmentCodeList'] = $this->budget_amount_model->departmentCodeListByStatus('1');

            // $formData['id_department'] = $this->security->xss_clean($this->input->post('id_department'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;


            $data['budgetAmountList'] = $this->budget_amount_model->budgetAmountListSearch($formData);

            // echo "<Pre>"; print_r($data['budgetAmountList']);exit;
            $this->global['pageTitle'] = 'FIMS : List Budget Amount';
            $this->loadViews("budget_amount/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('budget_amount.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
        	$id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $id_budget_year = $this->security->xss_clean($this->input->post('id_budget_year'));
                $id_financial_year = $this->security->xss_clean($this->input->post('id_financial_year'));
                // $id_department = $this->security->xss_clean($this->input->post('id_department'));
                $department_code = $this->security->xss_clean($this->input->post('department_code'));
                $amount = $this->security->xss_clean($this->input->post('amount'));


                $data = array(
                    'id_budget_year' => $id_budget_year,
                    'id_financial_year' => $id_financial_year,
                    // 'id_department' => $id_department,
                    'department_code' => $department_code,
                    'amount' => $amount,
                    'balance_amount' => $amount,
                    'status' => 0,
                    'created_by' => $user_id

                );
            
                $check_duplication = $this->budget_amount_model->checkDuplicateBudgetAmount($data);
                if($check_duplication)
                {
                	echo "Duplicate Budget Allocated For The Department On Same Budget Year";exit();
                }

                $result = $this->budget_amount_model->addNewBudgetAmount($data);
                redirect('/budget/budgetAmount/list');
            }

            $data['financialYearList'] = $this->budget_amount_model->financialYearListByStatus('1');
            $data['departmentList'] = $this->budget_amount_model->departmentListByStatus('1');
            $data['budgetYearList'] = $this->budget_amount_model->budgetYearListByStatus('1');
            $data['departmentCodeList'] = $this->budget_amount_model->departmentCodeListByStatus('1');


            $this->global['pageTitle'] = 'FIMS : Add Budget Amount';
            $this->loadViews("budget_amount/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('budget_amount.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/budget/budgetAmount/list');
            }
            if($this->input->post())
            {
                
                $result = $this->budget_amount_model->editBudgetAmountllocation($data,$id);
                redirect('/budget/budgetAmount/list');
            }
            $data['budgetAmount'] = $this->budget_amount_model->getBudgetAmount($id);
            // echo "<Pre>"; print_r($data['budgetAllocation']);exit;
            $this->global['pageTitle'] = 'FIMS : View Budget Amount';
            $this->loadViews("budget_amount/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('budget_amount.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/budget/budgetAmount/approvalList');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );

                $check_duplication = $this->budget_amount_model->editBudgetAmount($data,$id);

                redirect('/budget/budgetAmount/approvalList');
            }
            $data['budgetAmount'] = $this->budget_amount_model->getBudgetAmount($id);
            // echo "<Pre>"; print_r($data['budgetAllocation']);exit;
            $this->global['pageTitle'] = 'FIMS : Approve Budget Amount';
            $this->loadViews("budget_amount/view", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {

        if ($this->checkAccess('budget_amount.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['financialYearList'] = $this->budget_amount_model->financialYearListByStatus('1');
            $data['departmentList'] = $this->budget_amount_model->departmentListByStatus('1');
            $data['budgetYearList'] = $this->budget_amount_model->budgetYearListByStatus('1');
            $data['departmentCodeList'] = $this->budget_amount_model->departmentCodeListByStatus('1');

            // $formData['id_department'] = $this->security->xss_clean($this->input->post('id_department'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;


            $data['budgetAmountList'] = $this->budget_amount_model->budgetAmountListSearch($formData);

            // echo "<Pre>"; print_r($data['budgetAmountList']);exit;
            $this->global['pageTitle'] = 'FIMS : List Budget Amount';
            $this->loadViews("budget_amount/approval_list", $this->global, $data, NULL);
        }
    }
}
