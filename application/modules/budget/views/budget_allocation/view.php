<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Budget Allocation</h3>
        </div>
        <form id="form_subject" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Budget Allocation</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->financial_year;?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->budget_year;?>" readonly>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code<span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->department . " - " . $budgetAmount->department_name;?>" readonly>
                    </div>
                </div>
            </div>


             <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Allocated Amount <span class='error-text'>*</span></label>
                       <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->amount; ?>" readonly>
                          <!-- . " - " . $budgetAmount->name;?>"> -->
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Balance Amount <span class='error-text'>*</span></label>
                       <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->balance_amount; ?>" readonly>
                    </div>
                </div>


               <!--  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Increment Amount <span class='error-text'>*</span></label>
                       <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->increment_amount; ?>" readonly>
                    </div>
                </div> -->

            </div>


             <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status</label>
                       <input type="text" class="form-control" id="name" name="name" value="<?php 
                       if( $budgetAmount->allocation_status == '0')
                      {
                        echo "Pending";
                      }
                      elseif($budgetAmount->allocation_status == '1')
                      {
                        echo "Approved";
                      } 
                      elseif($budgetAmount->allocation_status == '2')
                      {
                        echo "Rejected";
                      } 

                     ?>" readonly>
                          <!-- . " - " . $budgetAmount->name;?>"> -->
                    </div>
                </div>

            </div>

            <?php
            if($budgetAmount->allocation_status == '2')
            {
             ?>

            <div class="row">

                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $budgetAmount->allocation_reason; ?>" readonly>
                    </div>
                </div>
            </div>

            <?php
            }
            ?>

          </div>




            <h3>Budget Allocation Details</h3>

        <div class="form-container">
            <h4 class="form-group-title">Budget Allocation Details</h4>

          <div class="custom-table">
            <table class="table">
                <thead>
                    <tr>
                    <th>Sl. No</th>
                     <th>Fund Code</th>
                     <th>Department Code</th>
                     <th>Activity Code</th>
                     <th>Account Code</th>
                     <!-- <th>Increment Amount</th>
                     <th>Decrement Amount</th>
                     <th>Used Amount</th> -->
                     <th>Allocated Amount</th>
                     <th>Balance Amount</th>
                    </tr>
                </thead>
                <tbody>
                     <?php
                 $total = 0;
                  for($i=0;$i<count($budgetAllocationList);$i++)
                 { ?>
                    <tr>
                    <td><?php echo $i+1;?></td>
                    <td><?php echo $budgetAllocationList[$i]->fund_code;?></td>
                    <td><?php echo $budgetAllocationList[$i]->department_code;?></td>
                    <td><?php echo $budgetAllocationList[$i]->activity_code;?></td>
                    <td><?php echo $budgetAllocationList[$i]->account_code;?></td>
                   <!--  <td><?php echo $budgetAllocationList[$i]->increment_amount;?></td>
                    <td><?php echo $budgetAllocationList[$i]->decrement_amount;?></td>
                    <td><?php echo $budgetAllocationList[$i]->used_amount;?></td> -->
                    <td><?php echo $budgetAllocationList[$i]->allocated_amount;?></td>
                    <td><?php echo $budgetAllocationList[$i]->balance_amount;?></td>

                     </tr>
                  <?php 
                  $total = $total + $budgetAllocationList[$i]->balance_amount;
              } 
              $total = number_format($total, 2, '.', ',');
              ?>
                <tr>
                    <td bgcolor="" colspan="5"></td>
                    <td bgcolor=""><b> Total : </b></td>
                    <td bgcolor=""><b><?php echo $total; ?></b></td>
                </tr>

                  </tbody>
              </table>
          </div>

        </div>





            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>

    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }



    $(document).ready(function()
    {
        $("#form_subject").validate(
        {
            rules:
            {
                status:
                {
                    required: true
                },
                reason:
                {
                    required: true
                }
            },
            messages:
            {
                status:
                {
                    required: "<p class='error-text'>Select Status</p>",
                },
                reason:
                {
                    required: "<p class='error-text'>Reason Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>