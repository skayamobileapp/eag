<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Budget_year_model extends CI_Model
{
    function budgetYearList($search)
    {
        $this->db->select('*');
        $this->db->from('budget_year');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getBudgetYear($id)
    {
        $this->db->select('*');
        $this->db->from('budget_year');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function checkBudgetYearDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('budget_year');
        $this->db->where('name', $data['name']);
        $query = $this->db->get();
        return $query->row();
    }

    function checkBudgetYearDuplicationEdit($data,$id)
    {
        $this->db->select('*');
        $this->db->from('budget_year');
        $this->db->where('name', $data['name']);
        $this->db->where('id !=', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewBudgetYear($data)
    {
        $this->db->trans_start();
        $this->db->insert('budget_year', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editBudgetYear($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('budget_year', $data);
        return TRUE;
    }
    
    function delete($id, $date)
    {
        $this->db->where('id', $countryId);
        $this->db->update('budget_year', $countryInfo);
        return $this->db->affected_rows();
    }
}

