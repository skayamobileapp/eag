<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Journal Entry</h3>
            </div>

        <div class="form-container">
            <h4 class="form-group-title">Journal Entry </h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <select disabled="disabled" name="id_financial_year" id="id_financial_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($financialYearList))
                            {
                                foreach ($financialYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                    <?php if($record->id==$journalEntry->id_financial_year)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                    >
                                    <?php echo  $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <select disabled="disabled" name="id_budget_year" id="id_budget_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($budgetYearList))
                            {
                                foreach ($budgetYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                    <?php if($record->id==$journalEntry->id_budget_year)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                    >
                                    <?php echo  $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $journalEntry->description;?>" readonly="readonly">
                    </div>
                </div>

                

             </div>

            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Journal Entry Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="pr_entry_date" name="pr_entry_date" value="<?php echo date('d-m-Y', strtotime($journalEntry->date));?>" readonly="readonly">
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Journal Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reason" name="reason" value="<?php echo $journalEntry->journal_number;?>" readonly="readonly">
                    </div>
                </div>


                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="total_amount" name="total_amount" value="<?php echo $journalEntry->total_amount; ?>" readonly="readonly">
                    </div>
               </div>


             </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php 
                        if($journalEntry->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($journalEntry->status == '1')
                        {
                            echo "Approved";
                        } 
                        elseif($journalEntry->status == '2')
                        {
                            echo "Rejected";
                        } 
                        ?>
                        " readonly="readonly">
                    </div>
                </div>

                
               
                
            </div>

        </div>




            <h3>Journal Details</h3>

        <div class="form-container">
            <h4 class="form-group-title">Journal Entry Details</h4>



            <div class="custom-table">
            <table class="table">
                <thead>
                    <tr>
                    <th>Sl. No</th>
                     <th>Journal Type</th>
                     <th>GL Code</th>
                     <th>Credit Amount</th>
                     <th>Debit Amount</th>
                    </tr>
                </thead>
                <tbody>
                     <?php
                 $total_credit = 0;
                 $total_debit = 0;
                  for($i=0;$i<count($journalEntryDetails);$i++)
                 { ?>
                    <tr>
                    <td><?php echo $i+1;?></td>
                    <td><?php echo $journalEntryDetails[$i]->journal_type;?></td>
                    <td><?php echo $journalEntryDetails[$i]->fund_code . " - " . $journalEntryDetails[$i]->department_code . " - " . $journalEntryDetails[$i]->activity_code . " - " . $journalEntryDetails[$i]->account_code;?></td>
                    <td><?php echo $journalEntryDetails[$i]->credit_amount;?></td>
                    <td><?php echo $journalEntryDetails[$i]->debit_amount;?></td>

                     </tr>
                  <?php 
                  $total_credit = $total_credit + $journalEntryDetails[$i]->credit_amount;
                  $total_debit = $total_debit + $journalEntryDetails[$i]->debit_amount;
              } 
              $total_credit = number_format($total_credit, 2, '.', ',');
              $total_debit = number_format($total_debit, 2, '.', ',');
              ?>
                <tr>
                    <td bgcolor="" colspan="2"></td>
                    <td bgcolor=""><b> Total : </b></td>
                    <td bgcolor=""><b><?php echo $total_credit; ?></b></td>
                    <td bgcolor=""><b><?php echo $total_debit; ?></b></td>
                </tr>

                    </tbody>
                </table>
            </div>

        </div>

        

        <div class="button-block clearfix">
            <div class="bttn-group">
                <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script>

  $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                type_of_pr: {
                    required: true
                },
                 description: {
                    required: true
                }
                ,
                 pr_entry_date: {
                    required: true
                },
                 id_vendor: {
                    required: true
                },
                 id_department: {
                    required: true
                },
                 id_financial_year: {
                    required: true
                },
                 amount: {
                    required: true
                }
            },
            messages: {
                type_of_pr: {
                    required: "<p class='error-text'>Select Type Of PR",
                },
                description: {
                    required: "<p class='error-text'>Enter Description",
                },
                pr_entry_date: {
                    required: "<p class='error-text'>Select PR Entry Date</p>",
                },
                id_vendor: {
                    required: "<p class='error-text'>Select Vendor</p>",
                },
                id_department: {
                    required: "<p class='error-text'>Select Department</p>",
                },
                id_financial_year: {
                    required: "<p class='error-text'>Select Financial Year</p>",
                },
                amount: {
                    required: "<p class='error-text'>Enter Amount</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2020"
    });
  } );
</script>