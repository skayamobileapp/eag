<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Account Code</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Financial Year Details</h4>


            <div class="row">
               
               <!--  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="name" name="name" maxlength="4" value="<?php echo $financialYear->name; ?>">
                    </div>
                </div> -->

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Year <span class='error-text'>*</span></label>
                        <input type="number"  min="<?php echo date('Y'); ?>" max="<?php echo date('Y')+4; ?>" class="form-control" id="year" name="year" value="<?php echo $financialYear->year; ?>">
                    </div>
                </div>
                
                 <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($financialYear->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($financialYear->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>               
                
            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 year: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                year: {
                    required: "<p class='error-text'>Year required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>