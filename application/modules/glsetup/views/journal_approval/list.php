<div class="container-fluid page-wrapper">
        <form action="" method="post" id="searchForm">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Journal Approval List</h3>
      <a href="add" class="btn btn-primary">+ Add Journal</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Journal</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Journal Number</th>
            <th>Amount</th>
            <th>Reason</th>
            <th>Financial Year</th>
            <th>Created On</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($journalList)) {
            $i=1;
            foreach ($journalList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><input type="checkbox" name="checkvalue[]" class="check" value="<?php echo $record->id ?>"><?php echo $record->journal_number ?></td>
                <td><?php echo $record->total_amount ?></td>
                <td><?php echo $record->reason ?></td>
                <td><?php echo $record->financial_year ?></td>
               
               
                <td><?php echo date("d-m-Y", strtotime($record->created_dt_tm)) ?></td>
                <td class="text-center">
                  <!-- <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a> -->
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
          <div class="app-btn-group">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
    </div>
  </div>
</form>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  function clearSearchForm()
      {
        window.location.reload();
      }
</script>