<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Journal extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('journal_model');
        $this->load->model('financial_year_model');
        $this->load->model('department_code_model');
        $this->load->model('account_code_model');
        $this->load->model('activity_code_model');
        $this->load->model('fund_code_model');
        $this->isLoggedIn();
    }

    function list()
    {
       // echo "<Pre>"; print_r($this->session->userId);exit;
        if ($this->checkAccess('journal.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
            $data['searchParam'] = $formData;

            $data['journalList'] = $this->journal_model->journalListSearch($formData);
            $data['financialYearList'] = $this->journal_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->journal_model->budgetYearListByStatus('1');


            $this->global['pageTitle'] = 'FIMS : Journal List';
            $this->loadViews("journal/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('journal.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            
            if($this->input->post())
            {
                 $d['type'] = 'JR';
                $journal_number = $this->journal_model->generateJRNumber($d);

                // $journal_number = $this->security->xss_clean($this->input->post('journal_number'));
                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));
                $id_financial_year = $this->security->xss_clean($this->input->post('id_financial_year'));
                $id_budget_year = $this->security->xss_clean($this->input->post('id_budget_year'));
                $date = $this->security->xss_clean($this->input->post('date'));
                $description = $this->security->xss_clean($this->input->post('description'));
                // $approval_status = $this->security->xss_clean($this->input->post('approval_status'));

                 // 'journal_number' => $journal_number,
                    // 'approval_status' => $approval_status,

                $data = array(
                   
                    'id_financial_year' => $id_financial_year,
                    'id_budget_year' => $id_budget_year,
                    'total_amount' => $total_amount,
                    'journal_number' => $journal_number,
                    'date' => date('Y-m-d'),
                    'description' => $description,
                    'created_by' => $user_id,
                    'status'=>'0'
                );
                $inserted_id = $this->journal_model->addNewJournal($data);

                $temp_details = $this->journal_model->getTempJournalDetailsDataBySession($id_session,$inserted_id);

                $this->journal_model->deleteTempDataBySession($id_session);
                redirect('/glsetup/journal/list');
            }
            else
            {
                $this->journal_model->deleteTempDataBySession($id_session);
            }

            $data['financialYearList'] = $this->financial_year_model->financialYearList();
            $data['accountCodeList'] = $this->account_code_model->accountCodeList();
            $data['activityCodeList'] = $this->activity_code_model->activityCodeList();
            $data['departmentCodeList'] = $this->department_code_model->departmentCodeList();
            $data['fundCodeList'] = $this->fund_code_model->fundCodeList();
            $data['budgetYearList'] = $this->journal_model->budgetYearListByStatus('1');
            // echo "<Pre>";  print_r($data['accountCodeList']);exit;
            $this->global['pageTitle'] = 'FIMS : Add Journal';
            $this->loadViews("journal/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('journal.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/glsetup/journal/list');
            }
            if($this->input->post())
            {
                redirect('/glsetup/journal/list');
            }
            // $data['studentList'] = $this->journal_model->studentList();
            $data['financialYearList'] = $this->financial_year_model->financialYearList();
            $data['budgetYearList'] = $this->journal_model->budgetYearListByStatus('1');
            $data['budgetYearList'] = $this->journal_model->budgetYearListByStatus('1');

            $data['journalEntry'] = $this->journal_model->getJournal($id);
            $data['journalEntryDetails'] = $this->journal_model->getJournalDetails($id);
            $this->global['pageTitle'] = 'FIMS : Edit Journal';
            $this->loadViews("journal/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('journal.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if ($id == null)
            {
                redirect('/glsetup/journal/approvalList');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));


                $data = array(
                    'status' => $status,
                    'approved_by' => $id_user
                );

                $updated = $this->journal_model->editJournal($data,$id);

                if($status == '1')
                {
                    $add_ledger = $this->journal_model->addLedger($id);
                }



                redirect('/glsetup/journal/approvalList');
            }
            // $data['studentList'] = $this->journal_model->studentList();
            $data['financialYearList'] = $this->financial_year_model->financialYearList();
            $data['budgetYearList'] = $this->journal_model->budgetYearListByStatus('1');

            $data['journalEntry'] = $this->journal_model->getJournal($id);
            $data['journalEntryDetails'] = $this->journal_model->getJournalDetails($id);
            $this->global['pageTitle'] = 'FIMS : Edit Journal';
            $this->loadViews("journal/view", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
       // echo "<Pre>"; print_r($this->session->userId);exit;
        if ($this->checkAccess('journal.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '0';
            $data['searchParam'] = $formData;

            $data['journalList'] = $this->journal_model->journalListSearch($formData);
            $data['financialYearList'] = $this->journal_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->journal_model->budgetYearListByStatus('1');


            $this->global['pageTitle'] = 'FIMS : Approve Journal List';
            $this->loadViews("journal/approval_list", $this->global, $data, NULL);
        }
    }

    function tempDetailsDataAdd()
    {
        

        $id_session = $this->session->my_session_id;
        $journal_type = $this->security->xss_clean($this->input->post('journal_type'));
        $credit_amount = $this->security->xss_clean($this->input->post('credit_amount'));
        $debit_amount = $this->security->xss_clean($this->input->post('debit_amount'));
        $id_account_code = $this->security->xss_clean($this->input->post('id_account_code'));
        $id_activity_code = $this->security->xss_clean($this->input->post('id_activity_code'));
        $id_department_code = $this->security->xss_clean($this->input->post('id_department_code'));
        $id_fund_code = $this->security->xss_clean($this->input->post('id_fund_code'));

        $data = array(
            'id_session' => $id_session,
            'journal_type' => $journal_type,
            'id_account_code' => $id_account_code,
            'id_activity_code' => $id_activity_code,
            'id_department_code' => $id_department_code,
            'id_fund_code' => $id_fund_code,
            'credit_amount' => $credit_amount,
            'debit_amount' => $debit_amount
            );
        
        $inserted_id = $this->journal_model->addNewTempJournalDetails($data);       
        $table = $this->displaytempdata();       
        echo $table;
        
    }


     function displaytempdata()
     {
        $id_session = $this->session->my_session_id;
        $temp_details = $this->journal_model->getTempJournalDetailsData($id_session);

        if(!empty($temp_details))
        {

            $table = "
            <div class='custom-table'>
            <table  class='table' id='list-table'>
            <thead>
                <tr>
                    <th>Sl No.</th>
                    <th>Journal Type</th>
                    <th>GL Code</th>
                    <th>Cr Amount</th>
                    <th>Dt Amount</th>
                    <th>Action</th>
                </tr>
            </thead>";
                $total_dt = 0;
                $total_cr = 0;
                $total = 0;
                for($i=0;$i<count($temp_details);$i++)
                {
                    $id = $temp_details[$i]->id;
                    $journal_type = $temp_details[$i]->journal_type;
                    $credit_amount = $temp_details[$i]->credit_amount;
                    $debit_amount = $temp_details[$i]->debit_amount;
                    $account_code = $temp_details[$i]->account_code;
                    $activity_code = $temp_details[$i]->activity_code;
                    $department_code = $temp_details[$i]->department_code;
                    $fund_code = $temp_details[$i]->fund_code;
                    $j=$i+1;

                    $table .= "
                <tbody>
                <tr>
                    <td>$j</td>
                    <td>$journal_type</td>
                    <td>$fund_code - $department_code - $activity_code - $account_code</td>
                    <td>$credit_amount</td>
                    <td>$debit_amount</td>
                    <td>
                        <span onclick='deleteTempData($id)'>Delete</a>
                    <td>
                </tr>";

                $total_dt = $total_dt + $debit_amount;
                $total_cr = $total_cr + $credit_amount;
                
                }
                $total = $total_dt + $total_cr;
                        
            $table .= "

            <tr>
                <td bgcolor='' colspan='2'></td>
                <td bgcolor=''><b> Total : </b></td>
                <td bgcolor=''>
                    <b>
                        <input type='hidden' name='total_cr' id='total_cr' value='$total_cr'/>
                        $total_cr
                    </b>
                </td>
                <td bgcolor=''>
                    <b>
                        <input type='hidden' name='total_dt' id='total_dt' value='$total_dt'/>
                    $total_dt
                    </b>
                </td>
                <td bgcolor=''>
                    <input type='hidden' name='total' id='total' value='$total'/>
                    <b>$total</b>
                </td>
            </tr>

            </tbody>
            </table>
            </div>
            ";
        }
        else
        {
             $table = "";
        }

            
        return $table;
    }

   function tempadd()
   {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        $inserted_id = $this->journal_model->addNewTempJournalDetails($tempData);
        $data = $this->displaytempdata();

        echo $data;        
    }
    

    function tempDelete($id)
    {
        $inserted_id = $this->journal_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 

    function tempedit($id)
    {
        $data = $this->journal_model->getTempDetails($id);
        echo json_encode($data);
    }   
}