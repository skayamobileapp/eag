<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ActivityCode extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('activity_code_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('activity_code.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['activityCodeList'] = $this->activity_code_model->activityCodeListSearch($name);
            $this->global['pageTitle'] = 'FIMS : List Activity Code';
            //print_r($subjectDetails);exit;
            $this->loadViews("activity_code/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('activity_code.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'short_code' => $code,
                    'level' => 1,
                    'name' => $name
                );
                // ,
                    // 'status' => $status

                $duplicate_row = $this->activity_code_model->checkActivityCodeDuplication($data);

                // if($duplicate_row)
                // {
                //     echo "Duplicate Activity Code Not Allowed";exit();
                // }
            
                $result = $this->activity_code_model->addNewActivityCode($data);
                // redirect(['HTTP_REFERER']);
                redirect('/glsetup/activityCode/add');
            }
            $activity['level'] = '1';
            $activity['parent'] = '';

            $data['activityCodeList'] = $this->activity_code_model->activityCodeListByLevel($activity);   
            $this->global['pageTitle'] = 'FIMS : Add Activity Code';
            $this->loadViews("activity_code/add", $this->global, $data, NULL);
        }
    }

    function level2($id = NULL)
    {
        if ($this->checkAccess('activity_code.level2') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code1 = $this->security->xss_clean($this->input->post('code1'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $complete_code = $code1 . $code;

            
                $data = array(
                    'code' => $complete_code,
                    'short_code' => $code,
                    'level' => 2,
                    'id_parent' => $id,
                    'name' => $name
                );
                // ,
                    // 'status' => $status

                // $duplicate_row = $this->activity_code_model->checkActivityCodeDuplication($data);

                // if($duplicate_row)
                // {
                //     echo "Duplicate Activity Code Not Allowed";exit();
                // }

            
                $result = $this->activity_code_model->addNewActivityCode($data);
                // redirect(['HTTP_REFERER']);
                redirect('/glsetup/activityCode/level2/'.$id);
            }
            $activity['level'] = '2';
            $activity['parent'] = $id;

            $data['activityCode'] = $this->activity_code_model->getActivityCode($id);   
            $data['activityCodeList'] = $this->activity_code_model->activityCodeListByLevel($activity);   
            $this->global['pageTitle'] = 'FIMS : Add Activity Code';
            $this->loadViews("activity_code/level2", $this->global, $data, NULL);
        }
    }

    function level3($id,$id_parent)
    {
        if ($this->checkAccess('activity_code.level3') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code1 = $this->security->xss_clean($this->input->post('code1'));
                $code2 = $this->security->xss_clean($this->input->post('code2'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $complete_code = $code2 . $code;

                $data = array(
                    'code' => $complete_code,
                    'short_code' => $code,
                    'level' => 3,
                    'id_parent' => $id,
                    'name' => $name
                );
                // ,
                    // 'status' => $status

                // $duplicate_row = $this->activity_code_model->checkActivityCodeDuplication($data);

                // if($duplicate_row)
                // {
                //     echo "Duplicate Activity Code Not Allowed";exit();
                // }
            
                $result = $this->activity_code_model->addNewActivityCode($data);
                // redirect(['HTTP_REFERER']);
                redirect('/glsetup/activityCode/level3/'.$id.'/'.$id_parent);
            }
            $activity['level'] = '3';
            $activity['parent'] = $id;

            $data['activityCode1'] = $this->activity_code_model->getActivityCode($id_parent);
            $data['activityCode2'] = $this->activity_code_model->getActivityCode($id);
            $data['activityCodeList'] = $this->activity_code_model->activityCodeListByLevel($activity);   
            $data['id'] = $id;
            $data['id_parent'] = $id_parent;
            // echo "<Pre>";print_r($data);exit();
            $this->global['pageTitle'] = 'FIMS : Add Activity Code';
            $this->loadViews("activity_code/level3", $this->global, $data, NULL);
        }
    }

    function level1Edit($id = NULL)
    {
        if ($this->checkAccess('activity_code.level1_edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/glsetup/activityCode/add');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));

            
                $data = array(
                    'code' => $code,
                    'short_code' => $code,
                    'name' => $name
                );
                
                $result = $this->activity_code_model->editActivityCode($data,$id);
                redirect('/glsetup/activityCode/add');
            }
            $data['activityCode'] = $this->activity_code_model->getActivityCode($id);
            $this->global['pageTitle'] = 'FIMS : Edit Activity Level 1 Code';
            $this->loadViews("activity_code/level1_edit", $this->global, $data, NULL);
        }
    }


    function level2Edit($id = NULL,$id_parent = NULL)
    {
        if ($this->checkAccess('activity_code.level2_edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/glsetup/activityCode/level2/'.$id_parent);
            }
            if($this->input->post())
            {
                $code1 = $this->security->xss_clean($this->input->post('code1'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));

                $complete_code = $code1 . $code;
            
                $data = array(
                    'short_code' => $code,
                    'code' => $complete_code,
                    'name' => $name
                );
                
                $result = $this->activity_code_model->editActivityCode($data,$id);
                redirect('/glsetup/activityCode/level2/'.$id_parent);
            }
            $data['activityCode'] = $this->activity_code_model->getActivityCode($id);
            $data['activityCodeParent'] = $this->activity_code_model->getActivityCode($id_parent);
            $this->global['pageTitle'] = 'FIMS : Edit Activity Level 2 Code';
            $this->loadViews("activity_code/level2_edit", $this->global, $data, NULL);
        }
    }

    function level3Edit($id = NULL,$id_parent = NULL,$id_level1 = NULL)
    {
        if ($this->checkAccess('activity_code.level3_edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/glsetup/activityCode/level3/'.$id_parent.'/'.$id_level1);
            }
            if($this->input->post())
            {
                $code1 = $this->security->xss_clean($this->input->post('code1'));
                $code2 = $this->security->xss_clean($this->input->post('code2'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
            
                $complete_code = $code2 . $code;

                $data = array(
                    'short_code' => $code,
                    'code' => $complete_code,
                    'name' => $name
                );
                
                $result = $this->activity_code_model->editActivityCode($data,$id);
                redirect('/glsetup/activityCode/level3/'.$id_parent.'/'.$id_level1);
            }
            $data['activityCode'] = $this->activity_code_model->getActivityCode($id);
            $data['activityCodeParent'] = $this->activity_code_model->getActivityCode($id_parent);
            $data['activityCodeParentLevel1'] = $this->activity_code_model->getActivityCode($id_level1);
            $data['id_level1'] = $id_level1;

            // echo "<Pre>";print_r($data);exit();

            $this->global['pageTitle'] = 'FIMS : Edit Activity Level 3 Code';
            $this->loadViews("activity_code/level3_edit", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('activity_code.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/glsetup/activity_code/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status
                );
                
                $result = $this->activity_code_model->editActivityCode($data,$id);
                redirect('/glsetup/activityCode/list');
            }
            $data['activityCode'] = $this->activity_code_model->getActivityCode($id);
            $this->global['pageTitle'] = 'FIMS : Edit Activity Code';
            $this->loadViews("activity_code/edit", $this->global, $data, NULL);
        }
    }
}
