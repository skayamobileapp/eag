<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Ledger extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ledger_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('ledger.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
            $data['searchParam'] = $formData;

            $data['ledgerList'] = $this->ledger_model->ledgerListSearch($formData);
            $data['financialYearList'] = $this->ledger_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->ledger_model->budgetYearListByStatus('1');

       // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'FIMS : Journal List';
            $this->loadViews("ledger/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('ledger.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/glsetup/ledger/list');
            }
            if($this->input->post())
            {
                redirect('/glsetup/ledger/list');
            }
            // $data['studentList'] = $this->ledger_model->studentList();
            $data['financialYearList'] = $this->ledger_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->ledger_model->budgetYearListByStatus('1');

            $data['ledgerEntry'] = $this->ledger_model->getLedger($id);
            $data['ledgerEntryDetails'] = $this->ledger_model->getLedgerDetails($id);
            $this->global['pageTitle'] = 'FIMS : Edit Journal';
            $this->loadViews("ledger/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('ledger.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if ($id == null)
            {
                redirect('/glsetup/ledger/approvalList');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));


                $data = array(
                    'status' => $status,
                    'approved_by' => $id_user
                );

                $updated = $this->ledger_model->editJournal($data,$id);

                if($status == '1')
                {
                    $add_ledger = $this->ledger_model->addLedger($id);
                }



                redirect('/glsetup/ledger/approvalList');
            }
            // $data['studentList'] = $this->ledger_model->studentList();
            $data['financialYearList'] = $this->financial_year_model->financialYearList();
            $data['budgetYearList'] = $this->ledger_model->budgetYearListByStatus('1');

            $data['ledgerEntry'] = $this->ledger_model->getJournal($id);
            $data['ledgerEntryDetails'] = $this->ledger_model->getJournalDetails($id);
            $this->global['pageTitle'] = 'FIMS : Edit Journal';
            $this->loadViews("ledger/view", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
       // echo "<Pre>"; print_r($this->session->userId);exit;
        if ($this->checkAccess('ledger.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '0';
            $data['searchParam'] = $formData;

            $data['ledgerList'] = $this->ledger_model->ledgerListSearch($formData);
            $data['financialYearList'] = $this->ledger_model->financialYearListByStatus('1');


            $this->global['pageTitle'] = 'FIMS : Approve Journal List';
            $this->loadViews("ledger/approval_list", $this->global, $data, NULL);
        }
    }
}