<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Account_code_model extends CI_Model
{
    function accountCodeList()
    {
        $this->db->select('*');
        $this->db->from('account_code');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function accountCodeListByLevel($data)
    {
        $this->db->select('*');
        $this->db->from('account_code');
        if($data['level'] != '')
        {
            $this->db->where('level', $data['level']);
        }
        if($data['parent'] != '')
        {
            $this->db->where('id_parent', $data['parent']);
        }
        $this->db->order_by("id", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function accountCodeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('account_code');
        if (!empty($search))
        {
            $likeCriteria = "(code  LIKE '%" . $search . "%' or name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getAccountCode($id)
    {
        $this->db->select('*');
        $this->db->from('account_code');
        $this->db->where('id', $id);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function checkAccountCodeDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('account_code');
        $this->db->where('code', $data['code']);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewAccountCode($data)
    {
        $this->db->trans_start();
        $this->db->insert('account_code', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editAccountCode($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('account_code', $data);
        return TRUE;
    }
}

