<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Activity_code_model extends CI_Model
{
    function activityCodeList()
    {
        $this->db->select('*');
        $this->db->from('activity_code');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function activityCodeListByLevel($data)
    {
        $this->db->select('*');
        $this->db->from('activity_code');
        if($data['level'] != '')
        {
            $this->db->where('level', $data['level']);
        }
        if($data['parent'] != '')
        {
            $this->db->where('id_parent', $data['parent']);
        }
        $this->db->order_by("id", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function activityCodeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('activity_code');
        if (!empty($search))
        {
            $likeCriteria = "(code  LIKE '%" . $search . "%' or name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getActivityCode($id)
    {
        $this->db->select('*');
        $this->db->from('activity_code');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function checkActivityCodeDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('activity_code');
        $this->db->where('code', $data['code']);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewActivityCode($data)
    {
        $this->db->trans_start();
        $this->db->insert('activity_code', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editActivityCode($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('activity_code', $data);
        return TRUE;
    }
}

