<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProgrammeHasFeeStructure extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('programme_has_fee_structure_model');
        $this->load->model('programme_model');
        $this->load->model('intake_model');
        $this->load->model('semester_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('programme_has_fee_structure.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['programmeList'] = $this->programme_model->programmeList();
            $data['intakeList'] = $this->intake_model->intakeList();
            $data['semesterList'] = $this->semester_model->semesterList();

            $formData['scheme'] = $this->security->xss_clean($this->input->post('scheme'));
            $formData['code'] = $this->security->xss_clean($this->input->post('code'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));

            $data['searchParameters'] = $formData; 


            $data['programmeHasFeeStructureList'] = $this->programme_has_fee_structure_model->programmeHasFeeStructureListSearch($formData);
            $this->global['pageTitle'] = 'Campus Management System : Program Has Course List';
            $this->loadViews("programme_has_fee_structure/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('programme_has_fee_structure.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {



            $scheme = $this->security->xss_clean($this->input->post('scheme'));
            $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
            $code = $this->security->xss_clean($this->input->post('code'));
            $description = $this->security->xss_clean($this->input->post('description'));
            $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
            $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
            $description = $this->security->xss_clean($this->input->post('description'));
            $currency = $this->security->xss_clean($this->input->post('currency'));
            $student_category = $this->security->xss_clean($this->input->post('student_category'));
            $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'scheme' => $scheme,
                    'id_programme' => $id_programme,
                    'code' => $code,
                    'description' => $description,
                    'id_intake' => $id_intake,
                    'id_semester' => $id_semester,
                    'description' => $description,
                    'currency' => $currency,
                    'student_category' => $student_category,
                    'status' => $status

                );
                //echo "<pre>"; print_r($data);exit();
                $result = $this->programme_has_fee_structure_model->addNewProgrammeHasFeeStructure($data);
                redirect('/setup/programmeHasFeeStructure/list');
            }
            $data['programmeList'] = $this->programme_model->programmeList();
            $data['intakeList'] = $this->intake_model->intakeList();
            $data['semesterList'] = $this->semester_model->semesterList();
            //echo "<pre>";print_r($query);die;
            $this->global['pageTitle'] = 'Campus Management System : Add Program Has Couerse';
            $this->loadViews("programme_has_fee_structure/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('programme_has_fee_structure.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/programmeHasFeeStructure/list');
            }
            if($this->input->post())
            {
                $scheme = $this->security->xss_clean($this->input->post('scheme'));
            $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
            $code = $this->security->xss_clean($this->input->post('code'));
            $description = $this->security->xss_clean($this->input->post('description'));
            $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
            $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
            $description = $this->security->xss_clean($this->input->post('description'));
            $currency = $this->security->xss_clean($this->input->post('currency'));
            $student_category = $this->security->xss_clean($this->input->post('student_category'));
            $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'scheme' => $scheme,
                    'id_programme' => $id_programme,
                    'code' => $code,
                    'description' => $description,
                    'id_intake' => $id_intake,
                    'id_semester' => $id_semester,
                    'description' => $description,
                    'currency' => $currency,
                    'student_category' => $student_category,
                    'status' => $status

                );
                //echo "<pre>"; print_r($data);exit();

                $result = $this->programme_has_fee_structure_model_model->editprogrammeHasFeeStructure($data,$id);
                redirect('/setup/programmeHasFeeStructure/list');
            }

            $data['programmeList'] = $this->programme_model->programmeList();
            $data['intakeList'] = $this->intake_model->intakeList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['programmeHasFeeStructureDetails'] = $this->programme_has_fee_structure_model->getProgrammeHasFeeStructureDetails($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Program Has Couerse';
            $this->loadViews("programme_has_fee_structure/edit", $this->global, $data, NULL);
        }
    }
}
