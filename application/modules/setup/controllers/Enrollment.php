<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Enrollment extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('enrollment_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('enrollment.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_university'] = $this->security->xss_clean($this->input->post('id_university'));
            $formData['id_education_level'] = $this->security->xss_clean($this->input->post('id_education_level'));
            $data['searchParam'] = $formData;

            $data['universityList'] = $this->enrollment_model->getUniversityListByStatus('1');
            $data['educationLevelList'] = $this->enrollment_model->educationLevelListByStatus('1');
            $data['organisation'] = $this->enrollment_model->getOrganisaton();

            $data['enrollmentList'] = $this->enrollment_model->enrollmentListSearch($formData);
            $this->global['pageTitle'] = 'Campus Management System : New Student Enrollment List';
            $this->loadViews("enrollment/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('enrollment.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));
                $id_university = $this->security->xss_clean($this->input->post('id_university'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'description' => $description,
                    'id_education_level' => $id_education_level,
                    'id_university' => $id_university,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->enrollment_model->addNewEnrollment($data);
                redirect('/setup/enrollment/list');
            }

            $data['universityList'] = $this->enrollment_model->getUniversityListByStatus('1');
            $data['educationLevelList'] = $this->enrollment_model->educationLevelListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add New Student Enrollment';
            $this->loadViews("enrollment/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('enrollment.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/enrollment/list');
            }
            if($this->input->post())
            {
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));
                $id_university = $this->security->xss_clean($this->input->post('id_university'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'description' => $description,
                    'id_education_level' => $id_education_level,
                    'id_university' => $id_university,
                    'status' => $status
                );

                $result = $this->enrollment_model->editEnrollment($data,$id);
                redirect('/setup/enrollment/list');
            }

            $data['universityList'] = $this->enrollment_model->getUniversityListByStatus('1');
            $data['educationLevelList'] = $this->enrollment_model->educationLevelListByStatus('1');

            $data['enrollment'] = $this->enrollment_model->getEnrollment($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit New Student Enrollment';
            $this->loadViews("enrollment/edit", $this->global, $data, NULL);
        }
    }
}
