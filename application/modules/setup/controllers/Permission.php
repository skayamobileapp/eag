<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Permission extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('permission_model');
        $this->load->model('role_model');
        $this->isLoggedIn();
    }
    
    
    function list()
    {
        if($this->checkAccess('permission.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {         

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['module'] = $this->security->xss_clean($this->input->post('module'));
            $data['searchParam'] = $formData;
            
            $data['menuList'] = $this->permission_model->menuList();
            $data['permissionRecords'] = $this->permission_model->permissionListingSearch($formData);
            // echo "<Pre>";print_r($data['permissionRecords']);exit();
            
            $this->global['pageTitle'] = 'School : Permission Listing';
            
            $this->loadViews("permission/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if($this->checkAccess('permission.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit();

                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $module = $this->security->xss_clean($this->input->post('module'));
                $id_menu = $this->security->xss_clean($this->input->post('id_menu'));
                
                $permissionInfo = array(
                    'code'=>$code,
                    'module'=>$module,
                    'id_menu'=>$id_menu,
                    'description'=>$description,
                    'status' => 1
                    );
            
                $result = $this->permission_model->addNewPermission($permissionInfo);
                                
                redirect('/setup/permission/list');
            }

            $data['menuList'] = $this->permission_model->menuList();


            $this->global['pageTitle'] = 'School : Add New Permission';
            $this->loadViews("permission/add", $this->global, $data, NULL);
        }
    }

    function edit($permissionId = NULL)
    {
        if($this->checkAccess('permission.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($permissionId == null)
            {
                redirect('/setup/permission/list');
            }

            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $module = $this->security->xss_clean($this->input->post('module'));
                $id_menu = $this->security->xss_clean($this->input->post('id_menu'));
                
                $permissionInfo = array(
                    'code'=>$code,
                    'module'=>$module,
                    'id_menu'=>$id_menu,
                    'description'=>$description,
                    'status' => 1
                    );
                
                $result = $this->permission_model->editPermission($permissionInfo, $permissionId);
                                
                redirect('/setup/permission/list');
            }
            

            $data['menuList'] = $this->permission_model->menuList();
            $data['permissionInfo'] = $this->permission_model->getPermissionInfo($permissionId);

            
            // echo "<Pre>";print_r($data['permissionInfo']);exit();
            
            $this->global['pageTitle'] = 'School : Edit Permission';
            
            $this->loadViews("permission/edit", $this->global, $data, NULL);
        }
    }
    
    /**
     * This function is used to delete the permission using permissionId
     * @return boolean $result : TRUE / FALSE
     */
    function delete()
    {
        if($this->checkAccess('permission.delete') == 1)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $permissionId = $this->input->post('permissionId');
            $permissionInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->permission_model->deletePermission($permissionId, $permissionInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }

    function getSubMenuByModule()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $menu = $tempData['menu'];
        
        $results = $this->permission_model->getSubMenuByModule($menu);

        // echo "<Pre>"; print_r($results);exit;


            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_menu' id='id_menu' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->menu_name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }   
   
}

?>