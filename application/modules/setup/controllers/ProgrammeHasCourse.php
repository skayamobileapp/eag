<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProgrammeHasCourse extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('programme_has_course_model');
        $this->load->model('programme_model');
        $this->load->model('programme_landscape_model');
        $this->load->model('course_model');
        $this->load->model('semester_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('programme_has_course.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['programmeList'] = $this->programme_model->programmeList();
            $data['programmeLandscapeList'] = $this->programme_landscape_model->programmeLandscapeList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['courseList'] = $this->course_model->courseList();

            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_programme_landscape'] = $this->security->xss_clean($this->input->post('id_programme_landscape'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
 
            $data['searchParameters'] = $formData; 

                // echo "<pre>"; print_r($formData);exit();
            $data['programmeHasCourseList'] = $this->programme_has_course_model->programmeHasCourseListSearch($formData);
            $this->global['pageTitle'] = 'Campus Management System : Program Has Course List';
            $this->loadViews("programme_has_course/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('programme_has_course.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_programme_landscape = $this->security->xss_clean($this->input->post('id_programme_landscape'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_programme' => $id_programme,
					'id_programme_landscape' => $id_programme_landscape,
					'id_course' => $id_course,
					'id_semester' => $id_semester,
					'type' => $type,
                    'status' => $status
                );
                // echo "<pre>"; print_r($data);exit();
                $result = $this->programme_has_course_model->addNewProgrammeHasCourseDetails($data);
                redirect('/setup/programmeHasCourse/list');
            }
            $data['programmeList'] = $this->programme_model->programmeList();
            $data['programmeLandscapeList'] = $this->programme_landscape_model->programmeLandscapeList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['courseList'] = $this->course_model->courseList();
            //echo "<pre>";print_r($query);die;
            $this->global['pageTitle'] = 'Campus Management System : Add Program Has Couerse';
            $this->loadViews("programme_has_course/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('programme_has_course.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/programmeHasCourse/list');
            }
            if($this->input->post())
            {
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_programme_landscape = $this->security->xss_clean($this->input->post('id_programme_landscape'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_programme' => $id_programme,
					'id_programme_landscape' => $id_programme_landscape,
					'id_course' => $id_course,
					'id_semester' => $id_semester,
					'type' => $type,
                    'status' => $status
                );
                // echo "<pre>"; print_r($data);exit();

                $result = $this->programme_has_course_model->editProgrammeHasCourseDetails($data,$id);
                redirect('/setup/programmeHasCourse/list');
            }
            $data['programmeList'] = $this->programme_model->programmeList();
            $data['programmeLandscapeList'] = $this->programme_landscape_model->programmeLandscapeList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['courseList'] = $this->course_model->courseList();
            $data['programmeLandscapeDetails'] = $this->programme_has_course_model->getProgrammeHasCourseDetails($id);
            // echo "<pre>"; print_r($data['programmeLandscapeDetails']);exit();
            $this->global['pageTitle'] = 'Campus Management System : Edit Program Has Couerse';
            $this->loadViews("programme_has_course/edit", $this->global, $data, NULL);
        }
    }

    function getProgramLandscapeByProgram($id_program)
    {
            $results = $this->programme_has_course_model->getProgramLandscapeByProgramId($id_program);
            // echo "<Pre>"; print_r($results);exit;

            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_programme_landscape' id='id_programme_landscape' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }
}
