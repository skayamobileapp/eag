<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit School Details</h3>
        </div>
        <form id="form_school" action="" method="post">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name *</label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $schoolDetails->name;?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Registration No. *</label>
                        <input type="text" class="form-control" id="reg_no" name="reg_no" value="<?php echo $schoolDetails->reg_no;?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Phone No. *</label>
                        <input type="number" class="form-control" id="phone_number" name="phone_number" maxlength="10" value="<?php echo $schoolDetails->phone_number;?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 1 *</label>
                        <input type="text" class="form-control" id="address_one" name="address_one" value="<?php echo $schoolDetails->address_one;?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 2 *</label>
                        <input type="text" class="form-control" id="address_two" name="address_two" value="<?php echo $schoolDetails->address_two;?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 3</label>
                        <input type="text" class="form-control" id="address_three" name="address_three" value="<?php echo $schoolDetails->address_three;?>">
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mobile No. *</label>
                        <input type="number" class="form-control" id="mobile_number" name="mobile_number" value="<?php echo $schoolDetails->mobile_number;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>State *</label>
                        <select name="id_state" id="id_state" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countries)) {
                                foreach ($countries as $record)
                                    {?>
                             <option value="<?php echo $record->countryId;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>State *</label>
                        <input type="text" class="form-control" id="id_state" name="id_state" value="<?php echo $schoolDetails->id_state;?>">
                    </div>
                </div> -->
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City *</label>
                        <input type="text" class="form-control" id="id_city" name="id_city" value="<?php echo $schoolDetails->id_city;?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Pincode *</label>
                        <input type="number" class="form-control" id="pincode" name="pincode" value="<?php echo $schoolDetails->pincode;?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fax</label>
                        <input type="number" class="form-control" id="fax" name="fax" value="<?php echo $schoolDetails->fax;?>">
                    </div>
                </div>
                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio" value="<?php echo $schoolDetails->name;?>"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($schoolDetails->status=='1') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio" value="0" <?php if($schoolDetails->status=='0') {
                                 echo "checked=checked";
                              };?>></span> In-Active
                            </label>                              
                        </div>                         
                </div>
            </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $("#form_school").validate(
        {
            rules:
            {
                name:
                {
                    required: true
                },
                reg_no: {
                    required: true,
                    reg_no: true
                },
                phone_number: {
                    required: true,
                    phone_number: true
                },
                address_one: {
                    required: true,
                    address_one: true
                },
                address_two: {
                    required: true,
                    address_two: true
                },
                mobile_number: {
                    required: true,
                    mobile_number: true
                },
                id_state: {
                    required: true,
                    id_state: true
                },
                id_city: {
                    required: true,
                    id_city: true
                },
                pincode: {
                    required: true,
                    pincode: true
                }
            },
            messages:
            {
                name:
                {
                    required: "School Name Required",
                },
                reg_no:
                {
                    required: "Registration No. Required",
                },
                phone_number:
                {
                    required: "Phone No. Required",
                },
                address_one:
                {
                    required: "Address 1 Required",
                },
                address_two:
                {
                    required: "Address 2 Required",
                },
                mobile_number:
                {
                    required: "Mobile No. Required",
                },
                id_state:
                {
                    required: "State Required",
                },
                id_city:
                {
                    required: "City Required",
                },
                pincode:
                {
                    required: "Pincode Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>