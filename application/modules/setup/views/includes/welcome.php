<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <!-- <div class="page-title clearfix">
            <h3>Welcome : Module SETUP</h3>
        </div> -->
        
        <div class="row">
            <div class="col-sm-3">
                <div class="stats-col">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/pending_approval_icon.svg" />Application Pending Approvals
                    </div>
                    <div class="count">
                        03
                    </div>
                    <div class="text-center">
                        <a href="#">More Info</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="stats-col pending-acceptance">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/pending_acceptance_icon.svg" />Application Pending Acceptance
                    </div>
                    <div class="count">
                        00
                    </div>
                    <div class="text-center">
                        <a href="#">More Info</a>
                    </div>
                </div>
            </div>            
            <div class="col-sm-3">
                <div class="stats-col active-students">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/active_students_icon.svg" />Active Students
                    </div>
                    <div class="count">
                        295
                    </div>
                    <div class="text-center">
                        <a href="#">More Info</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="stats-col inactive-students">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/inactive_students_icon.svg" />Inactive Students
                    </div>
                    <div class="count">
                        734
                    </div>
                    <div class="text-center">
                        <a href="#">More Info</a>
                    </div>
                </div>
            </div>                                    
        </div>

        <hr/>



        <div class="row">            
            <div class="col-sm-3 col-lg-2">
                <a href="/setup/welcome" class="dashboard-menu"><span class="icon"></span>System Setup</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="/cm/welcome" class="dashboard-menu curriculm-management"><span class="icon"></span>Curriculum Management</a>
            </div>   
            <div class="col-sm-3 col-lg-2">
                <a href="/pm/welcome" class="dashboard-menu partners-management"><span class="icon"></span>Partners Management</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="/af/welcome" class="dashboard-menu academic-facilitator"><span class="icon"></span>Academic Facilitator Management</a>
            </div>  
            <div class="col-sm-3 col-lg-2">
                <a href="/facility/welcome" class="dashboard-menu facility-management"><span class="icon"></span>Facility Management</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="/cm/welcome" class="dashboard-menu class-timetable"><span class="icon"></span>Class Timetable &amp; Attendance</a>
            </div> 
            <div class="col-sm-3 col-lg-2">
                <a href="/cm/welcome" class="dashboard-menu exam-timetable"><span class="icon"></span>Exam Timetable &amp; Attendance</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="/admission/welcome" class="dashboard-menu admission"><span class="icon"></span>Admission</a>
            </div>   
            <div class="col-sm-3 col-lg-2">
                <a href="/registration/welcome" class="dashboard-menu registration"><span class="icon"></span>Registration</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="/records/welcome" class="dashboard-menu records"><span class="icon"></span>Records</a>
            </div>  
            <div class="col-sm-3 col-lg-2">
                <a href="/examination/welcome" class="dashboard-menu examination-management"><span class="icon"></span>Examination Management</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="/research/welcome" class="dashboard-menu pg-research"><span class="icon"></span>Postgraduate Research Manegement</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="/internship/welcome" class="dashboard-menu internship"><span class="icon"></span>Internship</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="/graduation/welcome" class="dashboard-menu graduation"><span class="icon"></span>Graduation</a>
            </div>   
            <div class="col-sm-3 col-lg-2">
                <a href="/hostel/welcome" class="dashboard-menu hostel"><span class="icon"></span>Hostel</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="#" class="dashboard-menu student-finance"><span class="icon"></span>Student Finance</a>
            </div>  
            <div class="col-sm-3 col-lg-2">
                <a href="#" class="dashboard-menu sponsorship"><span class="icon"></span>Sponsorship</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="#" class="dashboard-menu alumni"><span class="icon"></span>Alumni</a>
            </div>  
            <div class="col-sm-3 col-lg-2">
                <a href="#" class="dashboard-menu chatbot"><span class="icon"></span>Smart Chatbot</a>
            </div>  
            <div class="col-sm-3 col-lg-2">
                <a href="/communication/welcome" class="dashboard-menu communication-management"><span class="icon"></span>Communication Management</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="#" class="dashboard-menu reporting"><span class="icon"></span>Reporting</a>
            </div>                                                                   
        </div>

        <footer class="footer-wrapper">
            <p>&copy; 2020 All rights, reserved</p>
        </footer>

    </div>
</div>