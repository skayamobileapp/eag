<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ModeOfCategory extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mode_of_category_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('mode_of_category.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['categoryModeList'] = $this->mode_of_category_model->categoryModeList();
            $this->global['pageTitle'] = 'FIMS : List Category Mode';
            //print_r($subjectDetails);exit;
            $this->loadViews("mode_of_category/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('mode_of_category.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $category_code = $this->security->xss_clean($this->input->post('category_code'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'category_code' => $category_code,
                    'status' => $status
                );
            
                $result = $this->mode_of_category_model->addNewCategoryMode($data);
                redirect('/procurement/modeOfCategory/list');
            }
            
            $this->global['pageTitle'] = 'FIMS : Add Category Mode';
            $this->loadViews("mode_of_category/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('mode_of_category.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/procurement/mode_of_category/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $category_code = $this->security->xss_clean($this->input->post('category_code'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'category_code' => $category_code,
                    'status' => $status
                );
                
                $result = $this->mode_of_category_model->editCategoryMode($data,$id);
                redirect('/procurement/modeOfCategory/list');
            }
            $data['categoryModeDetails'] = $this->mode_of_category_model->getCategoryModeDetails($id);
            $this->global['pageTitle'] = 'FIMS : Edit Category Mode';
            $this->loadViews("mode_of_category/edit", $this->global, $data, NULL);
        }
    }

   
}
