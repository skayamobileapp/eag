<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Procurement_item_model extends CI_Model
{
    function procurementItemList()
    {
        $this->db->select('pi.*, pc.description as category_description, pc.code as category_code, psc.description as sub_category_description, psc.code as sub_category_code,');
        $this->db->from('procurement_item as pi');
        $this->db->join('procurement_category as pc', 'pi.id_procurement_category = pc.id');
        $this->db->join('procurement_sub_category as psc', 'pi.id_procurement_sub_category = psc.id');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function procurementItemListSearch($formData)
    {
        $this->db->select('pi.*, pc.description as pr_category_description, pc.code as pr_category_code, psc.description as pr_sub_category_description, psc.code as pr_sub_category_code,');
        $this->db->from('procurement_item as pi');
        $this->db->join('procurement_category as pc', 'pi.id_procurement_category = pc.id');
        $this->db->join('procurement_sub_category as psc', 'pi.id_procurement_sub_category = psc.id');
        if($formData['id_procurement_category']) {
            $likeCriteria = "(pi.id_procurement_category  LIKE '%" . $formData['id_procurement_category'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($formData['id_procurement_sub_category']) {
            $likeCriteria = "(pi.id_procurement_sub_category  LIKE '%" . $formData['id_procurement_sub_category'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($formData['name']) {
            $likeCriteria = "(pi.description  LIKE '%" . $formData['name'] . "%' or pi.code  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         // print_r($formData);exit();     
         return $result;
    }

    function getProcurementItem($id)
    {
        $this->db->select('*');
        $this->db->from('procurement_item');
        $this->db->where('id', $id);
        $query = $this->db->get();
         // print_r($query->row());exit();     
        return $query->row();
    }
    
    function addNewProcurementItem($data)
    {
        $this->db->trans_start();
        $this->db->insert('procurement_item', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProcurementItem($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('procurement_item', $data);
        return TRUE;
    }
}

