<?php $this->load->helper("form"); ?>

<form id="form_nonpo_entry" action="" method="post">

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Tender Quotation</h3>
            </div>

        <div class="form-container">
        <h4 class="form-group-title">Tender Quotation</h4>

            <div class="row">
                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Tender Quotation <span class='error-text'>*</span></label>
                            <select name='id_tender_quotation' id='id_tender_quotation' class='form-control' onChange="getDetails(this.value)">
                                <option value=''>Select</option>

                                <?php for($i=0;$i<count($tenderQuotationList);$i++)
                                {
                                    ?>
                                    <option value="<?php echo $tenderQuotationList[$i]->id;?> ">
                                    <?php echo $tenderQuotationList[$i]->quotation_number . " - " . $tenderQuotationList[$i]->description;?>  
                                    </option>
                                    <?php
                                } ?> 
                            </select>
                        </div>
                </div>
            </div>


        <div class="row">
            <div id="view"></div>
        </div>

    </div>



        <div class="form-container">
        <h4 class="form-group-title">Add Vendor For Tender Submission</h4>

            
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Vendor Name <span class='error-text'>*</span></label>
                        <select name="id_vendor" id="id_vendor" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($vendorList))
                            {
                                foreach ($vendorList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->code . " - " . $record->name; ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description </label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>
              
                <!-- <div class="col-sm-4">
                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveComiteeData()">Add</button>
                </div> -->
            </div>

        </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script>

    $(document).ready(function() {
        $("#form_nonpo_entry").validate({
            rules: {
                id_tender_quotation: {
                    required: true
                },
                 id_vendor: {
                    required: true
                }
            },
            messages: {
                id_tender_quotation: {
                    required: "<p class='error-text'>Select Tender Quotation</p>",
                },
                id_vendor: {
                    required: "<p class='error-text'>Select Vendor</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


  
    function getDetails(id) {       
            $.ajax(
            {
               url: '/procurement/tenderSubmission/getData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
               }
            });
    }
</script>
<script type="text/javascript">
    $('select').select2();
</script>