<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Research Proposal</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Topic</a> -->
    </div>



        <div class="form-container">
                <h4 class="form-group-title">Supervisor Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Supervisor Name :</dt>
                                <dd><?php echo ucwords($supervisor->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Supervisor Email :</dt>
                                <dd><?php echo $supervisor->email ?></dd>
                            </dl>                     
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Supervisor Type :</dt>
                                <dd><?php 
                                if($supervisor->type == 0)
                                {
                                    echo 'External';
                                }elseif($supervisor->type == 1)
                                {
                                    echo 'Internal';
                                } ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


        <br>




    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Student Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>

                  <!-- <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Phd Duration</label>
                      <div class="col-sm-8">
                        <select name="id_phd_duration" id="id_phd_duration" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($durationList)) {
                            foreach ($durationList as $record)
                            {
                              
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php
                                if ($record->id == $searchParam['id_phd_duration'])
                                {
                                  echo 'selected';
                                } ?>
                                >
                                <?php echo  $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div> -->

                </div>

                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Status</label>
                      <div class="col-sm-8">
                        <select name="status" id="status" class="form-control">
                          <option value="">-- All --</option>
                          <option value="0"
                          <?php
                            if ('0' == $searchParam['status'])
                            {
                              echo 'selected';
                            } ?>
                            >Pending</option>
                          <option value="1"
                          <?php
                            if ('1' == $searchParam['status'])
                            {
                              echo 'selected';
                            } ?>
                            >Approved</option>
                          <option value="2"
                          <?php
                            if ('2' == $searchParam['status'])
                            {
                              echo 'selected';
                            } ?>
                            >Rejected</option>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>


              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Application Number</th>
            <th>Student Name</th>
            <th>Student Email</th>
            <!-- <th>Duration</th>
            <th>Chapter</th> -->
            <th>Topic</th>
            <th>Submitted On</th>
            <!-- <th>Approved / Rejected On</th> -->
            <th>Status</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($deliverablesList)) {
            $i=1;
            foreach ($deliverablesList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->application_number ?></td>
                <td><?php echo $record->student_name ?></td>
                <td><?php echo $record->email_id ?></td>
                <!-- <td><?php echo $record->duration; ?></td>
                <td><?php echo $record->chapter ?></td> -->
                <td><?php echo $record->topic ?></td>
                <td><?php echo date('d-m-Y',strtotime($record->created_dt_tm)) ?></td>
                <!-- <td>
                <?php 
                if($record->approved_on)
                {
                    echo date('d-m-Y',strtotime($record->approved_on)); 
                }
                ?>
                </td> -->
                <td><?php
                if( $record->status == 0)
                {
                  echo "Pending";
                }
                else
                {
                  echo $record->status_code;
                }
                // if( $record->status == 0)
                // {
                //   echo "Pending";
                // }
                // elseif( $record->status == 1)
                // {
                //   echo "Approved";
                // }
                // elseif( $record->status == 2)
                // {
                //   echo "Rejected";
                // }
                ?></td>
                <td class="text-center">
                    <?php if( $record->status == 0)
                    {
                        ?>
                        <a href="<?php echo 'edit/' . $record->id; ?>" title="Approve | Preview">Approve</a>
                    <?php
                    }else
                    {
                        ?>
                        <a href="<?php echo 'view/' . $record->id; ?>" title="Preview">View</a>
                        <?php
                    }
                    ?>
                </td>
            </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script>
    $('select').select2();
</script>