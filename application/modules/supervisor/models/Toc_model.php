<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Toc_model extends CI_Model
{
    function getSupervisor($id)
    {
        $this->db->select('s.*');
        $this->db->from('research_supervisor as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudent($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
         return $query->row();
    }

    function getTocListBySupervisorId($id_supervisor)
    {
        $this->db->select('rd.*, rs.full_name as student_name, rs.nric');
        $this->db->from('research_toc as rd');
        $this->db->join('student as rs','rd.id_student = rs.id');
        $this->db->where('rd.id_supervisor', $id_supervisor);
        // if ($stage != '')
        // {
        //     $this->db->where('rd.stage', $stage);
        // }
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    
    function getOrganisation()
    {
        $this->db->select('*');
        $this->db->from('organisation');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.code as programme_code, p.name as programme_name, i.name as intake_name, st.ic_no, st.name as advisor_name, ms.name as mailing_state, mc.name as mailing_country, ps.name as permanent_state, pc.name as permanent_country, rs.name as race, rels.name as religion, brch.code as branch_code, brch.name as branch_name, salut.name as salutation, pu.code as partner_university_code, pu.name as partner_university_name, sch.code as scheme_code, sch.description as scheme_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('organisation_has_training_center as brch', 's.id_branch = brch.id','left'); 
        $this->db->join('salutation_setup as salut', 's.salutation = salut.id','left'); 
        $this->db->join('partner_university as pu', 's.id_university = pu.id','left'); 
        $this->db->join('scheme as sch', 's.id_program_has_scheme = sch.id','left'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('state as ms', 's.mailing_state = ms.id'); 
        $this->db->join('country as mc', 's.mailing_country = mc.id');
        $this->db->join('state as ps', 's.permanent_state = ps.id'); 
        $this->db->join('country as pc', 's.permanent_country = pc.id'); 
        $this->db->join('race_setup as rs', 's.id_race = rs.id'); 
        $this->db->join('religion_setup as rels', 's.religion = rels.id','left');
        $this->db->join('staff as st', 's.id_advisor = st.id','left'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function addToc($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_toc', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getToc($id)
    {
        $this->db->select('ia.*');
        $this->db->from('research_toc as ia');
         $this->db->where('ia.id', $id);
        $query = $this->db->get();
          $result = $query->row();
         return $result; 
    }

    function tocCommentsDetails($id_toc)
    {
        $this->db->select('rd.*');
        $this->db->from('toc_reporting_comments as rd');
        $this->db->where('rd.id_toc', $id_toc);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function addTocReportingComments($data)
    {
        $this->db->trans_start();
        $this->db->insert('toc_reporting_comments', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;

    }
}