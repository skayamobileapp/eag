<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Asset_category_model extends CI_Model
{
    function assetCategoryList()
    {
        $this->db->select('*');
        $this->db->from('asset_category');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function assetCategoryListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('asset_category');
        if (!empty($search))
        {
            $likeCriteria = "(code  LIKE '%" . $search . "%' or description  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getAssetCategoryDetails($id)
    {
        $this->db->select('*');
        $this->db->from('asset_category');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewAssetCategory($data)
    {
        $this->db->trans_start();
        $this->db->insert('asset_category', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editAssetCategory($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('asset_category', $data);
        return TRUE;
    }
}

