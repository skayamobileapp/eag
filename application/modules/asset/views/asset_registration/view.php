<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Asset Registration </h3>
            </div>

        <div class="form-container">
            <h4 class="form-group-title">Asset Registration</h4>


            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Category <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $assetRegistration->category_code . ' - ' . $assetRegistration->category_name;?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Sub <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $assetRegistration->sub_category_code . ' - ' . $assetRegistration->sub_category_name;?>" readonly="readonly">
                    </div>
                </div>

               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Item <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $assetRegistration->item_code . ' - ' . $assetRegistration->item_name;?>" readonly="readonly" >
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $assetRegistration->code;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $assetRegistration->name;?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Brand <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="brand" name="brand" value="<?php echo $assetRegistration->brand;?>" readonly>
                    </div>
                </div>

            </div>

            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="company" name="company" value="<?php echo $assetRegistration->company;?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Price <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="price" name="price" value="<?php echo $assetRegistration->price;?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Tax <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="tax" name="tax" value="<?php echo $assetRegistration->tax; ?>" readonly>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Depreciation Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="depriciation_code" name="depriciation_code" value="<?php echo $assetRegistration->depriciation_code;?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Depreciation Value <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="depriciation_value" name="depriciation_value" value="<?php echo $assetRegistration->depriciation_value;?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="balance_qty" name="balance_qty" value="<?php 

                        if($assetRegistration->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($assetRegistration->status == '1')
                        {
                            echo "Approved";
                        } 
                        elseif($assetRegistration->status == '2')
                        {
                            echo "Rejected";
                        } 
                        elseif($assetRegistration->status == '3')
                        {
                            echo "Added For Disposal";
                        } 
                        elseif($assetRegistration->status == '4')
                        {
                            echo "Disosed Asset";
                        } 

                        ?>" readonly="readonly">
                    </div>
                </div>

            </div>

        </div>


            <hr>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script>

    $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                name: {
                    required: true
                },
                 brand: {
                    required: true
                },
                 company: {
                    required: true
                },
                 price: {
                    required: true
                },
                 tax: {
                    required: true
                },
                depriciation_code: {
                    required: true
                },
                depriciation_value: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Enter Name",
                },
                brand: {
                    required: "<p class='error-text'>Enter Brand",
                },
                company: {
                    required: "<p class='error-text'>Enter Company</p>",
                },
                price: {
                    required: "<p class='error-text'>Enter Price",
                },
                tax: {
                    required: "<p class='error-text'>Enter Tax",
                },
                depriciation_code: {
                    required: "<p class='error-text'>Enter Depriciation Code</p>",
                },
                depriciation_value: {
                    required: "<p class='error-text'>Enter Depriciation Value</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>