<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Graduation</h3>
            </div>

          <div class="form-container">
            <h4 class="form-group-title">Graduation Detail</h4>


        

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Convocation <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $graduation->convocation_session;?>" readonly="readonly">
                    </div>
                </div>

               
                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Message <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $graduation->message;?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Convocation Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo date('d-m-Y',strtotime($graduation->message));?>" readonly="readonly">
                    </div>
                </div>

            </div>


            <div class="row">
                
               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($graduation->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($graduation->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($graduation->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>

            <?php
            if($graduation->status == '2')
            {
             ?>


                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $graduation->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>

                
            </div>

          </div>


          <h3>Student Selected For Graduation</h3>

        <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                         <th>Student Name</th>
                                         <th>Student NRIC</th>
                                         <th>Student Email</th>
                                         <th>Student Phone</th>
                                         <th>Program</th>
                                        <th>Intake</th>
                                        <th>Qualification Type</th>
                                        <th>Program Scheme</th>
                                        <th>Branch</th>
                                        <th>Total Credit Hours</th>
                                        <th>Completed Credit Hours</th>
                                        <th>Financial Status</th>
                                         <th class="text-center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($graduationDetails);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td><?php echo $graduationDetails[$i]->full_name;?></td>
                                        <td><?php echo $graduationDetails[$i]->nric;?></td>
                                        <td><?php echo $graduationDetails[$i]->email_id;?></td>
                                        <td><?php echo $graduationDetails[$i]->phone;?></td>
                                        <td><?php echo $graduationDetails[$i]->program_code . " - " . $graduationDetails[$i]->program_name; ?></td>
                                        <td><?php echo $graduationDetails[$i]->intake_name;?></td>
                                        <td><?php echo $graduationDetails[$i]->qualification_code . " - " . $graduationDetails[$i]->qualification_name; ?></td>
                                        <td><?php echo $graduationDetails[$i]->program_scheme;?></td>
                                        <td><?php
                                        if($graduationDetails[$i]->id_branch == 1)
                                        {
                                            echo $organisation->name . " - " . $organisation->short_name;   
                                        }
                                        else
                                        {
                                            echo $graduationDetails[$i]->branch_code . " - " . $graduationDetails[$i]->branch_name;
                                        }
                                        ?></td>
                                        <td><?php echo $graduationDetails[$i]->course_credit_hours;?></td>
                                        <td><?php echo $graduationDetails[$i]->cleared_credit_hours;?></td>
                                        <td><?php echo $graduationDetails[$i]->is_invoice_pending;?></td>
                                        <td class="text-center"><?php if( $graduationDetails[$i]->status == '1')
                                        {
                                          echo "Approved";
                                        }
                                        else if( $graduationDetails[$i]->status == '0')
                                        {
                                           echo "Pending";
                                        }
                                        else if( $graduationDetails[$i]->status == '2')
                                        {
                                          echo "Rejected";
                                        } 
                                        ?></td>
                                         </tr>
                                      <?php 
                                  } 
                                  ?>

                                    </tbody>
                                </table>
                              </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2020"
    });
  } );
</script>