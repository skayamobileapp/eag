<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Graduation Student List</h3>
        </div>






        <div class="form-container">
            <h4 class="form-group-title"> Graduation Students</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Tag Student Details</a>
                    </li>    
                    <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Tagged Students List</a>
                    </li>                
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">



                          



                            <br>


                    <div class="form-container">
            <h4 class="form-group-title">Graduation Detail</h4>


        

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Convocation <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $graduation->convocation_session;?>" readonly="readonly">
                    </div>
                </div>

               
                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Message <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $graduation->message;?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Convocation Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo date('d-m-Y',strtotime($graduation->message));?>" readonly="readonly">
                    </div>
                </div>

            </div>


            <div class="row">
                
               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($graduation->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($graduation->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($graduation->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>

            <?php
            if($graduation->status == '2')
            {
             ?>


                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $graduation->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>

                
            </div>

          </div>





                        <br>


                        <form id="form_comitee" action="" method="post">

                            <div class="form-container">
                            <h4 class="form-group-title">Graduation Details</h4>

                            <div class="row">

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Program <span class='error-text'>*</span></label>
                                        <select name="id_program" id="id_program" class="form-control" >
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($programList))
                                            {
                                                foreach ($programList as $record)
                                                {?>
                                             <option value="<?php echo $record->id;  ?>">
                                                <?php echo $record->code . " - " . $record->name;?>
                                             </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>  


                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Intake <span class='error-text'>*</span></label>
                                        <select name="id_intake" id="id_intake" class="form-control" >
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($intakeList))
                                            {
                                                foreach ($intakeList as $record)
                                                {?>
                                             <option value="<?php echo $record->id;  ?>">
                                                <?php echo $record->year . " - " . $record->name;?>
                                             </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>  

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Qualification Level <span class='error-text'>*</span></label>
                                        <select name="id_qualification" id="id_qualification" class="form-control" >
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($qualificationList))
                                            {
                                                foreach ($qualificationList as $record)
                                                {?>
                                             <option value="<?php echo $record->id;  ?>">
                                                <?php echo $record->code . " - " . $record->name;?>
                                             </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>  


                                 <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Student NRIC</label>
                                        <input type="text" class="form-control" id="nric" name="nric">
                                    </div>
                               </div>




                                  
                                    <!-- <div class="col-sm-4">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="searchStudent()">Search</button>
                                    </div> -->

                                </div>

                                <br>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" onclick="searchStudent()">Search</button>
                                    </div>

                            </div>







                        <div class="form-container" id="display_course_details" style="display: none">
                            <h4 class="form-group-title">Course Registration Student Details</h4>

                            <div class="row">

                                <div id="view_course_details">
                                </div>

                            </div>

                        </div>


                        <div class="button-block clearfix">
                            <div class="bttn-group">
                                <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                                <a href="../list" class="btn btn-link">Back</a>
                            </div>
                        </div>


                        </form>


                    


                        </div> 
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="col-12">



                        <br>


                        <?php

                    if(!empty($graduationDetails))
                    {
                        ?>

                        <div class="form-container">
                                <h4 class="form-group-title">Student List For Graduation</h4>
                        




                              <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                         <th>Student Name</th>
                                         <th>Student NRIC</th>
                                         <th>Student Email</th>
                                         <th>Student Phone</th>
                                         <th>Program</th>
                                        <th>Intake</th>
                                        <th>Qualification Type</th>
                                        <th>Program Scheme</th>
                                        <th>Branch</th>
                                        <th>Total Credit Hours</th>
                                        <th>Completed Credit Hours</th>
                                        <th>Financial Status</th>
                                         <th class="text-center">Status</th>
                                         <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($graduationDetails);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td><?php echo $graduationDetails[$i]->full_name;?></td>
                                        <td><?php echo $graduationDetails[$i]->nric;?></td>
                                        <td><?php echo $graduationDetails[$i]->email_id;?></td>
                                        <td><?php echo $graduationDetails[$i]->phone;?></td>
                                        <td><?php echo $graduationDetails[$i]->program_code . " - " . $graduationDetails[$i]->program_name; ?></td>
                                        <td><?php echo $graduationDetails[$i]->intake_name;?></td>
                                        <td><?php echo $graduationDetails[$i]->qualification_code . " - " . $graduationDetails[$i]->qualification_name; ?></td>
                                        <td><?php echo $graduationDetails[$i]->program_scheme;?></td>
                                        <td><?php
                                        if($graduationDetails[$i]->id_branch == 1)
                                        {
                                            echo $organisation->name . " - " . $organisation->short_name;   
                                        }
                                        else
                                        {
                                            echo $graduationDetails[$i]->branch_code . " - " . $graduationDetails[$i]->branch_name;
                                        }
                                        ?></td>
                                        <td><?php echo $graduationDetails[$i]->course_credit_hours;?></td>
                                        <td><?php echo $graduationDetails[$i]->cleared_credit_hours;?></td>
                                        <td><?php echo $graduationDetails[$i]->is_invoice_pending;?></td>
                                        <td class="text-center"><?php if( $graduationDetails[$i]->status == '1')
                                        {
                                          echo "Approved";
                                        }
                                        else if( $graduationDetails[$i]->status == '0')
                                        {
                                           echo "Pending";
                                        }
                                        else if( $graduationDetails[$i]->status == '2')
                                        {
                                          echo "Rejected";
                                        } 
                                        ?></td>

                                        <td class="text-center">
                                        <?php
                                        if( $graduationDetails[$i]->status == '0')
                                        {
                                            ?>

                                        <a onclick="deleteGraduationTaging(<?php echo $graduationDetails[$i]->id_graduation_detail; ?>)" title="Delete">Delete</a>

                                        <?php
                                        }
                                        ?>

                                        </td>

                                         </tr>
                                      <?php 
                                  } 
                                  ?>

                                    </tbody>
                                </table>
                              </div>



                            </div>

                    <?php
                    
                    }
                     ?>


                        </div> 
                    </div>


                </div>

            </div>
        </div> 





        





     



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>


    function searchStudent()
    {
        var tempPR = {};
        tempPR['id_program'] = $("#id_program").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_qualification'] = $("#id_qualification").val();
        tempPR['nric'] = $("#nric").val();
            $.ajax(
            {
               url: '/graduation/graduationList/searchStudent',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#display_course_details").show();
                $("#view_course_details").html(result);
                // $('#myModal').modal('hide');
                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#amount").val(ta);
               }
            });
    }


    function deleteGraduationTaging(id) {
      // alert(id);
         $.ajax(
            {
               url: '/graduation/graduationList/deleteGraduationTaging/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                alert('Deleted Successfully');
                window.location.reload();
                    // $("#view").html(result);
               }
            });
    }



    // $(document).ready(function() {
    //     $("#form_comitee").validate({
    //         rules: {
    //             id_program: {
    //                 required: true
    //             },
    //             id_intake: {
    //                 required: true
    //             },
    //             id_course_registered: {
    //                 required: true
    //             }
    //         },
    //         messages: {
    //             id_program: {
    //                 required: "<p class='error-text'>Select Program</p>",
    //             },
    //             id_intake: {
    //                 required: "<p class='error-text'>Select Intake</p>",
    //             },
    //             id_course_registered: {
    //                 required: "<p class='error-text'>Select Course</p>",
    //             }
    //         },
    //         errorElement: "span",
    //         errorPlacement: function(error, element) {
    //             error.appendTo(element.parent());
    //         }

    //     });
    // });
    


$( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2001"
    });
  } );

    $('select').select2();

</script>