<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Colloquium</h3>
        </div>
        <form id="form_unit" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Colloquium Details</h4>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $colloquium->name; ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $colloquium->description; ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>URL <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="url" name="url" value="<?php echo $colloquium->url; ?>">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" autocomplete="off" value="<?php if($colloquium->date_time){ echo date('d-m-Y', strtotime($colloquium->date_time)); } ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="to_date" name="to_date" autocomplete="off" value="<?php if($colloquium->to_date){ echo date('d-m-Y', strtotime($colloquium->to_date)); } ?>">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Mode <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="mode" id="mode" value="1" <?php if($colloquium->mode=='1') {
                             echo "checked=checked";
                          };?>><span class="check-radio"></span> Face To Face
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="mode" id="mode" value="0" <?php if($colloquium->mode=='0') {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio"></span> Online
                        </label>
                    </div>
                </div>



            </div>

            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Attendance <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="attendance" id="attendance" value="1" <?php if($colloquium->attendance=='1') {
                             echo "checked=checked";
                          };?>><span class="check-radio"></span> Compulsary
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="attendance" id="attendance" value="0" <?php if($colloquium->attendance=='0') {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio"></span> Optional
                        </label>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="registration_start_date" name="registration_start_date" autocomplete="off" value="<?php if($colloquium->registration_start_date){ echo date('d-m-Y', strtotime($colloquium->registration_start_date)); } ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="registration_end_date" name="registration_end_date" autocomplete="off" value="<?php if($colloquium->registration_end_date){ echo date('d-m-Y', strtotime($colloquium->registration_end_date)); } ?>">
                    </div>
                </div>


            </div>

            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" <?php if($colloquium->status=='1') {
                             echo "checked=checked";
                          };?>><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0" <?php if($colloquium->status=='0') {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio"></span> In-Active
                        </label>
                    </div>
                </div>

            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>


        </form>



        <br>



        <form id="form_details" action="" method="post">
            <div class="form-container">
            <h4 class="form-group-title">Research Colloquium Details</h4>

                <div class="row">




                    <div class="col-sm-4">
                      <div class="form-group">
                         <label>Programme <span class='error-text'>*</span></label>
                         <select name="id_programme" id="id_programme" class="form-control" onchange="getIntakeByProgramId(this.value)">
                            <option value="">Select</option>
                            <?php
                               if (!empty($programList))
                               {
                                 foreach ($programList as $record)
                                 {
                                        ?>
                            <option value="<?php echo $record->id;  ?>">
                               <?php echo $record->code . " - " . $record->name;  ?>
                            </option>
                            <?php
                                }
                              }
                               ?>
                         </select>
                      </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Intake <span class='error-text'>*</span></label>
                            <span id="view_intake">
                              <select class="form-control" id='id_intake' name='id_intake'>
                                <option value=''></option>
                              </select>
                            </span>
                        </div>
                    </div>






                  
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                    </div>
                </div>





            </div>



                <?php
                if(!empty($getColloquiumDetailsByColloquiumId))
                {
                ?>

                    <div class="form-container">
                            <h4 class="form-group-title">Committee Details</h4>

                        

                          <div class="custom-table">
                            <table class="table">
                                <thead>
                                    <tr>
                                    <th>Sl. No</th>
                                     <th>Programme</th>
                                     <th>Intake</th>
                                     <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php
                                 $total = 0;
                                  for($i=0;$i<count($getColloquiumDetailsByColloquiumId);$i++)
                                 { ?>
                                    <tr>
                                    <td><?php echo $i+1;?></td>
                                    
                                    <td><?php echo $getColloquiumDetailsByColloquiumId[$i]->programme_code ." - " .  $getColloquiumDetailsByColloquiumId[$i]->programme_name;?></td>

                                    <td><?php echo $getColloquiumDetailsByColloquiumId[$i]->intake_year . " - " .  $getColloquiumDetailsByColloquiumId[$i]->intake_name;?></td>
                                    <td>
                                    <a onclick="deleteColloquiumDetails(<?php echo $getColloquiumDetailsByColloquiumId[$i]->id; ?>)">Delete</a>
                                    </td>

                                     </tr>
                                  <?php 
                              } 
                              ?>
                                </tbody>
                            </table>
                          </div>

                        </div>

                <?php
                }
                ?>


        </form>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();



    function getIntakeByProgramId(id_programme)
    {
        if(id_programme != '')
        {

            $.get("/research/colloquium/getIntakeByProgramId/"+id_programme, function(data, status){
           
                $("#view_intake").html(data);
                $("#view_intake").show();
            });
        }
    }



    function saveData()
    {
        if($('#form_details').valid())
        {

        var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_colloquium'] = <?php echo $colloquium->id ?>;

            $.ajax(
            {
               url: '/research/colloquium/addColloquiumDetails',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                window.location.reload();
               }
            }); 
        }
    }


    function deleteColloquiumDetails(id)
    {
        if(id != '')
        {
            $.get("/research/colloquium/deleteColloquiumDetails/"+id, function(data, status)
            {
                window.location.reload();
            });
        }
    }


    $(document).ready(function() {
        $("#form_details").validate({
            rules: {
                id_programme: {
                    required: true
                },
                id_intake: {
                    required: true
                }
            },
            messages: {
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(document).ready(function() {
        $("#form_unit").validate({
            rules: {
                name: {
                    required: true
                },
                url: {
                    required: true
                },
                date_time: {
                    required: true
                },
                to_date: {
                    required: true
                },
                registration_start_date: {
                    required: true
                },
                registration_end_date: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                url: {
                    required: "<p class='error-text'>URL Required</p>",
                },
                date_time: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                to_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                registration_start_date: {
                    required: "<p class='error-text'>Select Registration Start Date</p>",
                },
                registration_end_date: {
                    required: "<p class='error-text'>Select Registration End Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

     $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "2019:2020"
    });
  } );
</script>
