<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Academic Advisor</h3>
        </div>
        <form id="form_unit" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Advisor Details</h4>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type <span class='error-text'>*</span></label>
                        <select class="form-control" id="type" name="type" onchange="showType(this.value)">
                            <option value="">Select</option>
                            <option value="1">Internal</option>
                            <option value="0">External</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Appointment Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Appointment End Date</label>
                        <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off">
                    </div>
                </div>


            </div>


            <div class="row">

                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Specialisation <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_specialisation" name="id_specialisation">
                            <option value="">Select</option>
                            <?php
                            if (!empty($specialisationList))
                            {
                                foreach ($specialisationList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->

                


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Email <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="email" name="email">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Password <span class='error-text'>*</span></label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                </div>

                <div class="col-sm-4" id="view_internal_staff" style="display: none;">
                    <div class="form-group">
                        <label>Staff <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_staff" name="id_staff">
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList))
                            {
                                foreach ($staffList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->ic_no . " - " . $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                
                
            </div>

            <div class="row" id="view_external_staff" style="display: none;">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Salutation <span class='error-text'>*</span></label>
                        <select class="form-control" id="salutation" name="salutation">
                            <option value="">Select</option>
                            <?php
                            if (!empty($salutationList))
                            {
                                foreach ($salutationList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>First name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="first_name" name="first_name">
                    </div>
                </div>

                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Last name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="last_name" name="last_name">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Adress <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address" name="address">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact No. <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="contact_no" name="contact_no">
                    </div>
                </div>

                


            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>







    <div class="form-container">
            <h4 class="form-group-title">Specialisation Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Specialisation Details</a>
                    </li>
                   <!--  <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Examiner Details</a>
                    </li> -->

                      
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <form id="form_supervisor" action="" method="post">

                        <div class="form-container">
                        <h4 class="form-group-title">Specialisation Details</h4>

                            <div class="row">


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Specialisation <span class='error-text'>*</span></label>
                                        <select class="form-control" id="id_specialisation" name="id_specialisation">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($specialisationList))
                                            {
                                                foreach ($specialisationList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php echo $record->name;?>
                                                 </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                            </div>


                            <div class="row">
                              
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="tempAddSpecialisationToSupervisor()">Add</button>
                                </div>
                            </div>

                        </div>


                    </form>


                    <div class="row">
                        <div id="view_specialisation"></div>
                    </div>


                        </div> 
                    </div>



                </div>

            </div>
        

    </div>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $('select').select2();

    function showType(type)
    {
        if(type == 0)
        {
            $("#view_external_staff").show();
            $("#view_internal_staff").hide();

        }else
        {
            $("#view_internal_staff").show();
            $("#view_external_staff").hide();
        }
    }


    function tempAddSpecialisationToSupervisor()
    {
        if($('#form_supervisor').valid())
        {

        var tempPR = {};
        tempPR['id_specialisation'] = $("#id_specialisation").val();
        tempPR['status'] = 1;

            $.ajax(
            {
               url: '/research/advisor/tempAddSpecialisationToSupervisor',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_specialisation").html(result);
               }
            });
        }
    }

    function deleteTempSpecialisationSupervisor(id) {
        // alert(id);
         $.ajax(
            {
               url: '/research/advisor/deleteTempSpecialisationSupervisor/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_specialisation").html(result);
               }
            });
    }


    $(document).ready(function() {
        $("#form_supervisor").validate({
            rules: {
                id_specialisation: {
                    required: true
                }
            },
            messages: {
                id_specialisation: {
                    required: "<p class='error-text'>Select Specialisation</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function() {
        $("#form_unit").validate({
            rules: {
                type: {
                    required: true
                },
                first_name: {
                    required: true
                },
                email: {
                    required: true
                },
                password: {
                    required: true
                },
                contact_no: {
                    required: true
                },
                id_staff: {
                    required: true
                },
                salutation: {
                    required: true
                },
                start_date: {
                    required: true
                },
                last_name: {
                    required: true
                },
                id_specialisation: {
                    required: true
                }
            },
            messages: {
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name Required</p>",
                },
                email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password Required</p>",
                },
                contact_no: {
                    required: "<p class='error-text'>Contact No. Required</p>",
                },
                id_staff: {
                    required: "<p class='error-text'>Select Staff</p>",
                },
                salutation: {
                    required: "<p class='error-text'>Select Salutation</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name Required</p>",
                },
                id_specialisation: {
                    required: "<p class='error-text'>Select Specialisation</p>",
                }

            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "2019:2020"
    });
  } );

</script>
