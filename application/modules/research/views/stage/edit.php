<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Stage</h3>
        </div>
        <form id="form_award" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Stage Details</h4>  

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $stage->name; ?>">
                        </div>
                    </div>

                   

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Description <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="description" name="description" value="<?php echo $stage->description; ?>">
                        </div>
                    </div>

                

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Type <span class='error-text'>*</span></label>
                            <select name="type" id="type" class="form-control">
                                <option value="">Select</option>
                                <option value="Part Time"
                                <?php
                                if('Part Time' == $stage->type)
                                {
                                    echo "selected";
                                } ?>
                                >Part Time</option>
                                <option value="Full Time"
                                <?php
                                if('Full Time' == $stage->type)
                                {
                                    echo "selected";
                                } ?>
                                >Full Time</option>
                            </select>
                        </div>
                    </div>



                </div>




                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                            <input type="radio" name="status" id="status" value="1" <?php if($stage->status=='1') {
                                echo "checked=checked";
                            };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                            <input type="radio" name="status" id="status" value="0" <?php if($stage->status=='0') {
                                echo "checked=checked";
                            };?>>
                            <span class="check-radio"></span> In-Active
                            </label>
                        </div>
                    </div>
                
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();
    
    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                name: {
                    required: true
                },
                type: {
                    required: true
                },
                description: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                description: {
                    required: "<p class='error-text'>Description required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>