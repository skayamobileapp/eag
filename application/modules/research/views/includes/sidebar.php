<?php
$roleModel = new Role_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];

$roleList  = $roleModel->getSideMenuListByModule($urlmodule);
// echo "<Pre>";print_r($roleList);exit();
$asdf1class = "collapse";
$asdf1topclass  = "collapsed";
if($urlcontroller=='fieldOfInterest' || 
   $urlcontroller=='researchCategory' || 
   $urlcontroller=='topic' || 
   $urlcontroller=='reasonApplying' || 
   $urlcontroller=='status' || 
   $urlcontroller=='mileStone' ||
   $urlcontroller=='mileStoneToSemester' ) {
    $asdf1topclass="";
$asdf1class="";
}


$asdf2class = "collapse";
$asdf2topclass  = "collapsed";
if($urlcontroller=='advisory' || 
   $urlcontroller=='comitee' || 
   $urlcontroller=='chapter' || 
   $urlcontroller=='submittedDeliverables' || 
   $urlcontroller=='deliverables' || 
   $urlcontroller=='phdDuration'  ) {
    $asdf2topclass="";
$asdf2class="";
}

   

$asdf3class = "collapse";
$asdf3topclass  = "collapsed";
if($urlcontroller=='supervisorRole' || 
   $urlcontroller=='supervisor' || 
   $urlcontroller=='supervisorChangeApplication' || 
   $urlcontroller=='supervisorTagging' ) {
    $asdf3topclass="";
$asdf3class="";
}


$asdf4class = "collapse";
$asdf4topclass  = "collapsed";
if($urlcontroller=='examinerRole' || 
   $urlcontroller=='examiner'  ) {
    $asdf4topclass="";
$asdf4class="";
}


$asdf5class = "collapse";
$asdf5topclass  = "collapsed";
if($urlcontroller=='applicant' || 
    $urlcontroller=='applicantApproval' || 
   $urlcontroller=='studentRecord'  ) {
    $asdf5topclass="";
$asdf5class="";
}



$asdf6class = "collapse";
$asdf6topclass  = "collapsed";
if($urlcontroller=='advisorTagging' ||
$urlcontroller=='studentSemester' ||
$urlcontroller=='courseRegistration' ||                       
$urlcontroller=='courseRegistration' ||
$urlcontroller=='colloquium' ||
$urlcontroller=='colloquium' ||
$urlcontroller=='submittedDeliverables' ||
$urlcontroller=='proposalReporting' ||
$urlcontroller=='comitee' ||
$urlcontroller=='comitee' ||
$urlcontroller=='proposal') {
    $asdf6topclass="";
$asdf6class="";
}



$asdf7class = "collapse";
$asdf7topclass  = "collapsed";
if($urlcontroller=='proposalReporting'  ) {
    $asdf7topclass="";
$asdf7class="";
}



$asdf8class = "collapse";
$asdf8topclass  = "collapsed";
if($urlcontroller=='proposalReporting' ) {
    $asdf8topclass="";
$asdf8class="";
}

?>


<div class="sidebar">
    <div class="user-profile clearfix">
        <a href="#" class="user-profile-link">
            <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $name; ?>
        </a>
        <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            </button>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                <li><a href="/sponser/user/profile">Edit Profile</a></li>
                <li><a href="/setup/user/logout">Logout</a></li>
            </ul>
        </div>
    </div>

 <div class="sidebar-nav">   
                                               
                        <h4><a role="button" data-toggle="collapse" class="<?php echo $asdf1topclass;?>" href="#asdf1"> Setup <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                        <ul class="<?php echo $asdf1class;?>" id="asdf1">

                                                    
                           <li><a href="/research/fieldOfInterest/list">Field Of Interest</a></li>
                            <li><a href="/research/researchCategory/list">Research Category</a></li>
                            <li><a href="/research/topic/list">Topics</a></li>
                            <li><a href="/research/reasonApplying/list">Reason For Applying</a></li>
                            <li><a href="/research/status/list">Research Status Setup</a></li>
                            <li><a href="/research/mileStone/list">Milestone Setup</a></li>
                            <li><a href="/research/mileStoneToSemester/list">Tag Milestone To Semester</a></li>

                        </ul>
                    
                                                 
                        <h4><a role="button" data-toggle="collapse" class="<?php echo $asdf2topclass;?>" href="#asdf2">Research Setup <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                        <ul class="<?php echo $asdf2class;?>" id="asdf2">

                                                    
                                       <li><a href="/research/advisory/list">Advisory</a></li>
            <li><a href="/research/readers/list">Readers</a></li>
            <li><a href="/research/comitee/list">Proposal Defense Comitee</a></li>
            <li><a href="/research/chapter/list">Chapter</a></li>
            <li><a href="/research/deliverables/list">Deliverables</a></li>
            <!-- <li><a href="/research/submittedDeliverables/list">Submitted Deliverables</a></li> -->
            <!-- <li><a href="/research/phdDuration/list">Phd Duration</a></li> -->
            <li><a href="/research/studentMilestone/add">Phd Duration Tagging</a></li>


                                                    </ul>


  <h4><a role="button" data-toggle="collapse" class="<?php echo $asdf3topclass;?>" href="#asdf3">Supervisor <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                        <ul class="<?php echo $asdf3class;?>" id="asdf3">

                                                    
                                    <li><a href="/research/supervisorRole/list">Supervisor Role</a></li>
            <li><a href="/research/supervisor/list">Add Supervisor</a></li>
            <li><a href="/research/supervisorChangeApplication/list">Supervisor Change Application</a></li>
            <li><a href="/research/supervisorTagging/add">Tag Students to Supervisor</a></li>


                                                    </ul>
<h4><a role="button" data-toggle="collapse" class="<?php echo $asdf4topclass;?>" href="#asdf4">Examiner <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                        <ul class="<?php echo $asdf4class;?>" id="asdf4">

                                                    
            <li><a href="/research/examinerRole/list">Examiner Role</a></li>
            <li><a href="/research/examiner/list">Add Examiner</a></li>


                                                    </ul>
                    
                        

<h4><a role="button" data-toggle="collapse" class="<?php echo $asdf5topclass;?>" href="#asdf5">Postgraduate Admission <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                        <ul class="<?php echo $asdf5class;?>" id="asdf5">
                                    <li><a href="/research/applicant/list">Application listing by programme structure</a></li>
            <li><a href="/research/applicantApproval/list">Offer and Acceptance Status</a></li>
            <li><a href="/research/studentRecord/list">Student Records</a></li>
  </ul>


   <h4><a role="button" data-toggle="collapse" class="<?php echo $asdf6topclass;?>" href="#asdf6">Stage 1 <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                        <ul class="<?php echo $asdf6class;?>" id="asdf6">

            <li><a href="/research/advisorTagging/add">Tag Students to Academic Advisor</a></li>
            <li><a href="/research/studentSemester/add">Student Semester Promotion</a></li>
            <!-- <li><a href="/research/studentMilestone/add">Phd Duration Tagging</a></li> -->
            <li><a href="/research/courseRegistration/list">Registration List & Status</a>
                 <h4><a role="button" data-toggle="collapse" class="" href="#asdf61">Audit Course Registration <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>

              
                    <ul class="" id="asdf61">
                        <li><a href="/research/courseRegistration/list">Registration</a></li>
                        <li><a href="/research/courseRegistration/addAttendance">Attendance</a></li>
                    </ul>
                </li>
             <li><h4><a role="button" data-toggle="collapse" class="" href="#asdf62">Colloquium Management Registration <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>

            <ul class="" id="asdf62">
                <li><a href="/research/colloquium/list">Colloquium List</a></li>
                <li><a href="/research/colloquium/registrationList">Registration and attendance</a></li>
            </ul>
            </li> 
            <li><h4><a role="button" data-toggle="collapse" class="" href="#asdf63">Research proposal <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                <ul class="" id="asdf63">
                    <li><a href="/research/submittedDeliverables/list">Student Listing & Research Proposal Status</a></li>
                     <li><a href="/research/proposalReporting/list">Research Progress Report (Chapter 1 and 2)</a></li>
                </ul>
            </li>
           <li><h4><a role="button" data-toggle="collapse" class="" href="#asdf64">proposal Defense <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                <ul class="" id="asdf64">
                <li><a href="/research/comitee/list">Assign Proposal Defense Committee</a></li>
                <li><a href="/research/comitee/updateStatusList">Student Listing and Status</a></li>
                <li><a href="/research/comingsoon/list">Assign Proposal Defense Date</a></li>
                <li><a href="/research/proposal/list">Proposal Defense Result update</a></li>
            </ul>
            </li>


           
           
           
        </ul>


 <h4><a role="button" data-toggle="collapse" class="<?php echo $asdf7topclass;?>" href="#asdf7">Stage 2 <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                        <ul class="<?php echo $asdf7class;?>" id="asdf7">        
          
            <li><a href="/research/proposalReporting/list2">Listing of students and their research progress report (Chapter 3)</a></li>
        </ul>

 <h4><a role="button" data-toggle="collapse" class="<?php echo $asdf8topclass;?>" href="#asdf8">Stage 3 <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                        <ul class="<?php echo $asdf8class;?>" id="asdf8">        
            <li><a href="/research/proposalReporting/list3"> Listing of students and their research progress report (Chapter 4)</a></li>
        </ul>
                    </div>


    
           <!--  <li><a href="/research/phdDuration/list">Phd Duration</a></li>
            <li><a href="/research/studentMilestone/add">Phd Duration Tagging</a></li>
        </ul>

        <h4>Supervisor</h4>
        <ul>
            <li><a href="/research/supervisorRole/list">Supervisor Role</a></li>
            <li><a href="/research/supervisor/list">Add Supervisor</a></li>
            <li><a href="/research/supervisorChangeApplication/list">Supervisor Change Application</a></li>
            <li><a href="/research/supervisorTagging/add">Tag Students to Supervisor</a></li>
            <li><a href="/research/supervisorChangeApplication/list">Supervisor Change Application</a></li>
        </ul>

        <h4>Examiner</h4>
        <ul>
            <li><a href="/research/examinerRole/list">Examiner Role</a></li>
            <li><a href="/research/examiner/list">Add Examiner</a></li>
        </ul>





        <h4>Postgraduate Admission</h4>
        <ul>
            <li><a href="/research/applicant/list">Application listing by programme structure</a></li>
            <li><a href="/research/applicantApproval/list">Offer and Acceptance Status</a></li>
            <li><a href="/research/studentRecord/list">Student Records</a></li>
        </ul>

        <h4>Stage 1</h4>
        <ul>
            <li><a href="/research/advisorTagging/add">Tag Students to Academic Advisor</a></li>
            <li><a href="/research/studentSemester/add">Student Semester Promotion</a></li>
            <li><a href="/research/comingsoon/list">Registration List & Status</a></li>
            <li><a href="/research/courseRegistration/list"></a></li>
            <h5 style="background-color: #b5eff5;padding:5px;">Audit Course Registration</h5>
                <ul>
                    <li><a href="/research/courseRegistration/list">Registration</a></li>
                    <li><a href="/research/courseRegistration/addAttendance">Attendance</a></li>
                </ul>
            <h5 style="background-color: #b5eff5;padding:5px;">Colloquium Management</h5>
            <ul>
                <li><a href="/research/colloquium/list">Colloquium List</a></li>
                <li><a href="/research/colloquium/registrationList">Registration and attendance</a></li>
            </ul>
            <li><a href="#" style="background-color: #b5eff5;padding:5px;">Research proposal</a>
                <ul>
                    <li><a href="/research/submittedDeliverables/list">Student Listing & Research Proposal Status</a></li>
                     <li><a href="/research/proposalReporting/list">Research Progress Report (Chapter 1 and 2)</a></li>
                </ul>
            </li>
            <li><a href="#" style="background-color: #b5eff5;padding:5px;">Proposal Defense</a>
            <ul>
                <li><a href="/research/comitee/list">Assign Proposal Defense Committee</a></li>
                <li><a href="/research/comitee/updateStatusList">Student Listing and Status</a></li>
                <li><a href="/research/comingsoon/list">Assign Proposal Defense Date</a></li>
                <li><a href="/research/proposal/list">Proposal Defense Result update</a></li>
            </ul>
            </li>
           
           
           
        </ul>

        <h4>Stage 2</h4>
        <ul>
            <li><a href="#" style="background-color: #b5eff5;padding:5px;">Data Collection and Analysis Report</a>
                <ul>
            <li><a href="/research/proposalReporting/list2">Listing of students and their research progress report (Chapter 3)</a></li>
        </ul>
        </ul>

        <h4>Stage 3</h4>
        <ul>
            <li><a href="/research/comingsoon/list">Thesis Writing Report</a></li>
            <li><a href="/research/proposalReporting/list3"> Listing of students and their research progress report (Chapter 4)</a></li>
        </ul>

        <h4>Stage 4</h4>
        <ul>
            <li><a href="#" style="background-color: #b5eff5;padding:5px;">Notice of Thesis submission </a>
                  <ul>
                    <li><a href="/research/comingsoon/list">Notice submission form</a></li>
            <li><a href="/research/toc/list">TOC (document upload)</a></li>

            <li><a href="/research/abstractt/list">Abstract (Document upload)</a></li>
                  </ul>
             </li>                     
            
            <li><a href="#" style="background-color: #b5eff5;padding:5px;">Examiners committee set-up</a>
                 <ul>
            <li><a href="/research/comingsoon/list">Chairman</a></li>
            <li><a href="/research/examiner/internalList">Internal examiner</a></li>
            <li><a href="/research/examiner/internalList">External examiner</a></li>

                 </ul>
             </li>
                <li><a href="/research/comingsoon/list">Dissertation Examination Date set-up</a></li>
                <li><a href="#" style="background-color: #b5eff5;padding:5px;">Submission of Unbound Copies</a>
                    <ul>
                <li><a href="/research/comingsoon/list">Submission status</a></li>
                <li><a href="/research/comingsoon/list">Postgraduate School Clearance Update</a></li>

                    </ul>

                </li>

       
            <li><a href="#" style="background-color: #b5eff5;padding:5px;">Submission status checklist</a>
                <ul>
                    <li><a href="/research/bound/list">5 copies of hard bound submission</a></li>
                    <li><a href="/research/sco/list">1 softcopy of final thesis in PDF</a></li>
                    <li><a href="/research/comingsoon/list">1 similarity report (Turnitin report)</a></li>
                    <li><a href="/research/ppt/list"> Powerpoint presentation</a></li>
                    <h5>Submission Receipt</h5>
                    <li>
                        <ul>
                            <li></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="#" style="background-color: #b5eff5;padding:5px;">Thesis Examination</a>
                <ul>
                    <li><a href="/research/comingsoon/list">Student List and Result Update</a></li>
                </ul>
            </li>

             <li><a href="#" style="background-color: #b5eff5;padding:5px;">Thesis Resubmission</a>
                <ul>
                    <li><a href="/research/comingsoon/list">Student List and Result Update</a></li>
                </ul>
            </li> -->
        

    </div>
</div>