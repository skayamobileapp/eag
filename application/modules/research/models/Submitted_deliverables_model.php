<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Submitted_deliverables_model extends CI_Model
{

    function getSupervisor($id)
    {
        $this->db->select('s.*');
        $this->db->from('research_supervisor as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getOrganisation()
    {
        $this->db->select('s.*');
        $this->db->from('organisation as s');
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }
    
    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.code as programme_code, p.name as programme_name, i.name as intake_name, st.ic_no, st.name as advisor_name, ms.name as mailing_state, mc.name as mailing_country, ps.name as permanent_state, pc.name as permanent_country, rs.name as race, rels.name as religion, brch.code as branch_code, brch.name as branch_name, salut.name as salutation, pu.code as partner_university_code, pu.name as partner_university_name, sch.code as scheme_code, sch.description as scheme_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('organisation_has_training_center as brch', 's.id_branch = brch.id','left'); 
        $this->db->join('salutation_setup as salut', 's.salutation = salut.id','left'); 
        $this->db->join('partner_university as pu', 's.id_university = pu.id','left'); 
        $this->db->join('scheme as sch', 's.id_program_has_scheme = sch.id','left'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('state as ms', 's.mailing_state = ms.id'); 
        $this->db->join('country as mc', 's.mailing_country = mc.id');
        $this->db->join('state as ps', 's.permanent_state = ps.id'); 
        $this->db->join('country as pc', 's.permanent_country = pc.id'); 
        $this->db->join('race_setup as rs', 's.id_race = rs.id'); 
        $this->db->join('religion_setup as rels', 's.religion = rels.id','left');
        $this->db->join('staff as st', 's.id_advisor = st.id','left'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudent($id_student)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function deliverablesList()
    {
        $this->db->select('*');
        $this->db->from('research_deliverables_student');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function durationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_phd_duration');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }

    function supervisorListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_supervisor');
        $this->db->where('status', $status);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }

    function researchStatusListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_status');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }

    function chapterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_chapter');
        $this->db->where('status', $status);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }

    function topicListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_topic');
        $this->db->where('status', $status);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }
    
    function getChapterByDuration($id_phd_duration)
    {
        $this->db->select('DISTINCT(rd.id_chapter) as id,rc.*');
        $this->db->from('research_deliverables as rd');
        $this->db->join('research_chapter as rc','rd.id_chapter = rc.id');
        $this->db->where('rd.id_phd_duration', $id_phd_duration);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getTopicByData($data)
    {
        $this->db->select('rd.*');
        $this->db->from('research_deliverables as rd');
        $this->db->join('research_chapter as rc','rd.id_chapter = rc.id');
        $this->db->where('rd.id_phd_duration', $data['id_phd_duration']);
        $this->db->where('rd.id_chapter', $data['id_chapter']);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getDeliverablesListSearch($data)
    {
        $this->db->select('rd.*, rst.code as status_code, rdur.name as topic, s.full_name as student_name, s.email_id, s.nric, rs.full_name as supervisor');
        $this->db->from('research_deliverables_student as rd');
        $this->db->join('research_topic as rdur','rd.id_topic = rdur.id');
        $this->db->join('research_status as rst','rd.status = rst.id');
        // $this->db->join('research_chapter as rc','rd.id_chapter = rc.id');
        // $this->db->join('research_phd_duration as rpd','rd.id_phd_duration = rpd.id');
        $this->db->join('student as s','rd.id_student = s.id');
        $this->db->join('research_supervisor as rs','rd.id_supervisor = rs.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['id_supervisor'] != '')
        {
            $this->db->where('rd.id_supervisor', $data['id_supervisor']);
        }
        if($data['id_topic'] != '')
        {
            $this->db->where('rd.id_topic', $data['id_topic']);
        }
        if($data['status'] != '')
        {
            $this->db->where('rd.status', $data['status']);
        }
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getDeliverables($id)
    {
        $this->db->select('rd.*, rst.code as status_code');
        $this->db->from('research_deliverables_student as rd');
        $this->db->join('research_status as rst','rd.status = rst.id');
        $this->db->where('rd.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewDeliverables($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_deliverables_student', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editDeliverables($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_deliverables_student', $data);
        return TRUE;
    }

    function generateDeliverableApplicationNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('research_deliverables_student as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "DEL" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }
}

