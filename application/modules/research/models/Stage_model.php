<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Stage_model extends CI_Model
{
    function stageListSearch($formData)
    {
        $this->db->select('a.*');
        $this->db->from('graduation_stage as a');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(a.name  LIKE '%" . $formData['name'] . "%' or a.description  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['type']))
        {
            $this->db->where('a.type', $formData['type']);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStage($id)
    {
        $this->db->select('*');
        $this->db->from('graduation_stage');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewStage($data)
    {
        $this->db->trans_start();
        $this->db->insert('graduation_stage', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editStage($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('graduation_stage', $data);
        return TRUE;
    }

    function addSemesterStage($data)
    {
        $this->db->trans_start();
        $this->db->insert('stage_semester', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getSemesterStage($id_stage)
    {
        $this->db->select('a.*');
        $this->db->from('stage_semester as a');
        $this->db->where('a.id_stage', $id_stage);
        $this->db->order_by("id", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteSemsterStage($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('stage_semester');
        return TRUE;
    }

    function getStageOverviewByType()
    {
        $one = 'Part Time';
        $two = 'Full Time';

        $part_time = $this->getStageOverview($one);
        $full_time = $this->getStageOverview($two);

        $details = array();

        if($part_time)
        {
            $data_one = new stdClass;
            $data_one->Parttime = $part_time;
            
            array_push($details, $data_one);
        }

        if($full_time)
        {
            $data_two = new stdClass;
            $data_two->Fulltime = $full_time;

            array_push($details, $data_two);
        }

        return $details;
    }

    function getStageOverview($type)
    {
        $this->db->select('a.*');
        $this->db->from('graduation_stage as a');
        if($type != '')
        {
            $this->db->where('a.type', $type);
        }
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $results = $query->result(); 

        $details = array();

        foreach ($results as $result)
        {
            $id_stage = $result->id;
            $semesters = $this->getSemesterStage($id_stage);
            if($semesters)
            {
                $result->semesters = $semesters;
            }
            else
            {
                $result->semesters = array();
            }
            array_push($details, $result);
        }
        
        return $details;
    }

    function getStageOverviewList()
    {
        $one = 'Part Time';
        $two = 'Full Time';

        $part_time = $this->getStageOverviewForData('');

        return $part_time;

    }

    function getStageOverviewForData($type)
    {
        $this->db->select('a.*');
        $this->db->from('graduation_stage as a');
        if($type != '')
        {
            $this->db->where('a.type', $type);
        }
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $results = $query->result(); 

        $details = array();

        foreach ($results as $result)
        {
            $id_stage = $result->id;

            $semesters = $this->getSemesterStage($id_stage);
            if($semesters)
            {
                $result->semesters = $semesters;
            }
            else
            {
                $result->semesters = array();
            }
            // array_push($details, $result);


            $mile_stone_tag = $this->getMileStoneSemesterByIdStage($id_stage);
            if($mile_stone_tag)
            {
                $result->mile_stone_tag = $mile_stone_tag;
            }
            else
            {
                $result->mile_stone_tag = array();
            }

            array_push($details, $result);

        }
        
        return $details;
    }

    function getMileStoneSemesterByIdStage($id_stage)
    {
        $this->db->select('a.*, ms.name as mile_stone');
        $this->db->from('mile_stone_tag_semester as a');
        $this->db->join('mile_stone as ms','a.id_mile_stone = ms.id');
        $this->db->where('a.id_stage', $id_stage);
        $this->db->order_by("id", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
}