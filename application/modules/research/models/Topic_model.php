<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Topic_model extends CI_Model
{
    function topicList()
    {
        $this->db->select('i.*');
        $this->db->from('research_topic as i');
        $this->db->order_by("i.name", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function topicListByStatus($status)
    {
        $this->db->select('i.*');
        $this->db->from('research_topic as i');
        $this->db->where("i.status", $status);
        $this->db->order_by("i.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function researchTopicCategoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_topic_category');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }


    function topicListSearch($data)
    {
        $this->db->select('i.*, p.name as topic_category_name');
        $this->db->from('research_topic as i');
        $this->db->join('research_topic_category as p', 'i.id_topic_category = p.id');
        if($data['name'] != '')
        {
            $likeCriteria = "(i.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['id_topic_category'] != '')
        {
            $this->db->where("i.id_topic_category", $data['id_topic_category']);
        }
        $this->db->order_by("i.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getTopic($id)
    {
        $this->db->select('i.*');
        $this->db->from('research_topic as i');
        $this->db->where('i.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewTopic($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_topic', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function tempAddTopicHasSupervisor($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_topic_has_supervisors', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempTopicHasSupervisorBySession($id_session)
    {
        $this->db->select('tihp.*, p.ic_no, p.name as staff_name');
        $this->db->from('temp_topic_has_supervisors as tihp');
        $this->db->join('staff as p', 'tihp.id_staff = p.id');
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempTopicHasSupervisor($id)
    {
       $this->db->where('id', $id);
       $this->db->delete('temp_topic_has_supervisors');
       return TRUE;
    }

    function moveTempToDetails($id_topic)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempTopicHasSupervisor($id_session);

        foreach ($temp_details as $result)
        {
            unset($result->id);
            unset($result->id_session);
            $result->id_topic = $id_topic;
            $this->addNewTopicHasSupervisors($result);
        }

        $deleted = $this->deleteTempTopicHasSupervisorBySessionId($id_session);
        return $deleted;
    }

    function getTempTopicHasSupervisor($id_session)
    {
        $this->db->select('tihp.*');
        $this->db->from('temp_topic_has_supervisors as tihp');    
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewTopicHasSupervisors($data)
    {
        $this->db->trans_start();
        $this->db->insert('topic_has_supervisors', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTempTopicHasSupervisorBySessionId($id_session)
    {
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_topic_has_supervisors');
       return TRUE;
    }

    function getTopicHasSupervisor($id)
    {
        $this->db->select('tihp.*, p.name as staff_name, p.ic_no');
        $this->db->from('topic_has_supervisors as tihp');
        $this->db->join('staff as p', 'tihp.id_staff = p.id');     
        $this->db->where('tihp.id_topic', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function editTopicDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_topic', $data);
        return TRUE;
    }

     function deleteTopicHasSupervisor($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('topic_has_supervisors');
       return TRUE;
    }
}