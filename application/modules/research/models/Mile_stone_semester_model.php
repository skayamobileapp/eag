<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Mile_stone_semester_model extends CI_Model
{
    function mileStoneListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('mile_stone as a');
        $this->db->where('status', $status);
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function stageListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('graduation_stage as a');
        $this->db->where('status', $status);
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function mileStoneSemesterListSearch($formData)
    {
        $this->db->select('a.*, s.name as stage, sem.name as semester, ms.name as mile_stone');
        $this->db->from('mile_stone_tag_semester as a');
        $this->db->join('graduation_stage as s','a.id_stage = s.id');
        $this->db->join('stage_semester as sem','a.id_semester = sem.id');
        $this->db->join('mile_stone as ms','a.id_mile_stone = ms.id');
        // if (!empty($formData['name']))
        // {
        //     $likeCriteria = "(a.name  LIKE '%" . $formData['name'] . "%' or a.description  LIKE '%" . $formData['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        if (!empty($formData['type']))
        {
            $this->db->where('a.type', $formData['type']);
        }
        if (!empty($formData['id_stage']))
        {
            $this->db->where('a.id_stage', $formData['id_stage']);
        }
        if (!empty($formData['id_mile_stone']))
        {
            $this->db->where('a.id_mile_stone', $formData['id_mile_stone']);
        }
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStageByType($type)
    {
        $this->db->select('a.*');
        $this->db->from('graduation_stage as a');
        $this->db->where('status', 1);
        $this->db->where('type', $type);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getMileStoneSemester($id)
    {
        $this->db->select('*');
        $this->db->from('mile_stone_tag_semester');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewMileStoneSemester($data)
    {
        $this->db->trans_start();
        $this->db->insert('mile_stone_tag_semester', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editMileStoneSemester($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('mile_stone_tag_semester', $data);
        return TRUE;
    }

    function getSemesterStage($data)
    {
        $this->db->select('a.*');
        $this->db->from('stage_semester as a');
        $this->db->join('graduation_stage as s','a.id_stage = s.id');
        $this->db->where('a.id_stage', $data['id_stage']);
        $this->db->where('s.type', $data['type']);
        $this->db->order_by("id", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
}