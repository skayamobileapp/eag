<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Abstract_model extends CI_Model
{
    function supervisorListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_supervisor');
        $this->db->where('status', $status);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }
    
    function getSupervisor($id)
    {
        $this->db->select('s.*');
        $this->db->from('research_supervisor as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudent($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
         return $query->row();
    }

    function abstractListSearch($data)
    {
        $this->db->select('rd.*, rs.full_name as supervisor_name, rs.email as supervisor_email, s.full_name as student_name, s.nric ');
        $this->db->from('research_abstract as rd');
        $this->db->join('research_supervisor as rs','rd.id_supervisor = rs.id');
        $this->db->join('student as s','rd.id_student = s.id');
        // $this->db->where('rd.id_student', $id_student);
        if ($data['name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['id_supervisor'] != '')
        {
            $this->db->where('rd.id_supervisor', $data['id_supervisor']);
        }
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.code as programme_code, p.name as programme_name, i.name as intake_name, st.ic_no, st.name as advisor_name, ms.name as mailing_state, mc.name as mailing_country, ps.name as permanent_state, pc.name as permanent_country, rs.name as race, rels.name as religion, brch.code as branch_code, brch.name as branch_name, salut.name as salutation, pu.code as partner_university_code, pu.name as partner_university_name, sch.code as scheme_code, sch.description as scheme_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('organisation_has_training_center as brch', 's.id_branch = brch.id','left'); 
        $this->db->join('salutation_setup as salut', 's.salutation = salut.id','left'); 
        $this->db->join('partner_university as pu', 's.id_university = pu.id','left'); 
        $this->db->join('scheme as sch', 's.id_program_has_scheme = sch.id','left'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('state as ms', 's.mailing_state = ms.id'); 
        $this->db->join('country as mc', 's.mailing_country = mc.id');
        $this->db->join('state as ps', 's.permanent_state = ps.id'); 
        $this->db->join('country as pc', 's.permanent_country = pc.id'); 
        $this->db->join('race_setup as rs', 's.id_race = rs.id'); 
        $this->db->join('religion_setup as rels', 's.religion = rels.id','left');
        $this->db->join('staff as st', 's.id_advisor = st.id','left'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getAbstract($id)
    {
        $this->db->select('ia.*');
        $this->db->from('research_abstract as ia');
         $this->db->where('ia.id', $id);
        $query = $this->db->get();
          $result = $query->row();
         return $result; 
    }

    function abstractCommentsDetails($id_abstract)
    {
        $this->db->select('rd.*');
        $this->db->from('abstract_reporting_comments as rd');
        $this->db->where('rd.id_abstract', $id_abstract);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
        
    }

    function getOrganisation()
    {
        $this->db->select('s.*');
        $this->db->from('organisation as s');
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }
}