<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Applicant_approval_model extends CI_Model
{
   function applicantList($applicantList)
    {
        $status = 'Draft';
        $this->db->select('a.*, p.code as program_code, p.name as program_name, inta.year as intake_year, inta.name as intake_name');
        $this->db->from('applicant as a');
        $this->db->join('programme as p', 'a.id_program = p.id');
        $this->db->join('intake as inta', 'a.id_intake = inta.id');

        if($applicantList['first_name']) {
            $likeCriteria = "(a.full_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        //  if($applicantList['last_name']) {
        //     $likeCriteria = "(a.last_name  LIKE '%" . $applicantList['last_name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
         if($applicantList['email_id']) {
            $likeCriteria = "(a.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['nric']) {
            $likeCriteria = "(a.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_program']) {
            $likeCriteria = "(a.id_program  LIKE '%" . $applicantList['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_intake']) {
            $likeCriteria = "(a.id_intake  LIKE '%" . $applicantList['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('a.applicant_status', $status);
        $this->db->where('a.email_verified', '1');
        $this->db->where('a.is_updated', '1');
        $this->db->where('a.is_submitted', '1');
        $likeCriteria = "(a.is_apeal_applied  = 3 or a.is_apeal_applied  = 1)";
        $this->db->where($likeCriteria);

        // $this->db->where('a.is_apeal_applied',3);
        // $this->db->or_where('a.is_apeal_applied',8);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($query);exit();     
         return $result;
    }

    function applicantListForApproval($applicantList)
    {
        $status = 'Submitted';
        $this->db->select('a.*, ahed.employee_status, ahad.alumni_status, ahsd.sibbling_status, p.code as program_code, p.name as program_name, inta.year as intake_year, inta.name as intake_name, pt.code as program_structure_code, pt.name as program_structure_name, train.name as training_center_name, train.code as training_center_code, el.name as education_level');
        $this->db->from('applicant as a');
        $this->db->join('programme as p', 'a.id_program = p.id','left');
        $this->db->join('intake as inta', 'a.id_intake = inta.id','left');
        $this->db->join('program_type as pt', 'a.id_program_structure_type = pt.id');
        $this->db->join('organisation_has_training_center as train', 'a.id_branch = train.id');
        $this->db->join('applicant_has_employee_discount as ahed', 'a.id = ahed.id_applicant','left');
        $this->db->join('applicant_has_alumni_discount as ahad', 'a.id = ahad.id_applicant','left');
        $this->db->join('applicant_has_sibbling_discount as ahsd', 'a.id = ahsd.id_applicant','left');
        $this->db->join('scholarship_education_level as el', 'a.id_degree_type = el.id');

        if($applicantList['first_name']) {
            $likeCriteria = "(a.full_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['email_id']) {
            $likeCriteria = "(a.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
        
         if($applicantList['nric']) {
            $likeCriteria = "(a.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_program']) {
            $likeCriteria = "(a.id_program  LIKE '%" . $applicantList['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_intake']) {
            $likeCriteria = "(a.id_intake  LIKE '%" . $applicantList['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        
        // $likeCriteria = "(a.is_sibbling_discount  != '0' and a.is_employee_discount  != '0' and a.is_alumni_discount  != '0')";
        // $this->db->where($likeCriteria);

        $this->db->where('a.applicant_status', $status);
        $this->db->where('el.name', 'POSTGRADUATE');
        $this->db->where('a.is_updated', '1');
        $this->db->where('a.email_verified', '1');
        $this->db->where('a.is_submitted', '1');
        $likeCriteria = "(a.is_apeal_applied  = 3 or a.is_apeal_applied  = 1)";
        $this->db->where($likeCriteria);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function intakeList()
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getPartnerUniversity($id)
    {
        $this->db->select('d.*');
        $this->db->from('partner_university as d');
        $this->db->where('d.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function nationalityList()
    {
        $this->db->select('*');
        $this->db->from('nationality');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }

    function programStructureTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('program_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result; 
    }

    function addNewStudent($id)
    {
        $query = $this->db->select('*')->from('applicant')->where('id',$id)->get();
        foreach ($query->result() as $row)
        {
            // echo "<Pre>";print_r($row);exit();
            unset($row->id);
            unset($row->is_sibbling_discount);
            unset($row->is_employee_discount);
            unset($row->approved_by);
            unset($row->email_verified);
            unset($row->is_updated);
            unset($row->is_submitted);
            
            
            $row->status = '1';
            $row->applicant_status = 'Approved';
            $row->id_applicant = $id;

            $this->db->insert('student',$row);
            $insert_id = $this->db->insert_id();
        }

        $this->db->trans_complete();
        return $insert_id;
    }

    function addStudentProfileDetail($id)
    {
        $data = ['id_student'=>$id];
          $this->db->insert('profile_details', $data);
          // $this->db->insert('visa_details', $data);
    }


    function getApplicantDetails($id)
    {
        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function editApplicantDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('applicant', $data);
        return TRUE;
    }
    
    // function deleteActivityDetails($id, $date)
    // {
    //     $this->db->where('id', $countryId);
    //     $this->db->update('academic_year', $countryInfo);
    //     return $this->db->affected_rows();
    // }


    function getFeeStructureMaster($id)
    {
        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function createNewMainInvoiceForStudent($id)
    {
        $id_student = $id;
        $user_id = $this->session->userId;

        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $applicant_data = $query->row();

        $id_applicant = $applicant_data->id;
        $id_branch = $applicant_data->id_branch;
        $id_university = $applicant_data->id_university;
        $id_fee_structure = $applicant_data->id_fee_structure;

        if($id_fee_structure == 0)
        {
            $id_fee_structure = $this->getFeeStructureMasterIdByData($applicant_data);
        }


        $id_program = $applicant_data->id_program;
        $id_intake = $applicant_data->id_intake;
        $nationality = $applicant_data->nationality;
        $id_program_scheme = $applicant_data->id_program_scheme;
        $id_program_has_scheme = $applicant_data->id_program_has_scheme;
        $id_program_landscape = $applicant_data->id_program_landscape;
        $is_sibbling_discount = $applicant_data->is_sibbling_discount;
        $is_employee_discount = $applicant_data->is_employee_discount;
        $is_alumni_discount = $applicant_data->is_alumni_discount;

        // echo "<Pre>";print_r($id_program_landscape);exit;


        $is_installment = 0;
        $installments = 0;
        $currency = 'MYR';

       
        // echo "<Pre>";print_r($fee_structure_training_data);exit;

        // echo "<Pre>";print_r($is_installment);exit;
        
        if($id_fee_structure != 0)
        {

            $trigger = 'OFFER ACCEPTED';

            // if($id_university == 1)
            // {

                $fee_structure_master = $this->getFeeStructureMaster($id_fee_structure);
                $id_currency = $fee_structure_master->id_currency;


                $get_data['id_fee_structure'] = $id_fee_structure;
                $get_data['id_training_center'] = 1;
                $get_data['trigger'] = $trigger;

                if($nationality == '1')
                {
                    $currency = 'MYR';
                    $get_data['currency'] = $currency;
                    $detail_data = $this->getFeeStructureByData($get_data);
                }
                elseif($nationality != '')
                {
                    $currency = 'USD';
                    $get_data['currency'] = $currency;
                    $detail_data = $this->getFeeStructureByData($get_data);
                }




                if(!empty($detail_data))
                {
                    $invoice_number = $this->generateMainInvoiceNumber();

                    $invoice['invoice_number'] = $invoice_number;
                    $invoice['type'] = 'Applicant';
                    $invoice['remarks'] = 'Application Registration Fee';
                    $invoice['id_application'] = '1';
                    $invoice['id_student'] = $id_student;
                    $invoice['id_program'] = $id_program;
                    $invoice['id_intake'] = $id_intake;
                    $invoice['currency'] = $id_currency;
                    $invoice['total_amount'] = '0';
                    $invoice['balance_amount'] = '0';
                    $invoice['paid_amount'] = '0';
                    $invoice['status'] = '1';
                    $invoice['is_migrate_applicant'] = $id;
                    $invoice['created_by'] = $user_id;

                    // $detail_data = $this->getFeeStructure('13','5');

                    
                    // echo "<Pre>";print_r($invoice);exit;
                    $inserted_id = $this->addNewMainInvoice($invoice);

                    if($inserted_id)
                    {
                        $applicant_update_data['is_invoice_generated'] = $inserted_id;
                        $updated_applicant = $this->editApplicantDetails($applicant_update_data,$id_student);
                    }



                    $total_amount = 0;
                    $total_discount_amount = 0;
                    $sibling_discount_amount = 0;
                    $employee_discount_amount = 0;
                    $alumni_discount_amount = 0;


                    // echo "<Pre>";print_r($detail_data);exit;

                    foreach ($detail_data as $fee_structure)
                    {
                        $is_installment = $fee_structure->is_installment;
                        $id_training_center = $fee_structure->id_training_center;
                        $trigger_name = $fee_structure->trigger_name;

                        // $instllment_data['id_fee_structure'] = $fee_structure->id;
                        // $instllment_data['id_fee_structure_master'] = $fee_structure->id_program_landscape;

            
                        if($trigger_name == 'OFFER ACCEPTED')
                        {
                            $data = array(
                                'id_main_invoice' => $inserted_id,
                                'id_fee_item' => $fee_structure->id_fee_item,
                                'amount' => $fee_structure->amount,
                                'price' => $fee_structure->amount,
                                'quantity' => 1,
                                'id_reference' => $fee_structure->id,
                                'description' => 'OFFER ACCEPTED Fee',
                                'status' => 1,
                                'created_by' => $user_id
                            );

                            $total_amount = $total_amount + $fee_structure->amount;

                            $this->addNewMainInvoiceDetails($data);
                        }
                        // echo "<Pre>";print_r($data);exit;
                    }

                    $total_invoice_amount = $total_amount;



                    if($is_sibbling_discount == '1')
                    {
                        $this->db->select('*');
                        $this->db->from('sibbling_discount');
                        // $likeCriteria = "(date(start_date)  <= '" . date('Y-m-d') . "')";
                        // $this->db->where($likeCriteria);
                        // $likeCriteria = "(date(end_date)  <= '" . date('Y-m-d') . "')";
                        // $this->db->where($likeCriteria);

                    //     $SQL = "Select * From sibbling_discount where date(start_date) <= 'getdate()' and date(end_date) >= 'getdate()' order by id DESC limit 0,1";
                    //     $query = $this->db->query($SQL);
                       
                    // echo "<Pre>";print_r($query->row());exit;

                        $this->db->where('currency', $currency);
                        $this->db->where('status', '1');
                        $query = $this->db->get();
                        $sibling_discount_data = $query->row();

                        if($sibling_discount_data)
                        {
                            $amount = $sibling_discount_data->amount;
                            $id_discount = $sibling_discount_data->id;

                            $sibling_insert = array(
                                'id_main_invoice' => $inserted_id,
                                'id_student' => $id_student,
                                'name' => 'Sibbling Discount Applied',
                                'amount' => $amount,
                                'id_reference' => $id_discount,
                            );
                            $discount_inserted_id = $this->addNewMainInvoiceDiscountDetail($sibling_insert);
                            if($discount_inserted_id)
                            {
                                $total_amount = $total_amount - $sibling_discount_data->amount;
                                $sibling_discount_amount = $sibling_discount_data->amount;
                            }
                        }

                    // echo "<Pre>";print_r($amount);exit;
                    }

                    if($is_employee_discount == '1')
                    {

                        $this->db->select('*');
                        $this->db->from('employee_discount');
                        // $this->db->where(date('Y-m-d').' BETWEEN  date(start_date) and date(end_date)');
                        // $this->db->where('date(start_date) >=', date('Y-m-d'));
                        // $this->db->where('date(end_date) <=', date('Y-m-d'));
                        $this->db->where('currency', $currency);
                        $this->db->where('status', '1');
                        $query = $this->db->get();
                        $employee_discount_data = $query->row();
                        if($employee_discount_data)
                        {
                            $amount = $employee_discount_data->amount;
                            $id_discount = $employee_discount_data->id;

                            $employee_insert = array(
                                'id_main_invoice' => $inserted_id,
                                'id_student' => $id_student,
                                'name' => 'Employee Discount Applied',
                                'amount' => $amount,
                                'id_reference' => $id_discount,
                            );
                            $sibbling_inserted_id = $this->addNewMainInvoiceDiscountDetail($employee_insert);
                            if($sibbling_inserted_id)
                            {
                                $total_amount = $total_amount - $employee_discount_data->amount;
                                $employee_discount_amount = $employee_discount_data->amount;
                            }
                        }
                    }




                    if($is_alumni_discount == '1')
                    {

                        $this->db->select('*');
                        $this->db->from('alumni_discount');
                        // $this->db->where(date('Y-m-d').' BETWEEN  date(start_date) and date(end_date)');
                        // $this->db->where('date(start_date) >=', date('Y-m-d'));
                        // $this->db->where('date(end_date) <=', date('Y-m-d'));
                        $this->db->where('currency', $currency);
                        $this->db->where('status', '1');
                        $query = $this->db->get();
                        $alumni_discount_data = $query->row();
                        if($alumni_discount_data)
                        {
                            $amount = $alumni_discount_data->amount;
                            $id_discount = $alumni_discount_data->id;

                            $employee_insert = array(
                                'id_main_invoice' => $inserted_id,
                                'id_student' => $id_student,
                                'name' => 'Alumni Discount Applied',
                                'amount' => $amount,
                                'id_reference' => $id_discount,
                            );
                            $alumni_inserted_id = $this->addNewMainInvoiceDiscountDetail($employee_insert);
                            if($alumni_inserted_id)
                            {
                                $total_amount = $total_amount - $alumni_discount_data->amount;
                                $alumni_discount_amount = $alumni_discount_data->amount;
                            }
                        }
                    }


                    $total_discount_amount = $sibling_discount_amount + $employee_discount_amount + $alumni_discount_amount;
                    // $total_amount = number_format($total_amount, 2, '.', ',');
                    // echo "<Pre>";print_r($total_amount);exit;

                    $invoice_update['total_amount'] = $total_amount;
                    $invoice_update['balance_amount'] = $total_amount;
                    $invoice_update['invoice_total'] = $total_invoice_amount;
                    $invoice_update['total_discount'] = $total_discount_amount;
                    $invoice_update['paid_amount'] = '0';
                    // $invoice_update['inserted_id'] = $inserted_id;
                    // echo "<Pre>";print_r($invoice_update);exit;
                    $this->editMainInvoice($invoice_update,$inserted_id);
                }
            


            // }

            // echo "<Pre>";print_r($detail_data);exit;


            // if($id_university > 1)
            // {
            //     $currency = 'USD';
            //     $detail_data = $this->getFeeStructureByTrainingCenterForInvoiceGeneration($id_program,$id_intake,$id_fee_structure,$id_university);
            

            //     // echo "<Pre>";print_r($detail_data);exit;
            //     // echo "<Pre>";print_r($is_employee_discount);exit;


            //     if(!empty($detail_data))
            //     {
            //         $currency = $detail_data[0]->currency;
                    
            //         $invoice_number = $this->generateMainInvoiceNumber();

            //         $invoice['invoice_number'] = $invoice_number;
            //         $invoice['type'] = 'Applicant';
            //         $invoice['remarks'] = 'Application Registration Fee';
            //         $invoice['id_application'] = '1';
            //         $invoice['id_student'] = $id_student;
            //         $invoice['id_program'] = $id_program;
            //         $invoice['id_intake'] = $id_intake;
            //         $invoice['currency'] = $currency;
            //         $invoice['total_amount'] = '0';
            //         $invoice['balance_amount'] = '0';
            //         $invoice['paid_amount'] = '0';
            //         $invoice['status'] = '1';
            //         $invoice['is_migrate_applicant'] = $id;
            //         $invoice['created_by'] = $user_id;

            //         // $detail_data = $this->getFeeStructure('13','5');

                    
            //         // echo "<Pre>";print_r($invoice);exit;
            //         $inserted_id = $this->addNewMainInvoice($invoice);

            //         if($inserted_id)
            //         {
            //             $applicant_update_data['is_invoice_generated'] = $inserted_id;
            //             $updated_applicant = $this->editApplicantDetails($applicant_update_data,$id_student);
            //         }



            //         $total_amount = 0;
            //         $total_discount_amount = 0;
            //         $sibling_discount_amount = 0;
            //         $employee_discount_amount = 0;
            //         $alumni_discount_amount = 0;


            //         // echo "<Pre>";print_r($detail_data);exit;

            //         foreach ($detail_data as $fee_structure)
            //         {
            //             $is_installment = $fee_structure->is_installment;
            //             $id_training_center = $fee_structure->id_training_center;
            //             $trigger_name = $fee_structure->trigger_name;

            //             $instllment_data['id_fee_structure'] = $fee_structure->id;
            //             $instllment_data['id_fee_structure_master'] = $fee_structure->id_program_landscape;

            //             if($is_installment == 1)
            //             {

            //                 $installment_details = $this->getTrainingCenterInstallmentDetails($instllment_data);

            //                 if($installment_details)
            //                 {
            //                     foreach ($installment_details as $installment_detail)
            //                     {
                                    
            //                         $installment_trigger_name = $installment_detail->trigger_name;

            //                         if($installment_trigger_name == 'APPLICATION REGISTRATION')
            //                         {

            //                             $data = array(
            //                                 'id_main_invoice' => $inserted_id,
            //                                 'id_fee_item' => $installment_detail->id_fee_item,
            //                                 'amount' => $installment_detail->amount,
            //                                 'price' => $installment_detail->amount,
            //                                 'quantity' => 1,
            //                                 'id_reference' => $id_student,
            //                                 'description' => 'Application Registration Fee',
            //                                 'status' => 1,
            //                                 'created_by' => $user_id
            //                             );

            //                             $total_amount = $total_amount + $installment_detail->amount;
                        
            //                             $this->addNewMainInvoiceDetails($data);
            //                         }
            //                     }
            //                 }
            //             }
            //             else
            //             {
            //                 if($trigger_name == 'APPLICATION REGISTRATION')
            //                 {

            //                     $data = array(
            //                         'id_main_invoice' => $inserted_id,
            //                         'id_fee_item' => $fee_structure->id_fee_item,
            //                         'amount' => $fee_structure->amount,
            //                         'price' => $fee_structure->amount,
            //                         'quantity' => 1,
            //                         'id_reference' => $id_student,
            //                         'description' => 'Application Registration Fee',
            //                         'status' => 1,
            //                         'created_by' => $user_id
            //                     );

            //                     $total_amount = $total_amount + $fee_structure->amount;

            //                     $this->addNewMainInvoiceDetails($data);
            //                 }
            //             }
            //             // echo "<Pre>";print_r($data);exit;


            //         }

            //         $total_invoice_amount = $total_amount;

            //         if($is_sibbling_discount == '1')
            //         {
            //             $this->db->select('*');
            //             $this->db->from('sibbling_discount');
            //             // $likeCriteria = "(date(start_date)  <= '" . date('Y-m-d') . "')";
            //             // $this->db->where($likeCriteria);
            //             // $likeCriteria = "(date(end_date)  <= '" . date('Y-m-d') . "')";
            //             // $this->db->where($likeCriteria);

            //         //     $SQL = "Select * From sibbling_discount where date(start_date) <= 'getdate()' and date(end_date) >= 'getdate()' order by id DESC limit 0,1";
            //         //     $query = $this->db->query($SQL);
                       
            //         // echo "<Pre>";print_r($query->row());exit;

            //             $this->db->where('currency', $currency);
            //             $this->db->where('status', '1');
            //             $query = $this->db->get();
            //             $sibling_discount_data = $query->row();

            //             if($sibling_discount_data)
            //             {
            //                 $amount = $sibling_discount_data->amount;
            //                 $id_discount = $sibling_discount_data->id;

            //                 $sibling_insert = array(
            //                     'id_main_invoice' => $inserted_id,
            //                     'id_student' => $id_student,
            //                     'name' => 'Sibbling Discount Applied',
            //                     'amount' => $amount,
            //                     'id_reference' => $id_discount,
            //                 );
            //                 $discount_inserted_id = $this->addNewMainInvoiceDiscountDetail($sibling_insert);
            //                 if($discount_inserted_id)
            //                 {
            //                     $total_amount = $total_amount - $sibling_discount_data->amount;
            //                     $sibling_discount_amount = $sibling_discount_data->amount;
            //                 }
            //             }

            //         // echo "<Pre>";print_r($amount);exit;
            //         }

            //         if($is_employee_discount == '1')
            //         {

            //             $this->db->select('*');
            //             $this->db->from('employee_discount');
            //             // $this->db->where(date('Y-m-d').' BETWEEN  date(start_date) and date(end_date)');
            //             // $this->db->where('date(start_date) >=', date('Y-m-d'));
            //             // $this->db->where('date(end_date) <=', date('Y-m-d'));
            //             $this->db->where('currency', $currency);
            //             $this->db->where('status', '1');
            //             $query = $this->db->get();
            //             $employee_discount_data = $query->row();
            //             if($employee_discount_data)
            //             {
            //                 $amount = $employee_discount_data->amount;
            //                 $id_discount = $employee_discount_data->id;

            //                 $employee_insert = array(
            //                     'id_main_invoice' => $inserted_id,
            //                     'id_student' => $id_student,
            //                     'name' => 'Employee Discount Applied',
            //                     'amount' => $amount,
            //                     'id_reference' => $id_discount,
            //                 );
            //                 $sibbling_inserted_id = $this->addNewMainInvoiceDiscountDetail($employee_insert);
            //                 if($sibbling_inserted_id)
            //                 {
            //                     $total_amount = $total_amount - $employee_discount_data->amount;
            //                     $employee_discount_amount = $employee_discount_data->amount;
            //                 }
            //             }
            //         }




            //         if($is_alumni_discount == '1')
            //         {

            //             $this->db->select('*');
            //             $this->db->from('alumni_discount');
            //             // $this->db->where(date('Y-m-d').' BETWEEN  date(start_date) and date(end_date)');
            //             // $this->db->where('date(start_date) >=', date('Y-m-d'));
            //             // $this->db->where('date(end_date) <=', date('Y-m-d'));
            //             $this->db->where('currency', $currency);
            //             $this->db->where('status', '1');
            //             $query = $this->db->get();
            //             $alumni_discount_data = $query->row();
            //             if($alumni_discount_data)
            //             {
            //                 $amount = $alumni_discount_data->amount;
            //                 $id_discount = $alumni_discount_data->id;

            //                 $employee_insert = array(
            //                     'id_main_invoice' => $inserted_id,
            //                     'id_student' => $id_student,
            //                     'name' => 'Alumni Discount Applied',
            //                     'amount' => $amount,
            //                     'id_reference' => $id_discount,
            //                 );
            //                 $alumni_inserted_id = $this->addNewMainInvoiceDiscountDetail($employee_insert);
            //                 if($alumni_inserted_id)
            //                 {
            //                     $total_amount = $total_amount - $alumni_discount_data->amount;
            //                     $alumni_discount_amount = $alumni_discount_data->amount;
            //                 }
            //             }
            //         }


            //         $total_discount_amount = $sibling_discount_amount + $employee_discount_amount + $alumni_discount_amount;
            //         // $total_amount = number_format($total_amount, 2, '.', ',');
            //         // echo "<Pre>";print_r($total_amount);exit;

            //         $invoice_update['total_amount'] = $total_amount;
            //         $invoice_update['balance_amount'] = $total_amount;
            //         $invoice_update['invoice_total'] = $total_invoice_amount;
            //         $invoice_update['total_discount'] = $total_discount_amount;
            //         $invoice_update['paid_amount'] = '0';
            //         // $invoice_update['inserted_id'] = $inserted_id;
            //         // echo "<Pre>";print_r($invoice_update);exit;
            //         $this->editMainInvoice($invoice_update,$inserted_id);
            //     }
            // }


        }

        return TRUE;
    }


    function getFeeStructureByTrainingCenterForInvoiceGeneration($id_programme,$id_intake,$id_fee_structure,$id_training_center)
    {
        $this->db->select('p.*, fstp.name as trigger_name');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id','left'); 
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id','left'); 
        // $this->db->where('p.id_programme', $id_programme);
        // $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_landscape', $id_fee_structure);
        $this->db->where('p.id_training_center', $id_training_center);
        $query = $this->db->get();
        $fee_structure = $query->result();

        return $fee_structure;
    }



    function getFeeStructureForTrainingCenterInstallment($id_programme,$id_intake,$id_fee_structure,$id_training_center)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id','left'); 
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'fs.id_fee_structure_trigger = fstp.id','left'); 
        // $this->db->where('p.id_programme', $id_programme);
        // $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_landscape', $id_fee_structure);
        $this->db->where('p.id_training_center', $id_training_center);
        $this->db->order_by("p.id", "desc");
        $query = $this->db->get();
        $fee_structure = $query->row();

        return $fee_structure;
    }

    function getFeeStructureForLocalStudentsForLandscape($id_programme,$id_intake,$id_program_landscape,$currency)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id'); 
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id'); 
        // $this->db->where('p.id_programme', $id_programme);
        // $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_landscape', $id_program_landscape);
        // $this->db->where('p.id_program_scheme', $id_program_scheme);
        // $this->db->where('p.id_training_center', 0);
        $this->db->where('p.currency', $currency);
        // $this->db->where('fm.code', 'ONE TIME');
        // $this->db->or_where('fm.code', 'APPLICATION');
        $query = $this->db->get();
        $fee_structure = $query->result();
        // $detail_data = $this->getFeeStructureDetails($fee_structure->id);
        return $fee_structure;
    }


    function getFeeStructure($id_programme,$id_intake,$id_fee_structure,$currency)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id'); 
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id'); 
        // $this->db->where('p.id_programme', $id_programme);
        // $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_landscape', $id_fee_structure);
        $this->db->where('p.id_training_center', 0);
        $this->db->where('p.currency', $currency);
        $this->db->where('fm.code', 'ONE TIME');
        $this->db->or_where('fm.code', 'APPLICATION');
        $query = $this->db->get();
        $fee_structure = $query->result();
        // $detail_data = $this->getFeeStructureDetails($fee_structure->id);
        return $fee_structure;
    }

    function getFeeStructureDetails($id_fee_structure)
    {
        $this->db->select('tfsd.*, fs.name as fee_structure, fm.name as frequency_mode');
        $this->db->from('fee_structure_details as tfsd');
        $this->db->join('fee_setup as fs', 'tfsd.id_fee_item = fs.id');   
        $this->db->join('frequency_mode as fm', 'tfsd.id_frequency_mode = fm.id');   
        $this->db->where('tfsd.id_fee_structure', $id_fee_structure);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        // return $insert_id;
    }

    function addNewMainInvoiceDiscountDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_discount_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function getApplicantSibblingDiscountDetails($id_applicant)
    {
        $this->db->select('ahsd.*, usr.name as user_name');
        $this->db->from('applicant_has_sibbling_discount as ahsd');
        $this->db->join('tbl_users as usr', 'ahsd.rejected_by = usr.userId','left'); 
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }

    function getApplicantEmployeeDiscountDetails($id_applicant)
    {
        $this->db->select('ahemd.*, usr.name as user_name');
        $this->db->from('applicant_has_employee_discount as ahemd');
        $this->db->join('tbl_users as usr', 'ahemd.rejected_by = usr.userId','left'); 
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }

    function getApplicantAlumniDiscountDetails($id_applicant)
    {
        $this->db->select('ahemd.*, usr.name as user_name');
        $this->db->from('applicant_has_alumni_discount as ahemd');
        $this->db->join('tbl_users as usr', 'ahemd.rejected_by = usr.userId','left'); 
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }    

    function raceList()
    {
        $this->db->select('*');
        $this->db->from('race_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function religionList()
    {
        $this->db->select('*');
        $this->db->from('religion_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function salutationListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('salutation_setup as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function countryListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('country as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function stateListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('state as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function branchListByStatus()
    {
        $organisation = $this->getOrganisaton();
        $details = array();

        if($organisation)
        {
            array_push($details, $organisation);
        }


        $this->db->select('a.*');
        $this->db->from('organisation_has_training_center as a');
        $query = $this->db->get();
        $result = $query->result();  

        foreach ($result as $value)
        {
           array_push($details, $value);
        }
        return $details;
    }

    function getOrganisaton()
    {
        $this->db->select('a.*, a.short_name as code');
        $this->db->from('organisation as a');
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function programRequiremntListList()
    {
        $this->db->select('ier.*, qs.code as qualification_code, qs.name as qualification_name, ws.code as work_code, ws.name as work_name');
        $this->db->from('scholarship_individual_entry_requirement as ier');
        $this->db->join('scholarship_education_level as qs','ier.id_education_qualification = qs.id','left');
        $this->db->join('scholarship_work_specialisation as ws','ier.id_work_specialisation = ws.id','left');
        $query = $this->db->get();
        return $query->result();
    }

    function getProgramDetails($id_program)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme as ihs');
        $this->db->where('ihs.id', $id_program);
        $query = $this->db->get();
        return $query->row();
    }

    function programEntryRequirementList($id_program)
    {
        $this->db->select('ier.*, qs.code as qualification_code, qs.name as qualification_name, ws.code as work_code, ws.name as work_name');
        $this->db->from('scholarship_individual_entry_requirement as ier');
        $this->db->join('scholarship_education_level as qs','ier.id_education_qualification = qs.id','left');
        $this->db->join('scholarship_work_specialisation as ws','ier.id_work_specialisation = ws.id','left');
        $this->db->where('ier.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function getApplicantUploadedFiles($id_applicant)
    {
        $this->db->select('shd.*, d.code as document_code, d.name as document_name');
        $this->db->from('applicant_has_document as shd');
        $this->db->join('documents as d','shd.id_document = d.id');
        $this->db->where('shd.id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->result();
    }   

    function schemeListByStatus($status)
    {
        $this->db->select('ihs.*');
        $this->db->from('scheme as ihs');
        $this->db->where('ihs.status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function getUniversityListByStatus($status)
    {
        $organisation = $this->getOrganisaton();
        $details = array();

        if($organisation)
        {
            array_push($details, $organisation);
        }

        $this->db->select('ahemd.*');
        $this->db->from('partner_university as ahemd');
        $this->db->where('ahemd.status', $status);
        $query = $this->db->get();
        $results = $query->result();

        foreach ($results as $result)
        {
            array_push($details, $result);
        }
        return $details;
    } 

    function getApplicantDetailsById($id)
    {
        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $sd = $query->row();
        return $sd;
    }

    function getfeeStructureMasterByApplicant($id_applicant)
    {
        $applicant = $this->getApplicantDetailsById($id_applicant);

        $id_university = $applicant->id_university;
        
        // echo "<Pre>";print_r($id_university);exit();

        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id_education_level', $applicant->id_degree_type);
        $this->db->where('id_intake', $applicant->id_intake);
        $this->db->where('id_programme', $applicant->id_program);
        $this->db->where('id_learning_mode', $applicant->id_program_scheme);
        // $this->db->where('id_program_scheme', $applicant->id_program_has_scheme);
        // $this->db->where('id_partner_university', $applicant->id_university);
        $this->db->where('status', '1');
        $query = $this->db->get();

        $result = $query->row();

        // echo "<Pre>";print_r($result);exit();


        if($result)
        {
            $id_fee_structure = $result->id;
            
            // if($id_university == 1)
            // { 
                $nationality = $applicant->nationality;
                $currency = 'MYR';

                if($nationality == '1')
                {
                    $currency = 'MYR';
                }
                else
                {
                    $currency = 'USD';
                }

                // echo "<Pre>";print_r($nationality);exit();

                $fee_structure = $this->getfeeStructureDetailsByIdFeeStructureMaster($id_fee_structure,$currency);
                
                // echo "<Pre>";print_r($fee_structure);exit();
                
                return $fee_structure;
            
            // }

            // else
            // {
            //     $fee_structure = $this->getfeeStructureDetailsByIdFeeStructureMasterForPartneruniversity($id_fee_structure,$id_university);

            //     $details = array();

            //     foreach ($fee_structure as $value)
            //     {
            //         // echo "<Pre>";print_r($value);exit();
            //         $is_installment = $value->is_installment;
            //         $data['id_fee_structure'] = $value->id;
            //         $data['id_fee_structure_master'] = $value->id_program_landscape;

            //         if($is_installment == 1)
            //         {
            //             $total_amount = 0;

            //             $installment_details = $this->getTrainingCenterInstallmentDetails($data);

            //             foreach ($installment_details as $detail)
            //             {
            //                $total_amount = $total_amount + $detail->amount; 
            //             }

            //             $value->amount = $total_amount;
            //         }

            //         array_push($details, $value);
            //     }

            //     return $details;
            // }
        }
        else
        {
            return array();
        }
    }

    function getfeeStructureDetailsByIdFeeStructureMaster($id_fee_structure_master,$currency)
    {

        $this->db->select('fst.*, fs.name as fee_structure, fs.code as fee_structure_code, fm.name as frequency_mode, fstp.name as trigger_name, cs.name as currency_name');
        $this->db->from('fee_structure as fst');
        $this->db->join('fee_setup as fs', 'fst.id_fee_item = fs.id','left');   
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'fst.id_fee_structure_trigger = fstp.id','left'); 
        $this->db->join('currency_setup as cs', 'fst.currency = cs.id','left'); 
        $this->db->where('fst.id_program_landscape', $id_fee_structure_master);
        $this->db->where('fst.currency', $currency);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->result();
        // echo "<Pre>";print_r($result);exit();
        return $result;
    }

    function getFeeStructureByData($data)
    {
        $this->db->select('fst.*, fs.name as fee_structure, fs.code as fee_structure_code, fm.name as frequency_mode, fstp.name as trigger_name, cs.name as currency_name');
        $this->db->from('fee_structure as fst');
        $this->db->join('fee_setup as fs', 'fst.id_fee_item = fs.id');   
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'fst.id_fee_structure_trigger = fstp.id','left'); 
        $this->db->join('currency_setup as cs', 'fst.currency = cs.id','left'); 
        $this->db->where('fst.id_training_center', $data['id_training_center']);
        $this->db->where('fst.id_program_landscape', $data['id_fee_structure']);
        $this->db->where('fst.currency', $data['currency']);
        $this->db->where('fstp.name', $data['trigger']);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->result();
        // echo "<Pre>";print_r($result);exit();
        return $result;
    }

    function getfeeStructureDetailsByIdFeeStructureMasterForPartneruniversity($id_fee_structure_master,$id_university)
    {
        $this->db->select('fst.*, fs.name as fee_structure, fs.code as fee_structure_code, fm.name as frequency_mode, fstp.name as trigger_name, cs.name as currency_name');
        $this->db->from('fee_structure as fst');
        $this->db->join('fee_setup as fs', 'fst.id_fee_item = fs.id','left');   
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'fst.id_fee_structure_trigger = fstp.id','left'); 
        // $this->db->join('fee_structure as fss', 'fst.id_fee_structure = fss.id','left'); 
        $this->db->join('currency_setup as cs', 'fst.currency = cs.id','left'); 
        $this->db->where('fst.id_program_landscape', $id_fee_structure_master);
        $this->db->where('fst.id_training_center', $id_university);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->result();
        // echo "<Pre>";print_r($result);exit();
        
        return $result;
    }


    function getTrainingCenterInstallmentDetails($data)
    {
        $this->db->select('p.*, sem.name as fee_name, sem.code as fee_code, fm.name as frequency_mode, fstp.name as trigger_name');
        $this->db->from('fee_structure_has_training_center as p');
        $this->db->join('fee_setup as sem', 'p.id_fee_item = sem.id');
        $this->db->join('frequency_mode as fm', 'sem.id_frequency_mode = fm.id');
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id');
        $this->db->where('p.id_fee_structure', $data['id_fee_structure']);
        $this->db->where('p.id_program_landscape', $data['id_fee_structure_master']);
         $query = $this->db->get();
         
         $result = $query->result();  
         return $result;
    }

    function getFeeStructureMasterIdByData($applicant)
    {

        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id_education_level', $applicant->id_degree_type);
        $this->db->where('id_intake', $applicant->id_intake);
        $this->db->where('id_programme', $applicant->id_program);
        $this->db->where('id_learning_mode', $applicant->id_program_scheme);
        $this->db->where('id_program_scheme', $applicant->id_program_has_scheme);
        $this->db->where('status', '1');
        $query = $this->db->get();

        $result = $query->row();

        if($result)
        {
            return $result->id;
        }
        else
        {
            return 0;
        }
    }

    
    function getSibblingDiscountByApplicantIdCurrency($id)
    {
        $applicant = $this->getApplicantDetailsById($id);

        if($applicant->nationality == '1')
        {
            $currency = 'MYR';
        }
        elseif($applicant->nationality != '')
        {
            $currency = 'USD';
        }

        $this->db->select('fst.*');
        $this->db->from('sibbling_discount as fst');
        $this->db->where('fst.currency', $currency);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getEmployeeDiscountByApplicantIdCurrency($id)
    {
        $applicant = $this->getApplicantDetailsById($id);

        if($applicant->nationality == '1')
        {
            $currency = 'MYR';
        }
        elseif($applicant->nationality != '')
        {
            $currency = 'USD';
        }

        $this->db->select('fst.*');
        $this->db->from('employee_discount as fst');
        $this->db->where('fst.currency', $currency);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getAlumniDiscountByApplicantIdCurrency($id)
    {
        $applicant = $this->getApplicantDetailsById($id);

        if($applicant->nationality == '1')
        {
            $currency = 'MYR';
        }
        elseif($applicant->nationality != '')
        {
            $currency = 'USD';

        }

        $this->db->select('fst.*');
        $this->db->from('alumni_discount as fst');
        $this->db->where('fst.currency', $currency);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }
}