<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Advisory_model extends CI_Model
{
    function advisoryList()
    {
        $this->db->select('*');
        $this->db->from('research_advisory');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function advisoryListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('research_advisory');
        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAdvisory($id)
    {
        $this->db->select('*');
        $this->db->from('research_advisory');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewAdvisory($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_advisory', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editAdvisory($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_advisory', $data);
        return TRUE;
    }
}

