<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Examiner extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('examiner_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_examiner.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $data['searchParam'] = $formData;

            $data['examinerList'] = $this->examiner_model->examinerListSearch($formData);

            $this->global['pageTitle'] = 'College Management System : Research Examiner List';
            $this->loadViews("examiner/list", $this->global, $data, NULL);
        }
    }

    function internalList()
    {
        if ($this->checkAccess('research_examiner.internal_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['type'] = '1';
            $data['searchParam'] = $formData;

            $data['examinerList'] = $this->examiner_model->examinerListSearch($formData);

            $this->global['pageTitle'] = 'College Management System : Internal Research Examiner List';
            $this->loadViews("examiner/internal_list", $this->global, $data, NULL);
        }
    }

    function externalList()
    {
        if ($this->checkAccess('research_examiner.external_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['type'] = '0';
            $data['searchParam'] = $formData;

            $data['examinerList'] = $this->examiner_model->examinerListSearch($formData);

            $this->global['pageTitle'] = 'College Management System : External Research Examiner List';
            $this->loadViews("examiner/external_list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_examiner.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $full_name = $this->security->xss_clean($this->input->post('full_name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $contact_no = $this->security->xss_clean($this->input->post('contact_no'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                if($type == 1)
                {
                    $staff = $this->examiner_model->getStaff($id_staff);                    

                    $full_name = '';
                    $email = '';
                    $password = '';
                    $contact_no = '';

                    if($staff)
                    {
                        $full_name = $staff->name;
                        $email = $staff->email;
                        $contact_no = $staff->phone_number;

                    }

                }
                elseif($type == 0)
                {
                    $id_staff = 0;
                }

                $data = array(
                    'type' =>$type,
                    'id_staff' => $id_staff,
                    'full_name' => $full_name,
                    'email' => $email,
                    'password' => $password,
                    'contact_no' => $contact_no,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'status' => 1
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->examiner_model->addNewExaminer($data);
                redirect('/research/examiner/list');
            }
            
            $data['staffList'] = $this->examiner_model->staffListByStatus('1');

            $this->global['pageTitle'] = 'College Management System : Add Research Examiner';
            $this->loadViews("examiner/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_examiner.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/examiner/list');
            }
            if($this->input->post())
            {
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $full_name = $this->security->xss_clean($this->input->post('full_name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $contact_no = $this->security->xss_clean($this->input->post('contact_no'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                if($type == 1)
                {
                    $staff = $this->examiner_model->getStaff($id_staff);                    

                    // echo "<Pre>"; print_r($staff);exit;

                    $full_name = '';
                    $email = '';
                    $password = '';
                    $contact_no = '';

                    if($staff)
                    {
                        $full_name = $staff->name;
                        $email = $staff->email;
                        $contact_no = $staff->phone_number;

                    }

                }
                elseif($type == 0)
                {
                    $id_staff = 0;
                }


                $data = array(
                    'type' =>$type,
                    'id_staff' => $id_staff,
                    'full_name' => $full_name,
                    'email' => $email,
                    'password' => $password,
                    'contact_no' => $contact_no,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'status' => 1
                );

                $result = $this->examiner_model->editExaminer($data,$id);
                redirect('/research/examiner/list');
            }

            $data['staffList'] = $this->examiner_model->staffListByStatus('1');
            $data['examiner'] = $this->examiner_model->getExaminer($id);
            $this->global['pageTitle'] = 'College Management System : Edit Research Examiner';
            $this->loadViews("examiner/edit", $this->global, $data, NULL);
        }
    }
}
