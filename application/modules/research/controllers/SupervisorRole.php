<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SupervisorRole extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('supervisor_role_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_supervisor_role.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;
            $data['supervisorRoleList'] = $this->supervisor_role_model->supervisorRoleListSearch($formData);
            
            $this->global['pageTitle'] = 'College Management System : Research Supervisor Role List';
            $this->loadViews("supervisor_role/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_supervisor_role.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $max_candidates = $this->security->xss_clean($this->input->post('max_candidates'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'max_candidates' => $max_candidates,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->supervisor_role_model->addNewSupervisorRole($data);
                redirect('/research/supervisorRole/list');
            }
            $this->global['pageTitle'] = 'College Management System : Add Research Supervisor Role';
            $this->loadViews("supervisor_role/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_supervisor_role.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/supervisorRole/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $max_candidates = $this->security->xss_clean($this->input->post('max_candidates'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'max_candidates' => $max_candidates,
                    'status' => $status
                );

                $result = $this->supervisor_role_model->editSupervisorRole($data,$id);
                redirect('/research/supervisorRole/list');
            }
            $data['supervisorRole'] = $this->supervisor_role_model->getSupervisorRole($id);

            $this->global['pageTitle'] = 'College Management System : Edit Research Supervisor Role';
            $this->loadViews("supervisor_role/edit", $this->global, $data, NULL);
        }
    }
}
