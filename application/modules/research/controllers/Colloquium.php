<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Colloquium extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('colloquium_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_colloquium.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['attendence'] = 0;
            $data['searchParam'] = $formData;

            $data['colloquiumList'] = $this->colloquium_model->colloquiumListSearch($formData);
            $this->global['pageTitle'] = 'College Management System : Research Colloquium List';
            $this->loadViews("colloquium/list", $this->global, $data, NULL);
        }
    }

    function registrationList()
    {
        if ($this->checkAccess('research_colloquium.registration_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['attendence'] = 1;
            $data['searchParam'] = $formData;

            $data['colloquiumList'] = $this->colloquium_model->colloquiumListSearch($formData);
            $this->global['pageTitle'] = 'College Management System : Research Colloquium List';
            $this->loadViews("colloquium/registration_list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_colloquium.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));
                $to_date = $this->security->xss_clean($this->input->post('to_date'));
                $mode = $this->security->xss_clean($this->input->post('mode'));
                $attendance = $this->security->xss_clean($this->input->post('attendance'));
                $registration_start_date = $this->security->xss_clean($this->input->post('registration_start_date'));
                $registration_end_date = $this->security->xss_clean($this->input->post('registration_end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'url' => $url,
                    'mode' => $mode,
                    'attendance' => $attendance,
                    'date_time' => date('Y-m-d', strtotime($date_time)),
                    'to_date' => date('Y-m-d', strtotime($to_date)),
                    'registration_start_date' => date('Y-m-d', strtotime($registration_start_date)),
                    'registration_end_date' => date('Y-m-d', strtotime($registration_end_date)),
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->colloquium_model->addNewColloquium($data);

                if($result)
                {
                    $moved_temp_details = $this->colloquium_model->moveTempToDetails($result);
                }

                redirect('/research/colloquium/list');
            }
            else
            {
                $deleted_temp_data = $this->colloquium_model->deleteTempColloquiumDetailsBySessionId($id_session);
            }
            
            $data['programList'] = $this->colloquium_model->programListForPostgraduate('POSTGRADUATE');

            $this->global['pageTitle'] = 'College Management System : Add Research Colloquium';
            $this->loadViews("colloquium/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_colloquium.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/colloquium/list');
            }
            
            
            if($this->input->post())
            {

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));
                $to_date = $this->security->xss_clean($this->input->post('to_date'));
                $mode = $this->security->xss_clean($this->input->post('mode'));
                $attendance = $this->security->xss_clean($this->input->post('attendance'));
                $registration_start_date = $this->security->xss_clean($this->input->post('registration_start_date'));
                $registration_end_date = $this->security->xss_clean($this->input->post('registration_end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'url' => $url,
                    'mode' => $mode,
                    'attendance' => $attendance,
                    'date_time' => date('Y-m-d', strtotime($date_time)),
                    'to_date' => date('Y-m-d', strtotime($to_date)),
                    'registration_start_date' => date('Y-m-d', strtotime($registration_start_date)),
                    'registration_end_date' => date('Y-m-d', strtotime($registration_end_date)),
                    'status' => $status
                );

                $result = $this->colloquium_model->editColloquium($data,$id);
                redirect('/research/colloquium/list');
            }
            

            $data['programList'] = $this->colloquium_model->programListForPostgraduate('POSTGRADUATE');
            $data['colloquium'] = $this->colloquium_model->getColloquium($id);
            $data['getColloquiumDetailsByColloquiumId'] = $this->colloquium_model->getColloquiumDetailsByColloquiumId($id);
            
            $this->global['pageTitle'] = 'College Management System : Edit Research Colloquium';
            $this->loadViews("colloquium/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('research_colloquium.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/colloquium/list');
            }
            
            
            if($this->input->post())
            {

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));
                $to_date = $this->security->xss_clean($this->input->post('to_date'));
                $mode = $this->security->xss_clean($this->input->post('mode'));
                $attendance = $this->security->xss_clean($this->input->post('attendance'));
                $registration_start_date = $this->security->xss_clean($this->input->post('registration_start_date'));
                $registration_end_date = $this->security->xss_clean($this->input->post('registration_end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'url' => $url,
                    'mode' => $mode,
                    'attendance' => $attendance,
                    'date_time' => date('Y-m-d', strtotime($date_time)),
                    'to_date' => date('Y-m-d', strtotime($to_date)),
                    'registration_start_date' => date('Y-m-d', strtotime($registration_start_date)),
                    'registration_end_date' => date('Y-m-d', strtotime($registration_end_date)),
                    'status' => $status
                );

                $result = $this->colloquium_model->editColloquium($data,$id);
                redirect('/research/colloquium/list');
            }
            

            $data['programList'] = $this->colloquium_model->programListForPostgraduate('POSTGRADUATE');
            $data['colloquium'] = $this->colloquium_model->getColloquium($id);
            $data['getColloquiumDetailsByColloquiumId'] = $this->colloquium_model->getColloquiumDetailsByColloquiumId($id);
            
            $this->global['pageTitle'] = 'College Management System : View Research Colloquium';
            $this->loadViews("colloquium/view", $this->global, $data, NULL);
        }
    }

    function registerStudents($id = NULL)
    {
        if ($this->checkAccess('research_colloquium.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/colloquium/registrationList');
            }
            
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;


            if($this->input->post())
            {

                // echo "<Pre>"; print_r($this->input->post());exit;

                $id_student = $this->security->xss_clean($this->input->post('id_student'));

                for($i=0;$i<count($id_student);$i++)
                {

                    $data = array(
                        'id_colloquium' => $id,
                        'id_student' => $id_student[$i],
                        'by_student' => 0,
                        'date_time' => date('Y-m-d h:i:s'),
                        'status' => 1,
                        'created_by' => $id_user
                    );

                    $result = $this->colloquium_model->addColloquiumInterest($data);

                }
                redirect('/research/colloquium/registrationList');
            }
            

            $data['programList'] = $this->colloquium_model->programListForPostgraduate('POSTGRADUATE');
            $data['colloquium'] = $this->colloquium_model->getColloquium($id);
            $data['getColloquiumDetailsByColloquiumId'] = $this->colloquium_model->getColloquiumDetailsByColloquiumId($id);
            $data['getColloquiumInterestedStudents'] = $this->colloquium_model->getColloquiumInterestedStudents($id);
            
            $this->global['pageTitle'] = 'College Management System : View Research Colloquium';
            $this->loadViews("colloquium/register_students", $this->global, $data, NULL);
        }

    }


    function addAttendance($id = NULL)
    {
        if ($this->checkAccess('research_colloquium.add_attendance') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/colloquium/registrationList');
            }
            
            
            if($this->input->post())
            {
                
                echo "<Pre>"; print_r($this->input->post());exit;

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));
                $to_date = $this->security->xss_clean($this->input->post('to_date'));
                $mode = $this->security->xss_clean($this->input->post('mode'));
                $attendance = $this->security->xss_clean($this->input->post('attendance'));
                $registration_start_date = $this->security->xss_clean($this->input->post('registration_start_date'));
                $registration_end_date = $this->security->xss_clean($this->input->post('registration_end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'url' => $url,
                    'mode' => $mode,
                    'attendance' => $attendance,
                    'date_time' => date('Y-m-d', strtotime($date_time)),
                    'to_date' => date('Y-m-d', strtotime($to_date)),
                    'registration_start_date' => date('Y-m-d', strtotime($registration_start_date)),
                    'registration_end_date' => date('Y-m-d', strtotime($registration_end_date)),
                    'status' => $status
                );

                $result = $this->colloquium_model->editColloquium($data,$id);
                redirect('/research/colloquium/registrationList');
            }
            

            $data['programList'] = $this->colloquium_model->programListForPostgraduate('POSTGRADUATE');
            $data['colloquium'] = $this->colloquium_model->getColloquium($id);
            $data['getColloquiumDetailsByColloquiumId'] = $this->colloquium_model->getColloquiumDetailsByColloquiumId($id);
            $data['getColloquiumInterestedStudents'] = $this->colloquium_model->getColloquiumInterestedStudents($id);


            // echo "<Pre>"; print_r($data['getColloquiumInterestedStudents']);exit;
            
            $this->global['pageTitle'] = 'College Management System : Add Attendance Research Colloquium';
            $this->loadViews("colloquium/attendence", $this->global, $data, NULL);
        }

    }


    function getIntakeByProgramId($id_program)
    {

        $student_list_data = $this->colloquium_model->getIntakeListByProgramme($id_program);
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_intake' id='id_intake' class='form-control'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id_intake;
        $intake_name = $student_list_data[$i]->intake_name;
        $intake_year = $student_list_data[$i]->intake_year;

        $table.="<option value=".$id.">". $intake_year . " - " . $intake_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function tempAddColloquiumDetails()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;

        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->colloquium_model->tempAddColloquiumDetails($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;

        $data = $this->displayColloquiumDetails();
        
        echo $data;        
    }

    function displayColloquiumDetails()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->colloquium_model->getTempColloquiumDetailsBySession($id_session); 
        
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Programme</th>
                    <th>Intake</th>
                    <th>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $programme_code = $temp_details[$i]->programme_code;
                    $programme_name = $temp_details[$i]->programme_name;
                    $intake_name = $temp_details[$i]->intake_name;
                    $intake_year = $temp_details[$i]->intake_year;

                    // if($status == 1)
                    // {
                    //     $status = 'Active';
                    // }else
                    // {
                    //     $status = 'In-Active';
                    // }

                    $j = $i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>                       
                            <td>$programme_code - $programme_name</td>
                            <td>$intake_year - $intake_name</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempColloquiumDetails($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
            // <span onclick='deleteTempProposalHasProgramme($id)'>Delete</a>                            
            $table.= "
            </tbody>
            </table>
            </div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempColloquiumDetails($id)
    {
        $inserted_id = $this->colloquium_model->deleteTempColloquiumDetails($id);
        if($inserted_id)
        {
            $data = $this->displayColloquiumDetails();
            echo $data;  
        }
    }

    function addColloquiumDetails()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->colloquium_model->addColloquiumDetails($tempData);
        echo "<Pre>";print_r($inserted_id);exit();

        echo "success";exit;
    }

    function deleteColloquiumDetails($id)
    {
        $inserted_id = $this->colloquium_model->deleteColloquiumDetails($id);
        echo "Success"; 
    }

    function searchStudentsByData()
    {


        $tempData = $this->security->xss_clean($this->input->post('tempData'));
         // echo "<Pre>"; print_r($tempData);exit();

        $temp_details = $this->colloquium_model->searchStudentsByData($tempData);

        // echo "<Pre>"; print_r($temp_details);exit();



        $table = "

        <h4 class='sub-title'>Select Student For Colloquium Registration</h4>

        <div class='custom-table'><table class='table' id='list-table'>
                   <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Email Id</th>
                    <th>NRIC</th>
                    <th>Phone No.</th>
                    <th>Gender</th>
                    <th>Programme</th>
                    <th>Intake</th>
                    <th>Program Scheme</th>
                    <th>DOB</th>
                    <th class='text-center'>Select Student</th>
                </tr></thead><tbody>";
                
        if($temp_details!=NULL)
        {
            

                    for($i=0;$i<count($temp_details);$i++)
                    {
                        $j = $i+1;

                    $id_student = $temp_details[$i]->id;
                    $full_name = $temp_details[$i]->full_name;
                    $nric = $temp_details[$i]->nric;
                    $phone = $temp_details[$i]->phone;
                    $email_id = $temp_details[$i]->email_id;
                    $gender = $temp_details[$i]->gender;
                    $program_scheme = $temp_details[$i]->program_scheme;
                    $date_of_birth = $temp_details[$i]->date_of_birth;
                    $program_name = $temp_details[$i]->program_name;
                    $program_code = $temp_details[$i]->program_code;
                    $intake_name = $temp_details[$i]->intake_name;


                    if($date_of_birth)
                    {
                        $date_of_birth = date('d-m-Y', strtotime($date_of_birth));
                    }


                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$full_name</td>
                            <td>$email_id</td>
                            <td>$nric</td>
                            <td>$phone</td>
                            <td>$gender</td>
                            <td>$program_code - $program_name</td>
                            <td>$intake_name</td>
                            <td>$program_scheme</td>
                            <td>$date_of_birth</td>
                            <td class='text-center'><input type='checkbox' name='id_student[]' id='id_student' value='$id_student'></td>
                        </tr>";
                    }
                $table.= "</tbody></table></div>";

        }
        
        echo $table;
    }
}
