<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Toc extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('toc_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if($this->checkAccess('research_toc.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_supervisor'] = $this->security->xss_clean($this->input->post('id_supervisor'));

            $data['searchParam'] = $formData;

            $data['tocList'] = $this->toc_model->tocListSearch($formData);

            $data['supervisorList'] = $this->toc_model->supervisorListByStatus('1');

            // echo "<Pre>";print_r($data['tocList']);exit();

            $this->global['pageTitle'] = 'College Management System : List TOC Reporting';
            $this->loadViews("toc/list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if($this->checkAccess('research_toc.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($id == null)
            {
                redirect('/research/toc/list');
            }
            $data['toc'] = $this->toc_model->getToc($id);
            $data['tocReportingComments'] = $this->toc_model->tocCommentsDetails($id);

            $data['organisationDetails'] = $this->toc_model->getOrganisation();
            $data['supervisor'] = $this->toc_model->getSupervisor($data['toc']->id_supervisor);
            $data['studentDetails'] = $this->toc_model->getStudentByStudentId($data['toc']->id_student);
                
            // echo "<Pre>"; print_r($data['tocCommentsDetails']);exit;

            $this->global['pageTitle'] = 'College Management System : View TOC Reporting';
            $this->loadViews("toc/edit", $this->global, $data, NULL);
        }
    }
}