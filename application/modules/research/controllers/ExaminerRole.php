<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExaminerRole extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('examiner_role_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_examiner_role.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;
            $data['examinerRoleList'] = $this->examiner_role_model->examinerRoleListSearch($formData);
            
            $this->global['pageTitle'] = 'College Management System : Research Supervisor Role List';
            $this->loadViews("examiner_role/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_examiner_role.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->examiner_role_model->addNewExaminerRole($data);
                redirect('/research/examinerRole/list');
            }
            $this->global['pageTitle'] = 'College Management System : Add Research Supervisor Role';
            $this->loadViews("examiner_role/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_examiner_role.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/examinerRole/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'status' => $status
                );

                $result = $this->examiner_role_model->editExaminerRole($data,$id);
                redirect('/research/examinerRole/list');
            }
            $data['examinerRole'] = $this->examiner_role_model->getExaminerRole($id);

            $this->global['pageTitle'] = 'College Management System : Edit Research Supervisor Role';
            $this->loadViews("examiner_role/edit", $this->global, $data, NULL);
        }
    }
}
