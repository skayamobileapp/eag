<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apply_change_status_model extends CI_Model
{
    
    function applyChangeStatusList()
    {
        $this->db->select('b.*, stu.first_name as student, sem.name as semester, bt.name as change_status');
        $this->db->from('apply_change_status as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('semester as sem', 'b.id_semester = sem.id');
        $this->db->join('change_status as bt', 'b.id_change_status = bt.id');
        $this->db->order_by("b.id", "DESC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function applyChangeStatusListSearch($formData)
    {
        $this->db->select('b.*, stu.full_name as student_name, stu.nric, sem.name as semester, bt.name as change_status, p.name as programme_name, p.code as programme_code, i.name as intake_name');
        $this->db->from('apply_change_status as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('semester as sem', 'b.id_semester = sem.id');
        $this->db->join('change_status as bt', 'b.id_change_status = bt.id');
        $this->db->join('programme as p', 'stu.id_program = p.id');
        $this->db->join('intake as i', 'stu.id_intake = i.id');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(b.reason  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_semester']))
        {
            $likeCriteria = "(b.id_semester  LIKE '%" . $formData['id_semester'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_student']))
        {
            $likeCriteria = "(b.id_student  LIKE '%" . $formData['id_student'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_change_status']))
        {
            $likeCriteria = "(b.id_change_status  LIKE '%" . $formData['id_change_status'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("b.id", "DESC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function getApplyChangeStatus($id)
    {
        $this->db->select('*');
        $this->db->from('apply_change_status');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($query);die;
        return $result;
    }
    

    function addNewApplyChangeStatus($data)
    {
        $this->db->trans_start();
        $this->db->insert('apply_change_status', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editApplyChangeStatus($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('apply_change_status', $data);

        return TRUE;
    }


       function updateData($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('apply_change_status', $data);

        return TRUE;
    }

    function deleteApplyChangeStatus($id, $stateInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('state', $stateInfo);

        return $this->db->affected_rows();
    }

    function studentList()
    {
        $this->db->select('*, full_name as name');
        // $this->db->from('student');
        $this->db->from('student');
        $this->db->order_by("first_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function applyChangeStatusListForApprovalSearch($formData)
    {
        $this->db->select('b.*, stu.full_name as student, stu.nric, sem.name as semester, bt.name as change_status, p.name as programme_name, p.code as programme_code, i.name as intake_name');
        $this->db->from('apply_change_status as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('semester as sem', 'b.id_semester = sem.id');
        $this->db->join('change_status as bt', 'b.id_change_status = bt.id');
        $this->db->join('programme as p', 'stu.id_program = p.id');
        $this->db->join('intake as i', 'stu.id_intake = i.id');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(b.reason  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_semester']))
        {
            $likeCriteria = "(b.id_semester  LIKE '%" . $formData['id_semester'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_student']))
        {
            $likeCriteria = "(b.id_student  LIKE '%" . $formData['id_student'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_change_status']))
        {
            $likeCriteria = "(b.id_change_status  LIKE '%" . $formData['id_change_status'] . "%')";
            $this->db->where($likeCriteria);
        }
        $where_pending = "(b.status  = '0')";
        $this->db->where($where_pending);
        $this->db->order_by("b.id", "DESC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function semesterList()
    {
        $this->db->select('s.*, ay.name as academic_year');
        $this->db->from('semester as s');
        $this->db->join('academic_year as ay', 'ay.id = s.id_academic_year');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getStudentByProgrammeId($id_programme)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_program', $id_programme);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getProgrammeById($id_programme)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id', $id_programme);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getFeeStructureActivityType($type,$trigger,$id_program)
    {
        $this->db->select('s.*');
        $this->db->from('fee_structure_activity as s');
        $this->db->join('activity_details as a', 's.id_activity = a.id');
        $this->db->where('a.name', $type);
        $this->db->where('s.trigger', $trigger);
        $this->db->where('s.id_program', $id_program);
        $this->db->where('s.status', 1);
        $this->db->order_by('s.id', 'DESC');
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function generateMainInvoice($data,$id_apply_change_status)
    {
        $user_id = $this->session->userId;


        $id_student = $data['id_student'];
        $add = $data['add'];

        $student_data = $this->getStudent($id_student);

        $nationality = $student_data->nationality;
        $id_program = $student_data->id_program;
        $id_intake = $student_data->id_intake;

        if($add == 1)
        {
            $fee_structure_data = $this->getFeeStructureActivityType('CHANGE STATUS','Application Level',$id_program);
        }
        elseif($add == 0)
        {
            $fee_structure_data = $this->getFeeStructureActivityType('CHANGE STATUS','Approval Level',$id_program);
        }




        if($fee_structure_data)
        {



            $currency = $fee_structure_data->id_currency;
            $invoice_amount = $fee_structure_data->amount_local;



        // if($nationality == 'Malaysian')
        // {
        //     $currency = 'MYR';
        //     $invoice_amount = $fee_structure_data->amount_local;
        // }
        // elseif($nationality == 'Other')
        // {
        //     $currency = 'USD';
        //     $invoice_amount = $fee_structure_data->amount_international;
        // }



            $invoice_number = $this->generateMainInvoiceNumber();


            $invoice['invoice_number'] = $invoice_number;
            $invoice['type'] = 'Student';
            $invoice['remarks'] = 'Student Apply Change Status';
            $invoice['id_application'] = '0';
            $invoice['id_program'] = $id_program;
            $invoice['id_intake'] = $id_intake;
            $invoice['id_student'] = $id_student;
            $invoice['id_student'] = $id_student;
            $invoice['currency'] = $currency;
            $invoice['total_amount'] = $invoice_amount;
            $invoice['invoice_total'] = $invoice_amount;
            $invoice['balance_amount'] = $invoice_amount;
            $invoice['paid_amount'] = '0';
            $invoice['status'] = '1';
            $invoice['created_by'] = $user_id;

            // $fee_structure_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme);

            
            // $update = $this->editStudentData($id_program_scheme,$id_program,$id_intake,$id_student);
            
            $inserted_id = $this->addNewMainInvoice($invoice);

            if($inserted_id)
            {
                $data = array(
                        'id_main_invoice' => $inserted_id,
                        'id_fee_item' => $fee_structure_data->id_fee_setup,
                        'amount' => $invoice_amount,
                        'status' => 1,
                        'quantity' => 1,
                        'price' => $invoice_amount,
                        'id_reference' => $id_apply_change_status,
                        'description' => 'APPLY CHANGE STATUS',
                        'created_by' => $user_id
                    );

                $this->addNewMainInvoiceDetails($data);
            }

        }
        
        return TRUE;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        // return $insert_id;
    }

    function getStudent($id_student)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }
}