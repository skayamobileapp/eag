<form id="form_apply_change_status" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Apply Change Status</h3>
        </div>
        <div class="form-container">
            <h4 class="form-group-title">Select Student For Apply Change Status</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" onchange="getStudentByProgramme(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Student <span class='error-text'>*</span></label>
                        <span id="student">
                          <select class="form-control" id='id_student' name='id_student'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>



            </div>

            
            <div id="view_programme_details"  style="display: none;">
                <table border="1px" style="width: 100%">
                    <tr>
                        <td colspan="4"><h5 style="text-align: center;">Programme Details</h5></td>
                    </tr>
                    <tr>
                        <th style="text-align: center;">Program Name</th>
                        <td style="text-align: center;"></td>
                        <th style="text-align: center;">Program Code</th>
                        <td style="text-align: center;"></td>
                    </tr>
                    <tr>
                        <th style="text-align: center;">Total Credit Hours</th>
                        <td style="text-align: center;"></td>
                        <th style="text-align: center;">Foundation</th>
                        <td style="text-align: center;"></td>
                    </tr>

                </table>
            </div>

            <div id="view_student_details"  style="display: none;">
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                                <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                                </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>     

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Change Status <span class='error-text'>*</span></label>
                        <select name="id_change_status" id="id_change_status" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($changeStatusList))
                            {
                                foreach ($changeStatusList as $record)
                                {?>
                                <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                                </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>   

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reason" name="reason">
                    </div>
                </div>  

                
            </div>


            <div class="row">
            </div>
        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
        </form>

<script>

    function getStudentByProgramme(id){

     $.get("/records/applyChangeStatus/getStudentByProgrammeId/"+id, function(data, status){
   
        $("#student").html(data);
        // $("#view_programme_details").html(data);
        // $("#view_programme_details").show();
    });
 }

 function getStudentByStudentId(id){

     $.get("/records/applyChangeStatus/getStudentByStudentId/"+id, function(data, status){
   
        $("#view_student_details").html(data);
        $("#view_student_details").show();
    });
 }

 



    $(document).ready(function()
    {
        $("#form_apply_change_status").validate(
        {
            rules:
            {
                id_student:
                {
                    required: true
                },
                id_programme:
                {
                    required: true
                },
                id_change_status:
                {
                    required: true
                },
                id_semester:
                {
                    required: true
                },
                reason:
                {
                    required: true
                }
            },
            messages:
            {
                id_student:
                {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_programme:
                {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_change_status:
                {
                    required: "<p class='error-text'>Select Change Status</p>",
                },
                id_semester:
                {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                reason:
                {
                    required: "<p class='error-text'>Reason Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>