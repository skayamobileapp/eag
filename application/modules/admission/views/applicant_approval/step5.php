 <div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <div class="page-title clearfix">
         <h3>Applicant Approval</h3>
         <a href="../list" class="btn btn-link">< Back</a>
      </div>
      <form id="form_applicant" action="" method="post" enctype="multipart/form-data">
         <div class="clearfix">
                <div id="wizard" class="wizard">
                    <div class="wizard__content">
                         <header class="wizard__header">
                        <div class="wizard__steps">
                           <nav class="steps">
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step1/<?php echo $getApplicantDetails->id;?>" class="step__text">Profile Details</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -start"></div>
    
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step2/<?php echo $getApplicantDetails->id;?>" class="step__text">Contact Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step3/<?php echo $getApplicantDetails->id;?>" class="step__text">Program Interest</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step4/<?php echo $getApplicantDetails->id;?>" class="step__text">Document Upload</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step5/<?php echo $getApplicantDetails->id;?>" class="step__text">Discount Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step6/<?php echo $getApplicantDetails->id;?>" class="step__text">Declaration Form</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
                             <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step7/<?php echo $getApplicantDetails->id;?>" class="step__text">Confirmation</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
                          </nav>
                        </div>
                      </header>
    
                      <div class="panels">
                        <div class="paneld">
                          
                           <div class="clearfix">
                            

                          




                                <div class="form-container">
                                        <h4 class="form-group-title">Discount Details</h4>

                                        <p>Please click on the discounts that you are interested (can be more than one selection) </p>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th style="width:10%">Sl. No</th>
                                                 <th style="width:10%">Name</th>
                                                 <th style="width:60%">Description</th>
                                                 <th style="width:20%">Date Range</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($discountDetails);$i++)
                                             { ?>
                                                <tr>
                                                <td>

                                                  <?php
                                                  $checked="";
                                                  for($k=0;$k<count($applicantDiscountDetails);$k++) {
                                                      if($applicantDiscountDetails[$k]->id_discount==$discountDetails[$i]->id) {
                                                        $checked="checked=checked";
                                                      }
                                                  }

                                                  ?>
                                                  <input type='checkbox' name='discount[]' value="<?php echo $discountDetails[$i]->id;?>"  disabled <?php echo $checked;?>


                                                  /><?php echo $i+1;?></td>
                                                <td><?php echo $discountDetails[$i]->name;?></td>
                                                 <td><?php echo $discountDetails[$i]->description;?></td>
                                                  <td><?php echo $discountDetails[$i]->start_date;?>  - <?php echo $discountDetails[$i]->end_date;?></td>
                                               

                                                 </tr>
                                              <?php
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>

                          
 <div class="form-container">
                             <h4 class="form-group-title">Approval Details</h4>
                             <div class="row">
                           <div class="col-sm-4">
                              <label>Verification Status <span class='error-text'>*</span></label>
                                    <select name="step5_status" id="step5_status" class="form-control" required>
                                      <option value="">Select</option>
                                       <option value="1" <?php if($getApplicantDetails->step5_status=='1') { echo "Selected=selected";} ?>>Verified</option>
                                       <option value="2" <?php if($getApplicantDetails->step5_status=='2') { echo "Selected=selected";} ?>>Rejected</option>
                                       <option value="3" <?php if($getApplicantDetails->step5_status=='3') { echo "Selected=selected";} ?>>Other Reason</option>
                                    </select>
                                
                           </div>
                       </div>
                                               <div class="row">

                           <div class="col-sm-8">
                              <div class="form-group">
                                 <label>Comments </label>
                                 <input type="text" class="form-control" id="step5_comments" name="step5_comments"  value="<?php echo $getApplicantDetails->step5_comments ?>">
                              </div>
                           </div>
                        </div>
                      </div>   

                           </div>              
                        </div>    
                      </div>
    
                      <div class="wizard__footer">
                        <a href="/admission/applicantApproval/step4/<?php echo $getApplicantDetails->id;?>" class="btn btn-primary">Previous</a>

                        <button class="btn btn-link mr-3"></button>
                        <button class="btn btn-primary next" type="submit">Save & Continue</button>
                      </div>
                    </div>
    
                  </div>
                <footer class="footer-wrapper">
                    <p>&copy; 2019 All rights, reserved</p>
                </footer>
            </div>        
        </div>
    </div>      
    </form>
  </div>
</div>
    
 