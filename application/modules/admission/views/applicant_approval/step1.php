 <div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <div class="page-title clearfix">
         <h3>Applicant Approval</h3>
      </div>
      <form id="form_applicant" action="" method="post" enctype="multipart/form-data">
         <div class="clearfix">
                <div id="wizard" class="wizard">
               <div class="wizard__content">
                  <header class="wizard__header">
                     <div class="wizard__steps">
                       <nav class="steps">
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step1/<?php echo $getApplicantDetails->id;?>" class="step__text">Profile Details</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -start"></div>
    
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step2/<?php echo $getApplicantDetails->id;?>" class="step__text">Contact Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step3/<?php echo $getApplicantDetails->id;?>" class="step__text">Program Interest</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step4/<?php echo $getApplicantDetails->id;?>" class="step__text">Document Upload</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step5/<?php echo $getApplicantDetails->id;?>" class="step__text">Discount Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step6/<?php echo $getApplicantDetails->id;?>" class="step__text">Declaration Form</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
                             <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step7/<?php echo $getApplicantDetails->id;?>" class="step__text">Confirmation</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
                          </nav>
                     </div>
                  </header>
                  <div class="panels">
                     <div class="paneld">





                    <div class="form-container">
                
                        <h4 class="form-group-title">Profile Details</h4>            
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Salutation <span class='error-text'>*</span></label>
                                    <select name="salutation" id="salutation" class="form-control" disabled>
                                        <?php
                                        if (!empty($salutationList)) {
                                            foreach ($salutationList as $record) {
                                        ?>
                                                <option value="<?php echo $record->id;  ?>"
                                                    <?php if($getApplicantDetails->salutation==$record->id)
                                                    {
                                                        echo "selected=selected";
                                                    }
                                                    ?>
                                                    >
                                                    <?php echo $record->name;  ?>        
                                                </option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>First Name <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $getApplicantDetails->first_name ?>" readonly>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Last Name <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $getApplicantDetails->last_name ?>" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">



                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Email <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="email_id" name="email_id" value="<?php echo $getApplicantDetails->email_id ?>" readonly>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Password <span class='error-text'>*</span></label>
                                    <input type="password" class="form-control" id="password" name="password" value="<?php echo $getApplicantDetails->password ?>" readonly>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Of Birth <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" autocomplete="off" value="<?php echo $getApplicantDetails->date_of_birth ?>" readonly>
                                </div>
                            </div>

                        </div>


                        <div class="row">


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>NRIC <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="nric" name="nric" value="<?php echo $getApplicantDetails->nric ?>" readonly>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Gender <span class='error-text'>*</span></label>
                                    <select class="form-control" id="gender" name="gender" disabled>
                                        <option value="">SELECT</option>
                                        <option value="Male" <?php if($getApplicantDetails->gender=='Male'){ echo "selected"; } ?>>MALE</option>
                                        <option value="Female" <?php if($getApplicantDetails->gender=='Female'){ echo "selected"; } ?>>FEMALE</option>
                                        <!-- <option value="Others"<?php if($getApplicantDetails->gender=='Others'){ echo "selected"; } ?> >OTHERS</option> -->
                                    </select>
                                </div>
                            </div>



                            
                            <div class="col-sm-4">
                               <div class="form-group">
                                  <label>Type Of Nationality <span class='error-text'>*</span></label>
                                  <select name="nationality" id="nationality" class="form-control" disabled="true">
                                     <option value="">Select</option>
                                     <?php
                                        if(!empty($nationalityList))
                                        {
                                          foreach ($nationalityList as $record)
                                          {
                                        ?>
                                     <option value="<?php echo $record->id;  ?>"
                                        <?php if($getApplicantDetails->nationality==$record->id)
                                           {
                                               echo "selected=selected";
                                           }
                                           ?>
                                        >
                                        <?php echo $record->name;  ?>        
                                     </option>
                                      <?php
                                          }
                                        }
                                        ?>
                                  </select>
                               </div>
                            </div>


                            
                        </div>


                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Martial Status <span class='error-text'>*</span></label>
                                    <select class="form-control" id="martial_status" name="martial_status" disabled>
                                        <option value="">SELECT</option>
                                        <option value="Single" <?php if($getApplicantDetails->martial_status=='Single'){ echo "selected"; } ?>>SINGLE</option>
                                        <option value="Married" <?php if($getApplicantDetails->martial_status=='Married'){ echo "selected"; } ?>>MARRIED</option>
                                        <option value="Divorced" <?php if($getApplicantDetails->martial_status=='Divorced'){ echo "selected"; } ?>>DIVORCED</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Religion <span class='error-text'>*</span></label>
                                    <select name="religion" id="religion" class="form-control" disabled="true">
                                        <option value="">Select</option>
                                        <?php
                                        if (!empty($religionList))
                                        {
                                            foreach ($religionList as $record)
                                            {?>
                                                <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $getApplicantDetails->religion)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?>
                                                </option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Race <span class='error-text'>*</span></label>
                                    <select name="id_race" id="id_race" class="form-control" disabled="true">
                                        <option value="">Select</option>
                                        <?php
                                        if (!empty($raceList))
                                        {
                                            foreach ($raceList as $record)
                                            {?>
                                                <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $getApplicantDetails->id_race)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?>
                                                </option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            
                        </div>


                        <div class="row">

                            


                            <div class="col-sm-4">
                                <label>Phone Number <span class='error-text'>*</span></label>
                                  <div class="row">
                                     <div class="col-sm-4">

                                       <select name="country_code" id="country_code" class="form-control" disabled>
                                        <option value="">Select</option>                    
                                        <?php
                                            if (!empty($countryList))
                                            {
                                              foreach ($countryList as $record)
                                              {
                                            ?>
                                         <option value="<?php echo $record->phone_code;  ?>"
                                              <?php 
                                             if($record->phone_code == $getApplicantDetails->country_code)
                                             {
                                               echo "selected";
                                             } ?>
                                            >
                                            <?php echo $record->phone_code . "  " . $record->name;  ?>
                                         </option>
                                           <?php
                                            }
                                          }
                                        ?>
                                      </select>
                                </div>
                                <div class="col-sm-8">
                                  <input type="number" class="form-control" id="phone" name="phone"  required value="<?php echo $getApplicantDetails->phone ?>" readonly>
                                </div>
                                </div>

                            </div>







                        </div>


                    </div> 






                    <!-- <div class="form-container">
                                                      <h4 class="form-group-title">Profile Information</h4>

                      <div class="row">
                         <div class="col-sm-4">
                            <div class="form-group">
                               <label>Salutation <span class='error-text'>*</span></label>
                               <select name="salutation" id="salutation" class="form-control">
                                  <option value="">Select</option>
                                  <?php
                                     if (!empty($salutationList)) {
                                         foreach ($salutationList as $record) {
                                     ?>
                                  <option value="<?php echo $record->id;  ?>"
                                     <?php if($getApplicantDetails->salutation==$record->id)
                                        {
                                            echo "selected=selected";
                                        }
                                        ?>
                                     >
                                     <?php echo $record->name;  ?>        
                                  </option>
                                  <?php
                                     }
                                     }
                                     ?>
                               </select>
                            </div>
                         </div>
                         <div class="col-sm-4">
                            <div class="form-group">
                               <label>First Name <span class='error-text'>*</span></label>
                               <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $getApplicantDetails->first_name ?>">
                            </div>
                         </div>
                         <div class="col-sm-4">
                            <div class="form-group">
                               <label>Last Name <span class='error-text'>*</span></label>
                               <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $getApplicantDetails->last_name ?>">
                            </div>
                         </div>
                      </div>


                      <div class="row">
                         <div class="col-sm-4">
                            <div class="form-group">
                               <label>Type Of Nationality <span class='error-text'>*</span></label>
                               <select name="nationality" id="nationality" class="form-control"onchange="checkNationality(this.value)">
                                  <option value="">Select</option>
                                  <?php
                                     if(!empty($nationalityList))
                                     {
                                       foreach ($nationalityList as $record)
                                       {
                                     ?>
                                  <option value="<?php echo $record->id;  ?>"
                                     <?php if($getApplicantDetails->nationality==$record->id)
                                        {
                                            echo "selected=selected";
                                        }
                                        ?>
                                     >
                                     <?php echo $record->name;  ?>        
                                  </option>
                                  <?php
                                     }
                                     }
                                     ?>
                               </select>
                            </div>
                         </div>
                         <div class="col-sm-4">
                            <div class="form-group">
                               <label>ID Type <span class='error-text'>*</span></label>
                               <select name="id_type" id="id_type" class="form-control" required onchange="getlabel()">
                                  <option value="">Select</option>
                                  <option value="NRIC" <?php if($getApplicantDetails->id_type=='NRIC') { echo "selected=selected";}?>>NRIC</option>
                                  <option value="PASSPORT" <?php if($getApplicantDetails->id_type=='PASSPORT') { echo "selected=selected";}?>>PASSPORT</option>
                               </select>
                            </div>
                         </div>
                         <div class="col-sm-4">
                            <div class="form-group">
                               <label><span id='labelspanid'></span> <span class='error-text'>*</span></label>
                               <input type="text" class="form-control" id="nric" name="nric" value="<?php echo $getApplicantDetails->nric ?>">
                            </div>
                         </div>
                      </div>



                      <div class="row">
                         <div class="col-sm-4" id="view_race">
                            <div class="form-group">
                               <label>Race <span class='error-text'>*</span></label>
                               <select name="id_race" id="id_race" class="form-control">
                                  <option value="">Select</option>
                                  <?php
                                     if (!empty($raceList))
                                     {
                                         foreach ($raceList as $record)
                                         {?>
                                  <option value="<?php echo $record->id;  ?>"
                                     <?php 
                                        if($record->id == $getApplicantDetails->id_race)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                     <?php echo $record->name;  ?>
                                  </option>
                                  <?php
                                     }
                                     }
                                     ?>
                               </select>
                            </div>
                         </div>
                         <div class="col-sm-4">
                            <div class="form-group">
                               <label>Religion <span class='error-text'>*</span></label>
                               <select name="religion" id="religion" class="form-control">
                                  <option value="">Select</option>
                                  <?php
                                     if (!empty($religionList))
                                     {
                                         foreach ($religionList as $record)
                                         {?>
                                  <option value="<?php echo $record->id;  ?>"
                                     <?php 
                                        if($record->id == $getApplicantDetails->religion)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                     <?php echo $record->name;  ?>
                                  </option>
                                  <?php
                                     }
                                     }
                                     ?>
                               </select>
                            </div>
                         </div>
                         <div class="col-sm-4">
                            <div class="form-group">
                               <label>Date Of Birth <span class='error-text'>*</span></label>
                               <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" autocomplete="off" value="<?php echo $getApplicantDetails->date_of_birth ?>">
                            </div>
                         </div>
                      </div>



                      <div class="row">
                         <div class="col-sm-4">
                            <div class="form-group">
                               <label>Martial Status <span class='error-text'>*</span></label>
                               <select class="form-control" id="martial_status" name="martial_status">
                                  <option value="">SELECT</option>
                                  <option value="Single" 
                                     <?php 
                                        if($getApplicantDetails->martial_status=='Single')
                                            { echo "selected"; } 
                                        ?>>
                                     SINGLE
                                  </option>
                                  <option value="Married" 
                                     <?php 
                                        if($getApplicantDetails->martial_status=='Married')
                                            { echo "selected"; }
                                             ?>>
                                     MARRIED
                                  </option>
                                  <option value="Divorced" <?php if($getApplicantDetails->martial_status=='Divorced'){ echo "selected"; } ?>>DIVORCED</option>
                               </select>
                            </div>
                         </div>
                         <div class="col-sm-4">
                            <div class="form-group">
                               <label>Email <span class='error-text'>*</span></label>
                               <input type="email" class="form-control" id="email_id" name="email_id" value="<?php echo $getApplicantDetails->email_id ?>" readonly>
                            </div>
                         </div>
                         <div class="col-sm-4">
                            <div class="form-group">
                               <label>Password <span class='error-text'>*</span></label>
                               <input type="password" class="form-control" id="password" name="password" value="<?php echo $getApplicantDetails->password ?>" readonly>
                            </div>
                         </div>
                      </div>




                      <div class="row">
                         <div class="col-sm-4">
                            <label>Phone Number <span class='error-text'>*</span></label>
                            <div class="row">
                               <div class="col-sm-4">
                                  <select name="country_code" id="country_code" class="form-control" required>
                                     <option value="+61">+61</option>
                                  </select>
                               </div>
                               <div class="col-sm-8">
                                  <input type="number" class="form-control" id="phone" name="phone"  required value="<?php echo $getApplicantDetails->phone ?>">
                               </div>
                            </div>
                         </div>
                         <div class="col-sm-4">
                            <div class="form-group">
                               <label>Gender <span class='error-text'>*</span></label>
                               <select class="form-control" id="gender" name="gender">
                                  <option value="">SELECT</option>
                                  <option value="Male" <?php if($getApplicantDetails->gender=='Male'){ echo "selected"; } ?>>MALE</option>
                                  <option value="Female" <?php if($getApplicantDetails->gender=='Female'){ echo "selected"; } ?>>FEMALE</option>
                               </select>
                            </div>
                            <br/><br/>
                         </div>
                      </div>


                    </div> -->




                      <div class="form-container">
                        <h4 class="form-group-title">Approval Details</h4>

                        <div class="row">
                           <div class="col-sm-4">
                              <label>Verification Status <span class='error-text'>*</span></label>
                                    <select name="step1_status" id="step1_status" class="form-control" required>
                                        <option value="0">Select</option>
                                       <option value="1" <?php if($getApplicantDetails->step1_status=='1') { echo "Selected=selected";} ?>>Verified</option>
                                       <option value="2" <?php if($getApplicantDetails->step1_status=='2') { echo "Selected=selected";} ?>>Rejected</option>
                                       <option value="3" <?php if($getApplicantDetails->step1_status=='3') { echo "Selected=selected";} ?>>Other Reason</option>
                                    </select>
                                
                           </div>
                        </div>
                        
                        <div class="row">

                           <div class="col-sm-8">
                              <div class="form-group">
                                 <label>Comments </label>
                                 <input type="text" class="form-control" id="step1_comments" name="step1_comments"   value="<?php echo $getApplicantDetails->step1_comments ?>">
                              </div>
                              <br/><br/>
                           </div>
                        </div>

                     </div>



                     </div>
                  </div>
                  <div class="wizard__footer">
                     <button class="btn btn-link">Cancel</button>
                     <button class="btn btn-primary next" type="submit">Save & Continue</button>
                  </div>
               </div>
            </div>
            <footer class="footer-wrapper">
               <p>&copy; 2019 All rights, reserved</p>
            </footer>
         </div>

</form>
   </div>
</div>
<script type="text/javascript">
   $('select').select2();
   
   $(function(){
   $(".datepicker").datepicker(
   {
       changeYear: true,
       changeMonth: true,
   });
   });
   
   
    function checkNationality(nationality)
   {
       if(nationality != '')
       {
           if(nationality == '1')
           {
               $('#view_nric').show();
               $('#view_race').show();
               $('#view_passport').hide();
   
           }else{
               $('#view_nric').hide();
               $('#view_race').hide();
               $('#view_passport').show();
           }
       }
   }
   
   function getlabel() {
     var labelnric = $("#id_type").val();
     //alert(labelnric);
     $("#labelspanid").html(labelnric);
   }
   
   $(document).ready(function()
   {
     var nationality = "<?php echo $getApplicantDetails->nationality; ?>";
     
     // alert(nationality);
     if(nationality != '')
     {
         checkNationality(nationality);
     }
     getlabel();
   
   });
   
</script>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>
</div>
</div>