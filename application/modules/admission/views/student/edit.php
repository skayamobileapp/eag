<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Migrate Applicant</h3>

        </div>



            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>


            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#program_detail" class="nav-link border rounded text-center"
                            aria-controls="program_detail" aria-selected="true"
                            role="tab" data-toggle="tab">Profile Details</a>
                    </li>

                    <li role="presentation"><a href="#program_scheme" class="nav-link border rounded text-center"
                            aria-controls="program_scheme" role="tab" data-toggle="tab">Contact Information</a>
                    </li>

                    <li role="presentation"><a href="#program_majoring" class="nav-link border rounded text-center"
                            aria-controls="program_majoring" role="tab" data-toggle="tab">Program Interest</a>
                    </li>

                    <li role="presentation"><a href="#program_minoring" class="nav-link border rounded text-center"
                            aria-controls="program_minoring" role="tab" data-toggle="tab">Document Upload</a>
                    </li>

                    <li role="presentation"><a href="#program_concurrent" class="nav-link border rounded text-center"
                            aria-controls="program_concurrent" role="tab" data-toggle="tab">Discount Information</a>
                    </li>

                    <li role="presentation"><a href="#tab_student_details" class="nav-link border rounded text-center"
                            aria-controls="tab_student_details" role="tab" data-toggle="tab">Program Interest & Payment Details</a>
                    </li>

                    
                    
                </ul>


            





            
                
                <div class="tab-content offers-tab-content">



                    <div role="tabpanel" class="tab-pane active" id="program_detail">
                        <div class="col-12 mt-4">






            <div class="form-container">
                <h4 class="form-group-title">Profile Details</h4>            
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Salutation <span class='error-text'>*</span></label>
                            <select name="salutation" id="salutation" class="form-control" disabled>
                                <option value="">Select</option>
                                <option value="Mr" <?php if($getApplicantDetails->salutation=='Mr'){ echo "selected"; } ?>>Mr</option>
                                <option value="Mrs" <?php if($getApplicantDetails->salutation=='Mrs'){ echo "selected"; } ?>>Mrs</option>
                                <option value="Dr" <?php if($getApplicantDetails->salutation=='Dr'){ echo "selected"; } ?>>Dr</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>First Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $getApplicantDetails->first_name ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Last Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $getApplicantDetails->last_name ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $getApplicantDetails->phone ?>" readonly>
                        </div>
                    </div><div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="email_id" name="email_id" value="<?php echo $getApplicantDetails->email_id ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Password <span class='error-text'>*</span></label>
                            <input type="password" class="form-control" id="password" name="password" value="<?php echo $getApplicantDetails->password ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>NRIC <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="nric" name="nric" value="<?php echo $getApplicantDetails->nric ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Gender <span class='error-text'>*</span></label>
                            <select class="form-control" id="gender" name="gender" disabled>
                                <option value="">SELECT</option>
                                <option value="Male" <?php if($getApplicantDetails->gender=='Male'){ echo "selected"; } ?>>MALE</option>
                                <option value="Female" <?php if($getApplicantDetails->gender=='Female'){ echo "selected"; } ?>>FEMALE</option>
                                <!-- <option value="Others"<?php if($getApplicantDetails->gender=='Others'){ echo "selected"; } ?> >OTHERS</option> -->
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Date Of Birth <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" autocomplete="off" value="<?php echo $getApplicantDetails->date_of_birth ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Martial Status <span class='error-text'>*</span></label>
                            <select class="form-control" id="martial_status" name="martial_status" disabled>
                                <option value="">SELECT</option>
                                <option value="Single" <?php if($getApplicantDetails->martial_status=='Single'){ echo "selected"; } ?>>SINGLE</option>
                                <option value="Married" <?php if($getApplicantDetails->martial_status=='Married'){ echo "selected"; } ?>>MARRIED</option>
                                <option value="Divorced" <?php if($getApplicantDetails->martial_status=='Divorced'){ echo "selected"; } ?>>DIVORCED</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Religion <span class='error-text'>*</span></label>
                            <select name="religion" id="religion" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($religionList))
                                {
                                    foreach ($religionList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $getApplicantDetails->religion)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Type Of Nationality <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="nationality" name="nationality" value="<?php echo $getApplicantDetails->nationality ?>" readonly>
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Race <span class='error-text'>*</span></label>
                            <select name="id_race" id="id_race" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($raceList))
                                {
                                    foreach ($raceList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $getApplicantDetails->id_race)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label> Race <span class='error-text'>*</span></label>
                            <select name="id_race" id="id_race" class="form-control" disabled>
                                <option value="">Select</option>
                                <option value="<?php echo $getApplicantDetails->id_race;?>"
                                    <?php 
                                    if ($getApplicantDetails->id_race == 'Bhumiputra')
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                            <?php echo "Bhumiputra";  ?>
                                </option>

                                <option value="<?php echo $getApplicantDetails->id_race;?>"
                                    <?php 
                                    if ($getApplicantDetails->id_race == 'International')
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                            <?php echo "International";  ?>
                                </option>
                            </select>
                        </div>
                    </div> -->

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Degree Level <span class='error-text'>*</span></label> <a href="editProgram" class="btn btn-link">
                                <!-- <span style='font-size:18px;'>&#9998;</span> -->
                            </a>
                            <select name="id_degree_type" id="id_degree_type" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($degreeTypeList))
                                {
                                    foreach ($degreeTypeList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $getApplicantDetails->id_degree_type)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->code . " - " . $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control" disabled>
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->id_program==$record->id){ echo "selected"; } ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                   <!--  <div class="col-sm-4">
                        <div class="form-group">
                            <p>Do you Wish To Apply For Hostel Accomodation <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_hostel" id="sd1" value="1" <?php if($getApplicantDetails->is_hostel=='1'){ echo "checked";}?> disabled><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_hostel" id="sd2" value="0" <?php if($getApplicantDetails->is_hostel=='0'){ echo "checked";}?> disabled><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                    </div> -->

                </div>

                <div class="row">



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Intake <span class='error-text'>*</span></label>
                            <select name="id_intake" id="id_intake" class="form-control selitemIcon" disabled>
                                <option value="">Select</option>
                                <?php
                                if (!empty($intakeList))
                                {
                                    foreach ($intakeList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->id_intake==$record->id){ echo "selected"; } ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                     
                    
                </div>
            </div>  



                         



                        </div> 
                    
                    </div>




                    <div role="tabpanel" class="tab-pane" id="program_scheme">
                        <div class="mt-4">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Contact Information</h4>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Phone Number <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $getApplicantDetails->phone ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Contact Email</label>
                                            <input type="text" class="form-control" id="contact_email" name="contact_email" value="<?php echo $getApplicantDetails->contact_email ?>" readonly>
                                        </div>
                                    </div>

                                </div>

                            </div>


                            <div class="form-container">
                                <h4 class="form-group-title">Mailing Address</h4>

                                <div class="row">
                                
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing Address 1 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="mail_address1" name="mail_address1" value="<?php echo $getApplicantDetails->mail_address1 ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing Address 2 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="mail_address2" name="mail_address2" value="<?php echo $getApplicantDetails->mail_address2 ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing Country <span class='error-text'>*</span></label>
                                            <select name="mailing_country" id="mailing_country" class="form-control" disabled>
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($countryList))
                                                {
                                                    foreach ($countryList as $record)
                                                    {?>
                                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->mailing_country==$record->id){ echo "selected"; } ?>>
                                                    <?php echo $record->name;?>
                                                </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing State <span class='error-text'>*</span></label>
                                            <select name="mailing_state" id="mailing_state" class="form-control" disabled>
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($stateList))
                                                {
                                                    foreach ($stateList as $record)
                                                    {?>
                                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->mailing_state==$record->id){ echo "selected"; } ?>>
                                                    <?php echo $record->name;?>
                                                </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing City <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="mailing_city" name="mailing_city" value="<?php echo $getApplicantDetails->mailing_city ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing Zipcode <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="mailing_zipcode" name="mailing_zipcode" value="<?php echo $getApplicantDetails->mailing_zipcode ?>" readonly>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            

                            <div class="form-container">
                                <h4 class="form-group-title">Permanent Address</h4>

                                <div class="row">
                                
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent Address 1 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="permanent_address1" name="permanent_address1" value="<?php echo $getApplicantDetails->permanent_address1 ?>" readonly>
                                        </div>
                                
                                    </div><div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent Address 2 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="permanent_address2" name="permanent_address2" value="<?php echo $getApplicantDetails->permanent_address2 ?>" readonly>
                                        </div>
                                    </div>
                                
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent Country <span class='error-text'>*</span></label>
                                            <select name="permanent_country" id="permanent_country" class="form-control" disabled>
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($countryList))
                                                {
                                                    foreach ($countryList as $record)
                                                    {?>
                                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->permanent_country==$record->id){ echo "selected"; } ?>>
                                                    <?php echo $record->name;?>
                                                </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent State <span class='error-text'>*</span></label>
                                            <select name="permanent_state" id="permanent_state" class="form-control" disabled>
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($stateList))
                                                {
                                                    foreach ($stateList as $record)
                                                    {?>
                                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->permanent_state==$record->id){ echo "selected"; } ?>>
                                                    <?php echo $record->name;?>
                                                </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent City <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="permanent_city" name="permanent_city" value="<?php echo $getApplicantDetails->permanent_city ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent Zipcode <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="permanent_zipcode" name="permanent_zipcode" value="<?php echo $getApplicantDetails->permanent_zipcode ?>" readonly>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                    
                    </div>






                    <div role="tabpanel" class="tab-pane" id="program_majoring">
                        <div class="mt-4">




                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Program Interests</h4>


                                <div class="row">

                                     <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Degree Level <span class='error-text'>*</span></label>
                                            <select name="id_degree_type" id="id_degree_type" class="form-control" disabled>
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($degreeTypeList))
                                                {
                                                    foreach ($degreeTypeList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php 
                                                            if($record->id == $getApplicantDetails->id_degree_type)
                                                            {
                                                                echo "selected=selected";
                                                            } ?>>
                                                            <?php echo $record->code . " - " . $record->name;  ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Program Scheme <span class='error-text'>*</span></label>
                                            <select name="program_scheme" id="program_scheme" class="form-control selitemIcon" disabled>
                                                <option value="">Select</option>
                                                <option value="Full Time" 
                                                <?php if($getApplicantDetails->program_scheme == 'Full Time')
                                                {
                                                    echo "selected=selected";
                                                } ?>
                                                >Full Time</option>
                                                <option value="Part Time" 
                                                <?php if($getApplicantDetails->program_scheme == 'Part Time')
                                                {
                                                    echo "selected=selected";
                                                } ?>
                                                >Part Time</option>
                                            </select>
                                        </div>
                                    </div>



                                                       <?php
                                if($getApplicantDetails->id_program > 0)
                                {
                                ?>



                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Program <span class='error-text'>*</span></label>
                                                <!-- <a href="editProgram" class="btn btn-link"><span style='font-size:18px;'>&#9998;</span></a> -->
                                                <select name="id_program" id="id_program" class="form-control" disabled="true">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($programList))
                                                    {
                                                        foreach ($programList as $record)
                                                        {?>
                                                            <option value="<?php echo $record->id;  ?>"
                                                                <?php 
                                                                if($record->id == $getApplicantDetails->id_program)
                                                                {
                                                                    echo "selected=selected";
                                                                } ?>>
                                                                <?php echo $record->code . " - " .$record->name;  ?>
                                                            </option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <!--  <script>
                                    $("#id_program").val() = <?php echo $getApplicantDetails->id_program; ?>
                                </script> -->

                                <?php 
                                }
                                else
                                {
                                ?>


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Program <span class='error-text'>*</span></label>
                                                <select name="id_program" id="id_program" class="form-control selitemIcon"  onchange="getIntakeByProgramme(this.value)" disabled>
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($programList))
                                                    {
                                                        foreach ($programList as $record)
                                                        {?>
                                                    <option value="<?php echo $record->id;  ?>">
                                                        <?php echo $record->name;?>
                                                    </option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                <?php
                                }
                                ?>


                                <?php
                                if($getApplicantDetails->id_intake > 0)
                                {
                                ?>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Intake <span class='error-text'>*</span></label>
                                            <!-- <a href="editProgram" class="btn btn-link"><span style='font-size:18px;'>&#9998;</span></a> -->
                                            <select name="id_intake" id="id_intake" class="form-control" disabled="true">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($intakeList))
                                                {
                                                    foreach ($intakeList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php 
                                                            if($record->id == $getApplicantDetails->id_intake)
                                                            {
                                                                echo "selected=selected";
                                                            } ?>>
                                                            <?php echo $record->name;  ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>




                                <?php 
                                }
                                else
                                {
                                ?>



                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Intake <span class='error-text'>*</span></label>
                                                <span id="view_intake"></span>
                                            </div>
                                        </div>


                                        <?php
                                }
                                ?>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Branch <span class='error-text'>*</span></label>
                                        <!-- <a href="editProgram" class="btn btn-link"><span style='font-size:18px;'>&#9998;</span></a> -->
                                        <select name="id_intake" id="id_intake" class="form-control" disabled="true">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($branchList))
                                            {
                                                foreach ($branchList as $record)
                                                {?>
                                                    <option value="<?php echo $record->id;  ?>"
                                                        <?php 
                                                        if($record->id == $getApplicantDetails->id_branch)
                                                        {
                                                            echo "selected=selected";
                                                        } ?>>
                                                        <?php echo $record->code . " - " . $record->name;  ?>
                                                    </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Program Structure Type <span class='error-text'>*</span></label>
                                            <select name="id_program_structure_type" id="id_program_structure_type" class="form-control" disabled="true">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($programStructureTypeList))
                                                {
                                                    foreach ($programStructureTypeList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php 
                                                            if($record->id == $getApplicantDetails->id_program_structure_type)
                                                            {
                                                                echo "selected=selected";
                                                            } ?>>
                                                            <?php echo $record->code . " - " . $record->name;  ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>





                                </div>

                            </div>





                        </div>
                    
                    </div>





                    <div role="tabpanel" class="tab-pane" id="program_minoring">
                        <div class="mt-4">

                            <br>

                            <div class="form-container" id="view_document" style="display: none">
                                <h4 class="form-group-title">Documents To Upload</h4>
                             
                                <div id='doc'>
                                </div>

                            </div>







                        </div>
                    
                    </div>







                    <div role="tabpanel" class="tab-pane" id="program_concurrent">
                        
                        <div class="mt-4">

                        <br>

                        <div class="form-container">
                                <h4 class="form-group-title">Other Details</h4>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <p>Do you have Sibbling/s studying with university? <span class='error-text'>*</span></p>
                                        <label class="radio-inline">
                                        <input type="radio" id="sd1" name="sibbling_discount" value="Yes" disabled="disabled" <?php if($getApplicantDetails->sibbling_discount=='Yes'){ echo "checked";}?> disabled><span class="check-radio"></span> Yes
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" id="sd2" name="sibbling_discount" value="No" disabled="disabled" <?php if($getApplicantDetails->sibbling_discount=='No'){ echo "checked";}?> disabled><span class="check-radio"></span> No
                                        </label>
                                    </div>
                                </div>
                            </div>


                            
                           <?php
                            if($getApplicantDetails->sibbling_discount=='Yes')
                            {
                                ?>

                                <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Name <span class='error-text'>*</span></label>
                                        <input type="text" id="sibbling_name" name="sibbling_name" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_name ?>" readonly="readonly">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>NRIC <span class='error-text'>*</span></label>
                                        <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_nric ?>" readonly="readonly">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Status <span class='error-text'>*</span></label>
                                        <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_status ?>" readonly="readonly">
                                    </div>
                                </div>
                            </div> 

                                <?php
                                if($sibblingDiscountDetails->sibbling_status=='Reject')
                                {
                                ?>

                                <div class="row">
                                    <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Reject Reason <span class='error-text'>*</span></label>
                                                <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $sibblingDiscountDetails->reason ?>" readonly="readonly">
                                            </div>
                                    </div>

                                    <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Rejected By <span class='error-text'>*</span></label>
                                                <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $sibblingDiscountDetails->user_name ?>" readonly="readonly">
                                            </div>
                                    </div>

                                    <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Rejected On <span class='error-text'>*</span></label>
                                                <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo date('d-m-Y h:i:s a', strtotime($sibblingDiscountDetails->rejected_on)) ?>" readonly="readonly">
                                            </div>
                                    </div>
                                </div> 


                                <?php
                                }
                                elseif($sibblingDiscountDetails->sibbling_status=='Approved')
                                {
                                    ?>
                                    

                                    <div class="row">

                                     <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Approved By <span class='error-text'>*</span></label>
                                                <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $sibblingDiscountDetails->user_name ?>" readonly="readonly">
                                            </div>
                                    </div>

                                    <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Approved On <span class='error-text'>*</span></label>
                                                <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo $sibblingDiscountDetails->rejected_on ?>" readonly="readonly">
                                            </div>
                                    </div>

                                    
                                </div> 

                                    <?php
                                }
                                ?> 

                            <?php
                            }
                             ?>


                             <br>

                            
                            <div class="row">      
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <p>Do you eligible for Employee discount <span class='error-text'>*</span></p>
                                        <label class="radio-inline">
                                          <input type="radio" name="employee_discount" id="ed1" value="Yes" disabled="disabled" <?php if($getApplicantDetails->employee_discount=='Yes'){ echo "checked";}?> disabled><span class="check-radio"></span> Yes
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="employee_discount" id="ed2" value="No" disabled="disabled" <?php if($getApplicantDetails->employee_discount=='No'){ echo "checked";}?> disabled><span class="check-radio"></span> No
                                        </label>                              
                                    </div>                         
                                </div>
                            </div>


                             <?php
                            if($getApplicantDetails->employee_discount=='Yes')
                            {
                                ?>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Name <span class='error-text'>*</span></label>
                                        <input type="text" id="employee_name" name="employee_name" class="form-control" value="<?php echo $employeeDiscountDetails->employee_name ?>" readonly="readonly">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>NRIC <span class='error-text'>*</span></label>
                                        <input type="text" id="employee_nric" name="employee_nric" class="form-control" value="<?php echo $employeeDiscountDetails->employee_nric ?>" readonly="readonly">
                                    </div>
                                </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Designation <span class='error-text'>*</span></label>
                                        <input type="text" id="employee_designation" name="employee_designation" class="form-control" value="<?php echo $employeeDiscountDetails->employee_designation ?>" readonly="readonly">
                                    </div>
                                </div>
                            </div> 


                            <div class="row">

                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Status <span class='error-text'>*</span></label>
                                        <input type="text" id="employee_designation" name="employee_designation" class="form-control" value="<?php echo $employeeDiscountDetails->employee_status ?>" readonly="readonly">
                                    </div>
                                </div>
                            </div>


                                <?php
                                if($employeeDiscountDetails->employee_status=='Reject')
                                {
                                ?>

                                <div class="row">

                                    <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Rejected Reason <span class='error-text'>*</span></label>
                                                <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control" value="<?php echo $employeeDiscountDetails->reason ?>" readonly="readonly">
                                            </div>
                                    </div>

                                     <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Rejected By <span class='error-text'>*</span></label>
                                                <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $employeeDiscountDetails->user_name ?>" readonly="readonly">
                                            </div>
                                    </div>
                                </div> 


                                <div class="row">

                                     <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Rejected On <span class='error-text'>*</span></label>
                                                <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo date('d-m-Y h:i:s a', strtotime($employeeDiscountDetails->rejected_on)) ?>" readonly="readonly">
                                            </div>
                                    </div>


                                </div> 


                                <?php
                                }
                                elseif($employeeDiscountDetails->employee_status=='Approved')
                                {
                                    ?>

                                <div class="row">

                                     <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Approved By <span class='error-text'>*</span></label>
                                                <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $employeeDiscountDetails->user_name ?>" readonly="readonly">
                                            </div>
                                    </div>

                                    <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Approved On <span class='error-text'>*</span></label>
                                                <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo $employeeDiscountDetails->rejected_on ?>" readonly="readonly">
                                            </div>
                                    </div>

                                    
                                </div> 

                                    <?php
                                }
                                ?> 

                            <?php
                            }
                             ?>
                             
                        </div>



                    
                        </div>

                    </div>




                    <div role="tabpanel" class="tab-pane" id="tab_student_details">
                        <div class="mt-4">

                            <br>

                            <div class="m-auto text-center">
                                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
                            </div>

                            <br>
                            <button type="button" class="btn btn-info btn-lg" onclick="viewProgramLandscape()">View Program Landscape Details</button>
                            <br>
                            <br>


                            <div class="form-container">
                                        <h4 class="form-group-title">Account Statement</h4>          
                                        <div class="m-auto text-center">
                                            <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
                                        </div>
                                        <div class="clearfix">
                                            <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                                                <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                                                        aria-controls="invoice" aria-selected="true"
                                                        role="tab" data-toggle="tab">Invoice</a>
                                                </li>
                                                <li role="presentation"><a href="#receipt" class="nav-link border rounded text-center"
                                                        aria-controls="receipt" role="tab" data-toggle="tab">Receipt</a>
                                                </li>
                                                
                                            </ul>

                                            
                                            <div class="tab-content offers-tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="invoice">
                                                    <div class="mt-4">
                                                        <div class="custom-table" id="printInvoice">
                                                            <table class="table" id="list-table">
                                                                <thead>
                                                                <tr>
                                                                    <th>Sl. no</th>
                                                                    <th>Invoice Number</th>
                                                                    <th>Invoice Type</th>
                                                                    <th>Invoice Total</th>
                                                                    <th>Total Payable</th>
                                                                    <th>Total Discount</th>
                                                                    <th>Balance </th>
                                                                    <th>Remarks</th>
                                                                    <th>Status</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                if (!empty($getInvoiceByStudentId))
                                                                {
                                                                    $i=1;
                                                                    foreach ($getInvoiceByStudentId as $record)
                                                                    {
                                                                ?>
                                                                    <tr>
                                                                        <td><?php echo $i ?></td>
                                                                        <td><?php echo $record->invoice_number ?></td>
                                                                        <td><?php echo $record->type ?></td>
                                                                        <td><?php echo $record->invoice_total ?></td>
                                                                        <td><?php echo $record->total_amount ?></td>
                                                                        <td><?php echo $record->total_discount ?></td>
                                                                        <td><?php if($record->balance_amount == ""){
                                                                            echo "0.00";
                                                                        } else { echo $record->balance_amount; } ?></td>
                                                                        <td><?php echo $record->remarks ?></td>

                                                                        <td><?php 
                                                                        if($record->status == "0")
                                                                        {
                                                                            echo "Pending";
                                                                        }
                                                                        elseif($record->status == "1")
                                                                        { 
                                                                            echo "Approved"; 
                                                                        }elseif($record->status == "2")
                                                                        { 
                                                                            echo "Rejected"; 
                                                                        } ?></td>

                                                                        <td class="">
                                                                        <a  title="" onclick="printDiv('printInvoice')">Print</a> 
                                                                        | 
                                                                         <a onclick="viewMainInvoice(<?php echo $record->id; ?>)" title="">View</a>
                                                                        </td>
                                                                    </tr>
                                                                <?php
                                                                    $i++;
                                                                    }
                                                                }
                                                                ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div> 
                                                </div>

                                                <div role="tabpanel" class="tab-pane" id="receipt">
                                                    <div class="mt-4">
                                                        <div class="custom-table" id="printReceipt">
                                                            <table class="table" id="list-table">
                                                                <thead>
                                                                <tr>
                                                                    <th>Sl. no</th>
                                                                    <th>Receipt Number</th>
                                                                    <th>Receipt Amount</th>
                                                                    <th>Remarks</th>
                                                                    <th>Status</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                if (!empty($getReceiptByStudentId))
                                                                {
                                                                    $j=1;
                                                                    foreach ($getReceiptByStudentId as $record)
                                                                    {
                                                                ?>
                                                                    <tr>
                                                                        <td><?php echo $j ?></td>
                                                                        <td><?php echo $record->receipt_number ?></td>
                                                                        <td><?php if($record->receipt_amount == ""){
                                                                            echo "0.00";
                                                                        } else { echo $record->receipt_amount; } ?></td>
                                                                        <td><?php echo $record->remarks ?></td>
                                                                        <td><?php 
                                                                        if($record->status == "0")
                                                                        {
                                                                            echo "Pending";
                                                                        }
                                                                        elseif($record->status == "1")
                                                                        { 
                                                                            echo "Approved"; 
                                                                        }elseif($record->status == "2")
                                                                        { 
                                                                            echo "Rejected"; 
                                                                        } ?></td>

                                                                        <td class="">
                                                                        <a  title="" onclick="printDiv('printReceipt')">Print</a> 
                                                                         | <a onclick="viewReceipt(<?php echo $record->id; ?>)" title="">View</a>
                                                                        </td>
                                                                    </tr>
                                                                <?php
                                                                    $j++;
                                                                    }
                                                                }
                                                                ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>





                        </div>
                    
                    </div>









                </div>

        <br>
            
            <form id="form_applicant" action="" method="post">

                
            <?php
                if($getApplicantDetails->applicant_status=='Approved')
                {
                ?>

            <div class="form-container">
                <h4 class="form-group-title">Applicant Migration</h4>

                <div class="row">

                    

                                        
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Applicant Migrate Status <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                    <input type="radio" id="sd1" name="applicant_status" value="Migrated" <?php if($getApplicantDetails->applicant_status=='Migrated'){ echo "checked";}?> ><span class="check-radio" onclick="hideRejectField()"></span> Migrate
                                </label>
                                <!-- <label class="radio-inline">

                                <input type="radio" id="sd2" name="applicant_status" value="Draft" <?php if($getApplicantDetails->applicant_status=='Draft'){ echo "checked";}?>><span class="check-radio" onclick="hideRejectField()"></span> Draft
                                </label> -->
                                <label class="radio-inline">

                                    <input type="radio" id="sd2" name="applicant_status" value="Migrate Rejected" <?php if($getApplicantDetails->applicant_status=='Migrate Rejected'){ echo "checked";}?>><span class="check-radio" onclick="showRejectField()"></span> Reject
                                </label>
                                </div>
                            </div>

                             <div class="col-sm-4" id="view_reject" style="display: none">
                                <div class="form-group">
                                    <label>Reason <span class='error-text'>*</span></label>
                                    <input type="text" id="reason" name="reason" class="form-control">
                                </div>
                            </div>

                </div>

            </div>
            

                    <?php
                        }
                    ?>
            



            <div class="button-block clearfix">
                <div class="bttn-group">

            <?php
                if($getApplicantDetails->applicant_status=='Approved')
                {
                ?>

                    <button type="submit" class="btn btn-primary btn-lg" >Save</button>

                <?php
                }
            ?>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>

    </form>

                


        </div>











        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!-- <h4 class="modal-title">Program Landscape</h4> -->
              </div>

              <div class="modal-body">

                <br>
                <div class="form-container">
                        
                <div class="row">
                    <div id='view_model'>
                    </div>
                </div>


                </div>

            </div>


              <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default" onclick="submitApp()">Submit</button> -->
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>









        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>

    function printDiv(divName)
    {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }


    $(document).ready(function()
    {
        $("#form_applicant").validate({
            rules: {
                applicant_status: {
                    required: true
                },
                reason: {
                    required: true
                }
            },
            messages: {
                applicant_status: {
                    required: "<p class='error-text'>Application Status Required</p>",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    function viewProgramLandscape()
     {
        var tempPR = {};

        tempPR['id_program'] = $("#id_program").val();
        tempPR['id_intake'] = $("#id_intake").val();

        if(tempPR['id_program'] != '' && tempPR['id_intake'] != '' )
        {

            $.ajax(
            {
               url: '/admission/student/viewProgramLandscape',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    if(result != '')
                    {
                        $("#view_model").html(result);
                        $('#myModal').modal('show');
                        
                    }
               }
            });
        }
     }

     function viewMainInvoice(id_main_invoice)
     {
        var tempPR = {};
        
        tempPR['id_main_invoice'] = id_main_invoice;

        if(tempPR['id_main_invoice'] != '' && tempPR['id_intake'] != '' )
        {
            // alert(tempPR['id_program']);

            $.ajax(
            {
               url: '/admission/student/viewMainInvoice',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    if(result != '')
                    {
                        $("#view_model").html(result);
                        $('#myModal').modal('show');
                        
                    }
               }
            });
        }
     }

     function viewReceipt(id_receipt)
     {       
        var tempPR = {}; 
        tempPR['id_receipt'] = id_receipt;

        if(tempPR['id_receipt'] != '')
        {
            // alert(tempPR['id_program']);

            $.ajax(
            {
               url: '/admission/student/viewReceipt',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    if(result != '')
                    {
                        $("#view_model").html(result);
                        $('#myModal').modal('show');
                        
                    }
               }
            });
        }
     }


    

    function submitApp()
    {
        $('#id_intake').prop('disabled', false);
        $('#id_program').prop('disabled', false);
        if($('#form_applicant').valid())
        {    
            $('#form_applicant').submit();
        }
    }

    $('select').select2();


    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });
</script>