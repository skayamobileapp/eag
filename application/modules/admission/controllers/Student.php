<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Student extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_model');
        $this->isLoggedIn();
    }

    function approvalList()
    {
        if ($this->checkAccess('student_approval.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['intakeList'] = $this->student_model->intakeList();
            $data['programList'] = $this->student_model->programListForPostgraduate('POSTGRADUATE');


            $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            // $formData['applicant_status'] = 'Approved';
 
            $data['applicantList'] = $this->student_model->applicantListForApproval($formData);
            $data['searchParam'] = $formData;

            // echo "<Pre>";print_r($data);exit;


            $this->global['pageTitle'] = 'Campus Management System : Applicant Approval For Student';
            $this->loadViews("student/list", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('applicant_approval.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/admission/applicantApproval/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->session_id;

                
                $applicant_status = $this->security->xss_clean($this->input->post('applicant_status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'applicant_status' => $applicant_status,
                    'reason' => $reason,
                    'approved_by' => $id_user,
                    'approved_dt_tm' => date('Y-m-d H:i')
                );

                $result = $this->student_model->editApplicantDetails($data,$id);
                if ($applicant_status == "Migrated")
                {
                    $insert_id =  $this->student_model->addNewStudent($id);
                // echo "<Pre>";print_r($insert_id);exit;
                    if($insert_id)
                    {
                        $this->student_model->addStudentProfileDetail($insert_id);

                        // echoPavan "<Pre>";print_r($insert_id);exit;
                        // $this->applicant_approval_model->createNewMainInvoiceForStudent($id,$insert_id);
                        // $this->applicant_approval_model->createNewMainInvoiceForStudent($id,'4');
                    }
                }
                redirect('/admission/applicantApproval/list');
            }
            $data['intakeList'] = $this->student_model->intakeList();
            $data['programList'] = $this->student_model->programList();
            $data['nationalityList'] = $this->student_model->nationalityList();
            $data['countryList'] = $this->student_model->countryListByStatus('1');
            $data['stateList'] = $this->student_model->stateListByStatus('1');
            $data['getApplicantDetails'] = $this->student_model->getApplicantDetailsById($id);
            $data['sibblingDiscountDetails'] = $this->student_model->getApplicantSibblingDiscountDetails($id);
            $data['employeeDiscountDetails'] = $this->student_model->getApplicantEmployeeDiscountDetails($id);
            $data['programStructureTypeList'] = $this->student_model->programStructureTypeListByStatus('1');

            $data['studentDetails'] = $this->student_model->getStudentByStudentId($id);
            $data['getInvoiceByStudentId'] = $this->student_model->getInvoiceByStudentId($id);
            $data['getReceiptByStudentId'] = $this->student_model->getReceiptByStudentId($id);

            $data['raceList'] = $this->student_model->raceListByStatus('1');
            $data['religionList'] = $this->student_model->religionListByStatus('1');
            $data['salutationList'] = $this->student_model->salutationListByStatus('1');
            $data['branchList'] = $this->student_model->branchListByStatus();
            $data['requiremntListList'] = $this->student_model->programRequiremntListList();
            




            // echo "<Pre>";print_r($data['sibblingDiscountDetails']);exit;
            $this->global['pageTitle'] = 'Campus Management System : Edit Applicant Approval';
            $this->loadViews("student/edit", $this->global, $data, NULL);
        }
    }

    

    function delete_exam()
    {
        $id = $this->input->get('id');

       $this->student_model->deleteExamDetails($id);

       redirect($_SERVER['HTTP_REFERER']);
    }

    function delete_english()
    {
        $id = $this->input->get('id');

       $this->student_model->deleteProficiencyDetails($id);

       redirect($_SERVER['HTTP_REFERER']);  
    }

    function delete_employment()
    {
        $id = $this->input->get('id');

       $this->student_model->deleteEmploymentDetails($id);

       redirect($_SERVER['HTTP_REFERER']);  
    }

    function delete_document()
    {
        $id = $this->input->get('id');

       $this->student_model->deleteOtherDocument($id);

       redirect($_SERVER['HTTP_REFERER']);  
    }

    function viewProgramLandscape()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $program_landscape_detail = $this->student_model->getProgramLandscapeByIntakeNProgram($tempData);
        if(!empty($program_landscape_detail))
        {
           $program = $program_landscape_detail->programme;
           $intake = $program_landscape_detail->intake;
           $program_scheme = $program_landscape_detail->program_scheme;
           $name = $program_landscape_detail->name;
           $min_total_cr_hrs = $program_landscape_detail->min_total_cr_hrs;
           $min_repeat_course = $program_landscape_detail->min_repeat_course;
           $max_repeat_exams = $program_landscape_detail->max_repeat_exams;
           $total_semester = $program_landscape_detail->total_semester;


             $table = "

            <h4 class='sub-title'>Program Landscape Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Program :</dt>
                                <dd>$program</dd>
                            </dl>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>$intake</dd>
                            </dl>
                            <dl>
                                <dt>Min. Total Cr. Hours :</dt>
                                <dd>$min_total_cr_hrs</dd>
                            </dl>
                            <dl>
                                <dt>Min. Repeat Course :</dt>
                                <dd>$min_repeat_course</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd>
                                    $program_scheme
                                </dd>
                            </dl>
                            <dl>
                                <dt>Landscape Name :</dt>
                                <dd>$name</dd>
                            </dl>
                            <dl>
                                <dt>Max. Repeat Exams</dt>
                                <dd>$max_repeat_exams</dd>
                            </dl>
                            <dl>
                                <dt>Total Semesters</dt>
                                <dd>$total_semester</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";



            // <div class="form-container" class="col-sm-8">
            //     <h4 class="form-group-title">Program Landscape Details</h4>             
            //     <div class="row">

            //         <div class="col-sm-4">
            //             <div class="form-group">
            //                 <label>Program <span class="error-text">*</span></label>
            //                 <input type="text" class="form-control" id="program" name="invoice_number" value="';

            //                  $program_landscape_detail->programme;
            //                  $table.='" readonly="readonly">
            //             </div>
            //         </div>

                    

            //         <div class="col-sm-4">
            //             <div class="form-group">
            //                 <label>Intake</label>
            //                 <input type="text" class="form-control" id="remarks" name="remarks" value="';
            //                 $program_landscape_detail->intake;
            //                 $table.='" readonly="readonly">
            //             </div>
            //         </div>

            //         <div class="col-sm-4">
            //             <div class="form-group">
            //                 <label>Program Scheme</label>
            //                 <input type="text" class="form-control" id="remarks" name="remarks" value="';
            //                  echo $program_landscape_detail->program_scheme;
            //                 $table.='" readonly="readonly">
            //             </div>
            //         </div>

                    
            //     </div>

            //     <div class="row">

            //         <div class="col-sm-4">
            //             <div class="form-group">
            //                 <label>Landscape Name <span class="error-text">*</span></label>
            //                 <input type="amount" class="form-control" id="invoice_total" name="invoice_total" value=';
            //                  echo $program_landscape_detail->name;

            //                 $table.=' " readonly="readonly">
            //             </div>
            //         </div>

            //         <div class="col-sm-4">
            //             <div class="form-group">
            //                 <label>Min. Total Credit Hours <span class="error-text">*</span></label>
            //                 <input type="amount" class="form-control" id="total_discount" name="total_discount" value="';

            //                  echo $program_landscape_detail->min_total_cr_hrs;
                            
            //                 $table.='
            //                  " readonly="readonly">
            //             </div>
            //         </div>


            //          <div class="col-sm-4">
            //             <div class="form-group">
            //                 <label>Mi. Repaet Hours <span class="error-text">*</span></label>
            //                 <input type="amount" class="form-control" id="total_amount" name="total_amount" value="';

            //                 echo $program_landscape_detail->min_repeat_course; 

            //                 $table.='
            //                 " readonly="readonly">
            //             </div>
            //         </div>
                    
                
            //     </div>

            //     <div class="row">

            //         <div class="col-sm-4">
            //             <div class="form-group">
            //                 <label>Min. Repeat Exam <span class="error-text">*</span></label>
            //                 <input type="amount" class="form-control" id="total_amount" name="total_amount" value="';

            //                 echo $program_landscape_detail->max_repeat_exams; 

            //                 $table.='
            //                 " readonly="readonly">
            //             </div>
            //         </div>


            //         <div class="col-sm-4">
            //             <div class="form-group">
            //                 <label>Total Semester <span class="error-text">*</span></label>
            //                 <input type="amount" class="form-control" id="total_amount" name="total_amount" value="';

            //                     echo $program_landscape_detail->total_semester;

            //                  $table.='
            //                  " readonly="readonly">
            //             </div>
            //         </div>

                    

            //          <div class="col-sm-4">
            //             <div class="form-group">
            //                 <label>Total Blocks <span class="error-text">*</span></label>
            //                 <input type="text" class="form-control" id="remarks" name="remarks" value=';

            //                     echo $program_landscape_detail->min_total_score;
            //                 // if($program_landscape_detail->status == '0')
            //                 // {
            //                 //     echo 'Pending';
            //                 // }
            //                 // elseif($program_landscape_detail->status == '1')
            //                 // {
            //                 //     echo 'Approved';
            //                 // }
            //                 // elseif($program_landscape_detail->status == '2')
            //                 // {
            //                 //     echo 'Rejected';
            //                 // }

            //                 $table.=' readonly="readonly">
            //             </div>
            //         </div>

                
            //     </div>';

                    

                
            //         $table.='


            // </div>
            

            // <div class="form-container">
            //     <h4 class="form-group-title">Main Invoice Details</h4>  
            //     <div class="custom-table">
            //         <table class="table" id="list-table">
            //             <thead>
            //             <tr>
            //                 <th>Sl. No</th>
            //                 <th>Fee Item</th>
            //                 <th>Amount</th>
            //             </tr>
            //             </thead>
            //             <tbody>
            //             ';

            //                 $total_amount = 0;
            //             if (!empty($program_landscape_detailDetailsList)) {
            //                 $i = 1;
            //                 foreach ($program_landscape_detailDetailsList as $record) {
            //         $table.='
            //                 <tr>
            //                     <td>'; echo $i; $table.='</td>
            //                     <td>'; echo $record->fee_setup; $table.='</td>
            //                     <td>'; echo $record->amount; $table.='</td>
            //                 </tr>
            //             ';
            //             $total_amount = $total_amount + $record->amount;
            //             $i++;
            //                 }
            //             }
            //             $total_amount = number_format($total_amount, 2, '.', ',');
            //             $table.='
            //             <tr>
            //                     <td bgcolor=""></td>
            //                     <td bgcolor="" style="text-align:  right;"><b> Total :  </b></td>
            //                     <td bgcolor=""><b>'; echo $total_amount; $table.='</b></td>
            //                 </tr>
            //             </tbody>
            //         </table>
            //     </div>
            // </div>

            // <div class="form-container">
            //     <h4 class="form-group-title">Main Invoice Discount Details</h4>  
            //     <div class="custom-table">
            //         <table class="table" id="list-table">
            //             <thead>
            //             <tr>
            //                 <th>Sl. No</th>
            //                 <th>Discount</th>
            //                 <th>Amount</th>
            //             </tr>
            //             </thead>
            //             <tbody>';
                        
            //                 $discount_total_amount = 0;
            //             if (!empty($program_landscape_detailDiscountDetailsList)) {
            //                 $i = 1;
            //                 foreach ($program_landscape_detailDiscountDetailsList as $record) {
            //             $table.='
            //                 <tr>
            //                     <td>'; echo $i;  $table.=' </td>
            //                     <td>'; echo $record->name; $table.=' </td>
            //                     <td>'; echo $record->amount; $table.=' </td>
            //                 </tr>
            //            ';
            //             $discount_total_amount = $discount_total_amount + $record->amount;
            //             $i++;
            //                 }
            //             }
            //             $discount_total_amount = number_format($discount_total_amount, 2, '.', ',');
            //             $table.=' 
            //             <tr>
            //                     <td bgcolor=""></td>
            //                     <td bgcolor="" style="text-align:  right;"><b> Total :  </b></td>
            //                     <td bgcolor=""><b> '; echo $discount_total_amount; $table.=' </b></td>
            //                 </tr>
            //             </tbody>
            //         </table>
            //     </div>
            // </div>

            // ';
        }
        else
        {
            $table = "";
        }
        print_r($table);exit();
    }



    function viewMainInvoice()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $main_invoice = $this->student_model->getMainInvoice($tempData['id_main_invoice']);
        $main_invoice_details = $this->student_model->getMainInvoiceDetails($tempData['id_main_invoice']);
        $main_invoice_discount = $this->student_model->getMainInvoiceDiscountDetails($tempData['id_main_invoice']);
        
        $table = "";

        // $main_invoice = $this->student_model->getMainInvoice($tempData);
        // $main_invoice_details = $this->student_model->getMainInvoiceDetails($tempData);
        // $main_invoice_discount = $this->student_model->getMainInvoiceDiscount($tempData);
           // echo "<Pre>"; print_r($main_invoice_discount);exit();
        if(!empty($main_invoice))
        {
            $type = $main_invoice->type;;
            $invoice_number = $main_invoice->invoice_number;
            $date_time = date('d-m-Y H:i', strtotime($main_invoice->date_time));
            $remarks = $main_invoice->remarks;
            $invoice_total = $main_invoice->invoice_total;
            $total_amount = $main_invoice->total_amount;
            $total_discount = $main_invoice->total_discount;
            $balance_amount = $main_invoice->balance_amount;
            $paid_amount = $main_invoice->paid_amount;
            $currency = $main_invoice->currency;

            $table = "

            <h3 class='sub-title'>Invoice Details</h3>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Invoice Number :</dt>
                                <dd>$invoice_number</dd>
                            </dl>
                            <dl>
                                <dt>Invoice Type :</dt>
                                <dd>$type</dd>
                            </dl>
                            <dl>
                                <dt>Invoice Date & Time :</dt>
                                <dd>$date_time</dd>
                            </dl>
                            <dl>
                                <dt>Remarks :</dt>
                                <dd>$remarks</dd>
                            </dl>
                            <dl>
                                <dt>Currency :</dt>
                                <dd>$currency</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Invoice Total Amount :</dt>
                                <dd>
                                    $invoice_total
                                </dd>
                            </dl>
                            <dl>
                                <dt>Payble Amount :</dt>
                                <dd>$total_amount</dd>
                            </dl>
                            <dl>
                                <dt>Total Discount</dt>
                                <dd>$total_discount</dd>
                            </dl>
                            <dl>
                                <dt>Paid Amount</dt>
                                <dd>$paid_amount</dd>
                            </dl>
                            <dl>
                                <dt>Balance Amount</dt>
                                <dd>$balance_amount</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";


            if(!empty($main_invoice_details))
            {
                
            // echo "<Pre>";print_r($details);exit;
            $table .= "
            <h4 class='sub-title'>Invoice Amount Details</h4>
            <div class='custom-table'>
            <table  class='table' id='list-table'>
                    <thead>
                      <tr>
                        <th>Sl. No</th>
                        <th>Fee Item</th>
                        <th>Amount </th>
                        </tr>
                    </thead>";
                        $total_detail = 0;
                        for($i=0;$i<count($main_invoice_details);$i++)
                        {
                            $id = $main_invoice_details[$i]->id;
                            $amount = $main_invoice_details[$i]->amount;
                            $fee_setup = $main_invoice_details[$i]->fee_setup;
                           


                            $j=$i+1;

                            $table .= "
                            <tbody>
                            <tr>
                                <td>$j</td>
                                <td>$fee_setup</td>                                
                                <td>$amount</td>
                            </tr>";
                                    // <span onclick='deleteTempData($id)'>Delete</a>
                        }

                         $table .= "
                        </tbody>
                        </table>
            </div>
            <br>";
            }
            else
            {
                $table .= "<h4 class='sub-title'>Invoice Details Not Available</h4>";
            }

            if(!empty($main_invoice_discount))
            {
                
            // echo "<Pre>";print_r($details);exit;
            $table .= "
            <h4 class='sub-title'>Invoice Discount Details</h4>
            <div class='custom-table'>
            <table  class='table' id='list-table'>
                    <thead>
                      <tr>
                        <th>Sl. No</th>
                        <th>Discount</th>
                        <th>Amount </th>
                        </tr>
                    </thead>";
                        $total_detail = 0;
                        for($i=0;$i<count($main_invoice_discount);$i++)
                        {
                            $id = $main_invoice_discount[$i]->id;
                            $amount = $main_invoice_discount[$i]->amount;
                            $name = $main_invoice_discount[$i]->name;
                            


                            $j=$i+1;

                            $table .= "
                            <tbody>
                            <tr>
                                <td>$j</td>
                                <td>$name</td>                                
                                <td>$amount</td>
                            </tr>";
                                    // <span onclick='deleteTempData($id)'>Delete</a>
                        }

                         $table .= "
                        </tbody>
                        </table>
            </div>
            <br>";
            }
            else
            {
                $table .= "<h4 class='sub-title'>Invoice Discount Details Not Available</h4>";
            }
        
        }
        else
        {
            $table .= "";
        }
        print_r($table);exit();
    }

    function viewReceipt()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // $program_landscape_details = $this->student_model->getProgramLandscapeByIntakeNProgram($tempData);
        $receipt = $this->student_model->getReceipt($tempData['id_receipt']);
        $invoice_details = $this->student_model->getReceiptInvoiceDetails($tempData['id_receipt']);
        $receipt_payment_details = $this->student_model->getReceiptPaymentDetails($tempData['id_receipt']);
        // echo "<Pre>"; print_r($invoice_details);exit();

        $table = "";

        // $main_invoice = $this->student_model->getMainInvoice($tempData);
        // $main_invoice_details = $this->student_model->getMainInvoiceDetails($tempData);
        // $main_invoice_discount = $this->student_model->getMainInvoiceDiscount($tempData);
           // echo "<Pre>"; print_r($invoice_details);exit();
        if(!empty($receipt))
        {
            $type = $receipt->type;;
            $receipt_number = $receipt->receipt_number;
            $receipt_date = date('d-m-Y H:i', strtotime($receipt->receipt_date));
            $remarks = $receipt->remarks;
            $receipt_amount = $receipt->receipt_amount;

            $table = "

            <h3 class='sub-title'>Receipt Details</h3>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Receipt Number :</dt>
                                <dd>$receipt_number</dd>
                            </dl>
                            <dl>
                                <dt>Receipt Type :</dt>
                                <dd>$type</dd>
                            </dl>
                            
                            <dl>
                                <dt>Remarks :</dt>
                                <dd>$remarks</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Receipt Date & Time :</dt>
                                <dd>$receipt_date</dd>
                            </dl>
                            <dl>
                                <dt>Receipt Amount :</dt>
                                <dd>$receipt_amount</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";


            if(!empty($invoice_details))
            {
                
            // echo "<Pre>";print_r($details);exit;
            $table .= "
            <h4 class='sub-title'>Receipt Invoice Details</h4>
            <div class='custom-table'>
            <table  class='table' id='list-table'>
                    <thead>
                      <tr>
                        <th>Sl. No</th>
                        <th>Invoice Number</th>
                        <th>Invoice Total Amount </th>
                        <th>Invoice Balance Amount </th>
                        </tr>
                    </thead>";
                        $total_detail = 0;
                        for($i=0;$i<count($invoice_details);$i++)
                        {
                            $id = $invoice_details[$i]->id;
                            $paid_amount = $invoice_details[$i]->paid_amount;
                            $remarks = $invoice_details[$i]->remarks;
                            $invoice_total = $invoice_details[$i]->invoice_total;
                            $invoice_number = $invoice_details[$i]->invoice_number;
                            $remarks = $invoice_details[$i]->remarks;
                            $balance_amount = $invoice_details[$i]->balance_amount;




                           


                            $j=$i+1;

                            $table .= "
                            <tbody>
                            <tr>
                                <td>$j</td>
                                <td>$invoice_number</td>                                
                                <td>$invoice_total</td>
                                <td>$balance_amount</td>
                            </tr>";
                                    // <span onclick='deleteTempData($id)'>Delete</a>
                        }

                         $table .= "
                        </tbody>
                        </table>
            </div>
            <br>";
            }
            else
            {
                $table .= "<h4 class='sub-title'>Invoice Details Not Available</h4>";
            }

            if(!empty($receipt_payment_details))
            {
                
            // echo "<Pre>";print_r($details);exit;
            $table .= "
            <h4 class='sub-title'>Receipt Details</h4>
            <div class='custom-table'>
            <table  class='table' id='list-table'>
                    <thead>
                      <tr>
                        <th>Sl. No</th>
                        <th>Payment Type</th>
                        <th>Reference Number </th>
                        <th>Amount </th>
                        </tr>
                    </thead>";
                        $total_detail = 0;
                        for($i=0;$i<count($receipt_payment_details);$i++)
                        {
                            $id = $receipt_payment_details[$i]->id;
                            $id_payment_type = $receipt_payment_details[$i]->id_payment_type;
                            $payment_reference_number = $receipt_payment_details[$i]->payment_reference_number;
                            $paid_amount = $receipt_payment_details[$i]->paid_amount;
                            


                            $j=$i+1;

                            $table .= "
                            <tbody>
                            <tr>
                                <td>$j</td>
                                <td>$id_payment_type</td>                                
                                <td>$payment_reference_number</td>
                                <td>$paid_amount</td>
                            </tr>";
                                    // <span onclick='deleteTempData($id)'>Delete</a>
                        }

                         $table .= "
                        </tbody>
                        </table>
            </div>
            <br>";
            }
            else
            {
                $table .= "<h4 class='sub-title'>Receipt Payment Details Not Available</h4>";
            }
        
        }
        else
        {
            $table .= "";
        }
        print_r($table);exit();
    }
}
