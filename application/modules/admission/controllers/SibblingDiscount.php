<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SibblingDiscount extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('sibbling_discount_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('sibbling_discount.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));


            $data['sibblingList'] = $this->sibbling_discount_model->sibblingList($name);

            $data['searchName'] = $name;
            $this->global['pageTitle'] = 'Campus Management System : Sibbling Discount';
            //print_r($subjectDetails);exit;
            $this->loadViews("sibbling_discount/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('sibbling_discount.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // print_r(expression)

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $data = array(
                    'name' => $name,
                    'amount' => $amount,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'status' => $status
                );
            
                $result = $this->sibbling_discount_model->addNewSibblingDiscount($data);
                redirect('/admission/sibblingDiscount/list');
            }

            $this->global['pageTitle'] = 'Campus Management System : Add Sibbling Discount';
            $this->loadViews("sibbling_discount/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('sibbling_discount.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/admission/sibblingDiscount/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $data = array(
                    'name' => $name,
                    'amount' => $amount,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'status' => $status
                );
                $result = $this->sibbling_discount_model->editSibblingDiscountDetails($data,$id);
                redirect('/admission/sibblingDiscount/list');
            }

            $data['sibblingDiscountDetails'] = $this->sibbling_discount_model->getSibblingDiscountDetails($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Sibbling Discount';
            $this->loadViews("sibbling_discount/edit", $this->global, $data, NULL);
        }
    }
}

