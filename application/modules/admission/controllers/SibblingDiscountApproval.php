<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SibblingDiscountApproval extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('sibbling_discount_approval_model');
        $this->load->model('applicant_approval_model');
        $this->load->model('setup/country_model');
        $this->load->model('setup/state_model');

        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('sibbling_discount_approval.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['intakeList'] = $this->sibbling_discount_approval_model->intakeList();
            $data['programList'] = $this->sibbling_discount_approval_model->programList();


            $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));


            $data['searchParam'] = $formData;

            $data['sibblingList'] = $this->sibbling_discount_approval_model->sibblingList($formData);

            $this->global['pageTitle'] = 'Campus Management System : Sibbling Discount';
            $data['applicantList'] = $this->sibbling_discount_approval_model->programList();
            //print_r($subjectDetails);exit;
            $this->loadViews("sibbling_discount_approval/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('sibbling_discount_approval.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/admission/sibblingDiscountApproval/list');
            }

            $data['sibblingDiscountDetails'] = $this->sibbling_discount_approval_model->getSibblingDiscountDetails($id);
            // echo "<Pre>";print_r($data['sibblingDiscountDetails']); exit();
            $id_applicant = $data['sibblingDiscountDetails']->id_applicant;

            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->session_id;



                $sibbling_status = $this->security->xss_clean($this->input->post('sibbling_status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'sibbling_status' => $sibbling_status,
                    'rejected_by' => $id_user,
                    'rejected_on' => date('Y-m-d H:i:s'),
                    'reason' => $reason
                );
                $result = $this->sibbling_discount_approval_model->editSibblingDiscountDetails($data,$id);
                if($result)
                {
                    if($sibbling_status == 'Approved')
                    {
                        $master_data = array('is_sibbling_discount' => 1);
                    }
                    elseif($sibbling_status == 'Reject')
                    {
                        $master_data = array('is_sibbling_discount' => 2);
                    }
                    $updated_applicant = $this->sibbling_discount_approval_model->editApplicantDetails($master_data,$id_applicant);
                }
                
                redirect('/admission/sibblingDiscountApproval/list');
            }
            // $data['degreeTypeList'] = $this->sibbling_discount_approval_model->qualificationListByStatus('1');
            $data['raceList'] = $this->sibbling_discount_approval_model->raceList();
            $data['religionList'] = $this->sibbling_discount_approval_model->religionList();
            $data['salutationList'] = $this->sibbling_discount_approval_model->salutationListByStatus('1');
            $data['degreeTypeList'] = $this->sibbling_discount_approval_model->qualificationList();
            $data['countryList'] = $this->country_model->countryList();
            $data['stateList'] = $this->state_model->stateList();
            $data['programList'] = $this->sibbling_discount_approval_model->programList();
            $data['intakeList'] = $this->sibbling_discount_approval_model->intakeList();
            $data['branchList'] = $this->sibbling_discount_approval_model->branchListByStatus();
            $data['requiremntListList'] = $this->sibbling_discount_approval_model->programRequiremntListList();
            $data['partnerUniversityList'] = $this->sibbling_discount_approval_model->getUniversityListByStatus('1');
            $data['schemeList'] = $this->sibbling_discount_approval_model->schemeListByStatus('1');
            $data['programStructureTypeList'] = $this->sibbling_discount_approval_model->programStructureTypeListByStatus('1');

            $data['nationalityList'] = $this->sibbling_discount_approval_model->nationalityListByStatus('1');
            

            $data['programEntryRequirementList'] = $this->sibbling_discount_approval_model->programEntryRequirementList($data['sibblingDiscountDetails']->id_program);
            $data['getProgramDetails'] = $this->sibbling_discount_approval_model->getProgramDetails($data['sibblingDiscountDetails']->id_program);
            $data['applicantUploadedFiles'] = $this->sibbling_discount_approval_model->getApplicantUploadedFiles($id_applicant);


            
            // echo "<Pre>";print_r($data['getProgramDetails']); exit();
            $this->global['pageTitle'] = 'Campus Management System : Edit Sibbling Discount Approval';
            $this->loadViews("sibbling_discount_approval/edit", $this->global, $data, NULL);
        }
    }
}

