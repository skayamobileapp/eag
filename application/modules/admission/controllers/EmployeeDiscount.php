<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class EmployeeDiscount extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('employee_discount_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('employee_discount.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));

            $data['employeeList'] = $this->employee_discount_model->employeeList($name);
            $data['searchName'] = $name;

            $this->global['pageTitle'] = 'Campus Management System : Employee Discount';
            //print_r($subjectDetails);exit;
            $this->loadViews("employee_discount/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('employee_discount.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // print_r(expression)

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $id_employee = $this->security->xss_clean($this->input->post('id_employee'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $data = array(
                    'name' => $name,
                    'amount' => $amount,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'status' => $status
                );
            
                $result = $this->employee_discount_model->addNewEmployeeDiscount($data);
                redirect('/admission/employeeDiscount/list');
            }

            $data['employeesList'] = $this->employee_discount_model->employeesList();
            $this->global['pageTitle'] = 'Campus Management System : Add Employee Discount';
            $this->loadViews("employee_discount/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('employee_discount.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/admission/employeeDiscount/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $id_employee = $this->security->xss_clean($this->input->post('id_employee'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $data = array(
                    'name' => $name,
                    'amount' => $amount,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'status' => $status
                );
                
                $result = $this->employee_discount_model->editEmployeeDiscountDetails($data,$id);
                redirect('/admission/employeeDiscount/list');
            }
            
            $data['employeesList'] = $this->employee_discount_model->employeesList();
            $data['employeeDiscountDetails'] = $this->employee_discount_model->getEmployeeDiscountDetails($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Employee Discount';
            $this->loadViews("employee_discount/edit", $this->global, $data, NULL);
        }
    }
}

