<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Applicant extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('applicant_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('applicant.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['intakeList'] = $this->applicant_model->intakeList();
            $data['programList'] = $this->applicant_model->programListForPostgraduate('POSTGRADUATE');

            $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['applicant_status'] = $this->security->xss_cleinterview_resultan($this->input->post('applicant_status'));
 
            $data['searchParam'] = $formData;
            $data['applicantList'] = $this->applicant_model->applicantList($formData);
            // echo "<pre>";print_r($data['applicantList']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Applicant';
            $this->loadViews("applicant/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('applicant.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            // print_r(expression)

            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->session_id;


                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $phone = $this->security->xss_clean($this->input->post('phone'));
                $email_id = $this->security->xss_clean($this->input->post('email_id'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $nric = $this->security->xss_clean($this->input->post('nric'));
                $gender = $this->security->xss_clean($this->input->post('gender'));
                $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
                $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
                $religion = $this->security->xss_clean($this->input->post('religion'));
                $nationality = $this->security->xss_clean($this->input->post('nationality'));
                $id_race = $this->security->xss_clean($this->input->post('id_race'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $mail_address1 = $this->security->xss_clean($this->input->post('mail_address1'));
                $mail_address2 = $this->security->xss_clean($this->input->post('mail_address2'));
                $mailing_country = $this->security->xss_clean($this->input->post('mailing_country'));
                $mailing_state = $this->security->xss_clean($this->input->post('mailing_state'));
                $mailing_city = $this->security->xss_clean($this->input->post('mailing_city'));
                $mailing_zipcode = $this->security->xss_clean($this->input->post('mailing_zipcode'));
                $permanent_address1 = $this->security->xss_clean($this->input->post('permanent_address1'));
                $permanent_address2 = $this->security->xss_clean($this->input->post('permanent_address2'));
                $permanent_country = $this->security->xss_clean($this->input->post('permanent_country'));
                $permanent_state = $this->security->xss_clean($this->input->post('permanent_state'));
                $permanent_city = $this->security->xss_clean($this->input->post('permanent_city'));
                $permanent_zipcode = $this->security->xss_clean($this->input->post('permanent_zipcode'));

                $sibbling_discount = $this->security->xss_clean($this->input->post('sibbling_discount'));
                
                $employee_discount = $this->security->xss_clean($this->input->post('employee_discount'));
                $is_hostel = $this->security->xss_clean($this->input->post('is_hostel'));
                $id_degree_type = $this->security->xss_clean($this->input->post('id_degree_type'));

                $data = array(

                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'full_name' => $salutation.". ".$first_name." ".$last_name,
                    'phone' => $phone,
                    'email_id' => $email_id,
                    'password' => $password,
                    'nric' => $nric,
                    'gender' => $gender,
                    'date_of_birth' => date("Y-m-d", strtotime($date_of_birth)),
                    'martial_status' => $martial_status,
                    'religion' => $religion,
                    'nationality' => $nationality,
                    'id_race' => $id_race,
                    'id_program' => $id_program,
                    'id_intake' => $id_intake,
                    'mail_address1' => $mail_address1,
                    'mail_address2' => $mail_address2,
                    'mailing_country' => $mailing_country,
                    'mailing_state' => $mailing_state,
                    'mailing_city' => $mailing_city,
                    'mailing_zipcode' => $mailing_zipcode,
                    'permanent_address1' => $permanent_address1,
                    'permanent_address2' => $permanent_address2,
                    'permanent_country' => $permanent_country,
                    'permanent_state' => $permanent_state,
                    'permanent_city' => $permanent_city,
                    'permanent_zipcode' => $permanent_zipcode,
                    'sibbling_discount' => $sibbling_discount,
                    'employee_discount' => $employee_discount,
                    'is_updated' => 1,
                    'is_hostel' => $is_hostel,
                    'id_degree_type' => $id_degree_type,
                    'created_by' => $id_user
                );

                if($sibbling_discount == 'Yes')
                {
                    $data['is_sibbling_discount'] = '0';
                }
                if($employee_discount == 'Yes')
                {
                    $data['is_employee_discount'] = '0';
                }

                // echo "<Pre>";print_r($data);exit();
                // For Discount is_sibbling_discount, is_employee_discount Fields
                // 3 -> Not Applicable / Not Applied
                // 0 -> Applied & Pending
                // 1 -> Applied &  Approved
                // 2 -> Applied & Rejected

                $inserted_id = $this->applicant_model->addNewApplicant($data);

                $sibbling_name = $this->security->xss_clean($this->input->post('sibbling_name'));
                $sibbling_nric = $this->security->xss_clean($this->input->post('sibbling_nric'));
                if ($sibbling_name == "" && $sibbling_nric== "")
                {

                }
                else
                {

                     $sibbileData = array(
                        'id_applicant' => $inserted_id,
                        'sibbling_name' => $sibbling_name,
                        'sibbling_nric' => $sibbling_nric,
                        'created_by' => $id_user
                    );

                        $result = $this->applicant_model->addNewSibblingDiscount($sibbileData);
                }
                
                $employee_name = $this->security->xss_clean($this->input->post('employee_name'));
                $employee_nric = $this->security->xss_clean($this->input->post('employee_nric'));
                $employee_designation = $this->security->xss_clean($this->input->post('employee_designation'));

                if ($employee_name == "" && $employee_nric== "" && $employee_designation == "")
                {

                }
                else
                {
                $employData = array(
                        'id_applicant' => $inserted_id,
                        'employee_name' => $employee_name,
                        'employee_nric' => $employee_nric,
                        'employee_designation' => $employee_designation,
                        'created_by' => $id_user
                    );

                $result = $this->applicant_model->addNewEmployeeDiscount($employData);
                }
                redirect('/admission/applicant/list');
            }

            $data['intakeList'] = $this->applicant_model->intakeList();
            $data['programList'] = $this->applicant_model->programListByStatus('1');
            $data['raceList'] = $this->applicant_model->raceListByStatus('1');
            $data['religionList'] = $this->applicant_model->religionListByStatus('1');
            $data['nationalityList'] = $this->applicant_model->nationalityListByStatus('1');
            $data['countryList'] = $this->applicant_model->countryListByStatus('1');
            $data['stateList'] = $this->applicant_model->stateList();
            $data['programModeList'] = $this->applicant_model->programModeList();
            $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
            $this->global['pageTitle'] = 'Campus Management System : Add Applicant';
            $this->loadViews("applicant/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('applicant.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/admission/applicant/list');
            }
            if($this->input->post())
            {
                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $phone = $this->security->xss_clean($this->input->post('phone'));
                $email_id = $this->security->xss_clean($this->input->post('email_id'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $nric = $this->security->xss_clean($this->input->post('nric'));
                $gender = $this->security->xss_clean($this->input->post('gender'));
                $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
                $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
                $religion = $this->security->xss_clean($this->input->post('religion'));
                $nationality = $this->security->xss_clean($this->input->post('nationality'));
                $id_race = $this->security->xss_clean($this->input->post('id_race'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $mail_address1 = $this->security->xss_clean($this->input->post('mail_address1'));
                $mail_address2 = $this->security->xss_clean($this->input->post('mail_address2'));
                $mailing_country = $this->security->xss_clean($this->input->post('mailing_country'));
                $mailing_state = $this->security->xss_clean($this->input->post('mailing_state'));
                $mailing_city = $this->security->xss_clean($this->input->post('mailing_city'));
                $mailing_zipcode = $this->security->xss_clean($this->input->post('mailing_zipcode'));
                $permanent_address1 = $this->security->xss_clean($this->input->post('permanent_address1'));
                $permanent_address2 = $this->security->xss_clean($this->input->post('permanent_address2'));
                $permanent_country = $this->security->xss_clean($this->input->post('permanent_country'));
                $permanent_state = $this->security->xss_clean($this->input->post('permanent_state'));
                $permanent_city = $this->security->xss_clean($this->input->post('permanent_city'));
                $permanent_zipcode = $this->security->xss_clean($this->input->post('permanent_zipcode'));

                $sibbling_discount = $this->security->xss_clean($this->input->post('sibbling_discount'));
                
                $employee_discount = $this->security->xss_clean($this->input->post('employee_discount'));
                $is_hostel = $this->security->xss_clean($this->input->post('is_hostel'));
                $id_degree_type = $this->security->xss_clean($this->input->post('id_degree_type'));

                $data = array(

                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'full_name' => $salutation.". ".$first_name." ".$last_name,
                    'phone' => $phone,
                    'email_id' => $email_id,
                    'password' => $password,
                    'nric' => $nric,
                    'gender' => $gender,
                    'date_of_birth' => date("Y-m-d", strtotime($date_of_birth)),
                    'martial_status' => $martial_status,
                    'religion' => $religion,
                    'nationality' => $nationality,
                    'id_race' => $id_race,
                    'id_program' => $id_program,
                    'id_intake' => $id_intake,
                    'mail_address1' => $mail_address1,
                    'mail_address2' => $mail_address2,
                    'mailing_country' => $mailing_country,
                    'mailing_state' => $mailing_state,
                    'mailing_city' => $mailing_city,
                    'mailing_zipcode' => $mailing_zipcode,
                    'permanent_address1' => $permanent_address1,
                    'permanent_address2' => $permanent_address2,
                    'permanent_country' => $permanent_country,
                    'permanent_state' => $permanent_state,
                    'permanent_city' => $permanent_city,
                    'permanent_zipcode' => $permanent_zipcode,
                    'sibbling_discount' => $sibbling_discount,
                    'employee_discount' => $employee_discount,
                    'is_updated' => 1,
                    'is_hostel' => $is_hostel,
                    'id_degree_type' => $id_degree_type,
                    'updated_by' => $id_user
                );

                if($sibbling_discount == 'Yes')
                {
                    $data['is_sibbling_discount'] = '0';
                }
                elseif($sibbling_discount == 'No')
                {
                    $data['is_sibbling_discount'] = '3';
                }
                if($employee_discount == 'Yes')
                {
                    $data['is_employee_discount'] = '0';
                }
                elseif($employee_discount == 'No')
                {
                    $data['is_sibbling_discount'] = '3';
                }

                $sibbling_name = $this->security->xss_clean($this->input->post('sibbling_name'));
                $sibbling_nric = $this->security->xss_clean($this->input->post('sibbling_nric'));

                     $sibbileData = array(
                        'sibbling_name' => $sibbling_name,
                        'sibbling_nric' => $sibbling_nric,
                    );
                    
                    $result = $this->applicant_model->editSibblingDetails($sibbileData, $id);

                $employee_name = $this->security->xss_clean($this->input->post('employee_name'));
                $employee_nric = $this->security->xss_clean($this->input->post('employee_nric'));
                $employee_designation = $this->security->xss_clean($this->input->post('employee_designation'));

                $employData = array(
                        'employee_name' => $employee_name,
                        'employee_nric' => $employee_nric,
                        'employee_designation' => $employee_designation
                    );
                
                $result = $this->applicant_model->editEmployeeDetails($employData, $id);

                $result = $this->applicant_model->editApplicantDetails($data,$id);
                redirect('/admission/applicant/list');
            }
            $data['intakeList'] = $this->applicant_model->intakeList();
            $data['programList'] = $this->applicant_model->programList();
            $data['countryList'] = $this->applicant_model->countryListByStatus('1');
            $data['stateList'] = $this->applicant_model->stateList();
            $data['degreeTypeList'] = $this->applicant_model->qualificationList();
            $data['raceList'] = $this->applicant_model->raceListByStatus('1');
            $data['religionList'] = $this->applicant_model->religionListByStatus('1');

            $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetails($id);
            $data['sibblingDiscountDetails'] = $this->applicant_model->getApplicantSibblingDiscountDetails($id);
            $data['employeeDiscountDetails'] = $this->applicant_model->getApplicantEmployeeDiscountDetails($id);
            // echo "<Pre>"; print_r($data['degreeTypeList']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Edit Applicant';
            $this->loadViews("applicant/edit", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('applicant_approval.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['intakeList'] = $this->applicant_model->intakeList();
            $data['programList'] = $this->applicant_model->programList();


                $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
                $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
                $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
                $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
                $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
                $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));
 
            $data['applicantList'] = $this->applicant_model->applicantListForApproval($formData);
            $data['searchParam'] = $formData;



            $this->global['pageTitle'] = 'Campus Management System : Applicant Approval';
            //print_r($subjectDetails);exit;
            $this->loadViews("applicant/approval_list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('applicant.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/admission/applicantApproval/list');
            }
            if($this->input->post())
            {
                
                $applicant_status = $this->security->xss_clean($this->input->post('applicant_status'));


                $data = array(
                    'applicant_status' => $applicant_status,
                    'approved_dt_tm' => date('Y-m-d')
                );
                $result = $this->applicant_model->editApplicantDetails($data,$id);
                if ($applicant_status == "Approved")
                {
                    $this->applicant_model->createNewMainInvoiceForStudent($id);
                    // $insert_id =  $this->applicant_approval_model->addNewStudent($id);
                    // if($insert_id)
                    // {
                    //     $this->applicant_approval_model->addStudentProfileDetail($insert_id);

                        // echoPavan "<Pre>";print_r($insert_id);exit;
                        // $this->applicant_approval_model->createNewMainInvoiceForStudent($id,'4');
                    // }
                }
                redirect('/admission/applicantApproval/list');
            }
            $data['intakeList'] = $this->applicant_model->intakeList();
            $data['programList'] = $this->applicant_model->programList();
            $data['nationalityList'] = $this->applicant_model->nationalityList();
            $data['countryList'] = $this->applicant_model->countryListByStatus('1');
            $data['stateList'] = $this->applicant_model->stateList();
            $data['degreeTypeList'] = $this->applicant_model->qualificationList();
            
            $data['raceList'] = $this->applicant_model->raceListByStatus('1');
            $data['religionList'] = $this->applicant_model->religionListByStatus('1');
            $data['salutationList'] = $this->applicant_model->salutationListByStatus('1');
            $data['branchList'] = $this->applicant_model->branchListByStatus();
            $data['requiremntListList'] = $this->applicant_model->programRequiremntListList();
            $data['partnerUniversityList'] = $this->applicant_model->getUniversityListByStatus('1');
            $data['schemeList'] = $this->applicant_model->schemeListByStatus('1');
            $data['programStructureTypeList'] = $this->applicant_model->programStructureTypeListByStatus('1');

            

            $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetails($id);
            $data['sibblingDiscountDetails'] = $this->applicant_model->getApplicantSibblingDiscountDetails($id);
            $data['employeeDiscountDetails'] = $this->applicant_model->getApplicantEmployeeDiscountDetails($id);
            $data['alumniDiscountDetails'] = $this->applicant_model->getApplicantAlumniDiscountDetails($id);



            $data['receiptStatus'] = $this->applicant_model->getReceiptStatus($id);


            $data['feeStructureDetails'] = $this->applicant_model->getfeeStructureMasterByApplicant($id);

            


            $invoiceDetails = $this->applicant_model->applicantInvoice($id);
            $data['applicantInvoice'] = $invoiceDetails;

            if($invoiceDetails)
            {
                $data['applicantInvoiceDetails'] = $this->applicant_model->applicantInvoiceDetails($invoiceDetails->id);
                $data['applicantInvoiceDiscountDetails'] = $this->applicant_model->getMainInvoiceDiscountDetails($invoiceDetails->id);
            }
            
            
            $data['sibblingDiscount'] = $this->applicant_model->getSibblingDiscountByApplicantIdCurrency($id);
            $data['employeeDiscount'] = $this->applicant_model->getEmployeeDiscountByApplicantIdCurrency($id);
            $data['alumniDiscount'] = $this->applicant_model->getAlumniDiscountByApplicantIdCurrency($id);

            // echo "<Pre>";print_r($data['applicantInvoice']);exit;
            
            
            $data['programEntryRequirementList'] = $this->applicant_model->programEntryRequirementList($data['getApplicantDetails']->id_program);
            $data['programDetails'] = $this->applicant_model->getProgramDetails($data['getApplicantDetails']->id_program);
            $data['applicantUploadedFiles'] = $this->applicant_model->getApplicantUploadedFiles($id);
            
            
            $this->global['pageTitle'] = 'Campus Management System : View Applicant Approval';
            $this->loadViews("applicant/view", $this->global, $data, NULL);
        }
    }

    function getStateByCountry($id_country)
    {
            $results = $this->applicant_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='mailing_state' id='mailing_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function getStateByCountryPermanent($id_country)
    {
            $results = $this->applicant_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
        <script type='text/javascript'>
             $('select').select2();
         </script>
         ";

            $table.="
            <select name='permanent_state' id='permanent_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }


    function getIntakeByProgramme($id_programme)
    {
            $intake_data = $this->applicant_model->getIntakeByProgrammeId($id_programme);

            // echo "<Pre>"; print_r($intake_data);exit;

            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_intake' id='id_intake' class='form-control' onchange='checkFeeStructure()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $year = $intake_data[$i]->year;

            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function checkFeeStructure()
    {
        $id_session = $this->session->session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $result = $this->applicant_model->checkFeeStructure($tempData);
        if($result == '')
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }
    }

    function getIntakeDetails($id_intake)
    {
        $intake_data = $this->applicant_model->getIntakeDetails($id_intake);
        // echo "<Pre>"; print_r($intake_data);exit;

        $is_alumni_discount = $intake_data->is_alumni_discount;
        $is_employee_discount = $intake_data->is_employee_discount;
        $is_sibbling_discount = $intake_data->is_sibbling_discount;


        $table = "

        <input type='hidden' name='is_intake_alumni_discount' id='is_intake_alumni_discount' value='$is_alumni_discount' />
        <input type='hidden' name='is_intake_employee_discount' id='is_intake_employee_discount' value='$is_employee_discount' />
        <input type='hidden' name='is_intake_sibbling_discount' id='is_intake_sibbling_discount' value='$is_sibbling_discount' />";
        
        echo $table;


    }
}
