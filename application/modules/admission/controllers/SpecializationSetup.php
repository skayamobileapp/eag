<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SpecializationSetup extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('specialization_setup_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('specialization_setup.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['specializationSetupList'] = $this->specialization_setup_model->specializationSetupListSearch($name);
            $this->global['pageTitle'] = 'Campus Management System : List Qualification Setup';
            //print_r($subjectDetails);exit;
            $this->loadViews("specialization_setup/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('specialization_setup.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status
                );
            
                $duplicate_row = $this->specialization_setup_model->checkSpecializationSetupDuplication($data);

                if($duplicate_row)
                {
                    echo "Duplicate Qualification Setup Not Allowed";exit();
                }

                $result = $this->specialization_setup_model->addNewSpecializationSetup($data);
                redirect('/admission/specializationSetup/list');
            }
            
            $this->global['pageTitle'] = 'Campus Management System : Add Qualification Setup';
            $this->loadViews("specialization_setup/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('specialization_setup.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/admission/specializationSetup/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status
                );

                $duplicate_row = $this->specialization_setup_model->checkSpecializationSetupDuplicationEdit($data,$id);

                if($duplicate_row)
                {
                    echo "Duplicate Qualification Setup Not Allowed";exit();
                }
                
                $result = $this->specialization_setup_model->editSpecializationSetup($data,$id);
                redirect('/admission/specializationSetup/list');
            }
            $data['specializationSetup'] = $this->specialization_setup_model->getSpecializationSetup($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Qualification Setup';
            $this->loadViews("specialization_setup/edit", $this->global, $data, NULL);
        }
    }
}
