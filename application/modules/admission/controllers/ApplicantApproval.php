<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ApplicantApproval extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('applicant_model');
        $this->load->model('applicant_approval_model');

        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('applicant_approval.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['intakeList'] = $this->applicant_model->intakeList();
            $data['programList'] = $this->applicant_model->programList();


            $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));
            // $formData['status'] = $this->security->xss_clean($this->input->post('status'));

            if($formData['applicant_status'] == '')
            {
                $formData['applicant_status'] = 'Submitted';
            }
 
            $data['applicantList'] = $this->applicant_approval_model->applicantListForApproval($formData);
            $data['searchParam'] = $formData;



            $this->global['pageTitle'] = 'Campus Management System : Applicant Approval';
            //print_r($subjectDetails);exit;
            $this->loadViews("applicant_approval/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('applicant_approval.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/applicantApproval/list');
            }
            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit;

                $id_user = $this->session->userId;
                $id_session = $this->session->session_id;

                
                $applicant_status = $this->security->xss_clean($this->input->post('applicant_status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'applicant_status' => $applicant_status,
                    'approved_dt_tm' => date('Y-m-d'),
                    'reason' => $reason,
                    'approved_by' => $id_user
                );
                $result = $this->applicant_approval_model->editApplicantDetails($data,$id);
                if ($applicant_status == "Approved")
                {
                    $id_university = $getApplicantDetails->id_university;
                    $partner_university = $this->applicant_approval_model->getPartnerUniversity($id_university);

                    if($id_university == 1)
                    {
                        // Hided On 13-09-2020 This Hided Because Need To Know Complete Flow Of Invoice Generation
                        // Enabled On 09-11-2020 Because Need To Show Demo Flow Of Invoice Generation For Meeting
                        $this->applicant_approval_model->createNewMainInvoiceForStudent($id);
                        

                        // Hided On 08-08-2020 FOr New Student Registration This Move Shifted To Registration->Student->approvalList
                        // $insert_id =  $this->applicant_approval_model->addNewStudent($id);
                        // if($insert_id)
                        // {
                            // $this->applicant_approval_model->addStudentProfileDetail($insert_id);

                            // echo "<Pre>";print_r($applicant_status);exit;
                            // $this->applicant_approval_model->createNewMainInvoiceForStudent($id,'4');
                        // }
                    }
                    elseif($partner_university->billing_to == 'Student')
                    {
                        $this->applicant_approval_model->createNewMainInvoiceForStudent($id);
                    }
                }
                redirect('/research/applicantApproval/list');
            }

            $data['raceList'] = $this->applicant_approval_model->raceList();
            $data['religionList'] = $this->applicant_approval_model->religionList();
            $data['salutationList'] = $this->applicant_approval_model->salutationListByStatus('1');
            $data['intakeList'] = $this->applicant_approval_model->intakeList();
            $data['programList'] = $this->applicant_approval_model->programList();
            $data['nationalityList'] = $this->applicant_approval_model->nationalityListByStatus('1');
            $data['countryList'] = $this->applicant_approval_model->countryListByStatus('1');
            $data['stateList'] = $this->applicant_approval_model->stateListByStatus('1');
            $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);
            $data['degreeTypeList'] = $this->applicant_model->qualificationList();
            $data['branchList'] = $this->applicant_model->branchListByStatus();
            $data['requiremntListList'] = $this->applicant_model->programRequiremntListList();
            $data['partnerUniversityList'] = $this->applicant_model->getUniversityListByStatus('1');
            $data['schemeList'] = $this->applicant_model->schemeListByStatus('1');
            $data['programStructureTypeList'] = $this->applicant_approval_model->programStructureTypeListByStatus('1');
            
            $data['sibblingDiscountDetails'] = $this->applicant_approval_model->getApplicantSibblingDiscountDetails($id);
            $data['employeeDiscountDetails'] = $this->applicant_approval_model->getApplicantEmployeeDiscountDetails($id);
            $data['alumniDiscountDetails'] = $this->applicant_approval_model->getApplicantAlumniDiscountDetails($id);


            $data['feeStructureDetails'] = $this->applicant_approval_model->getfeeStructureMasterByApplicant($id);

            $id_fee_structure = $this->applicant_approval_model->getfeeStructureMasterByApplicantID($id);
            $fee_structure = $this->applicant_approval_model->getfeeStructureMaster($id);
            
            $data['id_fee_structure'] = $id_fee_structure;
            $data['feeStructure'] = $fee_structure;


            $data['sibblingDiscount'] = $this->applicant_approval_model->getSibblingDiscountByApplicantIdCurrency($id);
            $data['employeeDiscount'] = $this->applicant_approval_model->getEmployeeDiscountByApplicantIdCurrency($id);
            $data['alumniDiscount'] = $this->applicant_approval_model->getAlumniDiscountByApplicantIdCurrency($id);

            

            // echo "<Pre>";print_r($data['feeStructureDetails']);exit;


            $data['programEntryRequirementList'] = $this->applicant_model->programEntryRequirementList($data['getApplicantDetails']->id_program);
            $data['programDetails'] = $this->applicant_model->getProgramDetails($data['getApplicantDetails']->id_program);
            $data['applicantUploadedFiles'] = $this->applicant_model->getApplicantUploadedFiles($id);

            // echo "<Pre>";print_r($data['getApplicantDetails']);exit;
            
            $this->global['pageTitle'] = 'Campus Management System : Edit Applicant Approval';
            $this->loadViews("applicant_approval/edit", $this->global, $data, NULL);
        }
    }


    function step1($id = NULL){

       

       
        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);
         if($this->input->post())
            {
                $datas = array();

                 $datas['step1_status'] = $this->security->xss_clean($this->input->post('step1_status'));
                 $datas['step1_comments'] = $this->security->xss_clean($this->input->post('step1_comments'));
                 $this->applicant_model->editApplicantDetails($datas,$id);
                                 redirect('/admission/applicantApproval/step2/'.$id);

            }


        

        $data['intakeList'] = $this->applicant_model->intakeList();
        $data['programList'] = $this->applicant_model->programList();
        $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        $data['stateList'] = $this->applicant_model->stateList();
        $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
        $data['raceList'] = $this->applicant_model->raceListByStatus('1');
        $data['religionList'] = $this->applicant_model->religionListByStatus('1');
        $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
        $data['salutationList'] = $this->applicant_model->salutationListByStatus('1'); 
        $data['nationalityList'] = $this->applicant_model->nationalityListByStatus('1');
 
        $this->global['pageTitle'] = 'Campus Management System : Edit Applicant';            
        $this->loadViews("applicant_approval/step1", $this->global, $data, NULL);
    

    }

    function step2($id = NULL)
    {
       if($this->input->post())
        {
            $datas = array();

             $datas['step2_status'] = $this->security->xss_clean($this->input->post('step2_status'));
             $datas['step2_comments'] = $this->security->xss_clean($this->input->post('step2_comments'));
             $this->applicant_model->editApplicantDetails($datas,$id);
                             redirect('/admission/applicantApproval/step3/'.$id);

        }

        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

        $data['intakeList'] = $this->applicant_model->intakeList();
        $data['programList'] = $this->applicant_model->programList();
        $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        $data['stateList'] = $this->applicant_model->stateList();
        $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
        $data['raceList'] = $this->applicant_model->raceListByStatus('1');
        $data['religionList'] = $this->applicant_model->religionListByStatus('1');
        $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
        $data['salutationList'] = $this->applicant_model->salutationListByStatus('1');

        $this->global['pageTitle'] = 'Campus Management System : Edit Applicant';            
        $this->loadViews("applicant_approval/step2", $this->global, $data, NULL);
    }


    function step3($id = NULL)
    {
        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);
       

       if($this->input->post())
        {
            $datas = array();

             $datas['step3_status'] = $this->security->xss_clean($this->input->post('step3_status'));
             $datas['portfolio_result'] = $this->security->xss_clean($this->input->post('portfolio_result'));
             $datas['portfolio_grade'] = $this->security->xss_clean($this->input->post('portfolio_grade'));

             $datas['challenged_test_status'] = $this->security->xss_clean($this->input->post('challenged_test_status'));
             $datas['challenged_test_grade'] = $this->security->xss_clean($this->input->post('challenged_test_grade'));

             $datas['interview_result'] = $this->security->xss_clean($this->input->post('interview_result'));
             $datas['interview_grade'] = $this->security->xss_clean($this->input->post('interview_grade'));

             
             $datas['step3_comments'] = $this->security->xss_clean($this->input->post('step3_comments'));
             $this->applicant_model->editApplicantDetails($datas,$id);
            redirect('/admission/applicantApproval/step4/'.$id);

        }




        $data['intakeList'] = $this->applicant_model->intakeList();
        // $data['programList'] = $this->applicant_model->programList();
        // $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        // $data['stateList'] = $this->applicant_model->stateList();
        // $data['raceList'] = $this->applicant_model->raceListByStatus('1');
        $data['degreeTypeList'] = $this->applicant_model->qualificationListByStatus('1');
        $data['religionList'] = $this->applicant_model->religionListByStatus('1');
        $data['salutationList'] = $this->applicant_model->salutationListByStatus('1');  
        $data['partnerUniversityList'] = $this->applicant_model->getUniversityListByStatus('1');

         // echo "<Pre>"; print_r($data);exit();

        $this->global['pageTitle'] = 'Campus Management System : Edit Applicant';  
        $this->loadViews("applicant_approval/step3", $this->global, $data, NULL);
    }
    
    function step4($id=NULL)
    {

        if($this->input->post())
        {

            // print_r($this->input->post());exit;
            $postData = $this->input->post();
            $datas = array();


            for($i=0;$i<count($postData['file_status']);$i++)
            {
                $file_status = $postData['file_status'][$i];
                $file_reason = $postData['file_reason'][$i];
                $file_id = $postData['file_id'][$i];

                $filestatusArray = array();
                $filestatusArray['reason'] = $file_reason;
                $filestatusArray['status'] = $file_status;

                $this->applicant_model->updateFileApplicant($filestatusArray,$file_id);
            }

            $datas['step4_status'] = $this->security->xss_clean($this->input->post('step4_status'));
            $datas['step4_comments'] = $this->security->xss_clean($this->input->post('step4_comments'));
            $this->applicant_model->editApplicantDetails($datas,$id);
            redirect('/admission/applicantApproval/step5/'.$id);
        }

        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);
        $data['applicantUploadedFiles'] = $this->applicant_model->getApplicantUploadedFiles($id);

        $this->global['pageTitle'] = 'Campus Management System : Applicant Approval';  
        $this->loadViews("applicant_approval/step4", $this->global, $data, NULL);
    }

    function step5($id=NULL)
    {

        if($this->input->post())
        {
            
            $datas = array();

            $datas['step5_status'] = $this->security->xss_clean($this->input->post('step5_status'));
            $datas['step5_comments'] = $this->security->xss_clean($this->input->post('step5_comments'));
            $this->applicant_model->editApplicantDetails($datas,$id);
            redirect('/admission/applicantApproval/step6/'.$id);
        }

        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);
        $data['applicantDiscountDetails'] = $this->applicant_model->getApplicantDiscountDetailsByApplicant($id);
        $data['discountDetails'] = $this->applicant_model->getApplicantDiscountDetails($data['getApplicantDetails']->id_intake);

        $this->global['pageTitle'] = 'Campus Management System : Applicant Approval';  
        $this->loadViews("applicant_approval/step5", $this->global, $data, NULL);
    }
    function step6($id=NULL)
    {

        $datagetApplicantDetails = $this->applicant_model->getApplicantDetailsById($id);



        if($this->input->post())
        {
            $datas = array();

            $datas['step6_status'] = $this->security->xss_clean($this->input->post('step6_status'));
            $datas['step6_comments'] = $this->security->xss_clean($this->input->post('step6_comments'));
            $this->applicant_model->editApplicantDetails($datas,$id);
            redirect('/admission/applicantApproval/step7/'.$id);
        }

        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetailsById($id);

        $this->global['pageTitle'] = 'Campus Management System : Applicant Approval';  
        $this->loadViews("applicant_approval/step6", $this->global, $data, NULL);
    }


     function step7($id=NULL)
    {

        $getApplicantDetails = $this->applicant_model->getApplicantDetailsById($id);



        if($this->input->post())
        {
            // echo "<Pre>"; print_r($this->input->post());exit;
            
            $datas = array();
            $datas['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));
            $datas['applicant_comments'] = $this->security->xss_clean($this->input->post('applicant_comments'));
            $this->applicant_model->editApplicantDetails($datas,$id);
            if ($datas['applicant_status'] == "Approved")
            {

                $id_university = $getApplicantDetails->id_university;
                $partner_university = $this->applicant_approval_model->getPartnerUniversity($id_university);

                if($id_university == 1)
                {
                    $this->applicant_approval_model->createNewMainInvoiceForStudent($id);
                }
                elseif($partner_university->billing_to == 'Student')
                {
                    $this->applicant_approval_model->createNewMainInvoiceForStudent($id);
                }
            }

            redirect('/admission/applicantApproval/list');
        }

        $data['getApplicantDetails'] = $getApplicantDetails;
        
        $this->global['pageTitle'] = 'Campus Management System : Applicant Approval';  
        $this->loadViews("applicant_approval/step7", $this->global, $data, NULL);
    }




    function getStateByCountry($id_country)
    {
            $results = $this->applicant_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>
                     
                 </script>
         ";

            $table.="
            <select name='mailing_state' id='mailing_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function getStateByCountryPermanent($id_country)
    {
            $results = $this->applicant_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
        <script type='text/javascript'>
             
         </script>
         ";

            $table.="
            <select name='permanent_state' id='permanent_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

     function getProgrammeByEducationLevelId($id_education_level)
    {
            // echo "<Pre>"; print_r($id_education_level);exit;
            $intake_data = $this->applicant_model->getProgrammeByEducationLevelId($id_education_level);

             $table="
            <script type='text/javascript'>
                $('select').select2();                
            </script>


            <select name='id_program' id='id_program' class='form-control' onchange='getIntakeByProgramme(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $code = $intake_data[$i]->code;

            $table.="<option value=".$id.">".$code . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;       
    }


    function getIntakeByProgramme($id_programme)
    {
            $intake_data = $this->applicant_model->getIntakeByProgrammeId($id_programme);

            // echo "<Pre>"; print_r($intake_data);exit;
             $table="
            <script type='text/javascript'>

            $('select').select2();

                
            </script>


            <select name='id_intake' id='id_intake' class='form-control' onchange='checkFeeStructure()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $year = $intake_data[$i]->year;

            $table.="<option value=".$id.">".$year . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;       
    }

    function getProgramSchemeByProgramId($id_program)
    {
        // It's A Learning Mode After Flow Change
         $intake_data = $this->applicant_model->getProgramSchemeByProgramId($id_program);
        
        // Multiple Programme Mode Ignored For Demo On 09-11-2020
        // $intake_data = $this->applicant_model->getProgramLandscapeSchemeByProgramId($id_program);

        // echo "<Pre>"; print_r($intake_data);exit;
        
        $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_program_scheme' id='id_program_scheme' class='form-control' onchange='checkFeeStructure()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $mode_of_program = $intake_data[$i]->mode_of_program;
            $mode_of_study = $intake_data[$i]->mode_of_study;

            $table.="<option value=".$id.">". $mode_of_program . " - " .  $mode_of_study .
                    "</option>";

            }
            $table.="</select>";

            echo $table;  
    }

    function getSchemeByProgramId($id_program)
    {
        // echo "<Pre>"; print_r($id_program);exit;
        // It's Actual Scheme What We Required
         $intake_data = $this->applicant_model->getSchemeByProgramId($id_program);

            // echo "<Pre>"; print_r($intake_data);exit;
             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_program_has_scheme' id='id_program_has_scheme' class='form-control' onchange='checkFeeStructure()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $code = $intake_data[$i]->code;
            $name = $intake_data[$i]->description;

            $table.="<option value=".$id.">". $code . " - " .  $name .
                    "</option>";

            }
            $table.="</select>";

            echo $table;  
    }


    function getProgramStructureTypeByProgramId($id_programme)
    {
        // echo "<Pre>"; print_r($id_program);exit;
        // It's Actual Scheme What We Required
         $intake_data = $this->applicant_model->getProgramStructureTypeByProgramId($id_programme);

        // echo "<Pre>"; print_r($intake_data);exit;


        $table = "
        
        <script type='text/javascript'>
            $('select').select2();
        </script>


        <select name='id_program_structure_type' id='id_program_structure_type' class='form-control'>";
        $table.="<option value=''>Select</option>";

        for($i=0;$i<count($intake_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $intake_data[$i]->id;
        $code = $intake_data[$i]->code;
        $name = $intake_data[$i]->name;

        $table.="<option value=".$id.">". $code . " - " .  $name .
                "</option>";
        }

        $table.="</select>";

        echo $table;
    }


    function getDocumentByProgramme($id_programme)
    {
            // echo "<Pre>"; print_r($id_programme);exit;
            $intake_data = $this->applicant_model->getDocumentByProgrammeId($id_programme);
            // echo "<Pre>"; print_r($intake_data);exit;


            $table="<div class='row'>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;

            // <span class='error-text'>*</span>
            
            $table.="<div class='col-sm-3'>
                      <label>$name </label>
                      <input type='file' name='file[]'>
                      <input type='hidden' name='fileid[]' value='$id' />
                     </div>";
            }
            $table.="</div>";

            echo $table;
    }


    function getBranchByProgram($id_programme)
    {
            // echo "<Pre>"; print_r($id_programme);exit;
            $intake_data = $this->applicant_model->getBranchByProgramId($id_programme);
            

            // echo "<Pre>"; print_r($intake_data);exit;

             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_branch' id='id_branch' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $code = $intake_data[$i]->code;

            $table.="<option value=".$id.">". $code . " - " .  $name .
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }


    function getBranchesByPartnerUniversity($id_partner_university)
    {
            // echo "<Pre>"; print_r($id_programme);exit;
            $intake_data = $this->applicant_model->getBranchesByPartnerUniversity($id_partner_university);
            

            // echo "<Pre>"; print_r($intake_data);exit;

             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_branch' id='id_branch' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $code = $intake_data[$i]->code;

            $table.="<option value=".$id.">". $code . " - " .  $name .
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getProgramByPartnerUniversity($id_partner_university) {

        // echo "<Pre>"; print_r($id_programme);exit;
            $intake_data = $this->applicant_model->getProgramByPartnerUniversity($id_partner_university);
            

            // echo "<Pre>"; print_r($intake_data);exit;

             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_program' id='id_program' class='form-control' onchange='getprogramScheme(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $code = $intake_data[$i]->code;

            $table.="<option value=".$id.">". $code . " - " .  $name .
                    "</option>";

            }
            $table.="</select>";

            echo $table;


    }

    function getIntakeDetails($id_intake)
    {
        $intake_data = $this->applicant_model->getIntakeDetails($id_intake);
        // echo "<Pre>"; print_r($intake_data);exit;

        $is_alumni_discount = $intake_data->is_alumni_discount;
        $is_employee_discount = $intake_data->is_employee_discount;
        $is_sibbling_discount = $intake_data->is_sibbling_discount;


        $table = "

        <input type='hidden' name='is_intake_alumni_discount' id='is_intake_alumni_discount' value='$is_alumni_discount' />
        <input type='hidden' name='is_intake_employee_discount' id='is_intake_employee_discount' value='$is_employee_discount' />
        <input type='hidden' name='is_intake_sibbling_discount' id='is_intake_sibbling_discount' value='$is_sibbling_discount' />";
        
        echo $table;


    }


    function checkenglish($id_program) {

         $programDetails = $this->applicant_model->programDetails($id_program);
      

         echo $programDetails->is_english_required;


    }


    function getIndividualEntryRequirement($id_program,$id_applicant)
    {


                $id = $id_applicant;


        $datagetApplicantDetails = $this->applicant_model->getApplicantDetailsById($id);


        $programEntryRequirementList = $this->applicant_model->programEntryRequirementList($id_program);
        $programDetails = $this->applicant_model->programDetails($id_program);


                    $programEntryAppelRequirementList = $this->applicant_model->programAppelRequirementList($id_program);

      


        
          $table='';
        if(!empty($programEntryRequirementList))
        {
             $checked = "";
             if($datagetApplicantDetails->id_program_requirement=='99999') {
                                     $checked = "checked=checked";
             }

            $table.= '
             <div class="form-container">
                        <h4 class="form-group-title" style="padding-left:20px;">Program APEL Details</h4>

                    

                      <div class="custom-table">
                        <table class="table">
                            <thead><tr>
                                 <th>';
                            if($datagetApplicantDetails->id_program_requirement=='99999') {
                                $table.='
                                 <input type="radio" name="entry" value="99999" checked="checked"/>';
                            } else {
                                 $table.='
                                 <input type="radio" name="entry" value="99999"/>';

                            }
                                $table.='Flexible entry via Accreditation of Prior Experiential Learning (APEL) Entry Requirements:</th>
                                </tr>
                            </thead>
                            <tbody>';


                             $total = 0;
                              for($i=0;$i<count($programEntryAppelRequirementList);$i++)
                             { 
                                $checked = "";
                                if($programEntryAppelRequirementList[$i]->id==$datagetApplicantDetails->id_program_requirement) {
                                     $checked = "checked=checked";
                                }
                                $data="";
                                $j = $i+1;

                                
                               
                                    $data.=  $programEntryAppelRequirementList[$i]->other_description. " .";
                               

                                $identry = $programEntryAppelRequirementList[$i]->id;

                            // print_r($identry);exit;



                                $table.="
                                <tr>
                                <td>$j - $data</td>
                                 </tr>";


                          } 
                        $table.='
                            </tbody>
                        </table>
                      </div>

                    </div> ';

        }


        if(!empty($programEntryRequirementList))
        {
            $table.= '
             <div class="form-container">
                        <h4 class="form-group-title" style="padding-left:20px;">Program Requirement Details</h4>

                    

                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                 <th>Entry Requirement</th>
                                </tr>
                            </thead>
                            <tbody>';


                             $total = 0;
                              for($i=0;$i<count($programEntryRequirementList);$i++)
                             { 
                                $checked = "";
                                if($programEntryRequirementList[$i]->id==$datagetApplicantDetails->id_program_requirement) {
                                     $checked = "checked=checked";
                                }
                                $data="";
                                $and="";
                                $j = $i+1;

                               
                                    $data.= $and . " " . $programEntryRequirementList[$i]->other_description. " .";
                               

                                $identry = $programEntryRequirementList[$i]->id;

                            // print_r($identry);exit;



                                $table.="
                                <tr>
                                <td><input type='radio' name='entry' value='$identry' <?php echo $checked;?>  $data</td>
                                 </tr>";


                          } 
                        $table.='
                            </tbody>
                        </table>
                      </div>

                    </div> ';

        }else
        {
            $table = '<p> No Requirements Available</p>';
        }

        print_r($table);exit;


    }



}
