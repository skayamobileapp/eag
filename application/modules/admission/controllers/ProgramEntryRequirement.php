<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProgramEntryRequirement extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('program_entry_requirement_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('program_entry_requirement.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;
            $data['programEntryRequirementList'] = $this->program_entry_requirement_model->programEntryRequirementListSearch($formData);

            $data['programList'] = $this->program_entry_requirement_model->programList();
// echo "<Pre>";print_r($data['mainInvoiceList']);exit();
            $this->global['pageTitle'] = 'Campus Management System : Entry Requirement List';
            $this->loadViews("program_entry_requirement/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('program_entry_requirement.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;                    
            
            if($this->input->post())
            {

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;


                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $from_dt = $this->security->xss_clean($this->input->post('from_dt'));
                $to_dt = $this->security->xss_clean($this->input->post('to_dt'));
                $status = $this->security->xss_clean($this->input->post('status'));


                // $invoice_number = $this->program_entry_requirement_model->generateMainInvoiceNumber();

            
                $data = array(
                    'id_program' => $id_program,
                    'description' => $description,
                    'from_dt' => date('Y-m-d',strtotime($from_dt)),
                    'to_dt' => date('Y-m-d',strtotime($to_dt)),
                    'status' => $status,
                    'created_by' => $user_id
                );

                // echo "<Pre>";print_r($data);exit;
                $inserted_id = $this->program_entry_requirement_model->addNewEntryRequirement($data);
                if($inserted_id)
                {
                    $inserted_req_id = $this->program_entry_requirement_model->moveTempRequirementsToDetail($inserted_id);
                    $inserted_other_req_id = $this->program_entry_requirement_model->moveTempOtherRequirementsToDetail($inserted_id);
                }
                redirect('/admission/programEntryRequirement/list');
            }
            else
            {
                $this->program_entry_requirement_model->deleteTempRequirementDetailsBySession($id_session);
                $this->program_entry_requirement_model->deleteTempOtherRequirementDetailsBySession($id_session);
            }
            $data['programList'] = $this->program_entry_requirement_model->programListByStatus('1');
            $data['groupList'] = $this->program_entry_requirement_model->groupListByStatus('1');
            $data['qualificationList'] = $this->program_entry_requirement_model->qualificationListByStatus('1');
            $data['specializationList'] = $this->program_entry_requirement_model->specializationListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add Entry Requirement';
            $this->loadViews("program_entry_requirement/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('program_entry_requirement.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/admission/programEntryRequirement/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;   

            if($this->input->post())
            {
                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $from_dt = $this->security->xss_clean($this->input->post('from_dt'));
                $to_dt = $this->security->xss_clean($this->input->post('to_dt'));
                $status = $this->security->xss_clean($this->input->post('status'));


                // $invoice_number = $this->program_entry_requirement_model->generateMainInvoiceNumber();

            
                $data = array(
                    'id_program' => $id_program,
                    'description' => $description,
                    'from_dt' => date('Y-m-d',strtotime($from_dt)),
                    'to_dt' => date('Y-m-d',strtotime($to_dt)),
                    'status' => $status,
                    'updated_by' => $user_id
                );

                // echo "<Pre>"; print_r($data);exit;
                $inserted_id = $this->program_entry_requirement_model->editEntryRequirement($data,$id);
                redirect('/admission/programEntryRequirement/list');
            }
            $data['entryRequirement'] = $this->program_entry_requirement_model->getEnteryRequirement($id);
            $data['requirementDetailsList'] = $this->program_entry_requirement_model->getRequirementDetailsByEntryId($id);
            $data['otherRequirementDetailsList'] = $this->program_entry_requirement_model->getOtherRequirementDetailByEntryId($id);
            $data['programList'] = $this->program_entry_requirement_model->programListByStatus('1');
            $data['groupList'] = $this->program_entry_requirement_model->groupListByStatus('1');
            $data['qualificationList'] = $this->program_entry_requirement_model->qualificationListByStatus('1');
            $data['specializationList'] = $this->program_entry_requirement_model->specializationListByStatus('1');
            $data['id_requirement_entry'] = $id;
            // echo "<Pre>";  print_r($data);exit;

            $this->global['pageTitle'] = 'Campus Management System : View Entry Requirement';
            $this->loadViews("program_entry_requirement/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('program_entry_requirement.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/admission/programEntryRequirement/list');
            }
            if($this->input->post())
            {
                redirect('/admission/programEntryRequirement/list');
            }
            $data['entryRequirement'] = $this->program_entry_requirement_model->getEnteryRequirement($id);
            $data['requirementDetailsList'] = $this->program_entry_requirement_model->getRequirementDetailsByEntryId($id);
            $data['otherRequirementDetailsList'] = $this->program_entry_requirement_model->getOtherRequirementDetailByEntryId($id);
            // echo "<Pre>";  print_r($data);exit;
            // echo "<Pre>";  print_r($data['mainInvoice']);exit;

            $this->global['pageTitle'] = 'Campus Management System : View Entry Requirement';
            $this->loadViews("program_entry_requirement/view", $this->global, $data, NULL);
        }
    }

     function tempAddTempRequirements()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        $inserted_id = $this->program_entry_requirement_model->addTempRequirementDetails($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;

        $data = $this->displayTempRequirements();
        
        echo $data;        
    }

    function displayTempRequirements()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->program_entry_requirement_model->getTempRequirementDetailsBySessionId($id_session); 
        // echo "<Pre>";print_r($temp_details);exit;
        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Group</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
                </thead>";
                $total_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $group_code = $temp_details[$i]->group_code;
                    $group_name = $temp_details[$i]->group_name;
                    $description = $temp_details[$i]->description;
                    // $qualification_code = $temp_details[$i]->qualification_code;
                    // $qualification_name = $temp_details[$i]->qualification_name;
                    // $specialization_code = $temp_details[$i]->specialization_code;
                    // $specialization_name = $temp_details[$i]->specialization_name;
                    // $validity = $temp_details[$i]->validity;
                    // $result_item = $temp_details[$i]->result_item;
                    // $condition = $temp_details[$i]->condition;
                    // $result_value = $temp_details[$i]->result_value;
                    $j = $i+1;
                        $table .= "
                    <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$group_code - $group_name</td>
                            <td>$description</td>
                            <td>
                                <a onclick='deleteTempRequirementData($id)'>Delete</a>
                            </td>
                        </tr>";
                        // $total_amount = $total_amount + $amount;
                    }

        $table.= "
        </tbody>
        </table>
        </div>";
        }
        else
        {
            $table= "";
        }
        return $table;
    }

    function deleteTempRequirementData($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $id_session = $this->session->my_session_id;
        $inserted_id = $this->program_entry_requirement_model->deleteTempRequirementDetails($id);
        $data = $this->displayTempRequirements();
        echo $data;
    }

    function tempAddOtherRequirements()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        $inserted_id = $this->program_entry_requirement_model->addTempOtherRequirementDetails($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;

        $data = $this->displayTempOtherRequirements();
        
        echo $data;     
    }

    function displayTempOtherRequirements()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->program_entry_requirement_model->getTempOtherRequirementDetailsBySessionId($id_session); 
        // echo "<Pre>";print_r($temp_details);exit;
        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Group</th>
                    <th>Condition</th>
                    <th>Type</th>
                    <th>Result Value</th>
                    <th>Action</th>
                </tr>
                </thead>";
                $total_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $group_code = $temp_details[$i]->group_code;
                    $group_name = $temp_details[$i]->group_name;
                    $type = $temp_details[$i]->type;
                    $condition = $temp_details[$i]->condition;
                    $result_value = $temp_details[$i]->result_value;
                    $j = $i+1;
                        $table .= "
                    <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$group_code - $group_name</td>
                            <td>$condition</td>
                            <td>$type</td>
                            <td>$result_value</td>                           
                            <td>
                                <a onclick='deleteTempOtherRequirementData($id)'>Delete</a>
                            </td>
                        </tr>";
                        // $total_amount = $total_amount + $amount;
                    }

        $table.= "
        </tbody>
        </table>
        </div>";
        }
        else
        {
            $table= "";
        }
        return $table;
    }

    function deleteTempOtherRequirementData($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $id_session = $this->session->my_session_id;
        $inserted_id = $this->program_entry_requirement_model->deleteTempOtherRequirementDetails($id);
        $data = $this->displayTempOtherRequirements();
        echo $data;
    }

    function directAddRequirements()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;        
        $inserted_id = $this->program_entry_requirement_model->addRequirementDetails($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;        
        echo "Sucess";     
    }


    function directAddOtherRequirements()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;        
        $inserted_id = $this->program_entry_requirement_model->addOtherRequirementDetails($tempData);
        echo "Sucess";     
    }


    function deleteRequirementData($id)
    {
        $inserted_id = $this->program_entry_requirement_model->deleteRequirementDetails($id);
        echo "Sucess";
    }



    function deleteOtherDetails($id)
    {
        $inserted_id = $this->program_entry_requirement_model->deleteOtherRequirementDetails($id);
        echo "Sucess";
    }
}
