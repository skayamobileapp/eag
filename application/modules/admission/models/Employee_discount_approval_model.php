<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Employee_discount_approval_model extends CI_Model
{
    function nationalityListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('nationality');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }
    
    function employeeList($applicantList)
    {
        $this->db->select('a.employee_status, b.*, b.id as id_applicant, a.id, ahsd.sibbling_status, ahad.alumni_status, p.code as program_code, p.name as program_name, inta.year as intake_year, inta.name as intake_name, pt.code as program_structure_code, pt.name as program_structure_name, train.name as training_center_name, train.code as training_center_code');
        $this->db->from('applicant_has_employee_discount as a');
        $this->db->join('applicant as b', 'a.id_applicant = b.id');
        $this->db->join('programme as p', 'b.id_program = p.id');
        $this->db->join('intake as inta', 'b.id_intake = inta.id');
        $this->db->join('program_type as pt', 'b.id_program_structure_type = pt.id');
        $this->db->join('organisation_has_training_center as train', 'b.id_branch = train.id');
        $this->db->join('applicant_has_sibbling_discount as ahsd', 'a.id_applicant = ahsd.id_applicant','left');
        $this->db->join('applicant_has_alumni_discount as ahad', 'a.id_applicant = ahad.id_applicant','left');
        if($applicantList['first_name']) {
            $likeCriteria = "(b.full_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['email_id']) {
            $likeCriteria = "(b.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['nric']) {
            $likeCriteria = "(b.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_program']) {
            $likeCriteria = "(b.id_program  LIKE '%" . $applicantList['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_intake']) {
            $likeCriteria = "(b.id_intake  LIKE '%" . $applicantList['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('a.employee_status', 'Pending');
        $this->db->where('b.is_submitted', '1');
        $likeCriteria = "(b.is_apeal_applied  = 3 or b.is_apeal_applied  = 1)";
        $this->db->where($likeCriteria);

        // $this->db->where('b.is_apeal_applied',8);
        // $this->db->or_where('b.is_apeal_applied',3);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getEmployeeDiscountDetails($id)
    {
        $this->db->select('a.*, b.*, ahsd.sibbling_status, ahad.alumni_status');
        $this->db->from('applicant_has_employee_discount as a');
        $this->db->join('applicant as b', 'a.id_applicant = b.id');
        $this->db->join('applicant_has_sibbling_discount as ahsd', 'a.id_applicant = ahsd.id_applicant','left');
        $this->db->join('applicant_has_alumni_discount as ahad', 'a.id_applicant = ahad.id_applicant','left');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewSibblingDiscount($data)
    {
        $this->db->trans_start();
        $this->db->insert('sibbling_discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editEmployeeDiscountDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('applicant_has_employee_discount', $data);
        return TRUE;
    }

    function editApplicantDetails($data,$id_application)
    {
        $this->db->where('id', $id_application);
        $this->db->update('applicant', $data);
        return TRUE;
    }

    function raceListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('race_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programStructureTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('program_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result; 
    }

    function raceList()
    {
        $this->db->select('*');
        $this->db->from('race_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function religionList()
    {
        $this->db->select('*');
        $this->db->from('religion_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function qualificationList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_education_level');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function intakeList()
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function salutationListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('salutation_setup as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function branchListByStatus()
    {
        $organisation = $this->getOrganisaton();
        $details = array();

        if($organisation)
        {
            array_push($details, $organisation);
        }


        $this->db->select('a.*');
        $this->db->from('organisation_has_training_center as a');
        $query = $this->db->get();
        $result = $query->result();  

        foreach ($result as $value)
        {
           array_push($details, $value);
        }
        return $details;
    }

    function getOrganisaton()
    {
        $this->db->select('a.*, a.short_name as code');
        $this->db->from('organisation as a');
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function programRequiremntListList()
    {
        $this->db->select('ier.*, qs.code as qualification_code, qs.name as qualification_name, ws.code as work_code, ws.name as work_name');
        $this->db->from('scholarship_individual_entry_requirement as ier');
        $this->db->join('scholarship_education_level as qs','ier.id_education_qualification = qs.id','left');
        $this->db->join('scholarship_work_specialisation as ws','ier.id_work_specialisation = ws.id','left');
        $query = $this->db->get();
        return $query->result();
    }

    function getProgramDetails($id_program)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme as ihs');
        $this->db->where('ihs.id', $id_program);
        $query = $this->db->get();
        return $query->row();
    }

    function programEntryRequirementList($id_program)
    {
        $this->db->select('ier.*, qs.code as qualification_code, qs.name as qualification_name, ws.code as work_code, ws.name as work_name');
        $this->db->from('scholarship_individual_entry_requirement as ier');
        $this->db->join('scholarship_education_level as qs','ier.id_education_qualification = qs.id','left');
        $this->db->join('scholarship_work_specialisation as ws','ier.id_work_specialisation = ws.id','left');
        $this->db->where('ier.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function getApplicantUploadedFiles($id_applicant)
    {
        $this->db->select('shd.*, d.code as document_code, d.name as document_name');
        $this->db->from('applicant_has_document as shd');
        $this->db->join('documents as d','shd.id_document = d.id');
        $this->db->where('shd.id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->result();
    }

    function schemeListByStatus($status)
    {
        $this->db->select('ihs.*');
        $this->db->from('scheme as ihs');
        $this->db->where('ihs.status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function getUniversityListByStatus($status)
    {
        $organisation = $this->getOrganisaton();
        $details = array();

        if($organisation)
        {
            array_push($details, $organisation);
        }

        $this->db->select('ahemd.*');
        $this->db->from('partner_university as ahemd');
        $this->db->where('ahemd.status', $status);
        $query = $this->db->get();
        $results = $query->result();

        foreach ($results as $result)
        {
            array_push($details, $result);
        }
        return $details;
    }
}