<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ScholarshipApplication extends BaseController
{
    public function __construct()
    {
        // $test = new BaseController();
        // $test->isStudentLoggedIn();
        parent::__construct();
        $this->load->model('scholarship_application_model');
        $this->isStudentLoggedIn();
    }

    function list()
    {       
        $id = $this->session->id_student;
        $check['id_student'] = $this->session->id_student;
        $check['id_intake'] = $this->session->id_intake;
        $check['id_program'] = $this->session->id_program;
        
        $data['studentDetails'] = $this->scholarship_application_model->getStudentByStudentId($id);
        $data['scholarshipApplicationList'] = $this->scholarship_application_model->getScholarshipApplicationListByStudentId($id);


        $data['maxLimit'] = $this->scholarship_application_model->getMaxLimit();
        $data['check_limit'] = $this->scholarship_application_model->checkStudentScholarshipApplication($check);


                 // echo "<Pre>";print_r($data);exit();

        $this->global['studentPageCode'] = 'scholarship_application.list';
        $this->global['pageTitle'] = 'Student Portal : List Scholarship Application';
        $this->loadViews("scholarship/list", $this->global, $data, NULL);
    }
    
    function add()
    {

        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;

        if($this->input->post())
        {
            $form = $this->input->post();


            $id_scholarship_scheme = $this->security->xss_clean($this->input->post('id_scholarship_scheme'));
            $father_name = $this->security->xss_clean($this->input->post('father_name'));
            $mother_name = $this->security->xss_clean($this->input->post('mother_name'));
            $father_deceased = $this->security->xss_clean($this->input->post('father_deceased'));
            $father_occupation = $this->security->xss_clean($this->input->post('father_occupation'));
            $no_siblings = $this->security->xss_clean($this->input->post('no_siblings'));
            $year = $this->security->xss_clean($this->input->post('year'));
            $result_item = $this->security->xss_clean($this->input->post('result_item'));
            $max_marks = $this->security->xss_clean($this->input->post('max_marks'));
            $obtained_marks = $this->security->xss_clean($this->input->post('obtained_marks'));
            $percentage = $this->security->xss_clean($this->input->post('percentage'));
            $est_fee = $this->security->xss_clean($this->input->post('est_fee'));


            $generated_number = $this->scholarship_application_model->generateScholarshipApplicationNumber();


            $data = array(
                'id_scholarship_scheme' => $id_scholarship_scheme,
                'application_number' => $generated_number,
                'father_name' => $father_name,
                'mother_name' => $mother_name,
                'father_deceased' => $father_deceased,
                'father_occupation' => $father_occupation,
                'no_siblings' => $no_siblings,
                'year' => $year,
                'result_item' => $result_item,
                'max_marks' => $max_marks,
                'obtained_marks' => $obtained_marks,
                'percentage' => $percentage,
                'id_intake' => $id_intake,
                'id_program' => $id_program,
                'id_student' => $id_student,
                'id_qualification' => $id_qualification,
                'est_fee' => $est_fee,
                'status' => 0
            );

            // $check_limit = $this->scholarship_application_model->checkStudentScholarshipApplication($data);
            // if($check_limit == 1)
            // {
            //      echo "<Pre>";print_r("Max Application Limit Reached For Active & Pending Scholarship Application");exit();
            // }

            // echo "<Pre>";print_r($data);exit();


            $insert_id = $this->scholarship_application_model->addScholarshipApplication($data);
            redirect('/student/scholarshipApplication/list');
        }

        
        $check['id_student'] = $this->session->id_student;
        $check['id_intake'] = $this->session->id_intake;
        $check['id_program'] = $this->session->id_program;


        $data['studentDetails'] = $this->scholarship_application_model->getStudentByStudentId($id_student);
        $data['schemeList'] = $this->scholarship_application_model->getSchemesByProgramId($id_program);
        
        // echo "<Pre>";print_r($data);exit();

        $this->global['studentPageCode'] = 'scholarship_application.add';
        $this->global['pageTitle'] = 'Student Portal : Add Scholarship Application';
        $this->loadViews("scholarship/add", $this->global, $data, NULL);
    }


    function view($id = NULL)
    {
        $id_student = $this->session->id_student;

        if ($id == null)
        {
            redirect('/student/scholarshipApplication/list');
        }
        $data['studentDetails'] = $this->scholarship_application_model->getStudentByStudentId($id_student);
        $data['scholarshipApplication'] = $this->scholarship_application_model->getScholarshipApplication($id);
        $data['schemeList'] = $this->scholarship_application_model->schemeListByStatus('1');

            // echo "<Pre>"; print_r($data);exit;

        $this->global['studentPageCode'] = 'scholarship_application.view';
        $this->global['pageTitle'] = 'Student Portal : View Scholarship Application';
        $this->loadViews("scholarship/view", $this->global, $data, NULL);
    }





    function getPercentage()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit;

        $type = $tempData['type'];
        $max_marks = $tempData['max_marks'];
        $obtained_marks = $tempData['obtained_marks'];
       
       switch ($type)
       {
            case 'Band':

                $one_percent = $max_marks/100;
                $percentage = $obtained_marks/$one_percent;
                $percentage = round($percentage,2);
               break;

            case 'CGPA':

                $one_percent = $max_marks/10;
                $percentage = $obtained_marks/$one_percent;
               break;

            // case 'Grade':

            //     $one_percent = $max_marks/100;
            //     $percentage = $obtained_marks/$one_percent;

            //     $percentage = getGrade($percentage);

               
            //    break;

            // case 'Marks':

                

            //    # code...
            //    break;
           
           default:
                $one_percent = $max_marks/100;
                $percentage = $obtained_marks/$one_percent;
                $percentage = round($percentage,2);
               break;
       }
        echo $percentage;
            exit;
    }






















    function diffDates()
    {
        // // Declare two dates 
        // $start_date = strtotime("2018-06-08"); 
        // $end_date = strtotime("2018-09-01"); 
          
        // // Get the difference and divide into  
        // // total no. seconds 60/60/24 to get  
        // // number of days 
        // echo "Difference between two dates: "
        //     . ($end_date - $start_date)/60/60/24;exit();
    }
}

