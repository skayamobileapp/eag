<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Colloquium extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('colloquium_model');
        $this->isStudentLoggedIn();
    }

    function list()
    {
        $student_education_level = $this->session->student_education_level;
        if ($student_education_level == 'MASTER' || $student_education_level == 'P.hd' || $student_education_level == 'POSTGRADUATE')
        {
            $id_student = $this->session->id_student;
            $id_intake = $this->session->id_intake;
            $id_program = $this->session->id_program;
            $id_qualification = $this->session->id_qualification;
            $id_program_scheme = $this->session->id_program_scheme;

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_intake'] = $id_intake;
            $formData['id_programme'] = $id_program;
            $formData['id_student'] = $id_student;
            $data['searchParam'] = $formData;

            $data['colloquiumList'] = $this->colloquium_model->colloquiumListSearch($formData);

            // echo "<Pre>"; print_r($data);exit;

            $this->global['studentPageCode'] = 'colloquium.list';
            $this->global['pageTitle'] = 'College Management System : Research Colloquium List';
            $this->loadViews("colloquium/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        $student_education_level = $this->session->student_education_level;
        if ($student_education_level == 'MASTER' || $student_education_level == 'P.hd' || $student_education_level == 'POSTGRADUATE')
        {
            if ($id == null)
            {
                redirect('/student/colloquium/list');
            }
            
            $id_student = $this->session->id_student;
            $id_intake = $this->session->id_intake;
            $id_program = $this->session->id_program;
            $id_qualification = $this->session->id_qualification;
            $id_program_scheme = $this->session->id_program_scheme;
        
            
            if($this->input->post())
            {
                

            
                $data = array(
                    'id_colloquium' => $id,
                    'id_student' => $id_student,
                    'by_student' => $id_student,
                    'date_time' => date('Y-m-d h:i:s'),
                    'status' => 1
                );

                $result = $this->colloquium_model->addColloquiumInterest($data);
                redirect('/student/colloquium/list');
            }
            
            $colloquium['id_colloquium'] = $id;
            $colloquium['id_student'] = $id_student;

            $data['programList'] = $this->colloquium_model->programListForPostgraduate('POSTGRADUATE');
            $data['colloquium'] = $this->colloquium_model->getColloquium($id);
            $data['colloquium_interest'] = $this->colloquium_model->getColloquiumInterested($id,$id_student);
            $data['interest'] = $this->colloquium_model->getColloquiumInterestByData($colloquium);
            // $data['getColloquiumDetailsByColloquiumId'] = $this->colloquium_model->getColloquiumDetailsByColloquiumId($id);
            
            $this->global['studentPageCode'] = 'colloquium.add';
            $this->global['pageTitle'] = 'College Management System : Edit Research Colloquium';
            $this->loadViews("colloquium/edit", $this->global, $data, NULL);
        }
    }


    function getIntakeByProgramId($id_program)
    {

        $student_list_data = $this->colloquium_model->getIntakeListByProgramme($id_program);
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_intake' id='id_intake' class='form-control'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id_intake;
        $intake_name = $student_list_data[$i]->intake_name;
        $intake_year = $student_list_data[$i]->intake_year;

        $table.="<option value=".$id.">". $intake_year . " - " . $intake_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function tempAddColloquiumDetails()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;

        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->colloquium_model->tempAddColloquiumDetails($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;

        $data = $this->displayColloquiumDetails();
        
        echo $data;        
    }

    function displayColloquiumDetails()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->colloquium_model->getTempColloquiumDetailsBySession($id_session); 
        
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Programme</th>
                    <th>Intake</th>
                    <th>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $programme_code = $temp_details[$i]->programme_code;
                    $programme_name = $temp_details[$i]->programme_name;
                    $intake_name = $temp_details[$i]->intake_name;
                    $intake_year = $temp_details[$i]->intake_year;

                    // if($status == 1)
                    // {
                    //     $status = 'Active';
                    // }else
                    // {
                    //     $status = 'In-Active';
                    // }

                    $j = $i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>                       
                            <td>$programme_code - $programme_name</td>
                            <td>$intake_year - $intake_name</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempColloquiumDetails($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
            // <span onclick='deleteTempProposalHasProgramme($id)'>Delete</a>                            
            $table.= "
            </tbody>
            </table>
            </div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempColloquiumDetails($id)
    {
        $inserted_id = $this->colloquium_model->deleteTempColloquiumDetails($id);
        if($inserted_id)
        {
            $data = $this->displayColloquiumDetails();
            echo $data;  
        }
    }

    function addColloquiumDetails()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->colloquium_model->addColloquiumDetails($tempData);
        echo "<Pre>";print_r($inserted_id);exit();

        echo "success";exit;
    }

    function deleteColloquiumDetails($id)
    {
        $inserted_id = $this->colloquium_model->deleteColloquiumDetails($id);
        echo "Success"; 
    }
}
