<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Approve Research Articleship</h3>
        </div>



        <h4 class='sub-title'>Student Details</h4>

        <div class='data-list'>
            <div class='row'>

                <div class='col-sm-6'>
                    <dl>
                        <dt>Student Name :</dt>
                        <dd><?php echo $studentDetails->full_name ?></dd>
                    </dl>
                    <dl>
                        <dt>Student Email :</dt>
                        <dd><?php echo $studentDetails->email_id ?></dd>
                    </dl>
                    <dl>
                        <dt>Student NRIC :</dt>
                        <dd><?php echo $studentDetails->nric ?></dd>
                    </dl>
                    
                </div>        
                
                <div class='col-sm-6'>
                    <dl>
                        <dt>Intake :</dt>
                        <dd>
                            <?php echo $studentDetails->intake_name ?>
                        </dd>
                    </dl>
                    <dl>
                        <dt>Programme :</dt>
                        <dd><?php echo $studentDetails->programme_name ?></dd>
                    </dl>
                    <dl>
                        <dt>Nationality :</dt>
                        <dd><?php echo $studentDetails->nationality ?></dd>
                    </dl>
                </div>

            </div>
        </div>















        <form id="form_main" action="" method="post">



        <div class="form-container">
            <h4 class="form-group-title">Research Articleship Details</h4>




            <div class="row">

                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester of Commencement <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_semester" name="id_semester" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $researchArticleship->id_semester)
                                    {
                                        echo 'selected';
                                    }
                                    ?>>
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off" value="<?php if($researchArticleship->start_date){ echo date('d-m-Y', strtotime($researchArticleship->start_date)); } ?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off" value="<?php if($researchArticleship->end_date){ echo date('d-m-Y', strtotime($researchArticleship->end_date)); } ?>" readonly>
                    </div>
                </div>


            </div>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Require Assistance <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                            <input type="radio" name="assistance" id="assistance" value="0" <?php if($researchArticleship->assistance=='0') {
                             echo "checked=checked";
                          };?>><span class="check-radio"></span> No
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="assistance" id="assistance" value="1" <?php if($researchArticleship->assistance=='1') {
                             echo "checked=checked";
                          };?>><span class="check-radio"></span> Yes
                        </label>
                    </div>
                </div>


            </div>

        </div>







        <div class="form-container">
            <h4 class="form-group-title">Organisation Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="company_name" name="company_name" value="<?php echo $researchArticleship->company_name; ?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Designation <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="designation" name="designation" value="<?php echo $researchArticleship->designation; ?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Person <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="contact_person" name="contact_person" value="<?php echo $researchArticleship->contact_person; ?>" readonly>
                    </div>
                </div>


            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address" name="address" value="<?php echo $researchArticleship->address; ?>" readonly>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Country <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_country" name="id_country" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $researchArticleship->id_country)
                                    {
                                        echo 'selected';
                                    }
                                    ?>>
                                    <?php echo $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label> State <span class='error-text'>*</span></label>
                        <span id='view_state'></span>
                    </div>
                </div>

            </div>

            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city" value="<?php echo $researchArticleship->city; ?>" readonly>
                    </div>
                </div>


            


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Zipcode <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode" value="<?php echo $researchArticleship->zipcode; ?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="email" name="email" value="<?php echo $researchArticleship->email; ?>" readonly>
                    </div>
                </div>


            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Phone No. <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="phone_number" name="phone_number" value="<?php echo $researchArticleship->phone_number; ?>" readonly>
                    </div>
                </div>


            


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fax <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="fax" name="fax" value="<?php echo $researchArticleship->fax; ?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Are You an employee here? <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                            <input type="radio" name="is_employee" id="is_employee" value="0" checked="checked"><span class="check-radio"></span> No
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="is_employee" id="is_employee" value="1"><span class="check-radio"></span> Yes
                        </label>                 
                    </div>                         
                </div>


            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Year Of Service (Years)</label>
                        <select class="form-control" id="id_research_topic" name="id_research_topic">
                            <option value="">Select</option>
                            <option value="Below 1">Below 1</option>
                            <option value="1-4">1-4</option>
                            <option value="5-10">5-10</option>
                            <option value="Above 10">Above 10</option>
                        </select>
                    </div>
                </div>




                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="synopsis" name="synopsis" name="max_candidates" value="<?php 
                        if($researchArticleship->status == 0)
                        {
                            echo 'Pending';
                        }elseif($researchArticleship->status == 1)
                        {
                            echo 'Approved';
                        }if($researchArticleship->status == 2)
                        {
                            echo 'Rejected';
                        } ?>" readonly>
                    </div>
                </div>



                <?php 
                    if($researchArticleship->status == 2)
                    {

                    ?>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reason" name="reason" name="max_candidates" value="
                        <?php echo $researchArticleship->reason; ?>" readonly>
                    </div>
                </div>

                    <?php

                    }
                   ?>




            </div>




        <?php
            if($researchArticleship->status == '0')
            {
             ?>


             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p> Approval <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                            <input type="radio" id="ed1" name="status" value="1" onclick="hideRejectField()"><span class="check-radio"></span> Approve
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="ed2" name="status" value="2" onclick="showRejectField()"><span class="check-radio"></span> Reject
                        </label>
                    </div>
                </div>

                 <div class="col-sm-4" id="view_reject" style="display: none">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control">
                    </div>
                </div>

            </div>

          <?php
            }
            ?>



        </div>


    

        <div class="button-block clearfix">
            <div class="bttn-group">

            <?php
            if($researchArticleship->status == '0')
            {
             ?>
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
            <?php
            }
            ?>
                <a href="../approvalList" class="btn btn-link">Back</a>
            </div>
        </div>


    </form>











    <div class="form-container">
            <h4 class="form-group-title"> Articleship Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Supervisor Details</a>
                    </li>
                    <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Examiner Details</a>
                    </li>

                      
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">



                    <div class="form-container">
                        <h4 class="form-group-title">Supervisor Details</h4>


                    <?php

                    if(!empty($researchArticleshipHasSupervisor))
                    {
                        ?>

                        

                            

                              <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                        <th>Supervisor</th>
                                        <th>Role</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($researchArticleshipHasSupervisor);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td><?php echo $researchArticleshipHasSupervisor[$i]->ic_no . " - " . $researchArticleshipHasSupervisor[$i]->staff_name;?></td>
                                        <td><?php echo $researchArticleshipHasSupervisor[$i]->supervisor_role;?></td>

                                         </tr>
                                      <?php 
                                  } 
                                  ?>
                                    </tbody>
                                </table>
                              </div>

                    <?php
                    }
                    ?>



                        </div>


                        </div> 
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="col-12">



                    <div class="form-container">
                            <h4 class="form-group-title">Examiner Details</h4>



                    <?php

                    if(!empty($researchArticleshipHasExaminer))
                    {
                        ?>

                        

                            

                              <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                        <th>Type</th>
                                        <th>Examiner</th>
                                        <th>Role</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($researchArticleshipHasExaminer);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td>
                                        <?php 
                                        if($researchArticleshipHasExaminer[$i]->type == 1)
                                        {
                                            echo 'Internal';
                                        }
                                        else
                                        {
                                            echo 'External';
                                        }
                                        ?>
                                        </td>

                                        <td>
                                        <?php 
                                        if($researchArticleshipHasExaminer[$i]->type == 1)
                                        {
                                            echo $researchArticleshipHasExaminer[$i]->ic_no . " - " . $researchArticleshipHasExaminer[$i]->staff_name;
                                        }
                                        else
                                        {
                                            echo $researchArticleshipHasExaminer[$i]->full_name;
                                        }
                                        ?></td>
                                        <td><?php echo $researchArticleshipHasExaminer[$i]->examiner_role;?></td>

                                         </tr>
                                      <?php 
                                  } 
                                  ?>
                                    </tbody>
                                </table>
                              </div>


                    <?php
                    
                    }
                     ?>


                        </div>



                        </div> 
                    </div>




                </div>

            </div>
        

    </div>











           

      </div>
    </div>

   </div> <!-- END row-->

           
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();


     function getStateByCountry(id)
    {
        $.get("/research/articleship/getStateByCountry/"+id, function(data, status)
        {
            $("#view_state").html(data);
        });
    }



    getStateAutomatic();

    function getStateAutomatic()
    {
        var id_country = "<?php echo $researchArticleship->id_country;?>";

        if(id_country.length > 0)
        {
            $.get("/research/articleship/getStateByCountry/"+id_country, function(data, status)
            {
                var idstateselected = "<?php echo $researchArticleship->id_state;?>";

                $("#view_state").html(data);
                $("#id_state").find('option[value="'+idstateselected+'"]').attr('selected',true);
                $('select').select2();
            });
        }
    }


    function addResearchArticleshipHasSupervisor()
    {
        if($('#form_supervisor').valid())
        {

        var tempPR = {};
        tempPR['id_staff'] = $("#id_staff").val();
        tempPR['id_staff_role'] = $("#id_staff_role").val();
        // tempPR['status'] = $("#supervisor_status").val();
        tempPR['status'] = 1;
        tempPR['id_articleship'] = <?php echo $researchArticleship->id ?>;

            $.ajax(
            {
               url: '/research/articleship/addResearchArticleshipHasSupervisor',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
        }
    }

    function deleteResearchArticleshipHasSupervisor(id) {
        // alert(id);
         $.ajax(
            {
               url: '/research/articleship/deleteResearchArticleshipHasSupervisor/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }



    function addResearchArticleshipHasExaminer()
    {
        if($('#form_examiner').valid())
        {

        var tempPR = {};
        tempPR['id_examiner'] = $("#id_examiner").val();
        tempPR['id_examiner_role'] = $("#id_examiner_role").val();
        // tempPR['status'] = $("#examiner_status").val();
        tempPR['status'] = 1;
        tempPR['id_articleship'] = <?php echo $researchArticleship->id ?>;

            $.ajax(
            {
               url: '/research/articleship/addResearchArticleshipHasExaminer',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
        }
    }

    function deleteResearchArticleshipHasExaminer(id) {
        // alert(id);
         $.ajax(
            {
               url: '/research/articleship/deleteResearchArticleshipHasExaminer/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }
    

    $(document).ready(function() {
        $("#form_supervisor").validate({
            rules: {
                id_staff: {
                    required: true
                },
                id_staff_role: {
                    required: true
                }
            },
            messages: {
                id_staff: {
                    required: "<p class='error-text'>Select Supervisor</p>",
                },
                id_staff_role: {
                    required: "<p class='error-text'>Select Supervisor Role</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_examiner").validate({
            rules: {
                id_examiner: {
                    required: true
                },
                id_examiner_role: {
                    required: true
                }
            },
            messages: {
                id_examiner: {
                    required: "<p class='error-text'>Select Examiner</p>",
                },
                id_examiner_role: {
                    required: "<p class='error-text'>Select Examiner Role</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });





    $(document).ready(function()
    {

        $("#form_programme").validate({
            rules: {
                id_intake: {
                    required: true
                },
                id_programme: {
                    required: true
                },
                id_student: {
                    required: true
                },
                id_semester: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                company_name: {
                    required: true
                },
                designation: {
                    required: true
                },
                contact_person: {
                    required: true
                },
                address: {
                    required: true
                },
                id_country: {
                    required: true
                },
                id_state: {
                    required: true
                },
                city: {
                    required: true
                },
                zipcode: {
                    required: true
                },
                email: {
                    required: true
                },
                phone_number: {
                    required: true
                },
                fax: {
                    required: true
                }
            },
            messages: {
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                company_name: {
                    required: "<p class='error-text'>Company Name Required</p>",
                },
                designation: {
                    required: "<p class='error-text'>Designation Required</p>",
                },
                contact_person: {
                    required: "<p class='error-text'>Contact Person</p>",
                },
                address: {
                    required: "<p class='error-text'>Address Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                city: {
                    required: "<p class='error-text'>City Required</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                phone_number: {
                    required: "<p class='error-text'>Phone No. Required</p>",
                },
                fax: {
                    required: "<p class='error-text'>Fax. Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "2019:2020"
    });
  } );









     $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                status: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }



</script>