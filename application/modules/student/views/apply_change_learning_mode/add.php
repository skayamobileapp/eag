<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Apply Change Learning Mode</h3>
        </div>


        <div class="form-container">
            <h4 class="form-group-title">Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($getStudentData->full_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $getStudentData->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $getStudentData->email_id; ?></dd>
                            </dl>
                            <dl>
                                <dt>Learning Mode :</dt>
                                <dd><?php echo $getStudentData->program_scheme; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->mail_address1 ; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->mail_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing City :</dt>
                                <dd><?php echo $getStudentData->mailing_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Zipcode :</dt>
                                <dd><?php echo $getStudentData->mailing_zipcode; ?></dd>
                            </dl>
                            <dl>
                                <dt>Learning Mode :</dt>
                                <dd><?php echo $getStudentData->program_scheme; ?></dd>
                            </dl>
                            <dl>
                                <dt>Organisation :</dt>
                                <dd><?php

                                if($getStudentData->id_university != 1 && $getStudentData->id_university != 0)
                                {
                                    echo $getStudentData->partner_university_code , " - " . $getStudentData->partner_university_name;
                                }
                                else
                                {
                                    echo $organisationDetails->short_name . " - " . $organisationDetails->name;
                                }
                                
                                ?></dd>
                            </dl> 
                            <dl>
                                <dt>Academic Advisor :</dt>
                                <dd><?php 
                                if($getStudentData->advisor_type == '0')
                                {
                                    echo 'External';
                                }
                                elseif($getStudentData->advisor_type == '1')
                                {
                                    echo 'Internal';
                                }
                                echo " - " . $getStudentData->advisor_name ; ?></dd>
                            </dl>
                            
                            <?php 
                            if($getStudentData->qualification_name == 'POSTGRADUATE')
                            {
                            ?>

                            <dl>
                                <dt>Research Supervisor :</dt>
                                <dd><?php 
                                if($getStudentData->supervisor_type == '0')
                                {
                                    echo 'External';
                                }
                                elseif($getStudentData->supervisor_type == '1')
                                {
                                    echo 'Internal';
                                }
                                echo " - " . $getStudentData->supervisor_name ; ?></dd>
                            </dl>

                            <?php 
                            }
                            ?>
                            <?php 
                            if($getStudentData->qualification_name == 'MASTER' || $getStudentData->qualification_name == 'POSTGRADUATE')
                            {
                            ?>

                               <dl>
                                  <dt>Current Phd Duration :</dt>
                                  <dd><?php echo $getStudentData->phd_duration ?></dd>
                               </dl>

                            <?php
                            }
                            ?>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $getStudentData->nationality ?></dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd><?php echo $getStudentData->programme_code . " - " . $getStudentData->programme_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $getStudentData->intake_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $getStudentData->scheme_code . " - " .  $getStudentData->scheme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->permanent_address1; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->permanent_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent City :</dt>
                                <dd><?php echo $getStudentData->permanent_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Zipcode :</dt>
                                <dd><?php echo $getStudentData->permanent_zipcode; ?></dd>
                            </dl>
                            <dl>
                                <dt>Passport Number :</dt>
                                <dd><?php echo $getStudentData->passport_number; ?></dd>
                            </dl>
                            <dl>
                                <dt>Branch :</dt>
                                <dd><?php
                                
                                    echo $getStudentData->branch_code . " - " . $getStudentData->branch_name;
                                  ?></dd>
                            </dl> 
                            <dl>
                                <dt>Education Level :</dt>
                                <dd><?php echo $getStudentData->qualification_name; ?></dd>
                            </dl>

                            <dl>
                                <dt>Program Structure Type :</dt>
                                <dd><?php echo $getStudentData->program_structure_code . " - " . $getStudentData->program_structure_name; ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>


    </div>


    
        <form id="form_apply_change_programme" action="" method="post">


        <div class="form-container">
            <h4 class="form-group-title">Details For Apply Change Learning Mode</h4> 


            <div class="row">                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select New Semester <span class='error-text'>*</span></label>
                            <select name="id_new_semester" id="id_new_semester" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($semesterList))
                                {
                                    foreach ($semesterList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                 

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select New Learning Mode <span class='error-text'>*</span></label>
                            <span id="view_new_program_scheme">
                              <select class="form-control" id='id_new_program_scheme' name='id_new_program_scheme'>
                                <option value=''></option>
                              </select>
                            </span>
                        </div>
                    </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fees <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="fee" name="fee" readonly="readonly" value="0">
                    </div>
                 </div>

            </div>
            <div class="row"> 

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reason" name="reason">
                    </div>
                 </div>  


            </div>
            </div>

            <div class="form-container">
                <h4 class="form-group-title">Fee Structure Details</h4>
                <div id='view_fee'>
                </div>
            </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>


    function getPreviousProgramSchemeByProgramId(id)
    {
        $.get("/student/applyChangeLearningMode/getPreviousProgramSchemeByProgramId/"+id, function(data, status){
   
        $("#view_program_scheme").html(data);
        $("#view_program_scheme").show();
    });

        $.get("/student/applyChangeLearningMode/getIntakeByProgramme/"+id, function(data, status){
   
        $("#view_program_intake").html(data);
        $("#view_program_intake").show();
    });

    }


     function getStudentByData()
     {
        var id_programme = $("#id_programme").val();
        var id_program_scheme = $("#id_program_scheme").val();
        var id_intake = $("#id_intake").val();


         var tempPR = {};
       
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_program_scheme'] = $("#id_program_scheme").val();

        if(id_programme != '' && id_program_scheme != '' && id_intake != '')
        {

            $.ajax(
            {
               url: '/student/applyChangeLearningMode/getStudentByData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_students_list").html(result);
               }
            });


            $.get("/student/applyChangeLearningMode/getProgramSchemeByProgramId/"+id_programme, function(data, status)
            {
   
                $("#view_new_program_scheme").html(data);
                $("#view_new_program_scheme").show();
            });



        }
     }









    function getStudentByProgramme(id)
    {

     $.get("/student/applyChangeLearningMode/getStudentByProgrammeId/"+id, function(data, status){
   
        $("#student").html(data);
        // $("#view_programme_details").show();
        // // $("#view_programme_details").html(data);
        // var programme_name = $("#programme_name").val();
        //  // alert(programme_name);
        // $("#programme_name").val(programme_name);

        //  var programme_code = $("#programme_code").val();
        //  alert(programme_code);
        // $("#programme_code").val(programme_code);

        //  var total_cr_hrs = $("#total_cr_hrs").val();
        // $("#total_cr_hrs").val(total_cr_hrs);

        //  var graduate_studies = $("#graduate_studies").val();
        // $("#graduate_studies").val(graduate_studies);

    });
 }



 function getStudentByStudentId(id)
 {

     $.get("/student/applyChangeLearningMode/getStudentByStudentId/"+id, function(data, status){
   
        $("#view_student_details").html(data);
        $("#view_student_details").show();
    });
 }

 function getIntakeByProgramme(id)
 {

    var id_programme = $("#id_programme").val();
    var id_new_programme = $("#id_new_programme").val();



    $.get("/student/applyChangeLearningMode/getIntakeByProgramme/"+id, function(data, status){
   
        $("#view_new_intake").html(data);
        $("#view_new_intake").show();
    });
 }



 function getFeeByProgrammeNIntake() {

    var id_programme = <?php echo $getStudentData->id_program ?>;
    var id_intake = <?php echo $getStudentData->id_intake ?>;
    var currency ="MYR";
    var id_program_scheme = <?php echo $getStudentData->id_program_scheme ?>;
    var id_new_program_scheme = $("#id_new_program_scheme").val();

    if(id_program_scheme == id_new_program_scheme)
    {
        alert('Same Programme Schemes Selection For Change Not Allowed Select Anothe Combination');

        // $("#id_new_program_scheme").val('');

    }
    else
    {


        var tempPR = {};
        tempPR['id_programme'] = id_programme;
        tempPR['id_intake'] = id_intake;
        tempPR['id_program_scheme'] = id_program_scheme;
        tempPR['currency'] = currency;
        
            $.ajax(
            {
               url: '/student/applyChangeLearningMode/getFeeByProgrammeNIntake',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_fee").html(result);
                var ta = $("#amount").val();
                if(ta == '0')
                {
                    alert('No Fee Structure Defined For Entered Data, Select Anothe Combination');
                    $("#fee").val('0');

                }
                else
                {
                    $("#fee").val(ta);
                }
               }
            });

        }
        
    }




    $(document).ready(function()
    {
        var id_program = <?php echo $getStudentData->id_program; ?>

        // alert(id_program);

        if(id_program != 0)
        {
            $.get("/student/applyChangeLearningMode/getProgramSchemeByProgramId/"+id_program, function(data, status)
            {
   
                $("#view_new_program_scheme").html(data);
                $("#view_new_program_scheme").show();
            });
        }


        $("#form_apply_change_programme").validate(
        {
            rules:
            {
                id_programme:
                {
                    required: true
                },
                id_student:
                {
                    required: true
                },
                id_intake:
                {
                    required: true
                },
                id_program_scheme:
                {
                    required: true
                },
                id_new_semester:
                {
                    required: true
                },
                fee:
                {
                    required: true
                },
                reason:
                {
                    required: true
                },
                id_new_program_scheme:
                {
                    required: true
                }
            },
            messages:
            {
                id_programme:
                {
                    required: "<p class='error-text'>Select Current Program</p>",
                },
                id_student:
                {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_intake:
                {
                    required: "<p class='error-text'>Select Student For Intake</p>",
                },
                id_program_scheme:
                {
                    required: "<p class='error-text'>Select New Program Scheme</p>",
                },
                id_new_intake:
                {
                    required: "<p class='error-text'>Select Changing Intake</p>",
                },
                id_new_semester:
                {
                    required: "<p class='error-text'>Select Changing Semester</p>",
                },
                fee:
                {
                    required: "<p class='error-text'>Select Changing Intake & Program For Fee Structure</p>",
                },
                reason:
                {
                    required: "<p class='error-text'>Enter Reason</p>",
                },
                id_new_program_scheme:
                {
                    required: "<p class='error-text'>Select New Program Scheme</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>