<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Scholarship Application</h3>
        </div>

        <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl>                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $studentDetails->programme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Education Level :</dt>
                                <dd><?php echo $studentDetails->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $studentDetails->program_scheme; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


        <form id="form_scholarship" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Scholarship Application Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Application Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="no_siblings" name="no_siblings" value="<?php echo $scholarshipApplication->application_number;?>" readonly>
                    </div>
                </div> 

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Scholarship Scheme <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="no_siblings" name="no_siblings" value="<?php echo $scholarshipApplication->scholarship_code . " - " . $scholarshipApplication->scholarship_name;?>" readonly>
                    </div>
                </div> 


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Father Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="father_name" name="father_name" value="<?php echo $scholarshipApplication->father_name;?>" readonly>
                    </div>
                </div>               

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mother Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="mother_name" name="mother_name" value="<?php echo $scholarshipApplication->mother_name;?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Father Deceased <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="father_deceased" id="sd1" value="1" <?php if($scholarshipApplication->father_deceased=='1'){ echo "checked";}?> disabled><span class="check-radio"></span> Yes
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="father_deceased" id="sd2" value="0" <?php if($scholarshipApplication->father_deceased=='0'){ echo "checked";}?> disabled><span class="check-radio"></span> No
                        </label>                              
                    </div>                         
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="year" name="year" autocomplete="off" value="<?php echo $scholarshipApplication->year; ?>" readonly>
                    </div>
               </div>

                
                     

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Father Occupation <span class='error-text'>*</span></label>
                        <select name="father_occupation" id="father_occupation" class="form-control" disabled>
                            <option value="">Select</option>
                            <option value="Employee"
                                <?php 
                                if ($scholarshipApplication->father_occupation == 'Employee')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Employee";  ?>
                            </option>

                            <option value="Government Servent"
                                <?php 
                                if ($scholarshipApplication->father_occupation == 'Government Servent')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Government Servent";  ?>
                            </option>
                            <option value="Business"
                                <?php 
                                if ($scholarshipApplication->father_occupation == 'Business')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Business";  ?>
                            </option>
                            <option value="Other"
                                <?php 
                                if ($scholarshipApplication->father_occupation == 'Other')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Other";  ?>
                            </option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>No Of Siblings <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="no_siblings" name="no_siblings" value="<?php echo $scholarshipApplication->no_siblings;?>" readonly>
                    </div>
                </div>     

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Result Item <span class='error-text'>*</span></label>
                        <select name="result_item" id="result_item" class="form-control" disabled>
                            <option value="">Select</option>
                            <option value="Band"
                                <?php 
                                if ($scholarshipApplication->result_item == 'Band')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Band";  ?>
                            </option>

                            <option value="CGPA"
                                <?php 
                                if ($scholarshipApplication->result_item == 'CGPA')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "CGPA";  ?>
                            </option>
                            <option value="Grade"
                                <?php 
                                if ($scholarshipApplication->result_item == 'Grade')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Grade";  ?>
                            </option>
                            <option value="Marks"
                                <?php 
                                if ($scholarshipApplication->result_item == 'Marks')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Marks";  ?>
                            </option>
                        </select>
                    </div>
                </div>

            </div>

             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Estimated Fee <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" autocomplete="off" readonly="readonly" value="<?php echo $scholarshipApplication->est_fee;?>">
                    </div>
               </div>

               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($scholarshipApplication->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($scholarshipApplication->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($scholarshipApplication->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>


                <?php
            if($scholarshipApplication->status == '2')
            {
             ?>

                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $scholarshipApplication->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>

            </div>

        </div>


        <div class="form-container">
            <h4 class="form-group-title">Previous Marks Details</h4>


            <div class="row">

                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Max. Marks <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="max_marks" name="max_marks" value="<?php echo $scholarshipApplication->max_marks;?>" readonly>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Obtained Marks <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="obtained_marks" name="obtained_marks" onchange="getPercentage()" value="<?php echo $scholarshipApplication->obtained_marks;?>" readonly>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Percentage <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="percentage" name="percentage" value="<?php echo $scholarshipApplication->percentage;?>" readonly>
                    </div>
                </div>

                

            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
        <!-- <?php

            if($check_limit == 0)
            {
            ?> -->
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
            <!-- <?php
            }else
            {
            ?> -->
                    <!-- <h3 class="text-center"><span class='error-text'><?php echo $student_name; ?> You Have Exceeded The Max Application Limit</span></h3> -->
           <!--  <?php
            }
        ?> -->
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>

        </form>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


<script>

    $('select').select2();

    function getPercentage()
    {
        if($("#max_marks").val() != '' && $("#obtained_marks").val() != '')
        {

            var tempPR = {};
            tempPR['max_marks'] = $("#max_marks").val();
            tempPR['obtained_marks'] = $("#obtained_marks").val();
            // alert(tempPR['id_group']);

            $.ajax(
            {
               url: '/student/scholarshipApplication/getPercentage',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                if(result > 100)
                {
                    alert('Wrong Marks Entry');
                    $("#max_marks").val('');
                    $("#obtained_marks").val('');
                }
                else
                {
                    $("#percentage").val(result);
                }
               }
            });  
        }
    }




    $(document).ready(function() {
        $("#form_scholarship").validate({
            rules: {
                duration: {
                    required: true
                },
                 description: {
                    required: true
                },
                 id_company_type: {
                    required: true
                },
                 from_dt: {
                    required: true
                },
                 to_dt: {
                    required: true
                },
                 id_company: {
                    required: true
                }
            },
            messages: {
                duration: {
                    required: "<p class='error-text'>Duration Reequired</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                id_company_type: {
                    required: "<p class='error-text'>Select Company Type</p>",
                },
                from_dt: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                to_dt: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                id_company: {
                    required: "<p class='error-text'>Select Company</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(function(){
        $( ".datepicker" ).datepicker();
      });

</script>