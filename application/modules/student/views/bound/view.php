<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View 5 copies of hard bound Submission</h3>
        </div>


        <div class="form-container">
            <h4 class="form-group-title">Supervisor Details</h4>
            <div class='data-list'>
                <div class='row'> 
                    <div class='col-sm-6'>
                        <dl>
                            <dt>Supervisor Name :</dt>
                            <dd><?php echo ucwords($supervisor->full_name);?></dd>
                        </dl>
                        <dl>
                            <dt>Supervisor Email :</dt>
                            <dd><?php echo $supervisor->email ?></dd>
                        </dl>                     
                    </div>        
                    
                    <div class='col-sm-6'>                           
                        <dl>
                            <dt>Supervisor Type :</dt>
                            <dd><?php 
                            if($supervisor->type == 0)
                            {
                                echo 'External';
                            }elseif($supervisor->type == 1)
                            {
                                echo 'Internal';
                            } ?></dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>



        
        <form id="form_internship" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">5 copies of hard bound Submission Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>FILE 1

                            <span class='error-text'>*</span>
                            <?php 
                            if($bound->upload_file != '')
                            {
                            ?>
                            <a href="<?php echo '/assets/images/' . $bound->upload_file; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $bound->upload_file; ?>)" title="<?php echo  $bound->upload_file; ?>"> View </a>

                            <?php
                            }
                            ?>


                        </label>
                        <input type="file" name="image" />
                    </div>                    
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>FILE 2

                            <span class='error-text'>*</span>
                            <?php 
                            if($bound->upload_file2 != '')
                            {
                            ?>
                            <a href="<?php echo '/assets/images/' . $bound->upload_file2; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $bound->upload_file2; ?>)" title="<?php echo  $bound->upload_file2; ?>"> View </a>

                            <?php
                            }
                            ?>


                        </label>
                        <input type="file" name="image" />
                    </div>                    
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>FILE 3

                            <span class='error-text'>*</span>
                            <?php 
                            if($bound->upload_file3 != '')
                            {
                            ?>
                            <a href="<?php echo '/assets/images/' . $bound->upload_file3; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $bound->upload_file3; ?>)" title="<?php echo  $bound->upload_file3; ?>"> View </a>

                            <?php
                            }
                            ?>


                        </label>
                        <input type="file" name="image" />
                    </div>                    
                </div>

            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>FILE 4

                            <span class='error-text'>*</span>
                            <?php 
                            if($bound->upload_file4 != '')
                            {
                            ?>
                            <a href="<?php echo '/assets/images/' . $bound->upload_file4; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $bound->upload_file4; ?>)" title="<?php echo  $bound->upload_file4; ?>"> View </a>

                            <?php
                            }
                            ?>


                        </label>
                        <input type="file" name="image" />
                    </div>                    
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>FILE 5

                            <span class='error-text'>*</span>
                            <?php 
                            if($bound->upload_file5 != '')
                            {
                            ?>
                            <a href="<?php echo '/assets/images/' . $bound->upload_file5; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $bound->upload_file5; ?>)" title="<?php echo  $bound->upload_file5; ?>"> View </a>

                            <?php
                            }
                            ?>


                        </label>
                        <input type="file" name="image" />
                    </div>                    
                </div>


            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Submitted On <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="created_dt_tm" name="created_dt_tm" value="<?php if($bound->created_dt_tm){ echo date('d-m-Y', strtotime($bound->created_dt_tm)); }?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="status" name="status" value="<?php
                        if($bound->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($bound->status == '1')
                        {
                            echo 'Approved';
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>



            </div>  



            <div class="row">

                <div class="col-sm-12">
                    <div class="form-group shadow-textarea">
                      <label for="message">Description <span class='error-text'>*</span></label>
                      <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="description" id="description"><?php echo $bound->description;?></textarea>
                    </div>
                </div>

            </div>



        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>

        </form>

            <?php

            if(!empty($boundCommentsDetails))
            {
                ?>
                <br>

                <div class="form-container">
                        <h4 class="form-group-title">TOC Reporting Comments Details</h4>

                    

                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                <th>Sl. No</th>
                                 <th>Comments</th>
                                 <th>Updated By</th>
                                 <th>Date</th>
                                 <th style="text-align: center;">File</th>
                                 <!-- <th style="text-align: center;">Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                             $total = 0;
                              for($i=0;$i<count($boundCommentsDetails);$i++)
                             { ?>
                                <tr>
                                <td><?php echo $i+1;?></td>
                                <td><?php echo $boundCommentsDetails[$i]->comments;?></td>
                                <td><?php if($boundCommentsDetails[$i]->id_supervisor == $supervisor->id)
                                {
                                    echo $supervisor->full_name;
                                }
                                 ?></td>
                                <td><?php echo date('d-m-Y', strtotime($boundCommentsDetails[$i]->created_dt_tm));?></td>
                                <td class="text-center">

                                    <a href="<?php echo '/assets/images/' . $boundCommentsDetails[$i]->upload_file; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $boundCommentsDetails[$i]->upload_file; ?>)" title="<?php echo $boundCommentsDetails[$i]->upload_file; ?>">View</a>
                                </td>

                                 </tr>
                              <?php
                          } 
                          ?>
                            </tbody>
                        </table>
                      </div>

                    </div>

            <?php
            
            }
             ?>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<style type="text/css">
    .shadow-textarea textarea.form-control::placeholder {
    font-weight: 300;
}
.shadow-textarea textarea.form-control {
    padding-left: 0.8rem;
}
</style>

<script>

    $('select').select2();

    CKEDITOR.replace('description',{

      width: "800px",
      height: "200px"

    });


    $(document).ready(function() {
        $("#form_internship").validate({
            rules: {
                id_phd_duration: {
                    required: true
                },
                id_chapter: {
                    required: true
                },
                id_topic: {
                    required: true
                }
            },
            messages: {
                id_phd_duration: {
                    required: "<p class='error-text'>Select Phd Duration</p>",
                },
                id_chapter: {
                    required: "<p class='error-text'>Select Chapter</p>",
                },
                id_topic: {
                    required: "<p class='error-text'>Select Topic</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(function(){
        $( ".datepicker" ).datepicker();
      });

</script>