<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Internship Application</h3>
        </div>
        <form id="form_internship" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Internship Application Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Duration (Months) <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="duration" name="duration">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company Type <span class='error-text'>*</span></label>
                        <select name="id_company_type" id="id_company_type" class="form-control" onchange="getCompanyByCompanyType(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($companyTypeList))
                            {
                                foreach ($companyTypeList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>                

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company <span class='error-text'>*</span></label>
                        <span id='view_company'></span>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" autocomplete="off">
                    </div>
               </div>

              <!--  <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="to_dt" name="to_dt" autocomplete="off">
                    </div>
                </div> -->

               <!--  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Zipcode <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode">
                    </div>
                </div>-->  

            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
        <?php

            if($check_limit == 0)
            {
            ?>
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
            <?php
            }else
            {
            ?>
                    <h3 class="text-center"><span class='error-text'><?php echo $student_name; ?> You Have Exceeded The Max Application Limit</span></h3>
            <?php
            }
        ?>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        </form>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


<script>

    $('select').select2();

    function getCompanyByCompanyType(id)
    {
        if(id != '')
        {
        $.get("/student/internshipApplication/getCompanyByCompanyType/"+id,
            function(data, status)
            {
                $("#view_company").html(data);
                // $("#view_programme_details").html(data);
                // $("#view_programme_details").show();
            });
        }
    }



    $(document).ready(function() {
        $("#form_internship").validate({
            rules: {
                duration: {
                    required: true
                },
                 description: {
                    required: true
                },
                 id_company_type: {
                    required: true
                },
                 from_dt: {
                    required: true
                },
                 id_company: {
                    required: true
                },
                 to_dt: {
                    required: true
                }
            },
            messages: {
                duration: {
                    required: "<p class='error-text'>Duration Reequired</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                id_company_type: {
                    required: "<p class='error-text'>Select Company Type</p>",
                },
                from_dt: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                id_company: {
                    required: "<p class='error-text'>Select Company</p>",
                },
                to_dt: {
                    required: "<p class='error-text'>Select End Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(function(){
        $( ".datepicker" ).datepicker();
      });

</script>