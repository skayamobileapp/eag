<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<style>
    .highcharts-figure, .highcharts-data-table table {
    min-width: 360px; 
    max-width: 800px;
    margin: 1em auto;
}

.highcharts-data-table table {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #EBEBEB;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
    font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}

</style>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Welcome to EAG Student portal</h3>
        </div>
            <div class="row">
              <div class="col-sm-6">
                <div>    <div id="container"> <br/></div>
</div>
              </div>
              <div class="col-sm-6">
                <div><div id="container1"> <br/></div>
              </div>
                                        
            </div>
            <div class="row">            
              <div class="col-md-8" style="padding-left:30px;">
                <h4>Invoice Details</h4>
                  <div class="custom-table" id="printInvoice">
                    <table class="table" id="list-table">
                        <thead>
                        <tr>
                            <th>Sl. No</th>
                            <th>Invoice Number</th>
                            <th>Invoice Type</th>
                            <th>Invoice Total</th>
                            <th>Total Payable</th>
                            <th>Total Discount</th>
                            <th>Balance </th>
                            <th>Remarks</th>
                            <th>Currency</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (!empty($getInvoiceByStudentId))
                        {
                             $i=1;
                            foreach ($getInvoiceByStudentId as $record)
                            {
                        ?>
                            <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $record->invoice_number ?></td>
                                <td><?php echo $record->type ?></td>
                                <td><?php echo $record->invoice_total ?></td>
                                <td><?php echo $record->total_amount ?></td>
                                <td><?php echo $record->total_discount ?></td>
                                <td><?php if($record->balance_amount == ""){
                                    echo "0.00";
                                } else { echo $record->balance_amount; } ?></td>
                                <td><?php echo $record->remarks ?></td>
                                <td><?php 
                                if($record->currency_name == '')
                                {
                                  echo $record->currency;
                                }else
                                {
                                  echo $record->currency_name;
                                }
                                 ?></td>
                                <td><?php 
                                if($record->status == "0")
                                {
                                    echo "Pending";
                                }
                                elseif($record->status == "1")
                                { 
                                    echo "Approved"; 
                                }elseif($record->status == "2")
                                { 
                                    echo "Rejected"; 
                                } ?></td>

                                <td class="">
                                <a onclick="generateReceiptForTheInvoice(<?php echo $record->id; ?>)" title="Pay Online">Pay</a>
                                </td>
                            </tr>
                        <?php
                            $i++;
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                  </div>
               
                <h4>Receipt Details</h4> 
                  <div class="custom-table" id="printInvoice">


                        <table class="table" id="list-table">
                            <thead>
                            <tr>
                                <th>Sl. No</th>
                                <th>Receipt Number</th>
                                <th>Receipt Amount</th>
                                <th>Currency</th>
                                <th>Remarks</th>
                                <th>Status</th>
                                <!-- <th>Action</th> -->
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($getReceiptByStudentId)) {
                                $i=1;
                                foreach ($getReceiptByStudentId as $record) {
                            ?>
                                <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->receipt_number ?></td>
                                    <td><?php if($record->receipt_amount == ""){
                                        echo "0.00";
                                    } else { echo $record->receipt_amount; } ?></td>
                                    <td ><?php
                                     if($record->currency_name == '')
                                      {
                                        echo $record->currency;
                                      }
                                      else
                                      {
                                        echo $record->currency_name;
                                      }
                                      ?>
                                    </td>
                                    <td><?php echo $record->remarks ?></td>
                                    <td><?php 
                                    if($record->status == "0")
                                    {
                                        echo "Pending";
                                    }
                                    elseif($record->status == "1")
                                    { 
                                        echo "Approved"; 
                                    }elseif($record->status == "2")
                                    { 
                                        echo "Rejected"; 
                                    } ?></td>

                                    <!-- <td class="">
                                        <a href="#" title="" onclick="printDiv('printReceipt')">Print</a> | <a href="<?php echo 'showReceipt/' . $record->id. '/receipt'; ?>" title="">View</a>
                                    </td> -->
                                </tr>
                            <?php
                                $i++;
                                }
                            }
                            ?>
                            </tbody>

                        </table>


        </div>
        <br/>
                <h4>Current Program and course information</h4> 
                <div class="custom-table">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Course Code</th>
                        <th>Course Name</th>
                        <th>Credit Hours</th>
                        <th>Course Type</th>
                        <th>Registration Type</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td>MITM1001</td>
                          <td>Course A</td>
                          <td>3</td>
                          <td>Core</td>
                          <td>Register</td>
                        </tr>
                        <tr>
                          <td>MITM1001</td>
                          <td>Course A</td>
                          <td>3</td>
                          <td>Core</td>
                          <td>Register</td>
                        </tr>
                        <tr>
                          <td>MITM1001</td>
                          <td>Course A</td>
                          <td>3</td>
                          <td>Core</td>
                          <td>Register</td>
                        </tr>                                    
                    </tbody>
                  </table>
                </div>             
              </div> 
              <div class="col-md-4">
                  <div class="notifications">
                    <h4>Notifications</h4>
                    <ul class="notifications-list">

                         <?php  foreach ($notificationList as $record)
                        { ?>
                      <li>
                        <a href="#">
                          <div class="notifications-header">
                            <div class="notifications-heading"><?php echo $record->name; ?></div>
                          </div>
                          <p><?php echo $record->description; ?></p>
                        </a>
                      </li>
                     <?php } ?>                   
                    </ul>
                    <button class="btn btn-primary btn-block">View Older Notifications</button>
                  </div>

                  <div class="notifications">
                    <h4>Event</h4>
                    <ul class="notifications-list">

                         <?php  foreach ($eventsList as $record)
                        { ?>
                      <li>
                        <a href="#">
                          <div class="notifications-header">
                            <div class="notifications-heading"><?php echo $record->name; ?></div>
                            <div class="time">1.30 PM</div>
                          </div>
                          <p><?php echo $record->description; ?></p>
                        </a>
                      </li>
                       <?php } ?>   
                                         
                    </ul>
                    <button class="btn btn-primary btn-block">View Older Notifications</button>
                  </div>
                  <div>
                    Calendar Placeholder
                  </div>
              </div>      
            </div>
    </div>
</div>

<script>
    function generateReceiptForTheInvoice(id)
    {
        if(id != '0')
        {
            $.get("/student/profile/generateReceiptForTheInvoice/"+id,
            function(data, status)
            {
                alert('Receipt Generated, Reference Number : ' + data);
                window.location.reload();
            });
        }

    }
</script>
<script type="text/javascript">
Highcharts.chart('container', {
    chart: {
        type: 'spline'
    },
    title: {
        text: 'You are on track to graduate in May 2021'
    },
    xAxis: {
        categories: [' ', ' ', ' ', ' ', ' ']
    },
    yAxis: {
        title: {
            text: 'Graduate '
        },
        labels: {
            formatter: function () {
                return this.value;
            }
        }
    },
    tooltip: {
        crosshairs: true,
        shared: true
    },
    plotOptions: {
        spline: {
            marker: {
                radius: 4,
                lineColor: '#666666',
                lineWidth: 1
            }
        }
    },
    series: [{
        name: 'Courses Completed',
        marker: {
            symbol: 'square'
        },
        data: [1,3,4,7,10,11]

    }]
});


Highcharts.chart('container1', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Completion of the courses registered'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Branch',
        colorByPoint: true,
        data: [{
            name: 'MIT730',
            y: 61.41
        }, {
            name: 'MIT731',
            y: 11.84
        }, {
            name: 'MIT639',
            y: 10.85
        }, {
            name: 'MIT875',
            y: 4.18
        }]
    }]
});
</script>
</script>