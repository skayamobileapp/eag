<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <!-- <h3>Student Profile</h3> -->
    </div>

     <h2 align="center">Welcome <?php echo $student_name; ?></h2>

      <div class="form-container">
                <h4 class="form-group-title">Peding payment for invoice</h4>
                 <table class="table">
                    <thead>
                        <tr>
                        <th>Sl. No</th>
                         <th>Invoice Number </th>
                         <th>AMount </th>
                         <th>Invoice Date </th>
                         <th>View Invoice Details </th>
                         <th class="text-center">Patment Option </th>
                        </tr>

                    </thead>
                    <tbody><tr>
                        <th colspoan='6'>All invoices are paid</th>
                         
                        </tr>

                    </tbody>
                  </table>


              </div>




      <div class="form-container">
                <h4 class="form-group-title">Course Registered List</h4>

               
            <div class="custom-table">
                <table class="table">
                    <thead>
                        <tr>
                        <th>Sl. No</th>
                         <th>Course </th>
                         <th>Credit Hours </th>
                         <th>Semester </th>
                         <th>Course Registered By </th>
                         <th class="text-center">Registered Semester </th>
                         <th class="text-center">Registered On </th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php
                     $total = 0;
                      for($i=0;$i<count($courseRegisteredList);$i++)
                     { ?>
                        <tr>
                        <td><?php echo $i+1;?></td>
                        <td><?php echo $courseRegisteredList[$i]->course_code . " - " . $courseRegisteredList[$i]->course_name;?></td>
                        <td><?php echo $courseRegisteredList[$i]->credit_hours;?></td>
                        <td><?php echo $courseRegisteredList[$i]->semester_code . " - " . $courseRegisteredList[$i]->semester_name;?></td>
                        <td>
                            <?php if($courseRegisteredList[$i]->by_student > 0)
                            {
                                echo $student_name . " ( SELF )";
                            }
                            else
                            {
                                echo $courseRegisteredList[$i]->role; 
                            } 
                        ?>
                                
                        </td>

                        <td class="text-center">
                            <?php echo $courseRegisteredList[$i]->student_current_semester;  ?>
                        </td>


                        <td class="text-center">
                            <?php if($courseRegisteredList[$i]->created_dt_tm) echo date('d-m-Y', strtotime($courseRegisteredList[$i]->created_dt_tm));  ?>        
                        </td>
                       
                      <?php 
                     
                  }
                  ?>
                    </tbody>
                </table>
            </div>
        </div>


  </div>



  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    $('select').select2();
</script>