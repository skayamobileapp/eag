<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Colloquium For Registration</h3>
            <a href="../list" class="btn btn-link"> < Back</a>
        </div>
        <form id="form_unit" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Colloquium Details</h4>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $colloquium->name; ?>" readonly>
                        <input type="hidden" class="form-control" id="id_colloquium" name="id_colloquium" value="<?php echo $colloquium->id; ?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $colloquium->description; ?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>URL <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="url" name="url" value="<?php echo $colloquium->url; ?>" readonly>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" autocomplete="off" value="<?php if($colloquium->date_time){ echo date('d-m-Y', strtotime($colloquium->date_time)); } ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="to_date" name="to_date" autocomplete="off" value="<?php if($colloquium->to_date){ echo date('d-m-Y', strtotime($colloquium->to_date)); } ?>" readonly>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Mode <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="mode" id="mode" value="1" <?php if($colloquium->mode=='1') {
                             echo "checked=checked";
                          };?> disabled><span class="check-radio"></span> Face To Face
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="mode" id="mode" value="0" <?php if($colloquium->mode=='0') {
                             echo "checked=checked";
                          };?>  disabled>
                          <span class="check-radio"></span> Online
                        </label>
                    </div>
                </div>



            </div>

            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Attendance <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="attendance" id="attendance" value="1" <?php if($colloquium->attendance=='1') {
                             echo "checked=checked";
                          };?> disabled><span class="check-radio"></span> Compulsary
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="attendance" id="attendance" value="0" <?php if($colloquium->attendance=='0') {
                             echo "checked=checked";
                          };?> disabled>
                          <span class="check-radio"></span> Optional
                        </label>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="registration_start_date" name="registration_start_date" autocomplete="off" value="<?php if($colloquium->registration_start_date){ echo date('d-m-Y', strtotime($colloquium->registration_start_date)); } ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="registration_end_date" name="registration_end_date" autocomplete="off" value="<?php if($colloquium->registration_end_date){ echo date('d-m-Y', strtotime($colloquium->registration_end_date)); } ?>" readonly>
                    </div>
                </div>


            </div>

            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" <?php if($colloquium->status=='1') {
                             echo "checked=checked";
                          };?> disabled><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0" <?php if($colloquium->status=='0') {
                             echo "checked=checked";
                          };?> disabled>
                          <span class="check-radio"></span> In-Active
                        </label>
                    </div>
                </div>

            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">

                <?php

                if($colloquium_interest > 0)
                {
                    ?>

                    <a class="btn btn-link">Your Interest Already Submitted
                    <?php
                    if($interest->date_time)
                    {
                        ?>
                     On <?php echo date('d-m-Y', strtotime($interest->date_time)) ?>
                         
                    <?php
                    }
                    ?>
                     </a>

                    <?php

                }
                elseif(date('d-m-Y',strtotime($colloquium->registration_start_date)) <= date('d-m-Y') && date('d-m-Y',strtotime($colloquium->registration_end_date)) >= date('d-m-Y'))
                {
                ?>
                    <button type="submit" class="btn btn-primary btn-lg">Add Interest</button>
                    <a href="../list" class="btn btn-link">Back</a>
                <?php
                }
                else
                {
                    ?>

                    <a class="btn btn-link">Registration Strart OR End Dates Are Closed</a>

                    <?php
                }
                ?>


                

            </div>
        </div>


        </form>



        <br>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();


    $(document).ready(function() {
        $("#form_details").validate({
            rules: {
                id_programme: {
                    required: true
                },
                id_intake: {
                    required: true
                }
            },
            messages: {
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(document).ready(function() {
        $("#form_unit").validate({
            rules: {
                name: {
                    required: true
                },
                url: {
                    required: true
                },
                date_time: {
                    required: true
                },
                to_date: {
                    required: true
                },
                registration_start_date: {
                    required: true
                },
                registration_end_date: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                url: {
                    required: "<p class='error-text'>URL Required</p>",
                },
                date_time: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                to_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                registration_start_date: {
                    required: "<p class='error-text'>Select Registration Start Date</p>",
                },
                registration_end_date: {
                    required: "<p class='error-text'>Select Registration End Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

     $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "2019:2020"
    });
  } );
</script>
