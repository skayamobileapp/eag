<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Colloquium</h3>
        </div>
        <form id="form_unit" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Colloquium Details</h4>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>URL <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="url" name="url">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" autocomplete="off">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="to_date" name="to_date" autocomplete="off">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Mode <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="mode" id="mode" value="1" checked="checked"><span class="check-radio"></span> Face To Face
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="mode" id="mode" value="0"><span class="check-radio"></span> Online
                        </label>                              
                    </div>                         
                </div>


            </div>

            <div class="row">




                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Attendance <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="attendance" id="attendance" value="1" checked="checked"><span class="check-radio"></span> Compulsary
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="attendance" id="attendance" value="0"><span class="check-radio"></span> Optional
                        </label>                              
                    </div>                         
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Registration Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="registration_start_date" name="registration_start_date" autocomplete="off">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Registration End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="registration_end_date" name="registration_end_date" autocomplete="off">
                    </div>
                </div>



            </div>

            <div class="row">



                

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                        </label>                              
                    </div>                         
                </div>



            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>



        <br>




        <form id="form_details" action="" method="post">
            <div class="form-container">
            <h4 class="form-group-title">Research Colloquium Details</h4>

                <div class="row">




                    <div class="col-sm-4">
                      <div class="form-group">
                         <label>Programme <span class='error-text'>*</span></label>
                         <select name="id_programme" id="id_programme" class="form-control" onchange="getIntakeByProgramId(this.value)">
                            <option value="">Select</option>
                            <?php
                               if (!empty($programList))
                               {
                                 foreach ($programList as $record)
                                 {
                                        ?>
                            <option value="<?php echo $record->id;  ?>">
                               <?php echo $record->code . " - " . $record->name;  ?>
                            </option>
                            <?php
                                }
                              }
                               ?>
                         </select>
                      </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Intake <span class='error-text'>*</span></label>
                            <span id="view_intake">
                              <select class="form-control" id='id_intake' name='id_intake'>
                                <option value=''></option>
                              </select>
                            </span>
                        </div>
                    </div>






                  
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                    </div>
                </div>


                <div class="row">
                    <div id="view_colloquium_details"></div>
                </div>

            </div>
        </form>






















        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();


    function getIntakeByProgramId(id_programme)
    {
        if(id_programme != '')
        {

            $.get("/research/colloquium/getIntakeByProgramId/"+id_programme, function(data, status){
           
                $("#view_intake").html(data);
                $("#view_intake").show();
            });
        }
    }


    function saveData()
    {
        if($('#form_details').valid())
        {

        var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();

            $.ajax(
            {
               url: '/research/colloquium/tempAddColloquiumDetails',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_colloquium_details").html(result);
               }
            }); 
        }
    }


    function deleteTempColloquiumDetails(id)
    {
        if(id != '')
        {

            $.get("/research/colloquium/deleteTempColloquiumDetails/"+id, function(data, status){
           
                $("#view_colloquium_details").html(data);
                $("#view_colloquium_details").show();
            });
        }
    }


    $(document).ready(function() {
        $("#form_details").validate({
            rules: {
                id_programme: {
                    required: true
                },
                id_intake: {
                    required: true
                }
            },
            messages: {
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function() {
        $("#form_unit").validate({
            rules: {
                name: {
                    required: true
                },
                url: {
                    required: true
                },
                date_time: {
                    required: true
                },
                to_date: {
                    required: true
                },
                registration_start_date: {
                    required: true
                },
                registration_end_date: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                url: {
                    required: "<p class='error-text'>URL Required</p>",
                },
                date_time: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                to_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                registration_start_date: {
                    required: "<p class='error-text'>Select Registration Start Date</p>",
                },
                registration_end_date: {
                    required: "<p class='error-text'>Select Registration End Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "2019:2020"
    });
  } );

</script>
