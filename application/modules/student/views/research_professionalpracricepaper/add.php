<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Research Professional Pracrice Paper</h3>
            </div>


    <h4 class='sub-title'>Student Details</h4>

        <div class='data-list'>
            <div class='row'>

                <div class='col-sm-6'>
                    <dl>
                        <dt>Student Name :</dt>
                        <dd><?php echo $studentDetails->full_name ?></dd>
                    </dl>
                    <dl>
                        <dt>Student Email :</dt>
                        <dd><?php echo $studentDetails->email_id ?></dd>
                    </dl>
                    <dl>
                        <dt>Student NRIC :</dt>
                        <dd><?php echo $studentDetails->nric ?></dd>
                    </dl>
                    
                </div>        
                
                <div class='col-sm-6'>
                    <dl>
                        <dt>Intake :</dt>
                        <dd>
                            <?php echo $studentDetails->intake_name ?>
                        </dd>
                    </dl>
                    <dl>
                        <dt>Programme :</dt>
                        <dd><?php echo $studentDetails->programme_name ?></dd>
                    </dl>
                    <dl>
                        <dt>Nationality :</dt>
                        <dd><?php echo $studentDetails->nationality ?></dd>
                    </dl>
                </div>

            </div>
        </div>

    <form id="form_programme" action="" method="post">
        <div class="form-container">
            <h4 class="form-group-title">Research Professional Pracrice Paper Details</h4>


            <!-- <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" onchange="getPreviousProgramSchemeByProgramId(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Intake <span class='error-text'>*</span></label>
                        <span id='view_program_intake'></span>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Student <span class='error-text'>*</span></label>
                        <span id='view_students_list'></span>
                    </div>
                </div>     

            </div> -->

            <br>
            
            <div id="view_student_details">
            </div>

            <br>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Proposed Area Of PPP <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="proposed_area_ppp" name="proposed_area_ppp" >
                    </div>
                </div>      


            </div>

            <div class="row">
                
                <br>
                <br>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reason For Applying <span class='error-text'>*</span></label>
                        <br>
                        <input type="checkbox" id="skill_enhancement" name="skill_enhancement" >
                        Skill Enhancement
                        <br>
                        <label>Other Reason</label>
                        <input type="text" class="form-control" id="other_details" name="other_details" >
                    </div>
                </div>

                

            </div>

        </div>

    </form>

    <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="button" class="btn btn-primary btn-lg" onclick="validateProgram()">Save</button>
            <a href="list" class="btn btn-link">Cancel</a>
        </div>
    </div>






    <div class="form-container">
            <h4 class="form-group-title"> Professional Pracrice Paper Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Employment Details</a>
                    </li>
                    <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Supervisor Details</a>
                    </li>
                    <li role="presentation"><a href="#tab_three" class="nav-link border rounded text-center"
                            aria-controls="tab_three" role="tab" data-toggle="tab">Examiner Details</a>
                    </li>

                      
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">


                            <form id="form_employment" action="" method="post">

                        <div class="form-container">
                        <h4 class="form-group-title">Employment Details</h4>

                            <div class="row">


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Company Name <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="company_name" name="company_name" autocomplete="off">
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Company Address <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="address" name="address" autocomplete="off">
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Position <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="position" name="position" autocomplete="off">
                                    </div>
                                </div>



                            </div>

                            <div class="row">

                                

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Job Function <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="job_function" name="job_function" autocomplete="off">
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>From Year <span class='error-text'>*</span></label>
                                        <input type="number" class="form-control" id="from_year" name="from_year" autocomplete="off">
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>To Year <span class='error-text'>*</span></label>
                                        <input type="number" class="form-control" id="to_year" name="to_year" autocomplete="off">
                                    </div>
                                </div>


                            </div>

                            <div class="row">


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Reference Number <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="reference_number" name="reference_number" autocomplete="off">
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Reference Address <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="reference_address" name="reference_address" autocomplete="off">
                                    </div>
                                </div>
                              
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="tempAddResearchProfessionalpracricepaperHasEmployment()">Add</button>
                                </div>
                            </div>

                        </div>


                    </form>


                    <div class="row">
                        <div id="view_employment"></div>
                    </div>

                        


                        </div> 
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="col-12">




                        <form id="form_supervisor" action="" method="post">

                        <div class="form-container">
                        <h4 class="form-group-title">Supervisor Details</h4>

                            <div class="row">


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Supervisor <span class='error-text'>*</span></label>
                                        <select class="form-control" id="id_staff" name="id_staff">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($staffList))
                                            {
                                                foreach ($staffList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php echo $record->ic_no . " - " . $record->name;?>
                                                 </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Supervisor Role <span class='error-text'>*</span></label>
                                        <select class="form-control" id="id_staff_role" name="id_staff_role">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($supervisorRoleList))
                                            {
                                                foreach ($supervisorRoleList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php echo $record->code;?>
                                                 </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- <div class="col-sm-4">
                                    <div class="form-group">
                                        <p>Status <span class='error-text'>*</span></p>
                                        <label class="radio-inline">
                                          <input type="radio" name="supervisor_status" id="supervisor_status" value="1" checked="checked"><span class="check-radio"></span> Active
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="supervisor_status" id="supervisor_status" value="0"><span class="check-radio"></span> In-Active
                                        </label>                              
                                    </div>                         
                                </div> -->


                            </div>


                            <div class="row">
                              
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="tempAddResearchProfessionalpracricepaperHasSupervisor()">Add</button>
                                </div>
                            </div>

                        </div>


                    </form>


                    <div class="row">
                        <div id="view_supervisor"></div>
                    </div>




                       



                        </div> 
                    </div>



                <div role="tabpanel" class="tab-pane" id="tab_three">
                    <div class="col-12">



                         <form id="form_examiner" action="" method="post">

                        <div class="form-container">
                        <h4 class="form-group-title">Examiner Details</h4>

                            <div class="row">


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Examiner <span class='error-text'>*</span></label>
                                        <select class="form-control" id="id_examiner" name="id_examiner">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($examinerList))
                                            {
                                                foreach ($examinerList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php
                                                    if($record->type == 1)
                                                    {
                                                        echo $record->ic_no . " - " . $record->staff_name;
                                                    }
                                                    else
                                                    {
                                                        echo "External - " . $record->full_name;
                                                    }
                                                    ?>
                                                 </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Examiner Role <span class='error-text'>*</span></label>
                                        <select class="form-control" id="id_examiner_role" name="id_examiner_role">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($examinerRoleList))
                                            {
                                                foreach ($examinerRoleList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php echo $record->code;?>
                                                 </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- <div class="col-sm-4">
                                    <div class="form-group">
                                        <p>Status <span class='error-text'>*</span></p>
                                        <label class="radio-inline">
                                          <input type="radio" name="examiner_status" id="examiner_status" value="1" checked="checked"><span class="check-radio"></span> Active
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="examiner_status" id="examiner_status" value="0"><span class="check-radio"></span> Inactive
                                        </label>                              
                                    </div>                         
                                </div> -->

                            </div>


                            <div class="row">

                              
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="tempAddResearchProfessionalpracricepaperHasExaminer()">Add</button>
                                </div>
                            </div>

                        </div>


                    </form>


                    <div class="row">
                        <div id="view_examiner"></div>
                    </div>





                    </div>

                </div>



                </div>

            </div>
        

    </div>









    

         
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script>

    function getPreviousProgramSchemeByProgramId(id)
    {
        $.get("/student/professionalpracricepaper/getIntakeByProgramme/"+id, function(data, status)
        {
            $("#view_program_intake").html(data);
            $("#view_program_intake").show();
        });
    }


    function getStudentByData()
    {
        // alert('sas');
        var id_programme = $("#id_programme").val();
        var id_intake = $("#id_intake").val();

         

        if(id_programme != '' && id_intake != '')
        {

        var tempPR = {};
       
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();


            $.ajax(
            {
                url: '/student/professionalpracricepaper/getStudentByData',
                type: 'POST',
                data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_students_list").html(result);
               }
            });
        }
    }



     function getStudentByStudentId(id)
     {

         $.get("/student/professionalpracricepaper/getStudentByStudentId/"+id, function(data, status){
       
            $("#view_student_details").html(data);
            $("#view_student_details").show();
        });
     }






    function tempAddResearchProfessionalpracricepaperHasSupervisor()
    {
        if($('#form_supervisor').valid())
        {

        var tempPR = {};
        tempPR['id_staff'] = $("#id_staff").val();
        tempPR['id_staff_role'] = $("#id_staff_role").val();
        // tempPR['status'] = $("#supervisor_status").val();
        tempPR['status'] = 1;

            $.ajax(
            {
               url: '/student/professionalpracricepaper/tempAddResearchProfessionalpracricepaperHasSupervisor',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_supervisor").html(result);
               }
            });
        }
    }

    function deleteTempResearchProfessionalpracricepaperHasSupervisor(id) {
        // alert(id);
         $.ajax(
            {
               url: '/student/professionalpracricepaper/deleteTempResearchProfessionalpracricepaperHasSupervisor/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_supervisor").html(result);
               }
            });
    }



    function tempAddResearchProfessionalpracricepaperHasExaminer()
    {
        if($('#form_examiner').valid())
        {

        var tempPR = {};
        tempPR['id_examiner'] = $("#id_examiner").val();
        tempPR['id_examiner_role'] = $("#id_examiner_role").val();
        // tempPR['status'] = $("#examiner_status").val();
        tempPR['status'] = 1;

            $.ajax(
            {
               url: '/student/professionalpracricepaper/tempAddResearchProfessionalpracricepaperHasExaminer',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_examiner").html(result);
               }
            });
        }
    }

    function deleteTempResearchProfessionalpracricepaperHasExaminer(id) {
        // alert(id);
         $.ajax(
            {
               url: '/student/professionalpracricepaper/deleteTempResearchProfessionalpracricepaperHasExaminer/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_examiner").html(result);
               }
            });
    }


    function tempAddResearchProfessionalpracricepaperHasEmployment()
    {
        if($('#form_employment').valid())
        {

        var tempPR = {};
        tempPR['company_name'] = $("#company_name").val();
        tempPR['address'] = $("#address").val();
        tempPR['position'] = $("#position").val();
        tempPR['job_function'] = $("#job_function").val();
        tempPR['from_year'] = $("#from_year").val();
        tempPR['to_year'] = $("#to_year").val();
        tempPR['reference_number'] = $("#reference_number").val();
        tempPR['reference_address'] = $("#reference_address").val();
        tempPR['status'] = 1;

            $.ajax(
            {
               url: '/student/professionalpracricepaper/tempAddResearchProfessionalpracricepaperHasEmployment',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_employment").html(result);
               }
            });
        }
    }


    function deleteTempResearchProfessionalpracricepaperHasEmployment(id)
    {
         $.ajax(
            {
               url: '/student/professionalpracricepaper/deleteTempResearchProfessionalpracricepaperHasEmployment/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_employment").html(result);
               }
            });
    }
    

    $(document).ready(function() {
        $("#form_supervisor").validate({
            rules: {
                id_staff: {
                    required: true
                },
                id_staff_role: {
                    required: true
                }
            },
            messages: {
                id_staff: {
                    required: "<p class='error-text'>Select Supervisor</p>",
                },
                id_staff_role: {
                    required: "<p class='error-text'>Select Supervisor Role</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_examiner").validate({
            rules: {
                id_examiner: {
                    required: true
                },
                id_examiner_role: {
                    required: true
                }
            },
            messages: {
                id_examiner: {
                    required: "<p class='error-text'>Select Examiner</p>",
                },
                id_examiner_role: {
                    required: "<p class='error-text'>Select Examiner Role</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_employment").validate({
            rules: {
                company_name: {
                    required: true
                },
                address: {
                    required: true
                },
                position: {
                    required: true
                },
                job_function: {
                    required: true
                },
                from_year: {
                    required: true
                },
                to_year: {
                    required: true
                },
                reference_number: {
                    required: true
                },
                reference_address: {
                    required: true
                }
            },
            messages: {
                company_name: {
                    required: "<p class='error-text'>Company Name Required</p>",
                },
                address: {
                    required: "<p class='error-text'>Address Required</p>",
                },
                position: {
                    required: "<p class='error-text'>Position Required</p>",
                },
                job_function: {
                    required: "<p class='error-text'>Job Function Required</p>",
                },
                from_year: {
                    required: "<p class='error-text'>From Year Required</p>",
                },
                to_year: {
                    required: "<p class='error-text'>To Year Required</p>",
                },
                reference_number: {
                    required: "<p class='error-text'>Reference No. Required</p>",
                },
                reference_address: {
                    required: "<p class='error-text'>Reference Address Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                id_intake: {
                    required: true
                },
                id_programme: {
                    required: true
                },
                id_student: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                skill_enhancement: {
                    required: true
                },
                proposed_area_ppp: {
                    required: true
                }
            },
            messages: {
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                skill_enhancement: {
                    required: "<p class='error-text'>Select Skill Enhancement</p>",
                },
                proposed_area_ppp: {
                    required: "<p class='error-text'>Proposed Area Of PPP Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function validateProgram() {

    if($('#form_programme').valid())
      {
        console.log($("#view_supervisor").html());
        var dataSupervisor = $("#view_supervisor").html();
        var dataExaminer = $("#view_examiner").html();
        var dataEmployment = $("#view_employment").html();
        if(dataEmployment=='')
        {
            alert("Add Employment To Research Professional Pracrice Paper");
        }
        else if(dataSupervisor=='')
        {
            alert("Add Supervisor To Research Professional Pracrice Paper");
        }else if(dataExaminer=='')
        {
            alert("Add Examiner To Research Professional Pracrice Paper");
        }else
        {
            $('#form_programme').submit();
        }
      }     
    }

    $('select').select2();


    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "2019:2020"
    });
  } );
</script>