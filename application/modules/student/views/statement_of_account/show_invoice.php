<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <form id="form_receipt" action="" method="post">
        <div class="page-title clearfix">
            <h3>View Main Invoice</h3>
            <?php 
            if($route == 'invoice')
            {
            ?>
                <a href="../../viewInvoice" class="btn btn-link btn-back">‹ Back</a>

            <?php 
            }elseif($route == 'summary')
            {
            ?>
                <a href="../../viewSummary" class="btn btn-link btn-back">‹ Back</a>

            <?php 
            }
            ?>
        </div>

        <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl>                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $studentDetails->programme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Education Level :</dt>
                                <dd><?php echo $studentDetails->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $studentDetails->program_scheme; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>

        

        <div class="form-container">
                <h4 class="form-group-title">Main Invoice Details</h4>             
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Invoice Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="invoice_number" name="invoice_number" value="<?php echo $mainInvoice->invoice_number;?>" readonly="readonly">
                        </div>
                    </div>

                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Invoice Type</label>
                            <input type="text" class="form-control" id="remarks" name="remarks" value="<?php echo $mainInvoice->type;?>" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Remarks</label>
                            <input type="text" class="form-control" id="remarks" name="remarks" value="<?php echo $mainInvoice->remarks;?>" readonly="readonly">
                        </div>
                    </div>

                    
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Invoice Total Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="invoice_total" name="invoice_total" value="<?php echo number_format($mainInvoice->invoice_total, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Total Discount Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_discount" name="total_discount" value="<?php echo number_format($mainInvoice->total_discount, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>


                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>Total Payable Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_amount" name="total_amount" value="<?php echo number_format($mainInvoice->total_amount, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>
                    
                
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Paid Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_amount" name="total_amount" value="<?php echo number_format($mainInvoice->paid_amount, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Balance Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_amount" name="total_amount" value="<?php echo number_format($mainInvoice->balance_amount, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Currency <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_amount" name="total_amount" value="<?php echo $mainInvoice->currency_name; ?>" readonly="readonly">
                        </div>
                    </div>

                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Status <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="remarks" name="remarks" value="<?php 
                            if($mainInvoice->status == '0')
                            {
                                echo 'Pending';
                            }
                            elseif($mainInvoice->status == '1')
                            {
                                echo 'Approved';
                            }
                            elseif($mainInvoice->status == '2')
                            {
                                echo 'Rejected';
                            }?>" readonly="readonly">
                        </div>
                    </div>

                
                </div>

                <div class="row">

                    

                    <?php
                    if($mainInvoice->status == '2')
                    {
                     ?>


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Reject Reason <span class='error-text'>*</span></label>
                                <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $mainInvoice->reason; ?>" readonly>
                            </div>
                        </div>

                    <?php
                    }
                    ?>

                </div>


            </div>

            <!-- <div class="form-container">
                <h4 class="form-group-title">Student Details For Main Invoice Details</h4>  
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Student Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="student_name" name="student_name" value="<?php echo $mainInvoice->student_name;?>" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Student NRIC <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="student_nric" name="student_nric" value="<?php echo $mainInvoice->student_nric;?>" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Intake <span class='error-text'>*</span></label>
                            <input type="text" readonly="readonly" class="form-control" id="remarks" name="remarks" value="<?php echo $mainInvoice->intake_name;?>">
                        </div>
                    </div>

                </div>


                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="programme" name="programme" readonly="readonly" value="<?php echo $mainInvoice->programme_code . ' - ' . $mainInvoice->programme_name;?>" >
                        </div>
                    </div>
                </div>
            </div> -->
            

            <div class="form-container">
                <h4 class="form-group-title">Main Invoice Details</h4>  
                <div class="custom-table">
                    <table class="table" id="list-table">
                        <thead>
                        <tr>
                            <th>Sl. No</th>
                            <th>Fee Item</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $total_amount = 0;
                        if (!empty($mainInvoiceDetailsList)) {
                            $i = 1;
                            foreach ($mainInvoiceDetailsList as $record) {
                        ?>
                            <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $record->fee_setup ?></td>
                                <td><?php echo $record->amount ?></td>
                            </tr>
                        <?php
                        $total_amount = $total_amount + $record->amount;
                        $i++;
                            }
                        }
                        $total_amount = number_format($total_amount, 2, '.', ',');
                        ?>
                        <tr>
                                <td bgcolor=""></td>
                                <td bgcolor="" style="text-align:  right;"><b> Total :  </b></td>
                                <td bgcolor=""><b><?php echo $total_amount; ?></b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="form-container">
                <h4 class="form-group-title">Main Invoice Discount Details</h4>  
                <div class="custom-table">
                    <table class="table" id="list-table">
                        <thead>
                        <tr>
                            <th>Sl. No</th>
                            <th>Discount</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $discount_total_amount = 0;
                        if (!empty($mainInvoiceDiscountDetailsList)) {
                            $i = 1;
                            foreach ($mainInvoiceDiscountDetailsList as $record) {
                        ?>
                            <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $record->name ?></td>
                                <td><?php echo $record->amount ?></td>
                            </tr>
                        <?php
                        $discount_total_amount = $discount_total_amount + $record->amount;
                        $i++;
                            }
                        }
                        $discount_total_amount = number_format($discount_total_amount, 2, '.', ',');
                        ?>
                        <tr>
                                <td bgcolor=""></td>
                                <td bgcolor="" style="text-align:  right;"><b> Total :  </b></td>
                                <td bgcolor=""><b><?php echo $discount_total_amount; ?></b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>





        

       </form>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <?php 
                if($route == 'invoice')
                {
                ?>
                    <a href="../../viewInvoice" class="btn btn-link btn-back">‹ Back</a>

                <?php 
                }elseif($route == 'summary')
                {
                ?>
                    <a href="../../viewSummary" class="btn btn-link btn-back">‹ Back</a>

                <?php 
                }
                ?>
            </div>
        </div>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $('select').select2();
</script>