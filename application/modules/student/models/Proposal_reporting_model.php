<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Proposal_reporting_model extends CI_Model
{
    function getSupervisor($id)
    {
        $this->db->select('s.*');
        $this->db->from('research_supervisor as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, qs.name as qualification_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('scholarship_education_level as qs', 's.id_degree_type = qs.id');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudent($id_student)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function researchTopicListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_topic');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function deliverablesList()
    {
        $this->db->select('*');
        $this->db->from('research_deliverables_student');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function durationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_phd_duration');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }

    function chapterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_chapter');
        $this->db->where('status', $status);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }

    function researchStatusListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_status');
        $this->db->where('status', $status);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }

    function deliverableListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_deliverables');
        $this->db->where('status', $status);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }
    
    function getChapterByDuration($id_phd_duration)
    {
        $this->db->select('DISTINCT(rd.id_chapter) as id,rc.*');
        $this->db->from('research_deliverables as rd');
        $this->db->join('research_chapter as rc','rd.id_chapter = rc.id');
        $this->db->where('rd.id_phd_duration', $id_phd_duration);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getTopicByData($data)
    {
        $this->db->select('rd.*');
        $this->db->from('research_deliverables as rd');
        $this->db->join('research_chapter as rc','rd.id_chapter = rc.id');
        $this->db->where('rd.id_phd_duration', $data['id_phd_duration']);
        $this->db->where('rd.id_chapter', $data['id_chapter']);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getProposalReportingListByStudentId($id_student,$stage)
    {
        $this->db->select('rd.*, rdur.topic as deliverable, rs.full_name as supervisor_name, rs.email as supervisor_email, rc.name as chapter');
        $this->db->from('research_proposal_reporting as rd');
        $this->db->join('research_deliverables as rdur','rd.id_deliverable = rdur.id');
        $this->db->join('research_chapter as rc','rd.id_chapter = rc.id');
        // $this->db->join('research_status as rst','rd.status = rst.id','left');
        // $this->db->join('research_phd_duration as rpd','rd.id_phd_duration = rpd.id');
        $this->db->join('research_supervisor as rs','rd.id_supervisor = rs.id');
        $this->db->where('rd.id_student', $id_student);
        if ($stage != '')
        {
        	$this->db->where('rd.stage', $stage);
        }
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProposalReport($id)
    {
        $this->db->select('rd.*');
        $this->db->from('research_proposal_reporting as rd');
        // $this->db->join('research_status as rst','rd.status = rst.id','left');
        $this->db->where('rd.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProposalReporting($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_proposal_reporting', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editDeliverables($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_deliverables_student', $data);
        return TRUE;
    }

    function generateProposalReportingApplicationNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('research_proposal_reporting as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "RPR" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    
    function getProposalReportComments($id)
    {
        $this->db->select('rd.*');
        $this->db->from('proposal_reporting_comments as rd');
        $this->db->where('rd.id_proposal_reporting', $id);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }
}

