<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Toc_model extends CI_Model
{
    function getSupervisor($id)
    {
        $this->db->select('s.*');
        $this->db->from('research_supervisor as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudent($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
         return $query->row();
    }

    function getTocByStudentId($id_student)
    {
        $this->db->select('rd.*, rs.full_name as supervisor_name, rs.email as supervisor_email');
        $this->db->from('research_toc as rd');
        $this->db->join('research_supervisor as rs','rd.id_supervisor = rs.id');
        $this->db->where('rd.id_student', $id_student);
        // if ($stage != '')
        // {
        //     $this->db->where('rd.stage', $stage);
        // }
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, qs.name as qualification_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('scholarship_education_level as qs', 's.id_degree_type = qs.id');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function addToc($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_toc', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getToc($id)
    {
        $this->db->select('ia.*');
        $this->db->from('research_toc as ia');
         $this->db->where('ia.id', $id);
        $query = $this->db->get();
          $result = $query->row();
         return $result; 
    }

    function tocCommentsDetails($id_toc)
    {
        $this->db->select('rd.*');
        $this->db->from('toc_reporting_comments as rd');
        $this->db->where('rd.id_toc', $id_toc);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
        
    }
}