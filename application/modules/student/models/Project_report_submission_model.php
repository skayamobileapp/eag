<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Project_report_submission_model extends CI_Model
{
    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*,, n.name as nationality, p.name as programme_name, i.name as intake_name, qs.name as qualification_name');
        $this->db->from('student as s');
        $this->db->join('nationality as n', 's.nationality = n.id'); 
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('scholarship_education_level as qs', 's.id_degree_type = qs.id');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }


	function getProjectReportListByStudentId($id_student)
	{
        $this->db->select('ia.*');
        $this->db->from('project_report_submission as ia');
        $this->db->where('ia.id_student', $id_student);
        $query = $this->db->get();
         return $query->result();
    }

    function companyTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('internship_company_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function companyTypeList()
    {
        $this->db->select('*');
        $this->db->from('internship_company_type');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function companyRegistrationListByStatus($status)
    {
        $this->db->select('invt.*');
        $this->db->from('internship_company_registration as invt');
        $this->db->where('invt.status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function companyRegistrationList()
    {
        $this->db->select('invt.*');
        $this->db->from('internship_company_registration as invt');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getCompanyByCompanyTypeId($id_company_type,$id_program)
    {
        $this->db->select('DISTINCT(icr.id) as id, icr.*');
        $this->db->from('internship_company_registration as icr');
        $this->db->join('internship_company_has_program as ichp', 'icr.id = ichp.id_company');
        $this->db->where('icr.id_company_type', $id_company_type);
        $this->db->where('ichp.id_program', $id_program);
        $this->db->where('icr.status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function addProjectReport($data)
    {
        $this->db->trans_start();
        $this->db->insert('project_report_submission', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getProjectReport($id)
    {
        $this->db->select('ia.*');
        $this->db->from('project_report_submission as ia');
         $this->db->where('ia.id', $id);
        $query = $this->db->get();
          $result = $query->row();
         return $result; 
    }

    function getMaxLimit()
    {
        $this->db->select('ia.*');
        $this->db->from('internship_student_limit as ia');
        $this->db->order_by("id", "DESC");
        $query = $this->db->get();
          $result = $query->row();
         return $result; 
    }


    function generateProjectReportNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('*');
            $this->db->from('project_report_submission');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
            $generated_number = "INTR" .(sprintf("%'06d", $count)). "/" . $Year;
           // echo "<Pre>";print_r($generated_number);exit();
           return $generated_number;
    }

    // function checkStudentProjectReport($data)
    // {
    //     $check = 0;
    //     $this->db->select('ia.*');
    //     $this->db->from('project_report_submission as ia');
    //     $this->db->join('internship_company_type as ict', 'ia.id_company_type = ict.id');
    //     $this->db->join('internship_company_registration as icr', 'ia.id_company = icr.id');
    //     $this->db->where('ia.id_student', $data['id_student']);
    //     $this->db->where('ia.id_program', $data['id_program']);
    //     $this->db->where('ia.id_intake', $data['id_intake']);
    //     $this->db->where('ia.id_qualification', $data['id_qualification']);
    //     $this->db->where('ia.status', '0');
    //     $this->db->or_where('ia.status', '1');
    //     $this->db->order_by("ia.id", "desc");
    //     $query = $this->db->get();
    //     $result = $query->num_rows();

    //     $max_limit = $this->getMaxLimit()->max_limit;

    //     if($result >= $max_limit )
    //     {
    //         $check = 1;
    //     }
    //     return $check;
    //     // echo "<Pre>";print_r($result . " - " . $max_limit);exit();
    // }


















    


    

    function getRole($id)
    {
        $this->db->select('*');
        $this->db->from('roles');
        $this->db->where('id', $id);
        $query = $this->db->get();
            // echo "<pre>";print_r($query);die;
        
        return $query->row();
    }
}