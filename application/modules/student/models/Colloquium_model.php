<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Colloquium_model extends CI_Model
{
    function colloquiumList()
    {
        $this->db->select('*');
        $this->db->from('research_colloquium');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function colloquiumListSearch($data)
    {
        $this->db->select('DISTINCT(id_colloquium) as id_colloquium');
        $this->db->from('research_colloquium_details');
        if ($data['id_intake'] != '')
        {
            $this->db->where('id_intake', $data['id_intake']);
        }
        if ($data['id_programme'] != '')
        {
            $this->db->where('id_programme', $data['id_programme']);
        }
        // if ($data['name'] != '')
        // {
        //     $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $results = $query->result();  
        
        // echo "<Pre>"; print_r($result);exit;
        $details = array();
        foreach ($results as $result)
        {
            $id_colloquium = $result->id_colloquium;
            $colloquium = $this->getColloquium($id_colloquium);

            if($colloquium)
            {
                $data['id_colloquium'] = $id_colloquium;
                $is_interest = 0;
                $registered_date = 0;

                $colloquium_interest = $this->getColloquiumInterestByData($data);

                if($colloquium_interest)
                {
                    $is_interest = $colloquium_interest->id;
                    $registered_date = $colloquium_interest->date_time;
                }

                $colloquium->is_interest = $is_interest;
                $colloquium->registered_date = $registered_date;

                array_push($details, $colloquium);
            }

        }

         return $details;
    }


    function getColloquium($id)
    {
        $this->db->select('*');
        $this->db->from('research_colloquium');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function programListForPostgraduate($name)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->join('scholarship_education_level as el', 'p.id_education_level = el.id');
        $this->db->where('el.name', $name);
        $this->db->where('p.status', '1');
        $this->db->order_by("p.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    
    function addColloquiumInterest($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_colloquium_student_interest', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getColloquiumInterestByData($data)
    {
        $this->db->select('*');
        $this->db->from('research_colloquium_student_interest');
        $this->db->where('id_student', $data['id_student']);
        $this->db->where('id_colloquium', $data['id_colloquium']);
        $query = $this->db->get();
        return $query->row();
    }

    function getColloquiumInterested($id_colloquium,$id_student)
    {
        $this->db->select('*');
        $this->db->from('research_colloquium_student_interest');
        $this->db->where('id_student', $id_student);
        $this->db->where('id_colloquium', $id_colloquium);
        $query = $this->db->get();
        $result = $query->row();

        if($result)
        {
            return $result->id;
        }
        else
        {
            return 0;
        }
    }
}