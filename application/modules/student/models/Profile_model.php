<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Profile_model extends CI_Model
{
    function profileList()
    {
        $this->db->select('p.*');
        $this->db->from('payment_type as p');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProfile($id)
    {
        $this->db->select('p.*');
        $this->db->from('payment_type as p');
        $this->db->where('p.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProfile($data)
    {
        $this->db->trans_start();
        $this->db->insert('payment_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProfile($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('payment_type', $data);
        return TRUE;
    }

    function getStudentData($id)
    {
        $this->db->select('a.*, in.name as intake, p.name as program, qs.name as qualification_name');
        $this->db->from('student as a');
        $this->db->join('intake as in', 'a.id_intake = in.id','left');
        $this->db->join('programme as p', 'a.id_program = p.id','left');
        $this->db->join('scholarship_education_level as qs', 'a.id_degree_type = qs.id','left');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getInvoiceByStudentId($id_student,$id_applicant)
    {
        $student = "Student";
        $applicant = "Applicant";
        $balance_amount = '0.00';
        $this->db->select('mi.*, cs.name as currency_name');
        $this->db->from('main_invoice as mi');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        $likeCriteria = " mi.balance_amount  > '" . $balance_amount . "' and  (id_student  = '" . $id_student . "' and type  ='" . $student . "') or (id_student  ='" . $id_applicant . "' and type  ='" . $applicant . "')";
        // echo "<Pre>";print_r($likeCriteria);exit();
        $this->db->where($likeCriteria);
        $this->db->order_by('mi.id','ASC');


        $query = $this->db->get();
        $results = $query->result();

        $details = array();

        foreach ($results as $value)
        {
            $balance_amount = $value->balance_amount;

            if($balance_amount > 0)
            {
                array_push($details, $value);
            }
        }

        return$details;
    }

    function getReceiptByStudentId($id_student,$id_applicant)
    {
        
        $student = "Student";
        $applicant = "Applicant";

        $this->db->select('mi.*, cs.name as currency_name');
        $this->db->from('receipt as mi');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        $likeCriteria = "(id_student  = '" . $id_student . "' and type  ='" . $student . "') or (id_student  ='" . $id_applicant . "' and type  ='" . $applicant . "')";
        $this->db->where($likeCriteria);
        $this->db->order_by('mi.id','DESC');

        $query = $this->db->get();
        $result = $query->result();

        return$result;
    }

    function getCourseRegisteredList($id_student,$id_intake,$id_program,$id_qualification,$id_program_scheme)
    {
        $this->db->select('DISTINCT(cr.id_course) as id_course, cr.*, rl.name as role, sem.code as semester_code, sem.name as semester_name, lct.name as course_type');
        $this->db->from('course_registration as cr');
        $this->db->join('course_register as creg', 'cr.id_course_register = creg.id');
        // $this->db->join('course as c', 'cr.id_course = c.id');
        $this->db->join('semester as sem', 'cr.id_semester = sem.id');
        $this->db->join('add_course_to_program_landscape as acpl', 'cr.id_course_registered_landscape = acpl.id');
        $this->db->join('landscape_course_type as lct', 'acpl.course_type = lct.id','left');
        // $this->db->join('programme_landscape as pl', 'acpl.id_program_landscape = pl.id');
        $this->db->join('users as rl', 'cr.created_by = rl.id','left');
        $this->db->where('cr.id_student', $id_student);
        // $this->db->where('cr.id_program_scheme', $id_program_scheme);
        $this->db->where('creg.id_intake', $id_intake);
        $this->db->where('creg.id_programme', $id_program);
        // $this->db->where('acpl.id_qualification', $id_qualification);
        $this->db->where('cr.is_exam_registered', '0');
        $this->db->where('cr.is_bulk_withdraw', '0');
        $query = $this->db->get();
        
        $results = $query->result();
        
         // echo "<Pre>";print_r($results);exit();
         
        $details = array();

        foreach ($results as $result)
        {
            $id_course = $result->id_course;

            $course = $this->getCourse($id_course);

            if($course)
            {
                $result->course_code = $course->code;
                $result->course_name = $course->name;
                $result->credit_hours = $course->credit_hours;

                array_push($details, $result);
            }
        }

        return $details;
    }

    function getCourse($id)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function semesterListByStatus($status)
    {
        $this->db->select('s.*');
        $this->db->from('semester as s');
        $this->db->where('s.status', $status);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getPerogramLandscape($id_intake,$id_programme,$id_program_scheme,$id_program_landscape,$student_semester)
    {
        $id_student = $this->session->id_student;
        // $student_semester = $this->session->student_semester;

        $this->db->select('DISTINCT(a.id) as id, a.id_semester, c.name , c.code');
        $this->db->from('add_course_to_program_landscape as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('course as c', 'a.id_course = c.id');
        $this->db->join('programme_landscape as pl', 'a.id_program_landscape = pl.id');
        $this->db->where('a.id_intake', $id_intake);
        $this->db->where('pl.status', '1');
        $this->db->where('pl.id_programme', $id_programme);
        // $this->db->where('a.id_program_scheme', $id_program_scheme);
        $this->db->where('a.id_program_landscape', $id_program_landscape);
        $this->db->where('a.id_semester <=', $student_semester);
        
        $likeCriteria = " a.id NOT IN (SELECT cr.id_course_registered_landscape FROM course_registration cr where cr.id_student = " . $id_student . ")";
        $this->db->where($likeCriteria);                

        $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function notificationList($type)
    {
        $this->db->select('sp.*');
        $this->db->from('notification as sp');
        if ($type != '')
        {
            $this->db->where('sp.type', $type);
        }
        $this->db->order_by("sp.id", "DESC");
        $query = $this->db->get();
        $results = $query->result();
        return $results;
    }

    function eventsList($type)
    {
        $this->db->select('sp.*');
        $this->db->from('event as sp');
        if ($type != '')
        {
            $this->db->where('sp.type', $type);
        }
        $this->db->where('sp.start_date <=', date('Y-m-d'));
        $this->db->where('sp.end_date >=', date('Y-m-d'));
        $this->db->order_by("sp.id", "DESC");
        $query = $this->db->get();
        $results = $query->result();
        return $results;
    }

    function getMainIvoice($id)
    {
        $this->db->select('*');
        $this->db->from('main_invoice');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getStudentDetailsById($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function generateReceiptNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('receipt as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "REC" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function addReceipt($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addReceiptDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateMainInvoice($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function addReceiptPaidDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt_paid_details',$data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
}