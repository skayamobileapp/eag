<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
            <h3>View Staff Statement Of Accounts</h3>
        </div>




            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#tab_one" class="nav-link border rounded text-center"
                            aria-controls="tab_one" aria-selected="true"
                            role="tab" data-toggle="tab">Faculty Details</a>
                    </li>

                    <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Invoice</a>
                    </li>

                    <li role="presentation"><a href="#tab_three" class="nav-link border rounded text-center"
                            aria-controls="tab_three" role="tab" data-toggle="tab">Recept & Payment</a>
                    </li>
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="tab_one">
                        <div class="col-12 mt-4">




                    

                            <form id="form_staff" action="" method="post">

                            <div class="form-container">
                                <h4 class="form-group-title">Faculty Details</h4>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Salutation <span class='error-text'>*</span></label>
                                             <select name="salutation" id="salutation" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($salutationList)) {
                                                    foreach ($salutationList as $record) {
                                                ?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php if($staffDetails->salutation==$record->id)
                                                            {
                                                                echo "selected=selected";
                                                            }
                                                            ?>
                                                            >
                                                            <?php echo $record->name;  ?>        
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>First Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $staffDetails->first_name;?>">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Last Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $staffDetails->last_name;?>">
                                        </div>
                                    </div>

                                      
                                    
                                </div>


                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>IC No. <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="ic_no" name="ic_no" value="<?php echo $staffDetails->ic_no;?>">
                                        </div>
                                    </div>       

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Gender <span class='error-text'>*</span></label>
                                             <select name="gender" id="gender" class="form-control">
                                                <option value="Male" <?php if($staffDetails->gender=='Male')
                                                {
                                                    echo "selected=selected";
                                                }
                                                ?>
                                                >Male</option>
                                                <option value="Female" <?php if($staffDetails->gender=='Female')
                                                {
                                                    echo "selected=selected";
                                                }?>
                                                >Female</option>
                                            </select>
                                        </div>
                                    </div>

                                     <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mobile  Number <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="mobile_number" name="mobile_number" value="<?php echo $staffDetails->mobile_number;?>">
                                        </div>
                                    </div> 
                                   
                                
                                </div>

                                <div class="row"> 

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Email <span class='error-text'>*</span></label>
                                            <input type="email" class="form-control" id="email" name="email" value="<?php echo $staffDetails->email;?>">
                                        </div>
                                    </div>    

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Faculty ID <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="staff_id" name="staff_id" value="<?php echo $staffDetails->staff_id;?>">
                                        </div>
                                    </div> 

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Faculty Type <span class='error-text'>*</span></label>
                                             <select name="job_type" id="job_type" class="form-control">
                                                <option value="">Select</option>
                                                <option value="0" <?php if($staffDetails->job_type=='0') { echo "selected=selected";}?>>Part Time</option>
                                                <option value="1" <?php if($staffDetails->job_type=='1') { echo "selected=selected";}?>>Full TIme</option>
                                            </select>
                                        </div>
                                    </div>  

                                    

                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Department <span class='error-text'>*</span></label>
                                            <select name="id_department" id="id_department" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($departmentList))
                                                {
                                                    foreach ($departmentList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_department)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->code . " - " . $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>DOB <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="dob" name="dob" value="<?php echo date('d-m-Y',strtotime($staffDetails->dob));?>">
                                        </div>
                                    </div>
                                    
                                     <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Academic Type <span class='error-text'>*</span></label>

                                             <select name="academic_type" id="academic_type" class="form-control">
                                                <option value="1" <?php if($staffDetails->academic_type=='1') { echo "selected=selected";}?>>Academic Faculty</option>
                                                <option value="0" <?php if($staffDetails->academic_type=='0') { echo "selected=selected";}?>>Non-Academic Faculty</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Faculty Program <span class='error-text'>*</span></label>
                                            <select name="id_faculty_program" id="id_faculty_program" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($facultyProgramList))
                                                {
                                                    foreach ($facultyProgramList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_faculty_program)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->code . " - " . $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Highest Education Level <span class='error-text'>*</span></label>
                                            <select name="id_education_level" id="id_education_level" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($qualificationList))
                                                {
                                                    foreach ($qualificationList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_education_level)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                            <div class="form-group">
                                                <p>Status <span class='error-text'>*</span></p>
                                                <label class="radio-inline">
                                                  <input type="radio" name="status" id="status" value="1" <?php if($staffDetails->status=='1') {
                                                     echo "checked=checked";
                                                  };?>><span class="check-radio"></span> Active
                                                </label>
                                                <label class="radio-inline">
                                                  <input type="radio" name="status" id="status" value="0" <?php if($staffDetails->status=='0') {
                                                     echo "checked=checked";
                                                  };?>>
                                                  <span class="check-radio"></span> In-Active
                                                </label>                              
                                            </div>                         
                                    </div>
                                    
                                </div>


                            </div>

                            <div class="form-container">
                                <h4 class="form-group-title">Contact Details</h4>

                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Phone  Number <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="phone_number" name="phone_number" value="<?php echo $staffDetails->phone_number;?>">
                                        </div>
                                    </div>     

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Address <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="address" name="address" value="<?php echo $staffDetails->address;?>">
                                        </div>
                                    </div>    

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Address 2 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="address_two" name="address_two" value="<?php echo $staffDetails->address_two;?>">
                                        </div>
                                    </div> 

                                    
                                </div>  

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Country <span class='error-text'>*</span></label>
                                            <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($countryList))
                                                {
                                                    foreach ($countryList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_country)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select State <span class='error-text'>*</span></label>
                                            <select name="id_state" id="id_state" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($stateList))
                                                {
                                                    foreach ($stateList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_state)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div> -->
                                     <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Select State <span class='error-text'>*</span></label>
                                                <span id='view_state'>
                                                    <select name='id_state' id='id_state' class='form-control'>
                                                        <option value=''></option>
                                                    </select>
                                                </span>
                                            </div>
                                        </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Zipcode <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $staffDetails->zipcode;?>">
                                        </div>
                                    </div>

                                      
                                </div>


                                

                            </div>



                            <div class="button-block clearfix">
                                    <div class="bttn-group">
                                        <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="1">Save</button>
                                        <a href="../list" class="btn btn-link">Cancel</a>
                                    </div>
                            </div>


                            


                            <div class="form-container">
                                <h4 class="form-group-title">Assign Courses to Faculty</h4> 


                                <div class="row">
                                     <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Course <span class='error-text'>*</span></label>
                                                <select name="id_course" id="id_course" class="form-control">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($courseList))
                                                    {
                                                        foreach ($courseList as $record)
                                                        {?>
                                                            <option value="<?php echo $record->id;?>"
                                                            ><?php echo $record->code . " - " . $record->name;?>
                                                            </option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                      
                                        <div class="col-sm-4">
                                            <button type="button" class="btn-lg btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                                        </div>
                                </div>




                                <?php

                                if(!empty($getStaffCourse))
                                {
                                    ?>

                                    <div class="form-container">
                                            <h4 class="form-group-title">Semester Info. Details</h4>

                                        

                                          <div class="custom-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                    <th>Sl. No</th>
                                                     <th>Course</th>
                                                     <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                     <?php
                                                 $total = 0;
                                                  for($i=0;$i<count($getStaffCourse);$i++)
                                                 { ?>
                                                    <tr>
                                                    <td><?php echo $i+1;?></td>
                                                    <td><?php echo $getStaffCourse[$i]->coursename;?></td>
                                                    <td class="text-center">
                                                        <a onclick="deleteCourseDetailData(<?php echo $getStaffCourse[$i]->id; ?>)">Delete</a>
                                                    </td>

                                                     </tr>
                                                  <?php 
                                              } 
                                              ?>
                                                </tbody>
                                            </table>
                                          </div>

                                        </div>

                                <?php
                                
                                }
                                 ?>





                                <!-- <div class="row">
                                    <div id="view"></div>
                                </div> -->
                                
                            </div>



                            </form>

        
        



        

                        



                        </div> 
                    
                    </div>




                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="mt-4">


                        <form id="form_scheme" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Coming Soon</h4>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                        <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="2">Save</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>

                           <!--  <div class="row">
                                <div id="view_scheme"></div>
                            </div> -->


                        </form>


<!-- 
                            <?php

                            if(!empty($getStaffTeachingDetails))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Teaching Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Semester</th>
                                                 <th>Programme Type</th>
                                                 <th>Course</th>
                                                 <th>Mode Of Study</th>
                                                 <th>Organisation</th>
                                                 <th>Created By</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($getStaffTeachingDetails);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $getStaffTeachingDetails[$i]->semester_code . " - " . $getStaffTeachingDetails[$i]->semester_name;?></td>
                                                <td><?php echo $getStaffTeachingDetails[$i]->program_code . " - " . $getStaffTeachingDetails[$i]->program_name;?></td>
                                                <td><?php echo $getStaffTeachingDetails[$i]->course_code . " - " . $getStaffTeachingDetails[$i]->course_name;?></td>
                                                <td><?php echo $getStaffTeachingDetails[$i]->mode_of_program . " - " . $getStaffTeachingDetails[$i]->mode_of_study;?></td>
                                                <td><?php echo $getStaffTeachingDetails[$i]->organisation_code . " - " . $getStaffTeachingDetails[$i]->organisation_name;?></td>
                                                <td><?php echo $getStaffTeachingDetails[$i]->user;?></td>
                                                <td>
                                                <a onclick="deleteTeachingDetails(<?php echo $getStaffTeachingDetails[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?> -->


                                </div>
                            
                            </div>








                    <div role="tabpanel" class="tab-pane" id="tab_three">
                        <div class="mt-4">



                            <form id="form_scheme" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Payment & Receipt Details</h4>

                            </div>


                            

                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                        <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="3">Save</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>

                           <!--  <div class="row">
                                <div id="view_scheme"></div>
                            </div> -->


                        </form>


<!-- 
                            <?php

                            if(!empty($getStaffLeaveDetails))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Staff Leave Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Leave Type</th>
                                                 <th>From Date</th>
                                                 <th>To Date</th>
                                                 <th>Created By</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($getStaffLeaveDetails);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $getStaffLeaveDetails[$i]->name;?></td>
                                                <td><?php
                                                if($getStaffLeaveDetails[$i]->from_dt)
                                                {

                                                 echo date('d-m-Y', strtotime($getStaffLeaveDetails[$i]->from_dt));
                                                }
                                                 ?></td>
                                                  <td><?php
                                                if($getStaffLeaveDetails[$i]->to_dt)
                                                {
                                                    
                                                 echo date('d-m-Y', strtotime($getStaffLeaveDetails[$i]->to_dt));
                                                }
                                                 ?></td>
                                                <td><?php echo $getStaffLeaveDetails[$i]->user;?></td>
                                                <td>
                                                <a onclick="deleteStaffLeaveDetails(<?php echo $getStaffLeaveDetails[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?> -->






                        </div>
                    
                    </div>






                </div>


            </div>












        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<script type="text/javascript">
    $('select').select2();


    $(function()
    {
        $(".datepicker").datepicker(
        {
            changeYear: true,
            changeMonth: true,
        });
    });


    $( document ).ready(function()
    {
        $.get("/af/staff/getStateByCountry/"+<?php echo $staffDetails->id_country;?>, function(data, status)
        {
            var idstateselected = "<?php echo $staffDetails->id_state;?>";

            $("#view_state").html(data);
            $("#id_state").find('option[value="'+idstateselected+'"]').attr('selected',true);
            $('select').select2();
        });
    });



    function getStateByCountry(id)
    {
     $.get("/af/staff/getStateByCountry/"+id, function(data, status)
     {
        $("#view_state").html(data);
        // $("#view_programme_details").html(data);
        // $("#view_programme_details").show();
     });
    }



    function saveData() {


        var tempPR = {};
        tempPR['id_course'] = $("#id_course").val();
        tempPR['id'] = <?php echo $id_staff;?>;
            $.ajax(
            {
               url: '/af/staff/directadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
                    // $("#view").html(result);
               }
            });
        
    }


    function getProgramModeByProgram(id_program)
    {
        // alert(id_program);
        $.get("/af/staff/getSchemeByProgramId/"+id_program, function(data, status)
        {
            $("#view_program_mode").html(data);
        });
    }



    function deleteCourseDetailData(id) {
         $.ajax(
            {
               url: '/af/staff/deleteCourseDetailData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    window.location.reload();
               }
            });
    }


    function deleteTeachingDetails(id)
    {
        // alert(id);

        $.ajax(
            {
               url: '/af/staff/deleteTeachingDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }

    function deleteStaffChangeStatus(id)
    {
        $.ajax(
            {
               url: '/af/staff/deleteStaffChangeStatus/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }
    

    function deleteStaffLeaveDetails(id)
    {
        $.ajax(
            {
               url: '/af/staff/deleteStaffLeaveDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }


    function validateDetailsData()
    {
        if($('#form_staff').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam=='')
            {
                alert("Add Course To The Faculty.");
            }
            else
            {
                $('#form_staff').submit();
            }
        }    
    }



    $(document).ready(function()
    {
        $("#form_staff").validate(
        {
            rules:
            {
                salutation:
                {
                    required: true
                },
                first_name:
                {
                    required: true
                },
                last_name:
                {
                    required: true
                },
                ic_no:
                {
                    required: true
                },
                gender:
                {
                    required: true
                },
                mobile_number:
                {
                    required: true
                },
                phone_number:
                {
                    required: true
                },
                address:
                {
                    required: true
                },
                address_two:
                {
                    required: true
                },
                id_country:
                {
                    required: true
                },
                id_state:
                {
                    required: true
                },
                zipcode:
                {
                    required: true
                },
                job_type:
                {
                    required: true
                },
                email:
                {
                    required: true
                },
                staff_id:
                {
                    required: true
                },
                dob:
                {
                    required: true
                },
                academic_type:
                {
                    required: true
                },
                id_faculty_program:
                {
                    required: true
                },
                id_education_level:
                {
                    required: true
                }
            },
            messages:
            {
                salutation:
                {
                    required: "<p class='error-text'>Select Salutation</p>",
                },
                first_name:
                {
                    required: "<p class='error-text'>First Name Required</p>",
                },
                last_name:
                {
                    required: "<p class='error-text'>Last Name Required</p>",
                },
                ic_no:
                {
                    required: "<p class='error-text'>Code Required</p>",
                },
                gender:
                {
                    required: "<p class='error-text'>Select Gender</p>",
                },
                mobile_number:
                {
                    required: "<p class='error-text'>Mobile Number Required</p>",
                },
                phone_number:
                {
                    required: "<p class='error-text'>Phone Number Required</p>",
                },
                address:
                {
                    required: "<p class='error-text'>Faculty Address Required</p>",
                },
                address_two:
                {
                    required: "<p class='error-text'>Address 2 Required</p>",
                },
                id_country:
                {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state:
                {
                    required: "<p class='error-text'>Select State</p>",
                },
                zipcode:
                {
                    required: "<p class='error-text'>Enter Zipcode</p>",
                },
                job_type:
                {
                    required: "<p class='error-text'>Select Job Type</p>",
                },
                email:
                {
                    required: "<p class='error-text'>Email Required</p>",
                },
                staff_id:
                {
                    required: "<p class='error-text'>Enter Faculty ID</p>",
                },
                dob:
                {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                academic_type:
                {
                    required: "<p class='error-text'>Select Academic Type</p>",
                },
                id_faculty_program:
                {
                    required: "<p class='error-text'>Select Faculty Program</p>",
                },
                id_education_level:
                {
                    required: "<p class='error-text'>Select Highest Education Level</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>

