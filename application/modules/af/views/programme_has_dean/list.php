<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Program Has Dean</h3>
      <a href="add" class="btn btn-primary">+ Add Program Has Dean</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6">
                  
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Program</label>
                      <div class="col-sm-8">
                        <select name="id_programme" id="id_programme" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                                    <option value="<?php echo $record->id; ?>"
                                        <?php 
                                        if($record->id == $searchParameters['id_programme'])
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">CO-Ordinator</label>
                      <div class="col-sm-8">
                        <select name="id_staff" id="id_staff" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList))
                            {
                                foreach ($staffList as $record)
                                {?>
                                    <option value="<?php echo $record->id; ?>"
                                        <?php 
                                        if($record->id == $searchParameters['id_staff'])
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Staff Name</th>
            <th>Programme</th>
            <th>Effective Start Date</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($programmeHasDeanList))
          {
            $i=1;
            foreach ($programmeHasDeanList as $record)
            {
             ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->salutation . ". " . $record->staff; ?></td>
                <td><?php echo $record->programme; ?></td>
                <td><?php echo date("d-m-Y", strtotime($record->effective_start_date)) ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <td class="text-center">

                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">
                    Edit
                  </a>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
      function clearSearchForm()
      {
        window.location.reload();
      }
</script>