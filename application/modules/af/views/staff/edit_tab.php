<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
            <h3>Edit Academic Facilitator </h3>
        </div>





            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation"
                    <?php
                    if($tab == 0 || $tab == 1)
                    {
                        echo "class='active'"; 
                    }
                    ?>
                     ><a href="#tab_one" class="nav-link border rounded text-center"
                            aria-controls="tab_one" 
                    <?php
                    if($tab == 0 || $tab == 1)
                    {
                        echo "aria-selected='true'"; 
                    }
                    ?>
                    
                            role="tab" data-toggle="tab">Faculty Details</a>
                    </li>

                    <li role="presentation"
                    <?php
                    if($tab == 2)
                    {
                        echo "class='active'"; 
                    }
                    ?>
                    ><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two"

                    <?php
                    if($tab == 2)
                    {
                        echo "aria-selected='true'";
                    }
                    ?>
                     role="tab" data-toggle="tab">Teaching Details:</a>
                    </li>

                    <li role="presentation"
                    <?php
                    if($tab == 3)
                    {
                        echo "class='active'"; 
                    }
                    ?>
                    ><a href="#tab_three" class="nav-link border rounded text-center"
                            aria-controls="tab_three"
                    <?php
                    if($tab == 3)
                    {
                        echo "aria-selected='true'";
                    }
                    ?>
                     role="tab" data-toggle="tab">Records</a>
                    </li>

                    <li role="presentation"
                    <?php
                    if($tab == 4)
                    {
                        echo "class='active'"; 
                    }
                    ?>
                    ><a href="#tab_four" class="nav-link border rounded text-center"
                            aria-controls="tab_four" 
                    <?php
                    if($tab == 4)
                    {
                        echo "aria-selected='true'";
                    }
                    ?>
                    role="tab" data-toggle="tab">Statement of account</a>
                    </li>

                    <li role="presentation"
                    <?php
                    if($tab == 5)
                    {
                        echo "class='active'"; 
                    }
                    ?>
                    ><a href="#tab_five" class="nav-link border rounded text-center"
                            aria-controls="tab_five"
                    <?php
                    if($tab == 5)
                    {
                        echo "aria-selected='true'";
                    }
                    ?>
                     role="tab" data-toggle="tab">Change status menu</a>
                    </li>
                    
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane
                        <?php
                        if($tab == 0 || $tab == 1)
                        {
                            echo "active"; 
                        }
                        ?>" id="tab_one">
                        <div class="col-12 mt-4">




                    

                            <form id="form_staff" action="" method="post">

                            <div class="form-container">
                                <h4 class="form-group-title">Faculty Details</h4>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Salutation <span class='error-text'>*</span></label>
                                             <select name="salutation" id="salutation" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($salutationList)) {
                                                    foreach ($salutationList as $record) {
                                                ?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php if($staffDetails->salutation==$record->id)
                                                            {
                                                                echo "selected=selected";
                                                            }
                                                            ?>
                                                            >
                                                            <?php echo $record->name;  ?>        
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>First Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $staffDetails->first_name;?>">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Last Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $staffDetails->last_name;?>">
                                        </div>
                                    </div>

                                      
                                    
                                </div>


                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>IC No. <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="ic_no" name="ic_no" value="<?php echo $staffDetails->ic_no;?>">
                                        </div>
                                    </div>       

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Gender <span class='error-text'>*</span></label>
                                             <select name="gender" id="gender" class="form-control">
                                                <option value="Male" <?php if($staffDetails->gender=='Male')
                                                {
                                                    echo "selected=selected";
                                                }
                                                ?>
                                                >Male</option>
                                                <option value="Female" <?php if($staffDetails->gender=='Female')
                                                {
                                                    echo "selected=selected";
                                                }?>
                                                >Female</option>
                                            </select>
                                        </div>
                                    </div>

                                     <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mobile  Number <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="mobile_number" name="mobile_number" value="<?php echo $staffDetails->mobile_number;?>">
                                        </div>
                                    </div> 
                                   
                                
                                </div>

                                <div class="row"> 

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Email <span class='error-text'>*</span></label>
                                            <input type="email" class="form-control" id="email" name="email" value="<?php echo $staffDetails->email;?>">
                                        </div>
                                    </div>    

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Faculty ID <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="staff_id" name="staff_id" value="<?php echo $staffDetails->staff_id;?>">
                                        </div>
                                    </div> 

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Faculty Type <span class='error-text'>*</span></label>
                                             <select name="job_type" id="job_type" class="form-control">
                                                <option value="">Select</option>
                                                <option value="0" <?php if($staffDetails->job_type=='0') { echo "selected=selected";}?>>Part Time</option>
                                                <option value="1" <?php if($staffDetails->job_type=='1') { echo "selected=selected";}?>>Full TIme</option>
                                            </select>
                                        </div>
                                    </div>  

                                    

                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Department <span class='error-text'>*</span></label>
                                            <select name="id_department" id="id_department" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($departmentList))
                                                {
                                                    foreach ($departmentList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_department)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->code . " - " . $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>DOB <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="dob" name="dob" value="<?php echo date('d-m-Y',strtotime($staffDetails->dob));?>">
                                        </div>
                                    </div>
                                    
                                     <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Academic Type <span class='error-text'>*</span></label>

                                             <select name="academic_type" id="academic_type" class="form-control">
                                                <option value="1" <?php if($staffDetails->academic_type=='1') { echo "selected=selected";}?>>Academic Faculty</option>
                                                <option value="0" <?php if($staffDetails->academic_type=='0') { echo "selected=selected";}?>>Non-Academic Faculty</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Faculty Program <span class='error-text'>*</span></label>
                                            <select name="id_faculty_program" id="id_faculty_program" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($facultyProgramList))
                                                {
                                                    foreach ($facultyProgramList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_faculty_program)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->code . " - " . $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Highest Education Level <span class='error-text'>*</span></label>
                                            <select name="id_education_level" id="id_education_level" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($qualificationList))
                                                {
                                                    foreach ($qualificationList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_education_level)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                            <div class="form-group">
                                                <p>Status <span class='error-text'>*</span></p>
                                                <label class="radio-inline">
                                                  <input type="radio" name="status" id="status" value="1" <?php if($staffDetails->status=='1') {
                                                     echo "checked=checked";
                                                  };?>><span class="check-radio"></span> Active
                                                </label>
                                                <label class="radio-inline">
                                                  <input type="radio" name="status" id="status" value="0" <?php if($staffDetails->status=='0') {
                                                     echo "checked=checked";
                                                  };?>>
                                                  <span class="check-radio"></span> In-Active
                                                </label>                              
                                            </div>                         
                                    </div>
                                    
                                </div>


                            </div>

                            <div class="form-container">
                                <h4 class="form-group-title">Contact Details</h4>

                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Phone  Number <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="phone_number" name="phone_number" value="<?php echo $staffDetails->phone_number;?>">
                                        </div>
                                    </div>     

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Address <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="address" name="address" value="<?php echo $staffDetails->address;?>">
                                        </div>
                                    </div>    

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Address 2 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="address_two" name="address_two" value="<?php echo $staffDetails->address_two;?>">
                                        </div>
                                    </div> 

                                    
                                </div>  

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Country <span class='error-text'>*</span></label>
                                            <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($countryList))
                                                {
                                                    foreach ($countryList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_country)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select State <span class='error-text'>*</span></label>
                                            <select name="id_state" id="id_state" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($stateList))
                                                {
                                                    foreach ($stateList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_state)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div> -->
                                     <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Select State <span class='error-text'>*</span></label>
                                                <span id='view_state'>
                                                    <select name='id_state' id='id_state' class='form-control'>
                                                        <option value=''></option>
                                                    </select>
                                                </span>
                                            </div>
                                        </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Zipcode <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $staffDetails->zipcode;?>">
                                        </div>
                                    </div>

                                      
                                </div>


                                

                            </div>



                            <div class="button-block clearfix">
                                    <div class="bttn-group">
                                        <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="1">Save</button>
                                        <a href="../list" class="btn btn-link">Cancel</a>
                                    </div>
                            </div>


                            


                            <div class="form-container">
                                <h4 class="form-group-title">Assign Courses to Faculty</h4> 


                                <div class="row">
                                     <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Course <span class='error-text'>*</span></label>
                                                <select name="id_course" id="id_course" class="form-control">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($courseList))
                                                    {
                                                        foreach ($courseList as $record)
                                                        {?>
                                                            <option value="<?php echo $record->id;?>"
                                                            ><?php echo $record->code . " - " . $record->name;?>
                                                            </option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                      
                                        <div class="col-sm-4">
                                            <button type="button" class="btn-lg btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                                        </div>
                                </div>




                                <?php

                                if(!empty($getStaffCourse))
                                {
                                    ?>

                                    <div class="form-container">
                                            <h4 class="form-group-title">Semester Info. Details</h4>

                                        

                                          <div class="custom-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                    <th>Sl. No</th>
                                                     <th>Course</th>
                                                     <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                     <?php
                                                 $total = 0;
                                                  for($i=0;$i<count($getStaffCourse);$i++)
                                                 { ?>
                                                    <tr>
                                                    <td><?php echo $i+1;?></td>
                                                    <td><?php echo $getStaffCourse[$i]->coursename;?></td>
                                                    <td class="text-center">
                                                        <a onclick="deleteCourseDetailData(<?php echo $getStaffCourse[$i]->id; ?>)">Delete</a>
                                                    </td>

                                                     </tr>
                                                  <?php 
                                              } 
                                              ?>
                                                </tbody>
                                            </table>
                                          </div>

                                        </div>

                                <?php
                                
                                }
                                 ?>





                                <!-- <div class="row">
                                    <div id="view"></div>
                                </div> -->
                                
                            </div>



                            </form>

        
        



        

                        



                        </div> 
                    
                    </div>




                    <div role="tabpanel" class="tab-pane 
                        <?php
                        if($tab == 2)
                        {
                            echo "active"; 
                        }
                        ?>" id="tab_two">
                        <div class="mt-4">


                        <form id="form_scheme" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Teaching Details</h4>

                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Semester <span class='error-text'>*</span></label>
                                            <select name="id_teaching_semester" id="id_teaching_semester" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($semesterList))
                                                {
                                                    foreach ($semesterList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code ." - ".$record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Programme <span class='error-text'>*</span></label>
                                            <select name="id_teaching_programme" id="id_teaching_programme" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($semesterList))
                                                {
                                                    foreach ($semesterList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code ." - ".$record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Course <span class='error-text'>*</span></label>
                                            <select name="id_teaching_course" id="id_teaching_course" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($semesterList))
                                                {
                                                    foreach ($semesterList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code ." - ".$record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>


                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Mode Of Study <span class='error-text'>*</span></label>
                                            <select name="id_teaching_mode_of_study" id="id_teaching_mode_of_study" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($semesterList))
                                                {
                                                    foreach ($semesterList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code ." - ".$record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Learning centre <span class='error-text'>*</span></label>
                                            <select name="id_teaching_mode_of_study" id="id_teaching_mode_of_study" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($semesterList))
                                                {
                                                    foreach ($semesterList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code ." - ".$record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                        <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="2">Save</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>

                           <!--  <div class="row">
                                <div id="view_scheme"></div>
                            </div> -->


                            </form>



                            <?php

                            if(!empty($programmeLearningModeList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Scheme Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Mode Of Program</th>
                                                 <th>Mode Of Study</th>
                                                 <th>Program Type</th>
                                                 <th>Min Duration( Month )</th>
                                                 <th>Max Duration( Month )</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($programmeLearningModeList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $programmeLearningModeList[$i]->mode_of_program;?></td>
                                                <td><?php echo $programmeLearningModeList[$i]->mode_of_study;?></td>
                                                <td><?php echo $programmeLearningModeList[$i]->program_code . " - " . $programmeLearningModeList[$i]->program_name;?></td>
                                                <td><?php echo $programmeLearningModeList[$i]->min_duration;?></td>
                                                <td><?php echo $programmeLearningModeList[$i]->max_duration;?></td>
                                                <td>
                                                <a onclick="deleteSchemeDetails(<?php echo $programmeLearningModeList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div>
                    
                    </div>








                    <div role="tabpanel" class="tab-pane" id="tab_three
                        <?php
                        if($tab == 3)
                        {
                            echo "active"; 
                        }
                        ?>
                        ">
                        <div class="mt-4">


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                        <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="3">Save</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>



                            <?php

                            if(!empty($programmeSchemeList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Scheme Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Scheme</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($programmeSchemeList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $programmeSchemeList[$i]->scheme_code . " - " . $programmeSchemeList[$i]->scheme_name;?></td>
                                                <td>
                                                <a onclick="deleteProgramSchemeDetails(<?php echo $programmeSchemeList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div>
                    
                    </div>






                    <div role="tabpanel" class="tab-pane" id="tab_four
                        <?php
                        if($tab == 4)
                        {
                            echo "active"; 
                        }
                        ?>
                        ">
                        <div class="mt-4">


                        <form id="form_majoring" action="" method="post">


                            <br>

                                <div class="form-container">
                                <h4 class="form-group-title"> Coming Soon</h4>


                                <div class="button-block clearfix">
                                    <div class="bttn-group">
                                            <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="4">Save</button>
                                        <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                    </div>
                                </div>



                            </div>







                            <?php

                            if(!empty($programmeMajoringList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Majoring Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Code</th>
                                                 <th>Description</th>
                                                 <th>Description In Other Language</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($programmeMajoringList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $programmeMajoringList[$i]->code;?></td>
                                                <td><?php echo $programmeMajoringList[$i]->name;?></td>
                                                <td><?php echo $programmeMajoringList[$i]->name_in_optional_language;?></td>
                                                <td>
                                                <a onclick="deleteMajorDetails(<?php echo $programmeMajoringList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>




                        </div>
                    
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_five
                        <?php
                        if($tab == 5)
                        {
                            echo "active"; 
                        }
                        ?>
                        ">
                        <div class="mt-4">


                        <form id="form_minoring" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title">Change Status Details</h4>

                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Status <span class='error-text'>*</span></label>
                                             <select name="salutation" id="salutation" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($changeStatusList)) {
                                                    foreach ($changeStatusList as $record) {
                                                ?>
                                                        <option value="<?php echo $record->id;  ?>" >
                                                            <?php echo $record->name;  ?>        
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>



                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                        <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="5">Save</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>

                              


                            </form>



                            <?php

                            if(!empty($programmeMinoringList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Program Minoring Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Code</th>
                                                 <th>Description</th>
                                                 <th>Description In Other Language</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($programmeMinoringList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $programmeMinoringList[$i]->code;?></td>
                                                <td><?php echo $programmeMinoringList[$i]->name;?></td>
                                                <td><?php echo $programmeMinoringList[$i]->name_in_optional_language;?></td>
                                                <td>
                                                <a onclick="deleteMinorDetails(<?php echo $programmeMinoringList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>





                        </div>
                    
                    </div>







                </div>


            </div>












        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>

<script>
  $( function() {
    showPartner();
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>

<script>
    function showPartner(){
        var value = $("#internal_external").val();
        if(value=='Internal') {
             $("#partnerdropdown").hide();

        } else if(value=='External') {
             $("#partnerdropdown").show();

        }
    }
    function saveData()
    {
        if($('#form_programme_dean').valid())
        {
        

        var tempPR = {};
        tempPR['id_staff'] = $("#id_staff").val();
        tempPR['effective_start_date'] = $("#effective_start_date").val();
        tempPR['id'] = <?php echo $id_programme;?>;
            $.ajax(
            {
               url: '/setup/programme/directadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                location.reload();
                // windows().location.reload();

               }
            });
        }
    }

    function saveSchemeData()
    {
        if($('#form_scheme').valid())
        {

        var tempPR = {};
        tempPR['id_program_type'] = $("#id_program_type").val();
        tempPR['mode_of_program'] = $("#mode_of_program").val();
        tempPR['mode_of_study'] = $("#mode_of_study").val();
        tempPR['min_duration'] = $("#min_duration").val();
        tempPR['max_duration'] = $("#max_duration").val();
        tempPR['id_program'] = <?php echo $id_programme;?>;
            $.ajax(
            {
               url: '/setup/programme/directSchemeAdd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
                // $("#view_scheme").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }

    function saveProgramSchemeData()
    {
        if($('#form_program_scheme').valid())
        {

        var tempPR = {};
        tempPR['id_scheme'] = $("#id_scheme").val();
        tempPR['id_program'] = <?php echo $id_programme;?>;
            $.ajax(
            {
               url: '/setup/programme/saveProgramSchemeData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
                // $("#view_scheme").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }


    function saveMajorData()
    {
        if($('#form_majoring').valid())
        {

        var tempPR = {};
        tempPR['code'] = $("#major_code").val();
        tempPR['name'] = $("#major_description").val();
        tempPR['name_in_optional_language'] = $("#major_description_other_language").val();
        tempPR['id_program'] = <?php echo $id_programme;?>;
            $.ajax(
            {
               url: '/setup/programme/saveMajorData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
                // $("#view_scheme").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }

    function saveMinorData()
    {
        if($('#form_minoring').valid())
        {

        var tempPR = {};
        tempPR['code'] = $("#minor_code").val();
        tempPR['name'] = $("#minor_description").val();
        tempPR['name_in_optional_language'] = $("#minor_description_other_language").val();
        tempPR['id_program'] = <?php echo $id_programme;?>;
            $.ajax(
            {
               url: '/setup/programme/saveMinorData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
                // $("#view_scheme").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }

    function saveConcurrentData()
    {
        if($('#form_concurrent').valid())
        {

        var tempPR = {};
        tempPR['id_concurrent_program'] = $("#id_concurrent_program").val();
        tempPR['id_program'] = <?php echo $id_programme;?>;
            $.ajax(
            {
               url: '/setup/programme/saveConcurrentData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
                // $("#view_scheme").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }

    function saveAcceredationData()
    {
        if($('#form_acceredation').valid())
        {

        var tempPR = {};
        tempPR['category'] = $("#acceredation_category").val();
        tempPR['type'] = $("#acceredation_type").val();
        tempPR['acceredation_dt'] = $("#acceredation_dt").val();
        tempPR['acceredation_number'] = $("#acceredation_number").val();
        tempPR['valid_from'] = $("#valid_from").val();
        tempPR['valid_to'] = $("#valid_to").val();
        tempPR['approval_dt'] = $("#approval_date").val();
        tempPR['acceredation_reference'] = $("#acceredation_reference").val();
        tempPR['id_program'] = <?php echo $id_programme;?>;
            $.ajax(
            {
               url: '/setup/programme/saveAcceredationData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
                // $("#view_scheme").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }

    function saveProgramObjectiveData()
    {
        if($('#form_program_objective').valid())
        {

        var tempPR = {};
        tempPR['name'] = $("#objective_name").val();
        tempPR['id_programme'] = <?php echo $id_programme;?>;
            $.ajax(
            {
               url: '/setup/programme/saveProgramObjectiveData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
               }
            });
        }
    }






    function deleteDeanDetails(id) {
         $.ajax(
            {
               url: '/setup/programme/deleteStaffDetailData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    window.location.reload();
               }
            });
    }

    function deleteSchemeDetails(id)
    {
        $.ajax(
            {
               url: '/setup/programme/deleteProgrammeHasScheme/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }

    function deleteProgramSchemeDetails(id)
    {
        $.ajax(
            {
               url: '/setup/programme/deleteProgramSchemeDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }



    function deleteMajorDetails(id)
    {
        $.ajax(
            {
               url: '/setup/programme/deleteMajorDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }


    function deleteMinorDetails(id)
    {
        $.ajax(
            {
               url: '/setup/programme/deleteMinorDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }


    function deleteCuncurrentDetails(id)
    {
        $.ajax(
            {
               url: '/setup/programme/deleteCuncurrentDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }


    function deleteAcceredationDetails(id)
    {
        $.ajax(
            {
               url: '/setup/programme/deleteAcceredationDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }


    function deleteProgramObjectiveDetails(id)
    {
        $.ajax(
            {
               url: '/setup/programme/deleteProgramObjectiveDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    location.reload();
               }
            });
    }








    function validateDetailsData()
    {
        if($('#form_programme').valid())
        {
            
            $('#form_programme').submit();
        }    
    }





     $(document).ready(function()
     {
        $("#form_programme").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                graduate_studies: {
                    required: true
                },
                foundation: {
                    required: true
                },
                total_cr_hrs: {
                    required: true
                },
                id_award: {
                    required: true
                },
                status: {
                    required: true
                },
                id_scheme: {
                    required: true
                },
                id_education_level: {
                    required: true
                },
                mode: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Program  Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Program  Code Required</p>",
                },
                graduate_studies: {
                    required: "<p class='error-text'>Graduate Studies Required</p>",
                },
                foundation: {
                    required: "<p class='error-text'>Foundation Required</p>",
                },
                total_cr_hrs: {
                    required: "<p class='error-text'>Total Credit Hours required</p>",
                },
                id_award: {
                    required: "<p class='error-text'>Select Award</p>",
                },
                status: {
                    required: "<p class='error-text'>Select status</p>",
                },
                id_scheme: {
                    required: "<p class='error-text'>Select Scheme</p>",
                },
                id_education_level: {
                    required: "<p class='error-text'>Select Education Level</p>",
                },
                mode: {
                    required: "<p class='error-text'>Select Mode</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


     


    $(document).ready(function()
    {
        $("#form_program_scheme").validate({
            rules: {
                id_scheme: {
                    required: true
                }
            },
            messages: {
                id_scheme: {
                    required: "<p class='error-text'>Select Scheme</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    $(document).ready(function()
    {
        $("#form_programme_dean").validate({
            rules: {
                id_staff: {
                    required: true
                },
                effective_start_date: {
                    required: true
                }
            },
            messages: {
                id_staff: {
                    required: "<p class='error-text'>Select Staff</p>",
                },
                effective_start_date: {
                    required: "<p class='error-text'>Select Effective Start Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    $(document).ready(function()
     {
        $("#form_scheme").validate({
            rules: {
                id_program_type: {
                    required: true
                },
                mode_of_program: {
                    required: true
                },
                mode_of_study: {
                    required: true
                },
                min_duration: {
                    required: true
                },
                max_duration: {
                    required: true
                }
            },
            messages: {
                id_program_type: {
                    required: "<p class='error-text'>Select Program Type</p>",
                },
                mode_of_program: {
                    required: "<p class='error-text'>Select Mode Of Program</p>",
                },
                mode_of_study: {
                    required: "<p class='error-text'>Select Mode Of Study</p>",
                },
                min_duration: {
                    required: "<p class='error-text'>Min. Duration Required</p>",
                },
                max_duration: {
                    required: "<p class='error-text'>Max. Duration Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function()
     {
        $("#form_majoring").validate({
            rules: {
                major_code: {
                    required: true
                },
                major_description: {
                    required: true
                }
            },
            messages: {
                major_code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                major_description: {
                    required: "<p class='error-text'>Description Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function()
     {
        $("#form_minoring").validate({
            rules: {
                minor_code: {
                    required: true
                },
                minor_description: {
                    required: true
                }
            },
            messages: {
                minor_code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                minor_description: {
                    required: "<p class='error-text'>Description Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function()
     {
        $("#form_acceredation").validate({
            rules: {
                acceredation_category: {
                    required: true
                },
                acceredation_type: {
                    required: true
                },
                acceredation_dt: {
                    required: true
                },
                acceredation_number: {
                    required: true
                },
                valid_from: {
                    required: true
                },
                valid_to: {
                    required: true
                },
                approval_date: {
                    required: true
                },
                acceredation_reference: {
                    required: true
                }
            },
            messages: {
                acceredation_category: {
                    required: "<p class='error-text'>Select Category</p>",
                },
                acceredation_type: {
                    required: "<p class='error-text'>Select Acceredation Type</p>",
                },
                acceredation_dt: {
                    required: "<p class='error-text'>Select Acceredation Date</p>",
                },
                acceredation_number: {
                    required: "<p class='error-text'>Acceredation No. Required</p>",
                },
                valid_from: {
                    required: "<p class='error-text'>Select Validity Start Date</p>",
                },
                valid_to: {
                    required: "<p class='error-text'>Select Validity End Date</p>",
                },
                approval_date: {
                    required: "<p class='error-text'>Select Approval Date</p>",
                },
                acceredation_reference: {
                    required: "<p class='error-text'>Acceredation Reference Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function()
    {
        $("#form_program_objective").validate({
            rules: {
                objective_name: {
                    required: true
                }
            },
            messages: {
                objective_name: {
                    required: "<p class='error-text'>Objective Name Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>


<script>
  $( function() {
    $( ".datepicker" ).datepicker();
  } );
</script>

<script>

$( document ).ready(function()
{
    $.get("/af/staff/getStateByCountry/"+<?php echo $staffDetails->id_country;?>, function(data, status)
    {
        var idstateselected = "<?php echo $staffDetails->id_state;?>";

        $("#view_state").html(data);
        $("#id_state").find('option[value="'+idstateselected+'"]').attr('selected',true);
        $('select').select2();
    });
});


    function getStateByCountry(id)
    {
     $.get("/af/staff/getStateByCountry/"+id, function(data, status)
     {
        $("#view_state").html(data);
        // $("#view_programme_details").html(data);
        // $("#view_programme_details").show();
     });
    }



    function saveData() {


        var tempPR = {};
        tempPR['id_course'] = $("#id_course").val();
        tempPR['id'] = <?php echo $id_staff;?>;
            $.ajax(
            {
               url: '/af/staff/directadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
                    // $("#view").html(result);
               }
            });
        
    }

    function deleteCourseDetailData(id) {
         $.ajax(
            {
               url: '/af/staff/deleteCourseDetailData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    window.location.reload();
               }
            });
    }


    function validateDetailsData()
    {
        if($('#form_staff').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam=='')
            {
                alert("Add Course To The Faculty.");
            }
            else
            {
                $('#form_staff').submit();
            }
        }    
    }



    $(document).ready(function()
    {
        $("#form_staff").validate(
        {
            rules:
            {
                salutation:
                {
                    required: true
                },
                first_name:
                {
                    required: true
                },
                last_name:
                {
                    required: true
                },
                ic_no:
                {
                    required: true
                },
                gender:
                {
                    required: true
                },
                mobile_number:
                {
                    required: true
                },
                phone_number:
                {
                    required: true
                },
                address:
                {
                    required: true
                },
                address_two:
                {
                    required: true
                },
                id_country:
                {
                    required: true
                },
                id_state:
                {
                    required: true
                },
                zipcode:
                {
                    required: true
                },
                job_type:
                {
                    required: true
                },
                email:
                {
                    required: true
                },
                staff_id:
                {
                    required: true
                },
                dob:
                {
                    required: true
                },
                academic_type:
                {
                    required: true
                },
                id_faculty_program:
                {
                    required: true
                },
                id_education_level:
                {
                    required: true
                }
            },
            messages:
            {
                salutation:
                {
                    required: "<p class='error-text'>Select Salutation</p>",
                },
                first_name:
                {
                    required: "<p class='error-text'>First Name Required</p>",
                },
                last_name:
                {
                    required: "<p class='error-text'>Last Name Required</p>",
                },
                ic_no:
                {
                    required: "<p class='error-text'>Code Required</p>",
                },
                gender:
                {
                    required: "<p class='error-text'>Select Gender</p>",
                },
                mobile_number:
                {
                    required: "<p class='error-text'>Mobile Number Required</p>",
                },
                phone_number:
                {
                    required: "<p class='error-text'>Phone Number Required</p>",
                },
                address:
                {
                    required: "<p class='error-text'>Faculty Address Required</p>",
                },
                address_two:
                {
                    required: "<p class='error-text'>Address 2 Required</p>",
                },
                id_country:
                {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state:
                {
                    required: "<p class='error-text'>Select State</p>",
                },
                zipcode:
                {
                    required: "<p class='error-text'>Enter Zipcode</p>",
                },
                job_type:
                {
                    required: "<p class='error-text'>Select Job Type</p>",
                },
                email:
                {
                    required: "<p class='error-text'>Email Required</p>",
                },
                staff_id:
                {
                    required: "<p class='error-text'>Enter Faculty ID</p>",
                },
                dob:
                {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                academic_type:
                {
                    required: "<p class='error-text'>Select Academic Type</p>",
                },
                id_faculty_program:
                {
                    required: "<p class='error-text'>Select Faculty Program</p>",
                },
                id_education_level:
                {
                    required: "<p class='error-text'>Select Highest Education Level</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>

