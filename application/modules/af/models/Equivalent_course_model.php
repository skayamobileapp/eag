<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Equivalent_course_model extends CI_Model
{
        
    function equivalentCourseList()
    {
        $this->db->select('ec.*, c.name as course, cc.name as equivalent_course');
        $this->db->from('equivalent_course as ec');
        $this->db->join('course as c', 'ec.id_course = c.id');
        $this->db->join('course as cc', 'ec.id_equivalent_course = cc.id');
        $this->db->order_by("ec.id", "desc");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function equivalentCourseListSearch($formData)
    {
        $this->db->select('ec.*, c.name as course, cc.name as equivalent_course');
        $this->db->from('equivalent_course as ec');
        $this->db->join('course as c', 'ec.id_course = c.id');
        $this->db->join('course as cc', 'ec.id_equivalent_course = cc.id');
        if($formData['id_course']) {
            $likeCriteria = "(ec.id_course  LIKE '%" . $formData['id_course'] . "%')";
            $this->db->where($likeCriteria);
        }

        if($formData['id_equivalent_course']) {
            $likeCriteria = "(ec.id_equivalent_course  LIKE '%" . $formData['id_equivalent_course'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getCourseSearch($search)
    {
        $this->db->select('c.*');
        $this->db->from('course as c');
        if($search) {
            $likeCriteria = "(c.name  LIKE '%" . $search . "%' or c.code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("c.name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getHourDetails($id){
        $this->db->select('c.credit_hours');
        $this->db->from('course as c');
         $this->db->where('c.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getCourseByIdForEquivalent($id)
    {
        $this->db->select('c.*');
        $this->db->from('course as c');
         $this->db->where('c.id', $id);
        $query = $this->db->get();
        // echo "<pre>";print_r($query);die;

        return $query->row();
    }

     function getEquivalentCourse($id)
    {
        $this->db->select('ec.*, c.name as course, cc.name as equivalent_course');
        $this->db->from('equivalent_course as ec');
        $this->db->join('course as c', 'ec.id_course = c.id');
        $this->db->join('course as cc', 'ec.id_equivalent_course = cc.id');
        $this->db->where('ec.id', $id);
        $query = $this->db->get();
        // echo "<pre>";print_r($query);die;

        return $query->row();
    }

     function getEquivalentCourseDetails($id)
    {
        $this->db->select('ecd.*, c.name as course_name, c.code as course_code, c.credit_hours');
        $this->db->from('equivalent_course_details as ecd');
        $this->db->join('equivalent_course as ec', 'ecd.id_equivalent_course = ec.id');
        $this->db->join('course as c', 'ecd.id_course = c.id');
        $this->db->where('ec.id', $id);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;

        return $result;
    }

    function addNewEquivalentCourse($data)
    {
        $this->db->trans_start();
        $this->db->insert('equivalent_course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editEquivalentCourse($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('equivalent_course', $data);

        return TRUE;
    }

    function deleteEquivalentCourse($id, $equivalent_courseInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('equivalent_course', $equivalent_courseInfo);

        return $this->db->affected_rows();
    }

    function addNewTempEquivalentCourseDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_equivalent_course_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewEquivalentCourseDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('equivalent_course_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempEquivalentCourseDetails($id_session)
    {
        $this->db->select('tecd.*, c.name as course_name');
        $this->db->from('temp_equivalent_course_details as tecd');
        $this->db->join('course as c', 'tecd.id_course = c.id');        
        $this->db->where('tecd.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempData($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_equivalent_course_details');
    }

    function getCourseByIdForCheckup($id)
    {
        $this->db->select('ec.*');
        $this->db->from('equivalent_course as ec');
         $this->db->where('ec.id_course', $id);
        $query = $this->db->get();
        // echo "<pre>";print_r($query);die;

        return $query->row();
    }

    function deleteEquivalentCourseDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('equivalent_course_details');
    }
}
