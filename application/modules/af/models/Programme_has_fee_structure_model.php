<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Programme_has_fee_structure_model extends CI_Model
{
    function programmeHasFeeStructureList()
    {
        $this->db->select('phfs.*, p.name as programme, i.name as intake, s.name as semester');
        $this->db->from('programme_has_fee_structure as phfs');
        $this->db->join('programme as p', 'phfs.id_programme = p.id');
        $this->db->join('intake as i', 'phfs.id_intake = i.id');
        $this->db->join('semester as s', 'phfs.id_semester = s.id');
        $this->db->order_by("p.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // echo "<pre>";print_r($result);exit;
         return $result;
    }

    function programmeHasFeeStructureListSearch($formData)
    {
        $this->db->select('phfs.*, p.name as programme, i.name as intake, s.name as semester');
        $this->db->from('programme_has_fee_structure as phfs');
        $this->db->join('programme as p', 'phfs.id_programme = p.id');
        $this->db->join('intake as i', 'phfs.id_intake = i.id');
        $this->db->join('semester as s', 'phfs.id_semester = s.id');
        if($formData['scheme']) {
            $likeCriteria = "(phfs.scheme  LIKE '%" . $formData['scheme'] . "%')";
            $this->db->where($likeCriteria);
        }

        if($formData['code']) {
            $likeCriteria = "(phfs.code  LIKE '%" . $formData['code'] . "%')";
            $this->db->where($likeCriteria);
        }

        if($formData['id_programme']) {
            $likeCriteria = "(phfs.id_programme  LIKE '%" . $formData['id_programme'] . "%')";
            $this->db->where($likeCriteria);
        }

         if($formData['id_intake']) {
            $likeCriteria = "(phfs.id_intake  LIKE '%" . $formData['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }

        if($formData['id_semester']) {
            $likeCriteria = "(phfs.id_semester  LIKE '%" . $formData['id_semester'] . "%')";
            $this->db->where($likeCriteria);
        }

        $this->db->order_by("p.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // echo "<pre>";print_r($result);exit;
         return $result;
    }

    function getprogrammeHasFeeStructureDetails($id)
    {
        $this->db->select('phfs.*, p.name as programme, i.name as intake, s.name as semester');
        $this->db->from('programme_has_fee_structure as phfs');
        $this->db->join('programme as p', 'phfs.id_programme = p.id');
        $this->db->join('intake as i', 'phfs.id_intake = i.id');
        $this->db->join('semester as s', 'phfs.id_semester = s.id');
        $this->db->where('phfs.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProgrammeHasFeeStructure($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_fee_structure', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editprogrammeHasFeeStructure($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('programme_has_fee_structure', $data);
        return TRUE;
    }
}

