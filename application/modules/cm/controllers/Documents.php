<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Documents extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('documents_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('documents.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParameters'] = $formData;
            $data['documentsList'] = $this->documents_model->documentsListSearch($formData);
            $this->global['pageTitle'] = 'Campus Management System : Documents List';
            $this->loadViews("documents/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('documents.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->documents_model->addNewDocuments($data);
                redirect('/setup/documents/list');
            }
            $this->global['pageTitle'] = 'Campus Management System : Add Documents';
            $this->loadViews("documents/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('documents.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/documents/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->documents_model->editDocuments($data,$id);
                redirect('/setup/documents/list');
            }
            $data['documentDetails'] = $this->documents_model->getDocuments($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Documents';
            $this->loadViews("documents/edit", $this->global, $data, NULL);
        }
    }



    function addProgram($id_document)
    {
        if ($this->checkAccess('documents.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id_document == null)
            {
                redirect('/setup/documents/list');
            }
            if($this->input->post())
            {
                redirect('/setup/documents/list');
            }
            $data['documentDetails'] = $this->documents_model->getDocuments($id_document);
            $data['documentsProgramList'] = $this->documents_model->getDocumentsProgramListByDocumentId($id_document);
            $data['programList'] = $this->documents_model->programListByStatus('1');


            // echo "<Pre>";print_r($data);exit();

            $this->global['pageTitle'] = 'Campus Management System : Edit Documents';
            $this->loadViews("documents/add_program", $this->global, $data, NULL);
        }
    }

    function addProgramToDocument()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->documents_model->addNewDocumentsProgramDetails($tempData);
        echo $inserted_id;
    }

    function deleteDocumentsProgram($id)
    {
        $inserted_id = $this->documents_model->deleteDocumentsProgramDetails($id);
        echo $inserted_id;
    }
}
