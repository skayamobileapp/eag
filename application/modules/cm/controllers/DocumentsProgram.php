<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class DocumentsProgram extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('documents_program_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('documents_program.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_document'] = $this->security->xss_clean($this->input->post('id_document'));

            $data['searchParameters'] = $formData;
            $data['documentsProgramList'] = $this->documents_program_model->documentsProgramListSearch($formData);
            $data['documentsList'] = $this->documents_program_model->documentsListByStatus('1');
        // echo "<Pre>";print_r($data['documentsProgramList']);exit;

            $this->global['pageTitle'] = 'Campus Management System : List Documents For Program';
            $this->loadViews("documents_program/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('documents_program.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;
            
            if($this->input->post())
            {
                

                $description = $this->security->xss_clean($this->input->post('description'));
                $id_document = $this->security->xss_clean($this->input->post('id_document'));
                // $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'description' => $description,
                    'id_document' => $id_document,
                    'status' => 1,
                    'created_by' => $id_user
                );

                $result = $this->documents_program_model->addNewDocumentsProgram($data);
                if($result)
                {
                    $result = $this->documents_program_model->moveTempToDetails($result); 
                }
                redirect('/setup/documentsProgram/list');
            }
            else
            {
                $temp_deleted = $this->documents_program_model->deleteTempDataBySession($id_session);
            }
            $data['programmeList'] = $this->documents_program_model->programListByStatus('1');
            $data['documentsList'] = $this->documents_program_model->documentsListByStatus('1');
            
            $this->global['pageTitle'] = 'Campus Management System : Add Documents For Program';
            $this->loadViews("documents_program/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('documents_program.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/documentsProgram/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

               
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_document = $this->security->xss_clean($this->input->post('id_document'));
                // $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'description' => $description,
                    'id_document' => $id_document,
                    'updated_by' => $id_user
                );

                $result = $this->documents_program_model->editDocumentsProgram($data,$id);
                redirect('/setup/documentsProgram/list');
            }
            $data['programList'] = $this->documents_program_model->programListByStatus('1');
            $data['documentsList'] = $this->documents_program_model->documentsListByStatus('1');
            $data['documentsProgram'] = $this->documents_program_model->getDocumentsProgram($id);
            $data['idDocumentsProgram'] = $id;

            $this->global['pageTitle'] = 'Campus Management System : Edit Documents For Program';
            $this->loadViews("documents_program/edit", $this->global, $data, NULL);
        }
    }

    function tempadd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
       
        $inserted_id = $this->documents_program_model->addTempDetails($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;
        $data = $this->displaytempdata();
        
        echo $data;        
    }


    
    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->documents_program_model->getTempDetailsBySession($id_session); 
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Program</th>
                    <th>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $program_name = $temp_details[$i]->program_name;
                    $program_code = $temp_details[$i]->program_code;

                    $j = $i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$program_code - $program_name</td>                         
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempDocumentsProgram($id)'>Delete</a>
                            <td>
                        </tr>";
                    }


                                // <span onclick='deleteTempIntakeHasProgramme($id)'>Delete</a>
                            

            $table.= "
            </tbody>
            </table></div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }


    function directadd()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $data['id_program'] =  $tempData['id_program'];
        $data['id_documents_program'] =  $tempData['id'];

        $inserted_id = $this->documents_program_model->addNewDocumentsProgramDetails($data);
        
         $temp_details = $this->documents_program_model->getNewDocumentsProgramDetailsByMasterId($tempData['id']);
        // echo "<Pre>";print_r($temp_details);exit;
         if(!empty($temp_details))
        {
            
        // echo "<Pre>";print_r($temp_details);exit;
        $table = " <div class='custom-table'>
                    <table  class='table'>
                    <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Program</th>
                    <th>Action</th>
                </tr></thead>
                <tbody>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $program_name = $temp_details[$i]->program_name;
                    $program_code = $temp_details[$i]->program_code;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$program_code - $program_name</td>                         
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteDocumentsProgram($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
                    // <a onclick='deleteIntakeHasProgramme($id)'>Delete</a>

        $table.= "</tbody>
        </table>
        </div>";
        }
        else
        {
            $table="";
        }
        echo $table;           
    }

    function deleteTempDocumentsProgram($id)
    {
        $inserted_id = $this->documents_program_model->deleteTempData($id);
        if($inserted_id)
        {
            $data = $this->displaytempdata();
            echo $data;  
        }
        
        // echo "Success"; 
    }

    function deleteDocumentsProgram($id)
    {
        $inserted_id = $this->documents_program_model->deleteDocumentsProgram($id);
        // if($inserted_id)
        // {
        //     $data = $this->displaytempdata();
        //     echo $data;  
        // }
        
        echo "Success"; 
    }

    function tempDelete($id)
    {
        $inserted_id = $this->documents_program_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 
}
