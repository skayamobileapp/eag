<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Course_type_model extends CI_Model
{
    function courseTypeList()
    {
        $this->db->select('*');
        $this->db->from('course_type');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function courseTypeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('course_type');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getCourseType($id)
    {
        $this->db->select('*');
        $this->db->from('course_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewCourseType($data)
    {
        $this->db->trans_start();
        $this->db->insert('course_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editCourseType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('course_type', $data);
        return TRUE;
    }
}

