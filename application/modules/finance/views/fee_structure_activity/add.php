<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Fee Structure Activity</h3>
        </div>
        <form id="form_frequency_mode" action="" method="post">


            <div class="form-container">
                <h4 class="form-group-title">Fee Structure Activity Details</h4>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control" multiple>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->code . " - " . $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Activity <span class='error-text'>*</span></label>
                            <select name="id_activity" id="id_activity" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($activityList))
                                {
                                    foreach ($activityList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->name . " - " . $record->description;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Trigger <span class='error-text'>*</span></label>
                            <select name="trigger" id="trigger" class="form-control">
                                <option value="">Select</option>
                                <option value="Approval Level">Approval Level</option>
                                <option value="Application Level">Application Level</option>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fee Code <span class='error-text'>*</span></label>
                            <select name="id_fee_setup" id="id_fee_setup" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($feeSetupList))
                                {
                                    foreach ($feeSetupList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->code . " - " . $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Currency <span class='error-text'>*</span></label>
                            <select name="id_currency" id="id_currency" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($currencyList))
                                {
                                    foreach ($currencyList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->code . " - " . $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="amount_local" name="amount_local">
                             
                        </div>
                    </div>

                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount USD <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="amount_international" name="amount_international">
                             
                        </div>
                    </div> -->


                </div>

                <div class="row">


                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Performa <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="performa" id="performa" value="0" checked="checked"><span class="check-radio"></span> No
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="performa" id="performa" value="1"><span class="check-radio"></span> Yes
                                </label>                              
                            </div>                         
                    </div>               

                

                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>

                </div>

            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="saveData()">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>

    $('select').select2();

    function saveData()
    {
        if($('#form_frequency_mode').valid())
        {

        var tempPR = {};
        tempPR['id_program'] = $("#id_program").val();
        tempPR['id_activity'] = $("#id_activity").val();
        tempPR['id_currency'] = $("#id_currency").val();
        tempPR['trigger'] = $("#trigger").val();
        tempPR['id_fee_setup'] = $("#id_fee_setup").val();
        tempPR['performa'] = $("#performa").val();
        tempPR['amount_local'] = $("#amount_local").val();
        tempPR['amount_international'] = $("#amount_international").val();
        tempPR['status'] = $("#status").val();
            $.ajax(
            {
               url: '/finance/feeStructureActivity/addFeeStructureActivity',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                window.location.replace('/finance/feeStructureActivity/list');
                // alert(result);
                // window.location.reload();
                // $("#view").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }



    $(document).ready(function() {
        $("#form_frequency_mode").validate({
            rules: {
                id_program: {
                    required: true
                },
                id_activity: {
                    required: true
                },
                trigger: {
                    required: true
                },
                id_fee_setup: {
                    required: true
                },
                performa: {
                    required: true
                },
                amount_local: {
                    required: true
                },
                amount_international: {
                    required: true
                },
                id_currency: {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_activity: {
                    required: "<p class='error-text'>Select Activity</p>",
                },
                trigger: {
                    required: "<p class='error-text'>Select Trigger</p>",
                },
                id_fee_setup: {
                    required: "<p class='error-text'>Select Fee Code</p>",
                },
                performa: {
                    required: "<p class='error-text'>Select Performa</p>",
                },
                amount_local: {
                    required: "<p class='error-text'>Amount Required</p>",
                },
                amount_international: {
                    required: "<p class='error-text'>Amount Required</p>",
                },
                id_currency: {
                    required: "<p class='error-text'>Select Currency</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
