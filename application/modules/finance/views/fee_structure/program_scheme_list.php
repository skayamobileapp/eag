<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Program Details</h3>
     <a href="<?php echo '../../show_intake/'. $programme->id; ?>" class="btn btn-link btn-back">‹ Back</a>
    </div>

    <div class="form-container">
        <h4 class="form-group-title">Program Details</h4>  
        <div class="row">

              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Programme Name</label>
                      <input type="text" class="form-control" id="name" name="name" value="<?php echo $programme->name; ?>" readonly="readonly">
                  </div>
              </div>

              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Programme Code</label>
                      <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $programme->code; ?>" readonly="readonly">
                  </div>
              </div>

              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Name In Optional Language</label>
                      <input type="text" class="form-control" id="code" name="code" value="<?php echo $programme->name_optional_language; ?>" readonly="readonly">
                  </div>
              </div>
          </div>
        
      </div>



      <div class="form-container">
        <h4 class="form-group-title">Intake Details</h4>  
        <div class="row">

              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Intake Year</label>
                      <input type="text" class="form-control" id="name" name="name" value="<?php echo $intake->year; ?>" readonly="readonly">
                  </div>
              </div>

              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Intake  Name</label>
                      <input type="text" class="form-control" id="name" name="name" value="<?php echo $intake->name; ?>" readonly="readonly">
                  </div>
              </div>
          </div>


        
      </div>


      
      <br>


      <div class="form-container">
        <h4 class="form-group-title">Program Scheme List</h4>       

      <div class="custom-table">
        <table class="table" id="list-table">
          <thead>
            <tr>
              <th>Sl. No</th>
              <th>Mode Of Program</th>
              <th>Mode Of Study</th>
              <th>Min. Duration</th>
              <th>Max. Duration</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if (!empty($programSchemeList)) {$i=1;
              foreach ($programSchemeList as $record) {
            ?>
                <tr><td><?php echo $i ?></td>
                  <td><?php echo $record->mode_of_program ?></td>
                  <td><?php echo $record->mode_of_study ?></td>
                  <td><?php echo $record->min_duration ?></td>
                  <td><?php echo $record->max_duration ?></td>
                  <td class="text-center">
                    <a href="<?php echo '/finance/feeStructure/add/' . $intake->id.'/'.$programme->id. '/'. $record->id; ?>" title="Add | View">Add</a>
                  </td>
                </tr>
            <?php
              $i++;
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
