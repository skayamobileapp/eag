<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Payment Type</h3>
        </div>
        <form id="form_payment_type" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Payment Type Details</h4>          
                

                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="code" name="code" value="<?php echo $paymentTypeDetails->code;?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Description <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="description" name="description" value="<?php echo $paymentTypeDetails->description;?>">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Description Optional Language </label>
                            <input type="text" class="form-control" id="description_optional_language" name="description_optional_language" value="<?php echo $paymentTypeDetails->description_optional_language;?>">
                        </div>
                    </div>


                </div>

                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Payment Group <span class='error-text'>*</span></label>
                            <select name="payment_group" id="payment_group" class="form-control">
                                <option value="">Select</option>
                                <option value="Sponsership"
                                <?php
                                if($paymentTypeDetails->payment_group == 'Sponsership')
                                {
                                    echo 'selected';
                                }?>
                                >Sponsership</option>
                                <option value="Scholarship"
                                <?php
                                if($paymentTypeDetails->payment_group == 'Scholarship')
                                {
                                    echo 'selected';
                                }?>
                                >Scholarship</option>
                                <option value="Checque"
                                <?php
                                if($paymentTypeDetails->payment_group == 'Checque')
                                {
                                    echo 'selected';
                                }?>
                                >Checque</option>
                                <option value="Transfer Payment"
                                <?php
                                if($paymentTypeDetails->payment_group == 'Transfer Payment')
                                {
                                    echo 'selected';
                                }?>
                                >Transfer Payment</option>
                                <option value="Cash"
                                <?php
                                if($paymentTypeDetails->payment_group == 'Cash')
                                {
                                    echo 'selected';
                                }?>
                                >Cash</option>
                                <option value="Credit Card"<?php
                                if($paymentTypeDetails->payment_group == 'Credit Card')
                                {
                                    echo 'selected';
                                }?>
                                >Credit Card</option>
                                <option value="Others"
                                <?php
                                if($paymentTypeDetails->payment_group == 'Others')
                                {
                                    echo 'selected';
                                }?>
                                >Others</option>
                            </select>
                        </div>
                    </div> 

                    

                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>

                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();


   $(document).ready(function()
    {
        $("#form_payment_type").validate(
        {
            rules:
            {
                description:
                {
                    required: true
                },
                payment_group:
                {
                    required: true
                },
                code:
                {
                    required: true
                }
            },
            messages:
            {
                description:
                {
                    required: "<p class='error-text'>Description Required</p>",
                },
                payment_group:
                {
                    required: "<p class='error-text'>Payment Group Required</p>",
                },
                code:
                {
                    required: "<p class='error-text'>Payment Mode Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element)
            {
                error.appendTo(element.parent());
            }

        });
    });
</script>