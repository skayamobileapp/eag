<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class DebitNote extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('debit_note_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('debit_note.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['reference_number'] = $this->security->xss_clean($this->input->post('reference_number'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['debitNoteList'] = $this->debit_note_model->debitNoteListSearch($formData);

            $data['programmeList'] = $this->debit_note_model->programmeListByStatus('1');
            $data['intakeList'] = $this->debit_note_model->intakeListByStatus('1');
            $data['applicantList'] = $this->debit_note_model->applicantList();
            $data['studentList'] = $this->debit_note_model->studentList();
            $data['sponserList'] = $this->debit_note_model->sponserListByStatus('1');
            
            // echo "<Pre>"; print_r($data['debitNoteList']);exit;
            

            $this->global['pageTitle'] = 'Campus Management System : Credit Note List';
            $this->loadViews("debit_note/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('debit_note.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {

                  // echo "<Pre>"; print_r($this->input->post());exit;
                $id_session = $this->session->my_session_id;

                $ratio = $this->security->xss_clean($this->input->post('ratio'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_sponser = $this->security->xss_clean($this->input->post('id_sponser'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_invoice = $this->security->xss_clean($this->input->post('id_invoice'));
                $id_type = $this->security->xss_clean($this->input->post('id_type'));
                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));

                $generated_number = $this->debit_note_model->generateDebitNote();
                $data = array(
                    'reference_number' => $generated_number,
                    'ratio' => $ratio,
                    'amount' => $amount,
                    'description' => $description,
                    'type' => $type,
                    'id_program' => $id_programme,
                    'id_intake' => $id_intake,
                    'id_sponser' => $id_sponser,
                    'id_student' => $id_student,
                    'id_invoice' => $id_invoice,
                    'id_type' => $id_type,
                    'total_amount' => $total_amount,
                    'status' => 1,
                    // 'created_by' => $
                );
                $inserted_id = $this->debit_note_model->addNewDebitNote($data);


                // $temp_details = $this->debit_note_model->getTempDebitNoteDetails($id_session);
                //  for($i=0;$i<count($temp_details);$i++)
                //  {
                //     $id_main_invoice = $temp_details[$i]->id_main_invoice;
                //     $invoice_amount = $temp_details[$i]->invoice_amount;
                //     $debit_note_amount = $temp_details[$i]->debit_note_amount;

                //      $detailsData = array(
                //         'id_debit_note' => $inserted_id,
                //         'id_main_invoice' => $id_main_invoice,
                //         'invoice_amount' => $invoice_amount,
                //         'debit_note_amount' => $debit_note_amount,
                //     );
                //     //print_r($details);exit;
                //     $result = $this->debit_note_model->addNewDebitNoteDetails($detailsData);
                //  }

                // $this->debit_note_model->deleteTempDataBySession($id_session);

                redirect('/finance/debitNote/list');
            }

            $data['programmeList'] = $this->debit_note_model->programmeListByStatus('1');
            $data['intakeList'] = $this->debit_note_model->intakeListByStatus('1');
            $data['sponserList'] = $this->debit_note_model->sponserListByStatus('1');
            $data['creditNoteTypeList'] = $this->debit_note_model->debitNoteTypeListByStatus('1');


            $this->global['pageTitle'] = 'Campus Management System : Add Credit Note Details';
            $this->loadViews("debit_note/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('debit_note.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/debitNote/list');
            }
            if($this->input->post())
            {
                redirect('/finance/debitNote/edit/'.$id);
            }

            $data['debitNote'] = $this->debit_note_model->getDebitNote($id);

            $data['invoiceDetails'] = $this->debit_note_model->getInvoice($data['debitNote']->id_invoice);

            if($data['debitNote']->type == 'Sponsor')
            {
                $data['studentDetails'] = $this->debit_note_model->getStudentByStudentId($data['debitNote']->id_student);
            }

            $data['programmeList'] = $this->debit_note_model->programmeListByStatus('1');
            $data['intakeList'] = $this->debit_note_model->intakeListByStatus('1');
            $data['sponserList'] = $this->debit_note_model->sponserListByStatus('1');
            $data['applicantList'] = $this->debit_note_model->applicantList();
            $data['studentList'] = $this->debit_note_model->studentList();
            $data['creditNoteTypeList'] = $this->debit_note_model->debitNoteTypeListByStatus('1');


            $this->global['pageTitle'] = 'Campus Management System : View Credit Note Details';
            $this->loadViews("debit_note/edit", $this->global, $data, NULL);
        }
    }


    function getStudentByProgrammeId($id)
     {       
            // print_r($id);exit;
            $results = $this->debit_note_model->getStudentByProgrammeId($id);
            $programme_data = $this->debit_note_model->getProgrammeById($id);

            // echo "<Pre>"; print_r($programme_data);exit;
            $programme_name = $programme_data->name;
            $programme_code = $programme_data->code;
            $total_cr_hrs = $programme_data->total_cr_hrs;
            $graduate_studies = $programme_data->graduate_studies;
            $foundation = $programme_data->foundation;


            

            $table="

            <script type='text/javascript'>
                $('select').select2();
            </script>

            <select name='id_student' id='id_student' class='form-control' onchange='getStudentByStudentId(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $full_name = $results[$i]->full_name;
            $nric = $results[$i]->nric;
            $table.="<option value=".$id.">".$nric . " - " .$full_name.
                    "</option>";

            }
            $table.="</select>";

            $view  = "
            <table border='1px' style='width: 100%'>
                <tr>
                    <td colspan='4'><h5 style='text-align: center;'>Programme Details</h5></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Programme Name</th>
                    <td style='text-align: center;'>$programme_name</td>
                    <th style='text-align: center;'>Programme Code</th>
                    <td style='text-align: center;'>$programme_code</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Total Credit Hours</th>
                    <td style='text-align: center;'>$total_cr_hrs</td>
                    <th style='text-align: center;'>Graduate Studies</th>
                    <td style='text-align: center;'>$graduate_studies</td>
                </tr>

            </table>
            <br>
            <br>
            ";

            // $d['table'] = $table;
            // $d['view'] = $view;

            echo $table;
            exit;
    }

    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
          
            $invoice_data = $this->debit_note_model->getInvoicesByStudentId($id);
            // echo "<Pre>";print_r($invoice_data);exit;
            $table = '';
            $table .= $this->showInvoices($invoice_data);

            echo $table;
            exit;
    }








    function getStudentByProgram()
     {       
            // print_r($id);exit;
        $formData = $this->security->xss_clean($this->input->post('formData'));
            // echo "<Pre>"; print_r($formData);exit;

        $type = $formData['type'];
            switch ($type)
            {
                case 'Applicant':

                    $table = $this->getApplicantList($formData);

                    break;

                case 'Student':

                    $table = $this->getStudentList($formData);
                    
                    break;


                default:
                    # code...
                    break;
            }

            echo $table;
            exit;            
    }

    function getStudentList($data)
    {
        $data = $this->debit_note_model->getStudentListByData($data);
                // echo "<Pre>";print_r($data);exit();
        
            $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Student <span class='error-text'>*</span></label>
                <select name='id_student' id='id_student' class='form-control'  onchange='getStudentByStudentId(this.value)'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $nric = $data[$i]->nric;
                $full_name = $data[$i]->full_name;

                $table.="<option value=".$id.">".$nric. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getApplicantList($data)
    {
        $data = $this->debit_note_model->getApplicantListByData($data);
                // echo "<Pre>";print_r($data);exit();
        
            $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Applicant <span class='error-text'>*</span></label>
                <select name='id_student' id='id_student' class='form-control'  onchange='getApplicantByApplicantId(this.value)'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $nric = $data[$i]->nric;
                $full_name = $data[$i]->full_name;

                $table.="<option value=".$id.">".$nric. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getApplicantByApplicantId($id)
    {
         
            $table="";
            $invoice_data = $this->debit_note_model->getInvoicesByApplicantId($id);
            // echo "<Pre>";print_r($invoice_data);exit;
            $table .= $this->showInvoices($invoice_data);
            // if(!$invoice_data)
            // {


            //     $table .= "
            //     <br>
            //     <br>
            //     <div class='custom-table'>
            //         <table align='center' class='table' id='list-table'>
            //           <tr>
            //             <h3 style='text-align: center;'>No Balance Invoices Available For This Applicant</h3>
            //         </tr>
            //         </table>
            //     </div>
            //     <br>
            //     <br>";
            //     // echo "<Pre>";print_r("No Data");exit;
            // }
            // else
            // {
            //     $table .= "
            //     <h3>Receipt Details</h3>
            //     <div class='custom-table'>
            //         <table class='table' id='list-table'>
            //           <tr>
            //             <th>Sl. No</th>
            //             <th>Invoice Number</th>
            //             <th>Total Amount</th>
            //             <th>Balance Amount</th>
            //             <th>Credot Note</th>
            //         </tr>";


            //     for($i=0;$i<count($invoice_data);$i++)
            //         {
            //             $id = $invoice_data[$i]->id;
            //             $invoice_number = $invoice_data[$i]->invoice_number;
            //             $total_amount = $invoice_data[$i]->total_amount;
            //             $balance_amount = $invoice_data[$i]->balance_amount;
            //             $paid_amount = $invoice_data[$i]->paid_amount;
            //             $j=$i+1;
            //             $table .= "
            //         <tr>
            //             <td>$j
            //             <input type='number' hidden='hidden' readonly='readonly' id='invoice_id[]' name='invoice_id[]' value='$id'>
            //             <td>$invoice_number</td>
            //             <td>$total_amount</td>
            //             <td>$balance_amount</td>
            //             <td style='text-align: center;'>
            //             <div class='form-group'>
            //                 <input type='number' class='form-control' id='payable_amount[]' name='payable_amount[]' >
            //             </div>
            //             </td>
            //         </tr>";
            //         }
                            
            //     $table .= "
            //     </table>";
            // }


            echo $table;
            exit;
    }

    function showInvoices($data)
    {
                // echo "<Pre>";print_r($data);exit();
        
            $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Invoice <span class='error-text'>*</span></label>
                <select name='id_invoice' id='id_invoice' class='form-control'  onchange='getInvoiceByInvoiceId(this.value)'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $invoice_number = $data[$i]->invoice_number;
                $invoice_total = $data[$i]->invoice_total;

                $table.="<option value=".$id.">".$invoice_number. " - " . $invoice_total . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }


    function getStudentBySponser()
     {       
            // print_r($id);exit;
        $formData = $this->security->xss_clean($this->input->post('formData'));
            // echo "<Pre>"; print_r($formData);exit;

        $id_sponser = $formData['id_sponser'];
        $table = $this->getStudentBySponserId($id_sponser);
        echo $table;
        exit;   
    }

    function getStudentBySponserId($id_sponser)
    {
        $data = $this->debit_note_model->getStudentBySponser($id_sponser);
                // echo "<Pre>";print_r($data);exit();
        
            $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Student <span class='error-text'>*</span></label>
                <select name='id_student' id='id_student' class='form-control'  onchange='getStudentByStudentIdNSponser()'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $nric = $data[$i]->nric;
                $full_name = $data[$i]->full_name;

                $table.="<option value=".$id.">".$nric. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getStudentByStudentIdNSponser()
    {
        $formData = $this->security->xss_clean($this->input->post('formData'));

            // echo "<Pre>"; print_r($formData);exit;

        $id = $formData['id_student'];
        $id_sponser = $formData['id_sponser'];
        $type = $formData['type'];


            $invoice_data = $this->debit_note_model->getInvoicesByStudentIdNSponser($id);
            $table = '';
            // echo "<Pre>";print_r($invoice_data);exit;
            $table .= $this->showInvoices($invoice_data);

            // if(!$invoice_data)
            // {
            //     $table .= "
            //     <br>
            //     <br>
            //     <div class='custom-table'>
            //         <table align='center' class='table' id='list-table'>
            //           <tr>
            //             <h3 style='text-align: center;'>No Balance Invoices Available For This Student</h3>
            //         </tr>
            //         </table>
            //     </div>
            //     <br>
            //     <br>";
            //     // echo "<Pre>";print_r("No Data");exit;
            // }
            // else
            // {
            //     $table .= "
            //     <h3>Receipt Details</h3>
            //     <div class='custom-table'>
            //         <table class='table' id='list-table'>
            //           <tr>
            //             <th>Sl. No</th>
            //             <th>Invoice Number</th>
            //             <th>Total Amount</th>
            //             <th>Balance Amount</th>
            //             <th>Credit Note Amount</th>
            //         </tr>";


            //     for($i=0;$i<count($invoice_data);$i++)
            //         {
            //             $id = $invoice_data[$i]->id;
            //             $invoice_number = $invoice_data[$i]->invoice_number;
            //             $total_amount = $invoice_data[$i]->total_amount;
            //             $balance_amount = $invoice_data[$i]->balance_amount;
            //             $paid_amount = $invoice_data[$i]->paid_amount;
            //             $j=$i+1;
            //             $table .= "
            //         <tr>
            //             <td>$j
            //             <input type='number' hidden='hidden' readonly='readonly' id='invoice_id[]' name='invoice_id[]' value='$id'>
            //             <td>$invoice_number</td>
            //             <td>$total_amount</td>
            //             <td>$balance_amount</td>
            //             <td style='text-align: center;'>
            //             <div class='form-group'>
            //                 <input type='number' class='form-control' id='payable_amount[]' name='payable_amount[]' >
            //             </div>
            //             </td>
            //         </tr>";
            //         }
                            
            //     $table .= "
            //     </table>";
            // }


            echo $table;
            exit;
    }

    function getInvoiceByInvoiceId($id)
    {
        // print_r($id);exit;
            $invoice_data = $this->debit_note_model->getInvoice($id);
            // echo "<Pre>"; print_r($invoice_data);exit;

            $id = $invoice_data->id;
            $type = $invoice_data->type;
            $invoice_number = $invoice_data->invoice_number;
            $date_time = date('d-m-Y', strtotime($invoice_data->date_time));
            $remarks = $invoice_data->remarks;
            $total_amount = $invoice_data->total_amount;
            $invoice_total = $invoice_data->invoice_total;
            $balance_amount = $invoice_data->balance_amount;


            $table  = "

             <h4 class='sub-title'>Invoice Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                            <input type='hidden'  id='total_amount' name='total_amount' value='$invoice_total'>
                            <input type='hidden'  id='id_invoice' name='id_invoice' value='$id'>
                                <dt>Invoice Number :</dt>
                                <dd>$invoice_number</dd>
                            </dl>
                            <dl>
                                <dt>Invoice Date :</dt>
                                <dd>$date_time</dd>
                            </dl>
                            <dl>
                                <dt>Invoice Type :</dt>
                                <dd>$type</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Invoice Total :</dt>
                                <dd>
                                    $invoice_total
                                </dd>
                            </dl>
                            <dl>
                                <dt>Payable Amount :</dt>
                                <dd>$total_amount</dd>
                            </dl>
                            <dl>
                                <dt>Balance Amount</dt>
                                <dd>$balance_amount</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";

        echo $table;exit();

    }
}
