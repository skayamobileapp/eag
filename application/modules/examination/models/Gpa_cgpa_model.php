<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Gpa_cgpa_model extends CI_Model
{
    function gpaCgpaList()
    {
        $this->db->select('*');
        $this->db->from('gpa_cgpa_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function gpaCgpaListSearch($data)
    {
        $this->db->select('gcs.*, s.name as intake_name, s.year as intake_year');
        $this->db->from('gpa_cgpa_setup as gcs');
        $this->db->join('intake as s', 'gcs.id_intake = s.id');
        if($data['id_intake'] != '')
        {
            $this->db->where('gcs.id_intake', $data['id_intake']);
        }
        if($data['setup_by'] != '')
        {
            $this->db->where('gcs.setup_by', $data['setup_by']);
        }
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getGpaCgpaSetup($id)
    {
        $this->db->select('*');
        $this->db->from('gpa_cgpa_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getGpaCgpaSetupDetails($id_gpa_cgpa)
    {
        $this->db->select('*');
        $this->db->from('gpa_cgpa_setup_details');
        $this->db->where('id_gpa_cgpa', $id_gpa_cgpa);
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }
    
    function addNewGpaCgpaSetup($data)
    {
        $this->db->trans_start();
        $this->db->insert('gpa_cgpa_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editGpaCgpaSetupDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('gpa_cgpa_setup', $data);
        return TRUE;
    }

    function semesterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('semester');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         // print_r($result);exit();     
         return $result;
    }

    function awardListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('award');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function saveTempDetailData($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_gpa_cgpa_setup_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function saveGPACGPADetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('gpa_cgpa_setup_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempGPACGPADetailsBySessionId($id_session)
    {
        $this->db->select('*');
        $this->db->from('temp_gpa_cgpa_setup_details');
        $this->db->where('id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function deleteTempDetailData($id)
    {
         $this->db->where('id', $id);
        $this->db->delete('temp_gpa_cgpa_setup_details');
        return TRUE;
    }


    function deleteTempDetailBySessionId($id_session)
    {
         $this->db->where('id_session', $id_session);
        $this->db->delete('temp_gpa_cgpa_setup_details');
        return TRUE;
    }

    function moveDetailDataFromTempToMain($id_gpa_cgpa)
    {
        $id_session = $this->session->my_session_id;
        $details = $this->getTempGPACGPADetailsBySessionId($id_session);
        foreach ($details as $detail)
        {
            $detail->id_gpa_cgpa = $id_gpa_cgpa;
            unset($detail->id_session);
            unset($detail->id);

            $added = $this->saveGPACGPADetails($detail);
            # code...
        }
        
        $added = $this->deleteTempDetailBySessionId($id_session);
    }

    function deleteDetailData($id)
    {
         $this->db->where('id', $id);
        $this->db->delete('gpa_cgpa_setup_details');
        return TRUE;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }
}