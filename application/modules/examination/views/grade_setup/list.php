<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Grade Setup</h3>
      <a href="add" class="btn btn-primary">+ Add Grade Setup</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                
                <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Based On</label>
                    <div class="col-sm-8">
                      <select name="based_on" id="based_on" class="form-control">
                        <option value=''>Select</option>

                          <option value='Award' <?php if($searchParam['based_on'] =='Award')
                          { echo "selected=selected";} ?> >Award
                          </option>

                          <option value='Programme & Subject' <?php if($searchParam['based_on'] =='Program & Subject')
                            { echo "selected=selected";} ?> >Program & Subject
                          </option>
                          
                          <option value='Programme' <?php if($searchParam['based_on'] =='Program')
                          { echo "selected=selected";} ?> >Program
                          </option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Semester</label>
                    <div class="col-sm-8">
                      <select name="id_semester" id="id_semester" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($semesterList)) {
                          foreach ($semesterList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_semester']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name . " - " . $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>              

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>  
            <th>Sl. No</th>
            <th>Based On</th>
            <th>Intake</th>
            <th>Program</th>
            <th>Course</th>
            <th>Award</th>
            <th>Status</th>
            <th style="text-align:center; ">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($gradeSetupList))
          {
            $i=0;
            foreach ($gradeSetupList as $record) {
          ?>
              <tr>
                 <td><?php echo $i+1;?></td>
                <td><?php echo $record->based_on ?></td>
                <td><?php echo $record->intake_name?></td>

                <?php if($record->based_on=='Award') { ?>

                   <td>-</td>
                   <td>-</td>
                                   <td><?php echo $record->awardname ?></td> 



                <?php } else { ?> }
                 <td><?php echo $record->program_name ?></td>
                <td><?php echo $record->coursename ?></td>
                <td>-</td>
              <?php } ?> 
                <td><?php if( $record->status == '0')
                {
                  echo "In-Active";
                }
                elseif( $record->status == '1')
                {
                  echo "Active";
                }
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                  <!--  -->
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    $('select').select2();
    
    function clearSearchForm()
    {
      window.location.reload();
    }
</script>