<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Student Marks Entry</h3>
            <!-- <a href="../list" class="btn btn-link btn-back">‹ Back</a> -->
        </div>    

            <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl> 
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $studentDetails->program_scheme ?></dd>
                            </dl>                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $studentDetails->programme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Qualification Type :</dt>
                                <dd><?php echo $studentDetails->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $studentDetails->program_scheme; ?></dd>
                            </dl>
                            <dl>
                                <dt>Academic Advisor :</dt>
                                <dd><?php echo $studentDetails->ic_no . " - " . $studentDetails->advisor; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


            <br>


            <div class="form-container">
                <h4 class="form-group-title">Course Registered Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Course Name :</dt>
                                <dd><?php echo ucwords($courseRegisteredDetails->course_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Course Code :</dt>
                                <dd><?php echo $courseRegisteredDetails->course_code ?></dd>
                            </dl>                     
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Pre Reuisite :</dt>
                                <dd><?php echo $courseRegisteredDetails->pre_requisite ?></dd>
                            </dl>
                            <dl>
                                <dt>Course Type :</dt>
                                <dd><?php echo $courseRegisteredDetails->course_type; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>








        <form id="form_main" action="" method="post">


            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Select Grade <span class='error-text'>*</span></label>
                        <select name="grade" id="grade" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($gradeList))
                            {
                                foreach ($gradeList as $record)
                                {?>
                             <option value="<?php echo $record->name;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


            </div>

            <?php

            if(!empty($markDistributionDetails))
            {
                ?>
                <br>

            <input type="hidden" class="form-control" id="id_program" name="id_program" value="<?php echo $id_program ?>">
            
            <input type="hidden" class="form-control" id="id_intake" name="id_intake" value="<?php echo $id_intake ?>">
            
            <input type="hidden" class="form-control" id="id_course_registered_landscape" name="id_course_registered_landscape" value="<?php echo $id_course_registered_landscape ?>">
            
            <input type="hidden" class="form-control" id="id_student" name="id_student" value="<?php echo $id_student ?>">
            
            <input type="hidden" class="form-control" id="id_course_registration" name="id_course_registration" value="<?php echo $id_course_registration ?>">

            <input type="hidden" class="form-control" id="id_mark_distribution" name="id_mark_distribution" value="<?php echo $markDistributionDetails[0]->id_mark_distribution ?>">


                <div class="form-container">
                        <h4 class="form-group-title">Course Marks Distribution Details</h4>
                    

                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                <th>Sl. No</th>
                                <th>Exam Components</th>
                                <th>Max. Marks</th>
                                <th>Pass Compulsary</th>
                                <th>Pass Marks</th>
                                <th>Attendance Status</th>
                                <th style="text-align: center;">Marks Obtained</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                             $total = 0;
                              for($i=0;$i<count($markDistributionDetails);$i++)
                             { ?>
                                <tr>
                                <td><?php echo $i+1;?></td>
                                <td><?php echo $markDistributionDetails[$i]->component_code . " - " . $markDistributionDetails[$i]->component_name;?></td>
                                <td><?php echo $markDistributionDetails[$i]->max_marks; ?></td>
                                <td><?php if($markDistributionDetails[$i]->is_pass_compulsary == 1)
                                {
                                    echo 'Yes';
                                }else
                                {
                                    echo 'No';
                                }
                                ?></td>
                                <td><?php echo $markDistributionDetails[$i]->pass_marks; ?></td>
                                <td><?php if($markDistributionDetails[$i]->attendance_status == 1)
                                {
                                    echo 'Yes';
                                }else
                                {
                                    echo 'No';
                                }
                                ?></td>

                                <td style="text-align: center;">
                                    <input type="number" max="<?php echo $markDistributionDetails[$i]->max_marks; ?>" class="form-control" id="obtained_marks[]" name="obtained_marks[]">
                                    <input type="hidden" class="form-control" id="id_marks_distribution_details[]" name="id_marks_distribution_details[]" value="<?php echo $markDistributionDetails[$i]->id ?>">
                                    <input type="hidden" class="form-control" id="id_component[]" name="id_component[]" value="<?php echo $markDistributionDetails[$i]->id_component ?>">
                                    <input type="hidden" class="form-control" id="max_marks[]" name="max_marks[]" value="<?php echo $markDistributionDetails[$i]->max_marks ?>">
                                    <input type="hidden" class="form-control" id="pass_marks[]" name="pass_marks[]" value="<?php echo $markDistributionDetails[$i]->pass_marks ?>">
                                </td>



                                

                                 </tr>
                              <?php 
                          } 
                          ?>
                            </tbody>
                        </table>
                      </div>

                    </div>

            <?php
            
            }
             ?>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="/examination/studentMarksEntry/studentList" class="btn btn-link">Back</a>
            </div>
        </div>

            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>


</form>
<script type="text/javascript">


    $('select').select2();


    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                grade: {
                    required: true
                }
            },
            messages: {
                grade: {
                    required: "<p class='error-text'>Select Grade</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>