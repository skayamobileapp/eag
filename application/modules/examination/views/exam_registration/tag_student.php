<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Tag Examination To Student</h3>
        </div>






        <div class="form-container">
            <h4 class="form-group-title"> Examination Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Tag Student Details</a>
                    </li>    
                    <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Tagged Students List</a>
                    </li>                
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">



                          



                            <br>


                    <div class="form-container">
                        <h4 class="form-group-title">Exam Registratioin Details</h4>


                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                 <label>Program <span class='error-text'>*</span></label>
                                    <select name="id_p" id="id_p" class="form-control selitemIcon" disabled>
                                        <option value="">Select</option>
                                        <?php
                                        if (!empty($programList))
                                        {
                                            foreach ($programList as $record)
                                            {?>
                                        <option value="<?php echo $record->id;  ?>"
                                             <?php 
                                                if($record->id == $examination->id_programme)
                                                {
                                                    echo "selected=selected";
                                                } ?>
                                            >
                                            <?php echo $record->code . " - " . $record->name;?>
                                        </option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                 <label>Intake <span class='error-text'>*</span></label>
                                    <select name="id_i" id="id_i" class="form-control selitemIcon" disabled>
                                        <option value="">Select</option>
                                        <?php
                                        if (!empty($intakeList))
                                        {
                                            foreach ($intakeList as $record)
                                            {?>
                                        <option value="<?php echo $record->id;  ?>"
                                             <?php 
                                                if($record->id == $examination->id_intake)
                                                {
                                                    echo "selected=selected";
                                                } ?>
                                            >
                                            <?php echo $record->year . " - " . $record->name;?>
                                        </option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="col-sm-4">
                                    <div class="form-group">
                                     <label>Programme Landscape <span class='error-text'>*</span></label>
                                        <select name="id_programme_landscape" id="id_programme_landscape" class="form-control selitemIcon" disabled>
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($programmeLandscapeList))
                                            {
                                                foreach ($programmeLandscapeList as $record)
                                                {?>
                                            <option value="<?php echo $record->id;  ?>"
                                                 <?php 
                                                    if($record->id == $examination->id_programme_landscape)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>
                                                >
                                                <?php echo $record->program_landscape_type . " - " . $record->name;?>
                                            </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                            </div>


                            <div class="row">
                

                            <div class="col-sm-4">
                                <div class="form-group">
                                 <label>Exam Event <span class='error-text'>*</span></label>
                                    <select name="id_event" id="id_event" class="form-control selitemIcon" disabled>
                                        <option value="">Select</option>
                                        <?php
                                        if (!empty($examEventList))
                                        {
                                            foreach ($examEventList as $record)
                                            {?>
                                        <option value="<?php echo $record->id;  ?>"
                                             <?php 
                                                if($record->id == $examination->id_exam_event)
                                                {
                                                    echo "selected=selected";
                                                } ?>
                                            >
                                            <?php echo date('d-m-Y', strtotime($record->from_dt)) . " - " . $record->name;?>
                                        </option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                        

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Total Capacity <span class='error-text'>*</span></label>
                                    <input type="number" class="form-control" name="total_c" id="total_c" autocomplete="off" value="<?php echo $examination->total_capacity; ?>" readonly>
                                    <!-- <span id='view_centers'></span> -->
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Status <span class='error-text'>*</span></label>
                                    <input type="test" class="form-control" id="status" name="status" value="<?php 
                                    if($examination->status=='0')
                                    {
                                         echo "Pending";
                                      }
                                      elseif($examination->status=='1'){
                                        echo "Approved";
                                      }elseif($examination->status=='2'){
                                        echo "Rejected";
                                      };?>

                                    " readonly >
                                </div>
                            </div>

                        

                        </div>

                    </div>


                      <br>


                        <h4 class='sub-title'>Course Registration Details</h4>

                            <div class='data-list'>
                                <div class='row'>
                
                                    <div class='col-sm-6'>
                                        <dl>
                                            <input type='hidden' class='form-control' name='id_course' id='id_course' value='$id_course'>
                                            <dt>Course Name :</dt>
                                            <dd><?php echo $courseLandscape->name; ?></dd>
                                        </dl>
                                        <dl>
                                            <dt>Course Code :</dt>
                                            <dd><?php echo $courseLandscape->code; ?></dd>
                                        </dl>
                                        <dl>
                                            <dt>Pre-Requisite :</dt>
                                            <dd><?php echo $courseLandscape->pre_requisite; ?></dd>
                                        </dl>
                                    </div>        
                                    
                                    <div class='col-sm-6'>
                                        <dl>
                                            <dt>Credit Hours :</dt>
                                            <dd>
                                                <?php echo $courseLandscape->credit_hours; ?>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>Course Type :</dt>
                                            <dd><?php echo $courseLandscape->course_type; ?></dd>
                                        </dl>
                                    </div>
                
                                </div>
                            </div>





                        <br>


                        <form id="form_comitee" action="" method="post">

                            <div class="form-container">
                            <h4 class="form-group-title">Exam Registratioin Add Details</h4>

                            <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Student Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="name" name="name">
                                        </div>
                                    </div>

                                

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>NRIC <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="nric" name="nric">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Email <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="email_id" name="email_id">
                                        </div>
                                    </div>




                                  
                                    <!-- <div class="col-sm-4">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="searchStudent()">Search</button>
                                    </div> -->

                                </div>

                                <br>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" onclick="searchStudent()">Search</button>
                                    </div>

                            </div>







                        <div class="form-container" id="display_course_details" style="display: none">
                            <h4 class="form-group-title">Course Registration Student Details</h4>

                            <div class="row">

                                <div id="view_course_details">
                                </div>

                            </div>

                        </div>


                        <div class="button-block clearfix">
                            <div class="bttn-group">
                                <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                                <a href="../tagList" class="btn btn-link">Back</a>
                            </div>
                        </div>


                        </form>


                    


                        </div> 
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="col-12">



                        <br>


                        <?php

                    if(!empty($examRegistrationDetails))
                    {
                        ?>

                        <div class="form-container">
                                <h4 class="form-group-title">Exam Registered Student Details</h4>
                            

                              <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                        <th>Student</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($examRegistrationDetails);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td><?php echo $examRegistrationDetails[$i]->nric . " - " . $examRegistrationDetails[$i]->student_name;?></td>
                                        <td><?php echo $examRegistrationDetails[$i]->email_id; ?></td>
                                        <td><?php echo $examRegistrationDetails[$i]->phone; ?></td>
                                        <td>
                                        <a onclick="daleteExamRegistrationData(<?php echo $examRegistrationDetails[$i]->id; ?>)" title="Delete">Delete</a>
                                        </td>
                                        <!-- <td class="text-center">
                                        <a onclick="deleteDetailData(<?php echo $examRegistrationDetails[$i]->id; ?>)">Delete</a>
                                        </td> -->

                                       

                                         </tr>
                                      <?php 
                                  } 
                                  ?>
                                    </tbody>
                                </table>
                              </div>

                            </div>

                    <?php
                    
                    }
                     ?>


                        </div> 
                    </div>


                </div>

            </div>
        </div> 





        





     



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>




    function searchStudent()
    {
        var tempPR = {};

        tempPR['name'] = $("#name").val();
        tempPR['nric'] = $("#nric").val();
        tempPR['email_id'] = $("#email_id").val();
        tempPR['id_program'] = <?php echo $examination->id_programme ?>;
        tempPR['id_intake'] = <?php echo $examination->id_intake ?>;
        tempPR['id_course_registered_landscape'] = <?php echo $id_course_registered_landscape ?>;

        if (tempPR['id_course_registered_landscape'] != '')
        {
            $.ajax(
            {
               url: '/examination/examRegistration/searchStudent',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                
                    $("#view_course_details").html(result);
                    $("#display_course_details").show();

                
               }
            }); 
        }       
    }


    function daleteExamRegistrationData(id) {
      // alert(id);
         $.ajax(
            {
               url: '/examination/examRegistration/daleteExamRegistrationData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                alert('Deleted Successfully');
                window.location.reload();
                    // $("#view").html(result);
               }
            });
    }



    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_program: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                id_course_registered: {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_course_registered: {
                    required: "<p class='error-text'>Select Course</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
    


$( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2001"
    });
  } );

    $('select').select2();

</script>