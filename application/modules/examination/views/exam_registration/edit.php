<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Exam Registratioin</h3>
        </div>

        <div class="form-container">
            <h4 class="form-group-title">Exam Registratioin Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                     <label>Program <span class='error-text'>*</span></label>
                        <select name="id_program" id="id_program" class="form-control selitemIcon" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"
                                 <?php 
                                    if($record->id == $examination->id_programme)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                >
                                <?php echo $record->code . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                     <label>Intake <span class='error-text'>*</span></label>
                        <select name="id_intake" id="id_intake" class="form-control selitemIcon" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"
                                 <?php 
                                    if($record->id == $examination->id_intake)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                >
                                <?php echo $record->year . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                     <label>Programme Landscape <span class='error-text'>*</span></label>
                        <select name="id_programme_landscape" id="id_programme_landscape" class="form-control selitemIcon" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeLandscapeList))
                            {
                                foreach ($programmeLandscapeList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"
                                 <?php 
                                    if($record->id == $examination->id_programme_landscape)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                >
                                <?php echo $record->program_landscape_type . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


            </div>


            <div class="row">
                

                <div class="col-sm-4">
                    <div class="form-group">
                     <label>Exam Event <span class='error-text'>*</span></label>
                        <select name="id_exam_event" id="id_exam_event" class="form-control selitemIcon" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($examEventList))
                            {
                                foreach ($examEventList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"
                                 <?php 
                                    if($record->id == $examination->id_exam_event)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                >
                                <?php echo date('d-m-Y', strtotime($record->from_dt)) . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            <!-- </div>


        


            <div class="row"> -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Capacity <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" name="total_capacity" id="total_capacity" autocomplete="off" value="<?php echo $examination->total_capacity; ?>" readonly>
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="test" class="form-control" id="status" name="status" value="<?php 
                        if($examination->status=='0')
                        {
                             echo "Pending";
                          }
                          elseif($examination->status=='1'){
                            echo "Approved";
                          }elseif($examination->status=='2'){
                            echo "Rejected";
                          };?>

                        " readonly >
                    </div>
                </div>

            

            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <!-- <button type="submit" class="btn btn-primary btn-lg" >Save</button> -->
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>





     <?php

                    if(!empty($examRegistrationDetails))
                    {
                        ?>

                        <div class="form-container">
                                <h4 class="form-group-title">Exam Registered Student Details</h4>
                            

                              <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                        <th>Student</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <!-- <th>Attendance Status</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($examRegistrationDetails);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td><?php echo $examRegistrationDetails[$i]->nric . " - " . $examRegistrationDetails[$i]->student_name;?></td>
                                        <td><?php echo $examRegistrationDetails[$i]->email_id; ?></td>
                                        <td><?php echo $examRegistrationDetails[$i]->phone; ?></td>

                                        <!-- <td class="text-center">
                                        <a onclick="deleteDetailData(<?php echo $examRegistrationDetails[$i]->id; ?>)">Delete</a>
                                        </td> -->

                                       

                                         </tr>
                                      <?php 
                                  } 
                                  ?>
                                    </tbody>
                                </table>
                              </div>

                            </div>

                    <?php
                    
                    }
                     ?>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>


    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_program: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                id_course_registered: {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_course_registered: {
                    required: "<p class='error-text'>Select Course</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
    


$( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2001"
    });
  } );

    $('select').select2();

</script>