<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Partner_category_model extends CI_Model
{
    function partnerCategoryList()
    {
        $this->db->select('*');
        $this->db->from('partner_category');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function partnerCategoryListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('partner_category');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProgramType($id)
    {
        $this->db->select('*');
        $this->db->from('partner_category');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProgramType($data)
    {
        $this->db->trans_start();
        $this->db->insert('partner_category', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProgramType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('partner_category', $data);
        return TRUE;
    }
}

