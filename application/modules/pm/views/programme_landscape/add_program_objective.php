<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Program Landscape</h3>
        </div>


        <br>


        <div class="topnav">
          <a href="<?php echo '../../../edit/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme; ?>">Landscape Info</a> | 
          <a href="<?php echo '../../../programObjective/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme.'/' . $programmeLandscapeDetails->id_intake;; ?>" style="background: #aaff00" >Program Objective</a> |
          <a href="<?php echo '../../../subjectRegistration/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme.'/' . $programmeLandscapeDetails->id_intake;; ?>">Subject Registration</a>
          <!-- <a href="<?php echo '../../../editProgramRequirementTab/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme; ?>" >Program Requirement</a> | 
          <a href="<?php echo '../../../editCourseTab/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme .'/' . $programmeLandscapeDetails->id_intake; ?>">Course</a>
          <?php

            if($programme->mode == 0)
            {

            ?>
             |
          <a href="<?php echo '../../../addLearningMode/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme .'/' . $programmeLandscapeDetails->id_intake; ?>" style="background: #aaff00">Learning Mode
          </a>

          <?php

            }

            ?> -->
        </div>

        <br>



        <form id="form_programme_landscape" action="" method="post">


        <div class="form-container">
            <h4 class="form-group-title">Programme Landscape Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Landscape Name  <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $programmeLandscapeDetails->name; ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program  <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programmeLandscapeDetails->id_programme)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Intake <span class='error-text'>*</span></label>
                        <select name="id_intake" id="id_intake" disabled="disabled" class="form-control">

                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $id_intake)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->year . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Landscape Type  <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="program_landscape_type" name="program_landscape_type" value="<?php echo $programmeLandscapeDetails->program_landscape_type; ?>" readonly>
                    </div>
                </div>

                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Scheme <span class='error-text'>*</span></label>
                       
                         <select name="program_scheme" id="program_scheme" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeSchemeList))
                            {
                                foreach ($programmeSchemeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    <?php if($programmeLandscapeDetails->program_scheme==$record->id){ echo "selected"; } ?>

                                    ><?php echo $record->description;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->

            </div>

        </div>



        <div class="form-container">
        <h4 class="form-group-title">Program Objective Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course <span class='error-text'>*</span></label>
                        <select name="id_course" id="id_course" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        >
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Objective <span class='error-text'>*</span></label>
                        <select name="id_objective" id="id_objective" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeObjectiveList))
                            {
                                foreach ($programmeObjectiveList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        >
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course Registration Type <span class='error-text'>*</span></label>
                        <select name="id_course_registration_type" id="id_course_registration_type" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseTypeList))
                            {
                                foreach ($courseTypeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        >
                                        <?php echo $record->name . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>




                
            </div>


        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="<?php echo '../../../programmeLandscapeList/' . $id_programme; ?>" class="btn btn-link">Back</a>
            </div>
        </div>

    

        <form id="form_profile" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Program Objective Details</h4>
  
    
        <div class="m-auto text-center">
           <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
         </div>
        <div class="clearfix">
        <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
            <li role="presentation" class="active"><a href="#education" class="nav-link border rounded text-center"
                    aria-controls="education" aria-selected="true"
                    role="tab" data-toggle="tab">Program Objective</a>
            </li>
            <!-- <li role="presentation"><a href="#proficiency" class="nav-link border rounded text-center"
                    aria-controls="proficiency" role="tab" data-toggle="tab">Major Course </a>
            </li>
            <li role="presentation"><a href="#employment" class="nav-link border rounded text-center"
                    aria-controls="employment" role="tab" data-toggle="tab">Minor Course</a>
            </li>
            <li role="presentation"><a href="#profile" class="nav-link border rounded text-center"
                    aria-controls="profile" role="tab" data-toggle="tab">Not Compulsary</a>
            </li> -->
        </ul>



        <div class="tab-content offers-tab-content">
            <div role="tabpanel" class="tab-pane active" id="education">
            <div class="col-12 mt-4">
                <br>


            <div class="form-container">
            <h4 class="form-group-title">Learning Mode Details</h4>

                <div class="custom-table">
                  <table class="table" id="list-table">
                    <thead>
                      <tr>
                        <th>Sl. No</th>
                        <!-- <th>Landscape Name</th> -->
                        <th>Code</th>
                        <th>Course</th>
                        <th>Cr. Hrs</th>
                        <th>Objective</th>
                        <th>Course Registration Type</th>
                        <th>Status</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if (!empty($getProgramObjectiveByProgramLandscape)) {
                        $i=1;
                        foreach ($getProgramObjectiveByProgramLandscape as $record) {
                      ?>
                          <tr>
                            <td><?php echo $i ?></td>
                            <!-- <td><?php echo $record->programName ?></td> -->
                            <td><?php echo $record->course_code?></td>
                            <td><?php echo $record->course_name ?></td>
                            <td><?php echo $record->credit_hours?></td>
                            <td><?php echo $record->objective ?></td>
                            <td><?php echo $record->course_registration_type_code . " - " . $record->course_registration_type_name ?></td>
                            <td>
                                <?php 
                                if($record->status == 1)
                                {
                                    echo 'Active';
                                }
                                else
                                {
                                    echo 'In-Active';
                                }
                                ?>
                                
                            </td>
                            <td class="text-center">
                                <a onclick="editProgramObjectiveProgrammeLandscapeDetails(<?php echo $record->id; ?>)">Change Status</a>

                              <!-- <?php echo anchor('setup/programmeLandscape/delete_pl_learning_mode?id='.$record->id, 'Delete', 'id="$record->id"'); ?> -->
                               
                            </td>
                          </tr>
                      <?php
                      $i++;
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
            </div>
             
             </div> <!-- END col-12 -->  
            </div>

        

          </div>
        </div>

       </div> <!-- END row-->
    </div>
    </form>



   </div> <!-- END row-->
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();


    function editProgramObjectiveProgrammeLandscapeDetails(id)
    {

        $.ajax(
            {
               url: '/setup/programmeLandscape/editProgramObjectiveProgrammeLandscapeDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    alert("Status Updated Successfully");
                    window.location.reload();
               }
            });


       
        // var tempPR = {};
        // tempPR['status'] = <?php echo 1;?>;
        //     $.ajax(
        //     {
        //        url: '/setup/programme/directSchemeAdd',
        //         type: 'POST',
        //        data:
        //        {
        //         tempData: tempPR
        //        },
        //        error: function()
        //        {
        //         alert('Something is wrong');
        //        },
        //        success: function(result)
        //        {
        //         location.reload();
        //        }
        //     });
    }



    $(document).ready(function() {
        $("#form_programme_landscape").validate({
            rules: {
                id_objective:
                {
                    required: true
                },
                id_course:
                {
                    required: true
                },
                id_course_registration_type:
                {
                    required: true
                }
            },
            messages:
            {
                id_objective: {
                    required: "<p class='error-text'>Select Objective</p>",
                },
                id_course: {
                    required: "<p class='error-text'>Select Course</p>",
                },
                id_course_registration_type: {
                    required: "<p class='error-text'>Select Course Type</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
