<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Student extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_model');
        $this->isPartnerUniversityLoggedIn();
        error_reporting(0);
    }

    function index()
    {
        $this->welcome();
    }


    function welcome()
    {
        // $formData['name'] = $this->security->xss_clean($this->input->post('name'));

        // $data['searchParam'] = $formData;

        $this->global['pageTitle'] = 'Partner University Portal : Partner University Dashboard';
        $this->loadViews("student/v", $this->global, NULL, NULL);
    }


    function getcustomreplay($idmessage,$idapplicant)
    {





        switch($idmessage){
            case '1' :
                       $message = "  <p class='cb-help-text'>1</p>
                       <p class='cb-answer'>Your application status is under review, It may take 48hours to know the final status '<strong>0</strong>' :For main menu</p>";
                       break;
case '2' :
                       $message = "  <p class='cb-help-text'>2</p><p class='cb-answer'>Fee for the program will be RM 1500 <br/>For more information Type '<strong>0</strong>' :For main menu</p> ";
                       break;
case '3' :
                       $message = "  <p class='cb-help-text'>3</p><p class='cb-answer'>Temporary Offer Letter will be sent to your eail id.  <br/> For more information Type '<strong>0</strong>' :For main menu</p>";
                       break;
case '4' :
                       $message = " <p class='cb-help-text'>4</p><p class='cb-answer'>Semester will start from Oct 4th 2020 <br/> For more information Type '<strong>0</strong>' :For main menu</p>";
                       break;

case '9' :
                       $message = " <p class='cb-answer'>Thank your for your time, our representative will call back to you, you can also email us at info@gmail.com</p>
                    <p class='cb-help-text'>For more information Type '<strong>0</strong>' :For main menu</p> ";
                       break;
       

case '0' :
                       $message = "<p><ul class='cb-help'>
                        <li>Type '<strong>1</strong>' : For Application Status</li>
                        <li>Type '<strong>2</strong>' : For Fee Amount</li>
                        <li>Type '<strong>3</strong>' : For Offer Letter</li>
                        <li>Type '<strong>4</strong>' : To know the start date of the semester</li>
                        <li>Type '<strong>9</strong>' : To get a call from the representative</li>
                    </ul></p> ";
                       break;                                                                                   

        }

        echo $message;
        exit;

    }

    function list()
    {
        if ($this->checkAccess('partner_university_student.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_partner_university = $this->session->id_partner_university;

            $partner_university_name = $this->session->partner_university_name;
            $partner_university_code = $this->session->partner_university_code;

            $data['partner_university_name'] = $partner_university_name;
            $data['partner_university_code'] = $partner_university_code;
            

            $data['intakeList'] = $this->student_model->intakeList();
            $data['programList'] = $this->student_model->programList();

            $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));
            $formData['id_partner_university'] = $id_partner_university;
 
            $data['applicantList'] = $this->student_model->applicantListSearch($formData);
            $data['searchParam'] = $formData;



            $this->global['pageTitle'] = 'Partner University Portal : Student List';
            // echo "<Pre>";print_r($data['applicantList']);exit;
            $this->loadViews("student/list", $this->global, $data, NULL);
        }
    }


    function add1()
    {
        $id_partner_university = $this->session->id_partner_university;

        if($this->input->post())
        {
            $salutation = $this->security->xss_clean($this->input->post('salutation'));
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $phone = $this->security->xss_clean($this->input->post('phone'));
            $email_id = $this->security->xss_clean($this->input->post('email_id'));
            $contact_email = $this->security->xss_clean($this->input->post('contact_email'));
            $password = $this->security->xss_clean($this->input->post('password'));
            $passport = $this->security->xss_clean($this->input->post('passport'));
            $nric = $this->security->xss_clean($this->input->post('nric'));
            $gender = $this->security->xss_clean($this->input->post('gender'));
            $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
            $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
            $religion = $this->security->xss_clean($this->input->post('religion'));
            $nationality = $this->security->xss_clean($this->input->post('nationality'));
            $id_race = $this->security->xss_clean($this->input->post('id_race'));
           
            $salutationInfo = $this->student_model->getSalutation($salutation);

            $data = array(

                'salutation' => $salutation,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'full_name' => $salutationInfo->name . ". " . $first_name." ".$last_name,
                'phone' => $phone,
                'email_id' => $email_id,
                'password' => md5($password),
                'contact_email' => $contact_email,
                'password' => md5($password),
                'nric' => $nric,
                'passport' => $passport,
                'gender' => $gender,
                'date_of_birth' => date("Y-m-d", strtotime($date_of_birth)),
                'martial_status' => $martial_status,
                'id_university' => $id_partner_university,
                'religion' => $religion,
                'is_hostel' => 1,
                'nationality' => $nationality,
                'id_race' => $id_race,
                'added_by_partner_university' => $id_partner_university,
                'applicant_status' => ''
                
            );

            $result = $this->student_model->addStudent($data);
            redirect('/partner_university/student/step2/' . $result);
        }


        $data['intakeList'] = $this->student_model->intakeList();
        $data['programList'] = $this->student_model->programList();
        $data['countryList'] = $this->student_model->countryListByStatus('1');
        $data['stateList'] = $this->student_model->stateList();
        $data['degreeTypeList'] = $this->student_model->qualificationListByStatus('1');
        $data['raceList'] = $this->student_model->raceListByStatus('1');
        $data['religionList'] = $this->student_model->religionListByStatus('1');
        $data['degreeTypeList'] = $this->student_model->qualificationListByStatus('1');
        $data['salutationList'] = $this->student_model->salutationListByStatus('1');

        $this->global['pageTitle'] = 'Partner University Portal : Student Add';
        // echo "<Pre>";print_r($data['intakeList']);exit;
        $this->loadViews("student/add_setup1", $this->global, $data, NULL);
    }


    function step1($id)
    {
        $id_partner_university = $this->session->id_partner_university;

        if($this->input->post())
        {
            $salutation = $this->security->xss_clean($this->input->post('salutation'));
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $phone = $this->security->xss_clean($this->input->post('phone'));
            $email_id = $this->security->xss_clean($this->input->post('email_id'));
            $contact_email = $this->security->xss_clean($this->input->post('contact_email'));
            $password = $this->security->xss_clean($this->input->post('password'));
            $passport = $this->security->xss_clean($this->input->post('passport'));
            $nric = $this->security->xss_clean($this->input->post('nric'));
            $gender = $this->security->xss_clean($this->input->post('gender'));
            $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
            $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
            $religion = $this->security->xss_clean($this->input->post('religion'));
            $nationality = $this->security->xss_clean($this->input->post('nationality'));
            $id_race = $this->security->xss_clean($this->input->post('id_race'));
           
            $salutationInfo = $this->student_model->getSalutation($salutation);

            $data = array(

                'salutation' => $salutation,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'full_name' => $salutationInfo->name . ". " . $first_name." ".$last_name,
                'phone' => $phone,
                'email_id' => $email_id,
                'contact_email' => $contact_email,
                'password' => $password,
                'nric' => $nric,
                'passport' => $passport,
                'gender' => $gender,
                'date_of_birth' => date("Y-m-d", strtotime($date_of_birth)),
                'martial_status' => $martial_status,
                'religion' => $religion,
                // 'id_university' => $id_partner_university,
                'is_hostel' => 1,
                'nationality' => $nationality,
                'id_race' => $id_race,
                
            );

            $result = $this->student_model->editStudentDetails($data,$id);
            redirect('/partner_university/student/step2/'.$id);
        }


        $data['getApplicantDetails'] = $this->student_model->getStudentDetailsById($id);

        if($data['getApplicantDetails']->applicant_status != '')
        {
            redirect('/partner_university/student/view/'.$id);
        }

        $data['intakeList'] = $this->student_model->intakeList();
        $data['programList'] = $this->student_model->programList();
        $data['countryList'] = $this->student_model->countryListByStatus('1');
        $data['stateList'] = $this->student_model->stateList();
        $data['degreeTypeList'] = $this->student_model->qualificationListByStatus('1');
        $data['raceList'] = $this->student_model->raceListByStatus('1');
        $data['religionList'] = $this->student_model->religionListByStatus('1');
        $data['degreeTypeList'] = $this->student_model->qualificationListByStatus('1');
        $data['salutationList'] = $this->student_model->salutationListByStatus('1'); 
 
        $this->global['pageTitle'] = 'Partner University Portal : Edit Applicant';            
        $this->loadViews("student/step1", $this->global, $data, NULL);
    }
    function step2($id)
    {
        $id_partner_university = $this->session->id_partner_university;

        if($this->input->post())
        {
                      
            
            $mail_address1 = $this->security->xss_clean($this->input->post('mail_address1'));
            $mail_address2 = $this->security->xss_clean($this->input->post('mail_address2'));
            $mailing_country = $this->security->xss_clean($this->input->post('mailing_country'));
            $mailing_state = $this->security->xss_clean($this->input->post('mailing_state'));
            $mailing_city = $this->security->xss_clean($this->input->post('mailing_city'));
            $mailing_zipcode = $this->security->xss_clean($this->input->post('mailing_zipcode'));
            $permanent_address1 = $this->security->xss_clean($this->input->post('permanent_address1'));
            $permanent_address2 = $this->security->xss_clean($this->input->post('permanent_address2'));
            $permanent_country = $this->security->xss_clean($this->input->post('permanent_country'));
            $permanent_state = $this->security->xss_clean($this->input->post('permanent_state'));
            $permanent_city = $this->security->xss_clean($this->input->post('permanent_city'));
            $permanent_zipcode = $this->security->xss_clean($this->input->post('permanent_zipcode'));
            $salutationInfo = $this->student_model->getSalutation($salutation);


            $data = array(

               
                'mail_address1' => $mail_address1,
                'mail_address2' => $mail_address2,
                'mailing_country' => $mailing_country,
                'mailing_state' => $mailing_state,
                'mailing_city' => $mailing_city,
                'mailing_zipcode' => $mailing_zipcode,
                'permanent_address1' => $permanent_address1,
                'permanent_address2' => $permanent_address2,
                'permanent_country' => $permanent_country,
                'permanent_state' => $permanent_state,
                'permanent_city' => $permanent_city,
                'permanent_zipcode' => $permanent_zipcode
            );

            $result = $this->student_model->editStudentDetails($data,$id);
            redirect('/partner_university/student/step3/'.$id);
        }

        $data['getApplicantDetails'] = $this->student_model->getStudentDetailsById($id);

        if($data['getApplicantDetails']->applicant_status != '')
        {
            redirect('/partner_university/student/view/'.$id);
        }


        $data['intakeList'] = $this->student_model->intakeList();
        $data['programList'] = $this->student_model->programList();
        $data['countryList'] = $this->student_model->countryListByStatus('1');
        $data['stateList'] = $this->student_model->stateList();
        $data['degreeTypeList'] = $this->student_model->qualificationListByStatus('1');
        $data['raceList'] = $this->student_model->raceListByStatus('1');
        $data['religionList'] = $this->student_model->religionListByStatus('1');
        $data['degreeTypeList'] = $this->student_model->qualificationListByStatus('1');
        $data['salutationList'] = $this->student_model->salutationListByStatus('1');


        $this->global['pageTitle'] = 'Partner University Portal : Edit Student';
        $this->loadViews("student/step2", $this->global, $data, NULL);
    }

    function step3($id)
    {
        $id_partner_university = $this->session->id_partner_university;

        $data['getApplicantDetails'] = $this->student_model->getStudentDetailsById($id);

        if($data['getApplicantDetails']->applicant_status != '')
        {
            redirect('/partner_university/student/view/'.$id);
        }

        $data['intakeList'] = $this->student_model->intakeList();
        $data['programList'] = $this->student_model->programList();
        $data['countryList'] = $this->student_model->countryListByStatus('1');
        $data['stateList'] = $this->student_model->stateList();
        $data['degreeTypeList'] = $this->student_model->qualificationListByStatus('1');
        $data['raceList'] = $this->student_model->raceListByStatus('1');
        $data['religionList'] = $this->student_model->religionListByStatus('1');
        $data['degreeTypeList'] = $this->student_model->qualificationListByStatus('1');
        $data['salutationList'] = $this->student_model->salutationListByStatus('1');  
        $data['partnerUniversityList'] = $this->student_model->getUniversityListByStatus('1'); 


       if($this->input->post())
        {
            $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
            $id_program = $this->security->xss_clean($this->input->post('id_program'));
            $id_program_scheme = $this->security->xss_clean($this->input->post('id_program_scheme'));
            $id_program_has_scheme = $this->security->xss_clean($this->input->post('id_program_has_scheme'));
            $id_degree_type = $this->security->xss_clean($this->input->post('id_degree_type'));
            $id_branch = $this->security->xss_clean($this->input->post('id_branch'));
            $id_university = $this->security->xss_clean($this->input->post('id_university'));

            $data = array(
                'id_program' => $id_program,
                'id_intake' => $id_intake,
                'id_degree_type' => $id_degree_type,
                'id_program_scheme' => $id_program_scheme,
                'id_program_has_scheme' => $id_program_has_scheme,
                // 'id_university' => $id_university,
                'id_branch' => $id_branch
            );
            $result = $this->student_model->editStudentDetails($data,$id);
            redirect('/partner_university/student/step4/'.$id);
        }

        // echo "<Pre>";print_r($data['getApplicantDetails']);exit;
        
        $this->global['pageTitle'] = 'Partner University Portal : Edit Applicant';  
        $this->loadViews("student/step3", $this->global, $data, NULL);
    }

    function step4($id)
    {
        $id_partner_university = $this->session->id_partner_university;

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($this->input->post());exit();
            if($_POST['fileid'] != '')
            {
            for($f=0;$f<count($_POST['fileid']);$f++)
            {
                // echo "<Pre>";// print_r($_POST[$f]);exit();

                if($_FILES['file']['name'][$f])
                {
                    $certificate_name = $_FILES['file']['name'][$f];
                    $certificate_size = $_FILES['file']['size'][$f];
                    $certificate_tmp =$_FILES['file']['tmp_name'][$f];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Certificate');

                    $file = $this->uploadFile($certificate_name,$certificate_tmp,'Certificate');



                    $fileArray = array();

                    if($file)
                    {
                        $fileArray['file'] = $file;
                    }
                    $fileArray['id_document'] = $_POST['fileid'][$f];
                    $fileArray['file_name'] = $_FILES['file']['name'][$f];
                    $fileArray['id_student'] = $id;
                    $result = $this->student_model->addFileDownload($fileArray);
                }
            }

            redirect('/partner_university/student/step6/'.$id);
                
            }
        }

        $data['getApplicantDetails'] = $this->student_model->getStudentDetailsById($id);

        if($data['getApplicantDetails']->applicant_status != '')
        {
            redirect('/partner_university/student/view/'.$id);
        }

        $data['studentUploadedFiles'] = $this->student_model->getStudentUploadedFiles($id);

        // echo "<Pre>";print_r($data['studentUploadedFiles']);exit();
        $this->global['pageTitle'] = 'Partner University Portal : Edit Student';
        $this->loadViews("student/step4", $this->global, $data, NULL);
    }


    function step5($id)
    {
        $id_partner_university = $this->session->id_partner_university;


        if($this->input->post())
        {
            $sibbling_discount = $this->security->xss_clean($this->input->post('sibbling_discount'));
            $alumni_discount = $this->security->xss_clean($this->input->post('alumni_discount'));
            $employee_discount = $this->security->xss_clean($this->input->post('employee_discount'));
            $data = array(
                'sibbling_discount' => $sibbling_discount,
                'employee_discount' => $employee_discount,
                'alumni_discount' => $alumni_discount
            );

             if($sibbling_discount == 'Yes')
            {
                $data['is_sibbling_discount'] = '0';
            }
            elseif($sibbling_discount == 'No')
            {
                $data['is_sibbling_discount'] = '3';
            }

            if($employee_discount == 'Yes')
            {
                $data['is_employee_discount'] = '0';
            }
            elseif($employee_discount == 'No')
            {
                $data['is_sibbling_discount'] = '3';
            }


            if($alumni_discount == 'Yes')
            {
                $data['is_alumni_discount'] = '0';
            }
            elseif($alumni_discount == 'No')
            {
                $data['is_alumni_discount'] = '3';
            }

            $sibbling_name = $this->security->xss_clean($this->input->post('sibbling_name'));
            $sibbling_nric = $this->security->xss_clean($this->input->post('sibbling_nric'));

                 $sibbileData = array(
                    'id_applicant' => $id,
                    'sibbling_name' => $sibbling_name,
                    'sibbling_nric' => $sibbling_nric,
                );

            $checkSibblingDicount = $this->student_model->getSibblingDiscountByApplicantId($id);

            if($checkSibblingDicount)
            {
                $result = $this->student_model->editSibblingDetails($sibbileData, $id);
            }
            else
            {
                // $sibbileData['id_applicant'] = $id;
                if($sibbling_discount == 'Yes')
                {
                    $result = $this->student_model->addNewSibblingDiscount($sibbileData);
                }
            }

                

            $employee_name = $this->security->xss_clean($this->input->post('employee_name'));
            $employee_nric = $this->security->xss_clean($this->input->post('employee_nric'));
            $employee_designation = $this->security->xss_clean($this->input->post('employee_designation'));

            $employData = array(
                    'id_applicant' => $id,
                    'employee_name' => $employee_name,
                    'employee_nric' => $employee_nric,
                    'employee_designation' => $employee_designation
                );
            
            $checkEmployeeDicount = $this->student_model->getEmployeeDiscountApplicantId($id);

            if($checkEmployeeDicount)
            {
                $result = $this->student_model->editEmployeeDetails($employData, $id);
            }
            else
            {
                if($employee_discount == 'Yes')
                {

                // $employData['id_applicant'] = $id;
                    $result = $this->student_model->addNewEmployeeDiscount($employData);
                }
            }




            $alumni_name = $this->security->xss_clean($this->input->post('alumni_name'));
            $alumni_nric = $this->security->xss_clean($this->input->post('alumni_nric'));
            $alumni_email = $this->security->xss_clean($this->input->post('alumni_email'));

            $alumniData = array(
                    'id_applicant' => $id,
                    'alumni_name' => $alumni_name,
                    'alumni_nric' => $alumni_nric,
                    'alumni_email' => $alumni_email
                );
            

            $checkAlumniDicount = $this->student_model->getAlumniDiscountApplicantId($id);

            // echo "<Pre>"; print_r($checkAlumniDicount);exit;
            if($checkAlumniDicount)
            {
                $result = $this->student_model->editAlumniDetails($alumniData, $id);
            }
            else
            {
                if($alumni_discount == 'Yes')
                {

                // $employData['id_applicant'] = $id;
                    $result = $this->student_model->addNewAlumniDiscount($alumniData);
                }
            }


            // $result = $this->student_model->editEmployeeDetails($employData, $id);

            $result = $this->student_model->editStudentDetails($data,$id);
            redirect('/partner_university/student/step6/'.$id);

        }

            $data['sibblingDiscountDetails'] = $this->student_model->getSibblingDiscountByApplicantId($id);

            if(!empty($data['sibblingDiscountDetails']))
            {
                if($data['sibblingDiscountDetails']->sibbling_status != 'Pending')
                {
                    $data['sibblingDiscountDetails'] = $this->student_model->getApplicantSibblingDiscountDetails($id);
                }
            }
            $data['employeeDiscountDetails'] = $this->student_model->getEmployeeDiscountApplicantId($id);
            if(!empty($data['employeeDiscountDetails']))
            {

                if($data['employeeDiscountDetails']->employee_status != 'Pending')
                {
                    $data['employeeDiscountDetails'] = $this->student_model->getApplicantEmployeeDiscountDetails($id);
                }
            }
            $data['alumniDiscountDetails'] = $this->student_model->getAlumniDiscountApplicantId($id);
            if(!empty($data['alumniDiscountDetails']))
            {

                if($data['alumniDiscountDetails']->alumni_status != 'Pending')
                {
                    $data['alumniDiscountDetails'] = $this->student_model->getApplicantAlumniDiscountDetails($id);
                }
            }

         $data['getApplicantDetails'] = $this->student_model->getStudentDetailsById($id);

        if($data['getApplicantDetails']->applicant_status == 'Draft' || $data['getApplicantDetails']->is_submitted == 1)
        {
            redirect('/partner_university/student/view/'.$id);
        }

         // echo "<Pre>";print_r($data['getApplicantDetails']);exit();

        $this->global['pageTitle'] = 'Partner University Portal : Edit Student';
        $this->loadViews("student/step5", $this->global, $data, NULL);
    }

    function step6($id)
    {
        $id_partner_university = $this->session->id_partner_university;

        if($this->input->post())
        {
            $is_submitted = $this->security->xss_clean($this->input->post('is_submitted'));
            $entry = $this->security->xss_clean($this->input->post('entry'));

            // $data['is_submitted'] = $is_submitted;

            $data['pathway'] = 'DIRECT ENTRY';
            
            if($entry == 1)
            {
                $data['pathway'] = 'APEL';
            }

            $data['id_program_requirement'] = $entry;


            if($is_submitted == '1')
            {
                $applicant_data = $this->student_model->getStudentDetailsById($id);

                $id_program_scheme = $applicant_data->id_program_scheme;
                $id_program_has_scheme = $applicant_data->id_program_has_scheme;
                $id_intake = $applicant_data->id_intake;
                $id_program = $applicant_data->id_program;


                $get_program_scheme = $this->student_model->getProgramScheme($id_program_scheme);
                $program_landscape = $this->student_model->getProgramLandscape($id_intake, $id_program, $id_program_scheme,$id_program_has_scheme);


                $data['program_scheme'] = $get_program_scheme->mode_of_program . " - " . $get_program_scheme->mode_of_study;
                $data['id_program_landscape'] = $program_landscape->id;

                // $data['submitted_date'] = date('Y-m-d H:i:s');
                $data['applicant_status'] = 'Approved';
                
                $data['id_program_requirement'] = $entry;
            }

            // echo "<Pre>";print_r($data);exit();


            
            $result = $this->student_model->editStudentDetails($data,$id);
            redirect('/partner_university/student/view/' . $id);
            // $this->view();
        }

        $data['getApplicantDetails'] = $this->student_model->getStudentDetailsById($id);

        if($data['getApplicantDetails']->applicant_status != '')
        {
            redirect('/partner_university/student/view/'.$id);
        }

        
        $this->global['pageTitle'] = 'Partner University Portal : Student Submission Form';
        $this->loadViews("student/step6", $this->global, $data, NULL);
    }

    function chat()
    {
        $id = $this->session->id_applicant;


        $data['applicantDetails'] = $this->student_model->getStudentDetailsById($id);


        $this->global['pageTitle'] = 'Partner University Portal : Change Password';
        $this->loadViews("student/chat", $this->global, $data, NULL);
    }

    function changePasssword()
    {
        $id = $this->session->id_applicant;


        if($this->input->post())
        {
            $password = $this->security->xss_clean($this->input->post('password'));

            $data = array(
                    'password' => md5($password)
                );

            $result = $this->student_model->editApplicantDetails($data,$id);
        }

        $data['applicantDetails'] = $this->student_model->getStudentDetailsById($id);


        $this->global['pageTitle'] = 'Partner University Portal : Change Password';
        $this->loadViews("student/change_password", $this->global, $data, NULL);
    }

    function view($id)
    {
        $id_partner_university = $this->session->id_partner_university;

        $data['getApplicantDetails'] = $this->student_model->getStudentDetailsById($id);

        
        $data['intakeList'] = $this->student_model->intakeList();
        $data['programList'] = $this->student_model->programList();
        $data['nationalityList'] = $this->student_model->nationalityList();
        $data['countryList'] = $this->student_model->countryListByStatus('1');
        $data['stateList'] = $this->student_model->stateList();
        $data['degreeTypeList'] = $this->student_model->qualificationList();
        $data['raceList'] = $this->student_model->raceListByStatus('1');
        $data['salutationList'] = $this->student_model->salutationListByStatus('1');
        $data['religionList'] = $this->student_model->religionListByStatus('1');
        $data['branchList'] = $this->student_model->branchListByStatus();
        $data['requiremntListList'] = $this->student_model->programRequiremntListList();
        $data['schemeList'] = $this->student_model->schemeListByStatus('1');
        
        $data['receiptStatus'] = $this->student_model->getReceiptStatus($id);
        $data['temp_offer_letter'] = $this->student_model->getTemperoryOfferLetterByIntake($data['getApplicantDetails']->id_intake);
        $data['programEntryRequirementList'] = $this->student_model->programEntryRequirementList($data['getApplicantDetails']->id_program);
        $data['getProgramDetails'] = $this->student_model->getProgramDetails($data['getApplicantDetails']->id_program);

        $data['programDetails'] = $this->student_model->programDetails($data['getApplicantDetails']->id_program);

        if($data['getApplicantDetails']->added_by_partner_university > 0)
        {
            $data['studentUploadedFiles'] = $this->student_model->getStudentUploadedFiles($id);
        }
        else
        {
            $data['studentUploadedFiles'] = $this->student_model->getApplicantUploadedFiles($data['getApplicantDetails']->id_applicant);
        }

        // echo "<Pre>";print_r($data['getApplicantDetails']);exit;
        $this->global['pageTitle'] = 'Partner University Portal : View Applicant';
        $this->loadViews("student/view", $this->global, $data, NULL);
    }

    function generateTempOfferLetter($id)
    {
        // echo $id;exit();
        $base_url = $_SERVER['HTTP_HOST'];
         // $id = $this->session->id_applicant;


        $this->getMpdfLibrary();


        $applicant_information = $this->student_model->getStudentInformation($id);


         $organisation = $this->student_model->getOrganisation();



        $applicantNAme = $applicant_information->full_name;
        $program_name = $applicant_information->program_name;
        $intake_name = $applicant_information->intake_name;
        $nric = $applicant_information->nric;

        $mail_address1 = $applicant_information->mail_address1;
        $mail_address2 = $applicant_information->mail_address2;
        $mailing_city = $applicant_information->mailing_city;
        $mailing_zipcode = $applicant_information->mailing_zipcode;
        $nric = $applicant_information->nric;
        $created_dt_tm = $applicant_information->created_dt_tm;


        $mode_of_program = $applicant_information->mode_of_program;
        $mode_of_study = $applicant_information->mode_of_study;

        $branchname = $applicant_information->branchname;

        $id_branch = $applicant_information->id_branch;

        if($id_branch == 1)
        {
            $branchname = $organisation->short_name . " - " . $organisation->name;
        }




            $templateResult = $this->student_model->gettemplate('1');
            $message = $templateResult->message;


        
                     $message = str_replace("@studentname",$applicantNAme,$message);
                     $message = str_replace("@program",$program_name,$message);
                     $message = str_replace("@nric",$nric,$message);
                     $message = str_replace("@intake",$intake_name,$message);

                     $message = str_replace("@mail_address1",$mail_address1,$message);
                     $message = str_replace("@mail_address2",$mail_address2,$message);
                     $message = str_replace("@mailing_city",$mailing_city,$message);
                     $message = str_replace("@mailing_zipcode",$mailing_zipcode,$message);


                     $message = str_replace("@mode_of_program",$mode_of_program,$message);
                     $message = str_replace("@mode_of_study",$mode_of_study,$message);


                     $message = str_replace("@branchname",$branchname,$message);
                      $message = str_replace("@created_dt_tm",$mailing_zipcode,$message);

                     // print_r($message);exit;


                //include("/var/www/html/college/assets/mpdf/vendor/autoload.php");
                //  require_once __DIR__ . '/vendor/autoload.php';
            $mpdf=new \Mpdf\Mpdf();

            $mpdf->SetHeader("<table>
                              <tr>
                        <td>Partner University Portal
                               </td>
                              </tr>
                              </table>");

            $mpdf->SetFooter('<div>Partner University Portal</div>');
            // echo $file_data;exit;

            $file_data = $message;

            $mpdf->WriteHTML($file_data);

            $mpdf->Output('OFFER_LETTER_' . $applicantNAme . '_' . $nric .'.pdf', 'D');
            exit;
    }

    function submitApplication()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;

        if($this->input->post())
        {
            $is_submitted = $this->security->xss_clean($this->input->post('is_submitted'));

            $data = array(
                'is_submitted' => 0,
                'updated_by' => $id_user
            );

            $result = $this->student_model->editApplicantDetails($data,$id);
        }

        redirect('/partner_university/applicant/edit');
    }

    function editProgram()
    {
        $id = $this->session->id_applicant;
        $id_user = $this->session->userId;

        if($this->input->post())
        {
            $formData = $this->input->post();

           
            $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
            $id_program = $this->security->xss_clean($this->input->post('id_program'));

            $data = array(
                'id_program' => $id_program,
                'id_intake' => $id_intake,
                'updated_by' => $id_user
            );
            $result = $this->student_model->editApplicantDetails($data,$id);
            redirect('/partner_university/applicant/edit');
        }
        $data['getApplicantDetails'] = $this->student_model->getApplicant($id);
        if($data['getApplicantDetails']->applicant_status != 'Draft' || $data['getApplicantDetails']->is_submitted == 1)
        {
            $this->view();
        }
        else
        {
            $data['intakeList'] = $this->student_model->intakeList();
            $data['programList'] = $this->student_model->programList();        
            $this->global['pageTitle'] = 'Partner University Portal : View Applicant';
            $this->loadViews("student/edit_program", $this->global, $data, NULL);
            // echo "<Pre>";print_r($data['getApplicantDetails']);exit;
        }
    }

    

    function getStateByCountry($id_country)
    {
            $results = $this->student_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>
                     
                 </script>
         ";

            $table.="
            <select name='mailing_state' id='mailing_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function getStateByCountryPermanent($id_country)
    {
            $results = $this->student_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
        <script type='text/javascript'>
             
         </script>
         ";

            $table.="
            <select name='permanent_state' id='permanent_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function getIntakeByProgramme($id_programme)
    {
            $intake_data = $this->student_model->getIntakeByProgrammeId($id_programme);

            // echo "<Pre>"; print_r($intake_data);exit;
             $table="
            <script type='text/javascript'>

            $('select').select2();

                
            </script>


            <select name='id_intake' id='id_intake' class='form-control' onchange='checkFeeStructure()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $year = $intake_data[$i]->year;

            $table.="<option value=".$id.">".$year . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;       
    }

    function getProgramSchemeByProgramId($id_program)
    {
         $intake_data = $this->student_model->getProgramSchemeByProgramId($id_program);

            // echo "<Pre>"; print_r($intake_data);exit;
             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_program_scheme' id='id_program_scheme' class='form-control' onchange='checkFeeStructure()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $mode_of_program = $intake_data[$i]->mode_of_program;
            $mode_of_study = $intake_data[$i]->mode_of_study;

            $table.="<option value=".$id.">". $mode_of_program . " - " .  $mode_of_study .
                    "</option>";

            }
            $table.="</select>";

            echo $table;  
    }

    function getSchemeByProgramId($id_program)
    {
            // echo "<Pre>"; print_r($id_program);exit;
        // It's Actual Scheme What We Required
         $intake_data = $this->student_model->getSchemeByProgramId($id_program);

            // echo "<Pre>"; print_r($intake_data);exit;
             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_program_has_scheme' id='id_program_has_scheme' class='form-control' onchange='checkFeeStructure()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $code = $intake_data[$i]->code;
            $name = $intake_data[$i]->description;

            $table.="<option value=".$id.">". $code . " - " .  $name .
                    "</option>";

            }
            $table.="</select>";

            echo $table;  
    }

    function getDocumentByProgramme($id_programme)
    {
            // echo "<Pre>"; print_r($id_programme);exit;
            $intake_data = $this->student_model->getDocumentByProgrammeId($id_programme);
            // echo "<Pre>"; print_r($intake_data);exit;


            $table="<div class='row'>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;

            $table.="<div class='col-sm-3'>
                      <label>$name <span class='error-text'>*</span></label>
                      <input type='file' name='file[]'>
                      <input type='hidden' name='fileid[]' value='$id' />
                     </div>";
            }
            $table.="</div>";

            echo $table;
    }


    function getBranchByProgram($id_programme)
    {
            // echo "<Pre>"; print_r($id_programme);exit;
            $intake_data = $this->student_model->getBranchByProgramId($id_programme);
            

            // echo "<Pre>"; print_r($intake_data);exit;

             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_branch' id='id_branch' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $code = $intake_data[$i]->code;

            $table.="<option value=".$id.">". $code . " - " .  $name .
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }


    function getBranchesByPartnerUniversity($id_partner_university)
    {
            // echo "<Pre>"; print_r($id_programme);exit;
            $intake_data = $this->student_model->getBranchesByPartnerUniversity($id_partner_university);
            

            // echo "<Pre>"; print_r($intake_data);exit;

             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_branch' id='id_branch' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $code = $intake_data[$i]->code;

            $table.="<option value=".$id.">". $code . " - " .  $name .
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getIntakeDetails($id_intake)
    {
        $intake_data = $this->student_model->getIntakeDetails($id_intake);
        // echo "<Pre>"; print_r($intake_data);exit;

        $is_alumni_discount = $intake_data->is_alumni_discount;
        $is_employee_discount = $intake_data->is_employee_discount;
        $is_sibbling_discount = $intake_data->is_sibbling_discount;


        $table = "

        <input type='hidden' name='is_intake_alumni_discount' id='is_intake_alumni_discount' value='$is_alumni_discount' />
        <input type='hidden' name='is_intake_employee_discount' id='is_intake_employee_discount' value='$is_employee_discount' />
        <input type='hidden' name='is_intake_sibbling_discount' id='is_intake_sibbling_discount' value='$is_sibbling_discount' />";
        
        echo $table;


    }


    function getIndividualEntryRequirement($id_program)
    {
        $programEntryRequirementList = $this->student_model->programEntryRequirementList($id_program);
        $programDetails = $this->student_model->programDetails($id_program);


        $table = '';


        
        // echo "<Pre>"; print_r($table);exit;

        if(!empty($programEntryRequirementList))
        {
            $table.= '
             <div class="form-container">
                        <h4 class="form-group-title">Program Requirement Details</h4>

                    

                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                 <th>Requirement</th>
                                </tr>
                            </thead>
                            <tbody>';


                          if($programDetails->is_apel == 1)
                            {
                            $table.= "
                             <tr>
                                <td><input type='radio' name='entry' value=1> Appel Entry</td>
                                 </tr>
                            ";

                            }


                             $total = 0;
                              for($i=0;$i<count($programEntryRequirementList);$i++)
                             { 
                                $data="";
                                $j = $i+1;

                                if($programEntryRequirementList[$i]->age == 1)
                                {
                                    $data.= "Age Min. " . $programEntryRequirementList[$i]->min_age . " Year, Max. " . $programEntryRequirementList[$i]->max_age . " Year" ;
                                    $and = ", AND ";
                                }
                                if($programEntryRequirementList[$i]->education == 1)
                                {
                                    $data.= $and . "Education Requirement is : " 
                                    // . $programEntryRequirementList[$i]->qualification_code  .  " - "
                                     . $programEntryRequirementList[$i]->qualification_name ;
                                    $and = " ,  AND ";

                                }
                                if($programEntryRequirementList[$i]->work_experience == 1)
                                {
                                    $data.= $and . "Work Experience is : " . $programEntryRequirementList[$i]->work_code . " - " . $programEntryRequirementList[$i]->work_name . ", With Min. Experience Of " . $programEntryRequirementList[$i]->min_work_experience . " Years";
                                    $and = " ,  AND ";
                                    
                                }
                                if($programEntryRequirementList[$i]->other == 1)
                                {
                                    $data.= $and . "Other Requirements Description : " . $programEntryRequirementList[$i]->other_description. " .";
                                }

                                $identry = $programEntryRequirementList[$i]->id;

                            // print_r($identry);exit;



                                $table.="
                                <tr>
                                <td><input type='radio' name='entry' value='$identry'> $data</td>
                                 </tr>";


                          } 
                        $table.='
                            </tbody>
                        </table>
                      </div>

                    </div> ';

        }else
        {
            $table = '<p> No Requirements Available</p>';
        }

        print_r($table);exit;


    }


    function checkFeeStructure()
    {
        $id_session = $this->session->session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $result = $this->student_model->checkFeeStructure($tempData);
        
        echo $result;exit;
    }

    function deleteStudentUploadedDocument($id)
    {
        $deleted = $this->student_model->deleteStudentUploadedDocument($id);
        echo "success";exit;
    }
}