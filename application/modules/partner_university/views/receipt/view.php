<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Receipt</h3>
            <a href="list" class="btn btn-link btn-back">‹ Back</a>
        </div>


        
       

            <div class="form-container">
                <h4 class="form-group-title">Receipt Details</h4>             
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Receipt Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="invoice_number" name="invoice_number" value="REC00001/2020" readonly="readonly">
                        </div>
                    </div>

                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Receipt Type</label>
                            <input type="text" class="form-control" id="remarks" name="remarks" value="Partner University" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Remarks</label>
                            <input type="text" class="form-control" id="remarks" name="remarks" value="Added Partner Uni. Receipt" readonly="readonly">
                        </div>
                    </div>
                    
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Receipt Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_amount" name="total_amount" value="10000" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Status <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="remarks" name="remarks" value="<?php 
                                echo 'Pending';
                            ?>" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>University Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="remarks" name="remarks" value="<?php 
                                echo $partner_university_name;
                            ?>" readonly="readonly">
                        </div>
                    </div>


                </div>

                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>University Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="remarks" name="remarks" value="<?php 
                                echo $partner_university_code;
                            ?>" readonly="readonly">
                        </div>
                    </div>
                </div>


                </div>




             

                        <div class="form-container">
                            <h4 class="form-group-title">Invoice Details</h4>  

                            <div class="custom-table">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Sl. No</th>
                                        <th>Invoice Number</th>
                                        <th>Invoice Amount</th>
                                        <th>Paid Amount</th>
                                        <th>Balance Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>INV0001/2020</td>
                                            <td>1000</td>
                                            <td>980</td>
                                            <td>20</td>
                                        </tr>
                                         <tr>
                                            <td>1</td>
                                            <td>INV0001/2020</td>
                                            <td>1000</td>
                                            <td>980</td>
                                            <td>20</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>INV0001/2020</td>
                                            <td>1000</td>
                                            <td>980</td>
                                            <td>20</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>INV0001/2020</td>
                                            <td>1000</td>
                                            <td>980</td>
                                            <td>20</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>INV0001/2020</td>
                                            <td>1000</td>
                                            <td>980</td>
                                            <td>20</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>INV0001/2020</td>
                                            <td>1000</td>
                                            <td>980</td>
                                            <td>20</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>INV0001/2020</td>
                                            <td>1000</td>
                                            <td>980</td>
                                            <td>20</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>




        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


<script>

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                status: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }
</script>
