<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Permission extends BaseController
{
   
    public function __construct()
    {
        parent::__construct();
        $this->load->model('permission_model');
        $this->isScholarLoggedIn();   
    }
    
    
   
    function list()
    {
        if ($this->checkScholarAccess('permission.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
 
            $data['searchParam'] = $formData;
            $data['permissionList'] = $this->permission_model->permissionListSearch($formData);
            $this->global['pageTitle'] = 'Scholarship Management System : Permission List';
            $this->loadViews("permission/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('permission.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $description = $this->security->xss_clean($this->input->post('description'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'description' => $description,
                    'code' => $code,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->permission_model->addNewPermission($data);
                redirect('/scholarship/permission/edit/'.$result);
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add Permission';
            $this->loadViews("permission/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('permission.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/permission/list');
            }
            if($this->input->post())
            {
                $description = $this->security->xss_clean($this->input->post('description'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'description' => $description,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->permission_model->editPermission($data,$id);
                redirect('/scholarship/permission/list');
            }
            $data['permission'] = $this->permission_model->getPermission($id);
            $data['rolesList'] = $this->permission_model->rolesList($id);
            $data['rolePermissionList'] = $this->permission_model->rolePermissionListByPermissionId($id);


            $this->global['pageTitle'] = 'Scholarship Management System : Edit Permission';
            $this->loadViews("permission/edit", $this->global, $data, NULL);
        }
    }


   
    function delete()
    {
        if($this->checkScholarAccess('permission.delete') == 1)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $permissionId = $this->input->post('permissionId');
            $permissionInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->permission_model->deletePermission($permissionId, $permissionInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }


    function addRolesToPermission()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();

        $programs = $tempData['id_role'];

        if(count($programs) > 0)
        {
            foreach ($programs as $program)
            {
                $data['id_permission'] = $tempData['id_permission'];
                $data['id_role'] = $program;
                $inserted_id = $this->permission_model->addRolesToPermission($data);
            }
        }

        if($required_inserted_id)
        {
            echo "success";
        }
    }

    function deleteRoleFromPermission($id)
    {
        $deleted = $this->permission_model->deleteRoleFromPermission($id);
        echo "Success";
    }   
}

?>