<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Program Syllabus</h3>
        </div>
        <form id="form_bank" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Program Syllabus Details</h4> 

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Thrust <span class='error-text'>*</span></label>
                            <select name="id_thrust" id="id_thrust" class="form-control" onchange="getSubThrust(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($thrustList))
                                {
                                    foreach ($thrustList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $programSyllabus->id_thrust)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->code . " - " . $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Sub-Thrust <span class='error-text'>*</span></label>
                            <select name="id_sub_thrust" id="id_sub_thrust" class="form-control" onchange="getSubThrust(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($subThrustList))
                                {
                                    foreach ($subThrustList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $programSyllabus->id_sub_thrust)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->scholarship_code . " - " . $record->scholarship_name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $programSyllabus->id_program)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->code . " - " . $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                </div>


                <div class="row">               


                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" <?php if($programSyllabus->status=='1') {
                                    echo "checked=checked";
                                };?>><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0" <?php if($programSyllabus->status=='0') {
                                    echo "checked=checked";
                                };?>>
                                <span class="check-radio"></span> In-Active
                                </label>                              
                            </div>                         
                    </div>
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
            
        </form>



        <div class="m-auto text-center">
            <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
        </div>
        <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#tab_one" class="nav-link border rounded text-center"
                            aria-controls="tab_one" aria-selected="true"
                            role="tab" data-toggle="tab">Syllabus Module</a>
                    </li>
                    
                </ul>

                
                <div class="tab-content offers-tab-content">

                <div role="tabpanel" class="tab-pane active" id="tab_one">
                        <div class="col-12 mt-4">




            <form id="form_detail_one" action="" method="post">

                <div class="form-container">
                    <h4 class="form-group-title"> Syllabus Module Details</h4>
                    

                    <div class="row">


                        <div class="col-sm-4">
                            <div class="forintake_has_programmem-group">
                                <label>Mudule Code <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="code" name="code" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="forintake_has_programmem-group">
                                <label>Mudule Name <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="name" name="name" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-sm-4" style="align-content: left">
                            <div class="form-group">
                                <label>Module Type <span class='error-text'>*</span></label>
                                <select name="type" id="type" class="form-control" style="width: 408px">
                                    <option value="">Select</option>
                                    <option value="Compulsary">Compulsary</option>
                                    <option value="Elective">Elective</option>
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="row">


                        <div class="col-sm-4">
                            <div class="forintake_has_programmem-group">
                                <label>Credit Hours <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="credit_hours" name="credit_hours" autocomplete="off">
                            </div>
                        </div>

                    </div>

                </div>

            </form>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="addSyllabusModule()">Add</button>
                </div>
            </div>


            <?php

                if(!empty($syllanusModuleList))
                {
                    ?>
                    <br>

                    <div class="form-container">
                            <h4 class="form-group-title">Syllabus Module List</h4>

                        

                          <div class="custom-table">
                            <table class="table">
                                <thead>
                                    <tr>
                                    <th>Sl. No</th>
                                     <th>Code</th>
                                     <th>Name</th>
                                     <th>Type</th>
                                     <th>Credit Hours</th>
                                     <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php
                                 $total = 0;
                                  for($i=0;$i<count($syllanusModuleList);$i++)
                                 { ?>
                                    <tr>
                                    <td><?php echo $i+1;?></td>
                                    <td><?php echo $syllanusModuleList[$i]->code;?></td>
                                    <td><?php echo $syllanusModuleList[$i]->name;?></td>
                                    <td><?php echo $syllanusModuleList[$i]->type;?></td>
                                    <td><?php echo $syllanusModuleList[$i]->credit_hours;?></td>
                                    
                                    <td class="text-center">
                                    <a onclick="deleteSyllabusModule(<?php echo $syllanusModuleList[$i]->id; ?>)">Delete</a>
                                    </td>

                                     </tr>
                                  <?php 
                              } 
                              ?>
                                </tbody>
                            </table>
                          </div>

                        </div>

                <?php
                
                }
                 ?>


                    
                                   



                </div> 
            
            </div>

        </div>


     </div>




    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>

    function deleteSyllabusModule(id)
    {
        $.get("/scholarship/programSyllabus/deleteSyllabusModule/"+id,
            function(data, status)
            {
                window.location.reload();
            }
            );
    }

    function addSyllabusModule()
    {
        if($('#form_detail_one').valid())
        {

        var tempPR = {};
        tempPR['code'] = $("#code").val();
        tempPR['name'] = $("#name").val();
        tempPR['credit_hours'] = $("#credit_hours").val();
        tempPR['type'] = $("#type").val();
        tempPR['id_program_syllabus'] = <?php echo $programSyllabus->id; ?>;

            $.ajax(
            {
               url: '/scholarship/programSyllabus/addSyllabusModule',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                // alert(result);
                // $('#myModal').modal('show');
                // $("#view_requirement_data").html(result);
                // $("#view").html(result);

                // location.reload();
                window.location.reload();

               }
            });
        }
    }



    $(document).ready(function() {
        $("#form_bank").validate({
            rules: {
                id_thrust: {
                    required: true
                },
                id_sub_thrust: {
                    required: true
                },
                id_program: {
                    required: true
                }
            },
            messages: {
                id_thrust: {
                    required: "<p class='error-text'>Select Thrust</p>",
                },
                id_sub_thrust: {
                    required: "<p class='error-text'>Select Sub-Thrust</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(document).ready(function() {
        $("#form_detail_one").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                },
                type: {
                    required: true
                },
                credit_hours: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                type: {
                    required: "<p class='error-text'>Select Module Type</p>",
                },
                credit_hours: {
                    required: "<p class='error-text'>Credit Hours Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $('select').select2();

</script>
