            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $scholar_name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <!-- <li><a href="/sponser/user/profile">Edit Profile</a></li> -->
                            <li><a href="/scholarship/role/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    <h4>User Management</h4>
                    <ul>
                        <li><a href="/scholarship/role/list">Roles</a></li>
                        <li><a href="/scholarship/user/list">User</a></li>
                        <li><a href="/scholarship/permission/list">Permissions</a></li>
                        <li><a href="/scholarship/salutation/list">Salutation</a></li>
                        <li><a href="/scholarship/accreditationCategory/list">Accreditation Category</a></li>
                        <li><a href="/scholarship/accreditationType/list">Accreditation Type</a></li>
                        <li><a href="/scholarship/salutation/list">Salutation</a></li>
                    </ul>
                    <h4>Demographic Setup</h4>
                    <ul>
                        <li><a href="/scholarship/race/list">Race</a></li>
                        <li><a href="/scholarship/religion/list">Religion</a></li>
                        <li><a href="/scholarship/maritalStatus/list">Marital Status</a></li>
                    </ul>
                    <h4>Organisation Setup</h4>
                    <ul>
                        <li><a href="/scholarship/department/list">Department</a></li>
                        <li><a href="/scholarship/organisation/edit">Organisation Setup</a></li>
                        <li><a href="/scholarship/partnerUniversity/edit">Program Partner Setup</a></li>
                    </ul>
                    <h4>Program Setup</h4>
                    <ul>
                        <li><a href="/scholarship/moduleType/list">Module Type Setup</a></li>
                        <li><a href="/scholarship/award/list">Award Setup</a></li>
                        <li><a href="/scholarship/assesmentMethod/list">Assessment Method Setup</a></li>
                        <li><a href="/scholarship/programme/list">Program Setup</a></li>
                        <li><a href="/scholarship/programPartner/list">Program Partner</a></li>
                        <li><a href="/scholarship/programSyllabus/list">Program Syllabus</a></li>
                        <li><a href="/scholarship/programAccreditation/list">Program Accreditation</a></li>
                        <li><a href="/scholarship/IndividualEntryRequirement/list">Individual Entry Requirement</a></li>
                        <li><a href="/scholarship/applicantInformation/programList">Applicant Program Requirements</a></li>
                    </ul>
                    <h4>Scholarship Setup</h4>
                    <ul>
                        <li><a href="/scholarship/workSpecialisation/list">Work Specialisation</a></li>
                        <li><a href="/scholarship/educationLevel/list">Education Level</a></li>
                        <li><a href="/scholarship/selectionType/list">Selection Type</a></li>
                        <li><a href="/scholarship/thrust/list">Scholarship Thrust</a></li>
                        <li><a href="/scholarship/subThrust/list">Scholarship Sub-Thrust</a></li>
                    </ul>
                    <h4>Documents Required</h4>
                    <ul>
                        <li><a href="/scholarship/documents/list">Documents</a></li>
                        <li><a href="/scholarship/documentsProgram/list">Documents For Program</a></li>
                    </ul>
                    <h4>Cohort</h4>
                    <ul>
                        <li><a href="/scholarship/scheme/list">Cohort</a></li>
                    </ul>
                    <h4>Selection</h4>
                    <ul>
                        <li><a href="/scholarship/scholarshipApplication/approvalList">Application Approval</a></li>
                        <li><a href="/scholarship/scholarshipApplication/list">Application Summary</a></li>
                    </ul>
                    <h4>Finance Setup</h4>
                    <ul>
                        <li><a href="/scholarship/bankRegistration/list">Bank Registration</a></li>
                        <li><a href="/scholarship/amountCalculationType/list">Amount Calculation Type</a></li>
                        <li><a href="/scholarship/frequencyMode/list">Frequency Mode</a></li>
                        <li><a href="/scholarship/paymentType/list">Payment Type</a></li>
                        <!-- <li><a href="/scholarship/feeCategory/list">Fee Category</a></li>
                        <li><a href="/scholarship/feeSetup/list">Fee Setup</a></li>
                        <li><a href="/scholarship/feeStructure/list">Fee Structure</a></li> -->
                    </ul>
                </div>
            </div>