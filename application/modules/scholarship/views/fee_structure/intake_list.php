<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Programme Details</h3>
     <a href="../list" class="btn btn-link btn-back">‹ Back</a>
    </div>
    <div class="form-container">
        <h4 class="form-group-title">Programme Details</h4>  
        <div class="row">

              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Programme Name</label>
                      <input type="text" class="form-control" id="name" name="name" value="<?php echo $programme->name; ?>" readonly="readonly">
                  </div>
              </div>

              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Programme Code</label>
                      <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $programme->code; ?>" readonly="readonly">
                  </div>
              </div>

              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Name Optional Language</label>
                      <input type="text" class="form-control" id="code" name="code" value="<?php echo $programme->name_optional_language; ?>" readonly="readonly">
                  </div>
              </div>
          </div>


        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Total Credit Hours</label>
                    <input type="text" class="form-control" id="name" name="name" value="<?php echo $programme->total_cr_hrs; ?>" readonly="readonly" >
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Foundation</label>
                    <input type="text" class="form-control" id="name" name="name" value="<?php echo $programme->foundation; ?>" readonly="readonly" >
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Created On</label>
                    <input type="text" class="form-control" id="name" name="name" value="<?php echo date("d-m-Y", strtotime($programme->created_dt_tm)); ?>" readonly="readonly" >
                </div>
            </div>
        </div>
      </div>


      <div class="form-container">
        <h4 class="form-group-title">Intake List</h4>       

      <div class="custom-table">
        <table class="table" id="list-table">
          <thead>
            <tr><th>Sl. No</th>
              <th>Name</th>
              <th>Year</th>
              <th>Name In Malay</th>
              <th>Semester Sequester</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Status</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if (!empty($intakeList)) {$i=1;
              foreach ($intakeList as $record) {
            ?>
                <tr><td><?php echo $i ?></td>
                  <td><?php echo $record->name ?></td>
                  <td><?php echo $record->year ?></td>
                  <td><?php echo $record->name_in_malay ?></td>
                  <td><?php echo $record->semester_sequence ?></td>
                  <td><?php echo date("d-m-Y", strtotime($record->start_date)) ?></td>
                  <td><?php echo date("d-m-Y", strtotime($record->end_date)) ?></td>
                  <td><?php if( $record->status == '1')
                  {
                    echo "Active";
                  }
                  else
                  {
                    echo "In-Active";
                  } 
                  ?></td>
                  <td class="text-center">
                    <a href="<?php echo '/finance/feeStructure/add/' . $record->id.'/'.$programme_id; ?>" title="Edit">Add / Edit</a>
                  </td>
                </tr>
            <?php
              $i++;
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
