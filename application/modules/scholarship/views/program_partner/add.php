<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Program Partner</h3>
        </div>
        <form id="form_bank" action="" method="post">
        <div class="form-container">
                <h4 class="form-group-title">Program Partner Details</h4> 


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Thrust <span class='error-text'>*</span></label>
                        <select name="id_thrust" id="id_thrust" class="form-control" onchange="getSubThrust(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($thrustList))
                            {
                                foreach ($thrustList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Sub-Thrust <span class='error-text'>*</span></label>
                        <span id='view_subthrust'></span>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_program" id="id_program" class="form-control" style="width: 408px" onchange="getPartnerProgram(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " -  ". $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Partner <span class='error-text'>*</span></label>
                        <span id='view_partner'></span>
                    </div>
                </div>

            

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Location <span class='error-text'>*</span></label>
                        <select name="id_location" id="id_location" class="form-control" style="width: 408px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($stateList))
                            {
                                foreach ($stateList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Internship (Month) <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="internship" name="internship">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Apprenticeship (Month) <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="apprenticeship" name="apprenticeship">
                    </div>
                </div>


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>

            </div>

            
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>

    function getSubThrust(id)
    {
        $.get("/scholarship/programPartner/getSubThrustByThrustId/"+id,
            function(data, status)
            {
                $("#view_subthrust").html(data);
            }
            );
    }

    function getPartnerProgram(id)
    {
        $.get("/scholarship/programPartner/getPartnerProgram/"+id,

            function(data, status)
            {
                $("#view_partner").html(data);
        });
    }



    $(document).ready(function() {
        $("#form_bank").validate({
            rules: {
                id_thrust: {
                    required: true
                },
                id_sub_thrust: {
                    required: true
                },
                id_program: {
                    required: true
                },
                id_partner_university: {
                    required: true
                },
                id_location: {
                    required: true
                },
                internship: {
                    required: true
                },
                apprenticeship: {
                    required: true
                }
            },
            messages: {
                id_thrust: {
                    required: "<p class='error-text'>Select Thrust</p>",
                },
                id_sub_thrust: {
                    required: "<p class='error-text'>Select Sub-Thrust</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_partner_university: {
                    required: "<p class='error-text'>Select Partner University</p>",
                },
                id_location: {
                    required: "<p class='error-text'>Select Location</p>",
                },
                internship: {
                    required: "<p class='error-text'>Internship Required</p>",
                },
                apprenticeship: {
                    required: "<p class='error-text'>Apprenticeship Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $('select').select2();

</script>