<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Permission</h3>
        </div>
        <form id="form_religion" action="" method="post">

         <div class="form-container">
            <h4 class="form-group-title">Permission Details</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $permission->code; ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Descriotion <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $permission->description; ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" <?php if($permission->status=='1') {
                             echo "checked=checked";
                          };?>><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0" <?php if($permission->status=='0') {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio"></span> In-Active
                        </label>                
                    </div>
                </div>

                

            </div>

        </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>


        <div class="m-auto text-center">
            <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#tab_one" class="nav-link border rounded text-center"
                            aria-controls="tab_one" aria-selected="true"
                            role="tab" data-toggle="tab">Add Roles To Permissions</a>
                    </li>                    
                </ul>

                
                <div class="tab-content offers-tab-content">

                <div role="tabpanel" class="tab-pane active" id="tab_one">
                        <div class="col-12 mt-4">




                <form id="form_program_one" action="" method="post">

                <div class="form-container">
                    <h4 class="form-group-title"> Roles Permission Details</h4>
                    

                    <div class="row">


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Roles <span class='error-text'>*</span></label>
                                <select name="id_role" id="id_role" class="form-control" style="width: 408px" multiple>
                                    <!-- <option value="">Select</option> -->
                                    <?php
                                    if (!empty($rolesList))
                                    {
                                        foreach ($rolesList as $record)
                                        {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->role;?>
                                    </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        

                    </div>

                </div>


                <div id="view">
                </div>



            </form>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="addRolesToPermission()">Add</button>
                </div>
            </div>


            <?php

                if(!empty($rolePermissionList))
                {
                    ?>
                    <br>

                    <div class="form-container">
                            <h4 class="form-group-title">Roles Permission List</h4>

                        

                          <div class="custom-table">
                            <table class="table">
                                <thead>
                                    <tr>
                                    <th>Sl. No</th>
                                     <th>Role</th>
                                     <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php
                                 $total = 0;
                                  for($i=0;$i<count($rolePermissionList);$i++)
                                 { ?>
                                    <tr>
                                    <td><?php echo $i+1;?></td>
                                    <td><?php echo $rolePermissionList[$i]->role;?></td>
                                    <!-- <td><?php echo $subThrustRequirementsList[$i]->program_name . " - " . $subThrustRequirementsList[$i]->min_duration_type;?></td>
                                    <td><?php echo date('d-m-Y', strtotime($partnerProgramStudyModeDetails[$i]->end_date));?></td> -->
                                    
                                    <td class="text-center">
                                    <a onclick="deleteRoleFromPermission(<?php echo $rolePermissionList[$i]->id; ?>)">Delete</a>
                                    </td>

                                     </tr>
                                  <?php 
                              } 
                              ?>
                                </tbody>
                            </table>
                          </div>

                        </div>

                <?php
                
                }
                 ?>


                    
                                   



                </div> 
            
            </div>








                </div>


            </div>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $('select').select2();


    function addRolesToPermission()
    {
        if($('#form_program_one').valid())
        {


        var tempPR = {};
        tempPR['id_role'] = $("#id_role").val();
        tempPR['id_permission'] = <?php echo $permission->id; ?>;

            $.ajax(
            {
               url: '/scholarship/permission/addRolesToPermission',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // location.reload();
                window.location.reload();

               }
            });
        }
    }


    function deleteRoleFromPermission(id) {
        // alert(id);
         $.ajax(
            {
               url: '/scholarship/permission/deleteRoleFromPermission/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // window.location.reload();
                    // alert(result);
                    location.reload();
               }
            });
    }



    $(document).ready(function() {
        $("#form_program_one").validate({
            rules: {
                id_role: {
                    required: true
                }
            },
            messages: {
                id_role: {
                    required: "<p class='error-text'>Select Role</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });





    $(document).ready(function() {
        $("#form_religion").validate({
            rules: {
                description: {
                    required: true
                },
                code: {
                    required: true
                }
            },
            messages: {
                description: {
                    required: "<p class='error-text'>Descriotion required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
