<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Program Partner List</h3>
      <a href="add" class="btn btn-primary">+ Add Program Partner</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
               Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">URL</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="url" value="<?php echo $searchParam['url']; ?>">
                      </div>
                    </div>
                  </div>

                </div>

                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Email</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="email" value="<?php echo $searchParam['email']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Contact Number</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="contact_number" value="<?php echo $searchParam['contact_number']; ?>">
                      </div>
                    </div>
                  </div>

                </div>


                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Country</label>
                      <div class="col-sm-8">
                        <select name="id_country" id="id_country" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($countryList)) {
                            foreach ($countryList as $record)
                            {
                              $selected = '';
                              if ($record->id == $searchParam['id_country']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->name;  ?>
                              </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>


                
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Name</th>
            <th>Short Name</th>
            <!-- <th>Name Other Language</th> -->
            <th>URL</th>
            <th>Email</th>
            <th>Country</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($partnerUniversityList))
          {
            $i=1;
            foreach ($partnerUniversityList as $record)
            {
             ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->name; ?></td>
                <td><?php echo $record->short_name; ?></td>
                <!-- <td><?php echo $record->name_optional_language; ?></td> -->
                <td><?php echo $record->url; ?></td>
                <td><?php echo $record->email; ?></td>
                <td><?php echo $record->country; ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <!-- <td><?php echo date("d-m-Y", strtotime($record->created_dt_tm)) ?></td> -->
                <td class="text-center">

                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">
                    Edit
                  </a>
                    <i class="fa fa-trash"></i>
                  </a>

                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    $('select').select2();
  
    function clearSearchForm()
    {
      window.location.reload();
    }
</script>