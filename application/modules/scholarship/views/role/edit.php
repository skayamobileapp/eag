<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Role</h3>
        </div>
        <form id="form_race" action="" method="post">

         <div class="form-container">
            <h4 class="form-group-title">Role Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Role <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="role" name="role" value="<?php echo $role->role; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" <?php if($role->status=='1') {
                             echo "checked=checked";
                          };?>><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0" <?php if($role->status=='0') {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio"></span> In-Active
                        </label>                
                    </div>
                </div>

            </div>

        </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_race").validate({
            rules: {
                role: {
                    required: true
                }
            },
            messages: {
                role: {
                    required: "<p class='error-text'>Role required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
