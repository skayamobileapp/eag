<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Organisation_model extends CI_Model
{

    function getOrganisation()
    {
        $this->db->select('*');
        $this->db->from('scholarship_organisation_setup');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function countryListByActivity($status)
    {
    	$this->db->select('a.*');
        $this->db->from('scholarship_country as a');
		$this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
    }

    function stateListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('scholarship_state as a');
        $this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function departmentListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('scholarship_department as a');
        $this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    function staffListByActivity($status)
    {
        $this->db->select('a.*');
        $this->db->from('staff as a');
		$this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
    }

    function editOrganisation($data, $id)
    {
            // echo "<Pre>";print_r($data);exit();
        
        $this->db->where('id', $id);
        $this->db->update('scholarship_organisation_setup', $data);
            // echo "<Pre>";print_r($this->db);exit();
        return TRUE;
    }

    function addNewOrganisationComitee($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_organisation_setup_comitee', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteOrganisationComitee($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_organisation_setup_comitee');
        return TRUE;
    }

    function organisationComiteeList($id_scholarship_organisation_setup)
    {
        $this->db->select('a.*');
        $this->db->from('scholarship_organisation_setup_comitee as a');
        $this->db->where('a.id_organisation', $id_scholarship_organisation_setup);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function organisationDepartmentList($id_scholarship_organisation_setup)
    {
        $this->db->select('a.*, d.code as department_code, d.name as department_name');
        $this->db->from('scholarship_organisation_has_department as a');
        $this->db->join('scholarship_department as d', 'a.id_department = d.id');
        $this->db->where('a.id_organisation', $id_scholarship_organisation_setup);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function addNewOrganisationDepartment($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_organisation_has_department', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteOrganisationDepartment($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_organisation_has_department');
        return TRUE;
    }

}

