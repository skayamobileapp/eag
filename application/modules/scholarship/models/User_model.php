<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
    function usersList()
    {
        $this->db->select('a.*');
        $this->db->from('scholar as a');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function roleListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('scholar_roles as a');
        $this->db->where('status', $status);
        $this->db->order_by("role", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function salutationListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('scholarship_salutation_setup as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSalutation($id)
    {
        $this->db->select('a.*');
        $this->db->from('scholarship_salutation_setup as a');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function usersListSearch($formData)
    {
        $this->db->select('a.*, p.role');
        $this->db->from('scholar as a');
        $this->db->join('scholar_roles as p', 'a.id_role = p.id');
        if ($formData['name'] != '')
        {
            $likeCriteria = "(a.name  LIKE '%" . $formData['name'] . "%' or a.first_name  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($formData['email'] != '')
        {
            $likeCriteria = "(a.email  LIKE '%" . $formData['email'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($formData['mobile'] != '')
        {
            $likeCriteria = "(a.mobile  LIKE '%" . $formData['mobile'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($formData['role'] != '')
        {
            $this->db->where('a.id_role', $formData['role']);
        }
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getUser($id)
    {
        $this->db->select('*');
        $this->db->from('scholar');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewUser($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholar', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editUser($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholar', $data);
        return TRUE;
    }
}

