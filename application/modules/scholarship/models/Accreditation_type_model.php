<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Accreditation_type_model extends CI_Model
{
    function accreditationTypeList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_accreditation_type');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function accreditationTypeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('scholarship_accreditation_type');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAccreditationType($id)
    {
        $this->db->select('*');
        $this->db->from('scholarship_accreditation_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewAccreditationType($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_accreditation_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editAccreditationType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_accreditation_type', $data);
        return TRUE;
    }
}

