<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SponserHasStudents extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('sponser_has_students_model');
        $this->load->model('sponser_model');
        $this->isLoggedIn();
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Campus Management System : 404 - Page Not Found';
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    function list()
    {
        if ($this->checkAccess('sponser_has_students.sponser_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['sponserList'] = $this->sponser_model->sponserListSearch($name);

            $this->global['pageTitle'] = 'Campus Management System : Sponser List';
            $this->loadViews("sponser_has_students/list", $this->global, $data, NULL);
        }
    }
    
    function add($id = NULL)
    {
        if ($this->checkAccess('sponser_has_students.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                // echo "<Pre>"; print_r($this->input->post());exit;

            $id_student = $this->security->xss_clean($this->input->post('id_student'));
            $start_date = $this->security->xss_clean($this->input->post('start_date'));
            $end_date = $this->security->xss_clean($this->input->post('end_date'));
            $aggrement_no = $this->security->xss_clean($this->input->post('aggrement_no'));
            $amount = $this->security->xss_clean($this->input->post('amount'));
            $id_fee_item = $this->security->xss_clean($this->input->post('id_fee_item'));
            $calculation_mode = $this->security->xss_clean($this->input->post('calculation_mode'));


            $data = array(
                    'id_student' => $id_student,
                    'id_sponser' => $id,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'aggrement_no' => $aggrement_no,
                    'amount' => $amount,
                    'id_fee_item' => $id_fee_item,
                    'calculation_mode' => $calculation_mode,
                    'status' => '1',
                    'created_by' => $user_id

                );
            $result = $this->sponser_has_students_model->addNewSponserHasStudents($data);

            }



            $data['sponser'] = $this->sponser_model->getSponser($id);
            $data['studentList'] = $this->sponser_has_students_model->studentList();
            $data['programmeList'] = $this->sponser_has_students_model->programmeListByStatus('1');
            $data['intakeList'] = $this->sponser_has_students_model->intakeListByStatus('1');
            $data['sponserHasStudentsList'] = $this->sponser_has_students_model->getSponserHasStudents($id);
            $data['feeList'] = $this->sponser_has_students_model->feeListByStatus('1');



            // echo "<Pre>"; print_r($data['sponserHasStudentsList']);exit;
            $this->global['pageTitle'] = 'Campus Management System : Add Sponser';
            $this->loadViews("sponser_has_students/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('sponser_has_students.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/sponser/list');
            }
            if($this->input->post())
            {
                $id_sponser = $this->security->xss_clean($this->input->post('id_sponser'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'id_sponser' => $id_sponser,
                    'id_student' => $id_student,
                    'amount' => $amount,
                    'status' => $status
                );
                
                $result = $this->sponser_has_students_model->editSponserHasStudents($data,$id);
                redirect('/finance/sponserHasStudents/list');
            }
            $data['sponserList'] = $this->sponser_model->sponserList();
            $data['studentList'] = $this->sponser_has_students_model->studentList();
            $data['sponserHasStudentsDetails'] = $this->sponser_has_students_model->getSponserHasStudents($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Sponser';
            $this->loadViews("sponser_has_students/edit", $this->global, $data, NULL);
        }
    }

    function getStudentByProgrammeId($id)
     {       
            // print_r($id);exit;
            $results = $this->sponser_has_students_model->getStudentByProgrammeId($id);
            $programme_data = $this->sponser_has_students_model->getProgrammeById($id);

            // echo "<Pre>"; print_r($programme_data);exit;
            $programme_name = $programme_data->name;
            $programme_code = $programme_data->code;
            $total_cr_hrs = $programme_data->total_cr_hrs;
            $graduate_studies = $programme_data->graduate_studies;
            $foundation = $programme_data->foundation;

            $table="<select name='id_student' id='id_student' class='form-control' onchange='getStudentByStudentId(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $full_name = $results[$i]->full_name;
            $table.="<option value=".$id.">".$full_name.
                    "</option>";

            }
            $table.="</select>";

            $view  = "
            <table border='1px' style='width: 100%'>
                <tr>
                    <td colspan='4'><h5 style='text-align: center;'>Programme Details</h5></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Programme Name</th>
                    <td style='text-align: center;'>$programme_name</td>
                    <th style='text-align: center;'>Programme Code</th>
                    <td style='text-align: center;'>$programme_code</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Total Credit Hours</th>
                    <td style='text-align: center;'>$total_cr_hrs</td>
                    <th style='text-align: center;'>Graduate Studies</th>
                    <td style='text-align: center;'>$graduate_studies</td>
                </tr>

            </table>
            <br>
            <br>
            ";

            echo $table;
            exit;
    }

    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
            $student_data = $this->sponser_has_students_model->getStudentByStudentId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $programme_name = $student_data->programme_name;


            $table  = "


            <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    $intake_name
                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>
            ";
            echo $table;
            exit;
    }

    function getStudentsSearch()
    {
        $id_session = $this->session->my_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo"<Pre>";print_r($tempData);exit();
        $tempData['id_session'] = $id_session;
        
        $data = $this->sponser_has_students_model->getStudentsSearch($tempData);

        $details = $this->displaytempdata($data);
        
        echo $details;        
        exit;
    }

    function displaytempdata($temp_details)
    {
        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>NRIC</th>
                    <th>E Mail</th>
                    <th>Programme</th>
                    <th>Intake</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $full_name = $temp_details[$i]->full_name;
                    $programme_name = $temp_details[$i]->programme_name;
                    $intake_name = $temp_details[$i]->intake_name;
                    $email = $temp_details[$i]->email_id;
                    $nric = $temp_details[$i]->nric;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$full_name</td>
                            <td>$nric</td>                           
                            <td>$email</td>                           
                            <td>$programme_name</td>                           
                            <td>$intake_name</td>                        
                            
                            <td class='text-center'>
                          <input type='checkbox' name='checkvalue[]' class='check' value='".$id."'>
                        </td>
                       
                        </tr>";
                    }


        $table.= "</table>";
        return $table;
    }

    function deleteStudentFromSponser($id)
    {
        $inserted_id = $this->sponser_has_students_model->deleteStudentFromSponser($id);
        // $data = $this->displaytempdata();
        echo $inserted_id; 
    } 
}