<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Fee Category</h3>
        </div>
        <form id="form_fee_category" action="" method="post">

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fee Category Code *</label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description *</label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description (Optional Language) *</label>
                        <input type="text" class="form-control" id="description_optional_language" name="description_optional_language">
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Fee Group *</label>
                        <select name="fee_group" id="fee_group" class="form-control">
                            <option value="">Select</option>
                            <option value="Rental">Rental</option>
                            <option value="Statement Of Account">Statement Of Account</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Sequence *</label>
                        <input type="number" class="form-control" id="sequence" name="sequence">
                    </div>
                </div>



                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>
    $(document).ready(function() {
        $("#form_fee_category").validate({
            rules: {
                code: {
                    required: true
                },
                description: {
                    required: true
                },
                description_optional_language: {
                    required: true
                },
                fee_group: {
                    required: true
                },
                sequence: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "Category Code Required",
                },
                description: {
                    required: "Fee Description Required",
                },
                description_optional_language: {
                    required: "Fee Description In Malay Required",
                },
                fee_group: {
                    required: "Select Fee Group",
                },
                sequence: {
                    required: "Enter Sequence",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
