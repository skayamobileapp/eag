<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Sponsor</h3>
        </div>
        <form id="form_sponser" action="" method="post">
            <div class="form-container">
                    <h4 class="form-group-title">Sponsor Details</h4>  
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Sponsor Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="code" name="code" readonly onfocus="showCodeAfterSave()">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>

                </div>

            </div>

            <div class="form-container">
                    <h4 class="form-group-title">Contact Details</h4>  
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="address" name="address">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address 2 <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="address2" name="address2">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mobile Number <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="mobile_number" name="mobile_number">
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Country <span class='error-text'>*</span></label>
                            <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select State <span class='error-text'>*</span></label>
                            <span id='view_state'></span>
                        </div>
                    </div>

                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Location <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="location" name="location">
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>ZIP Code <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="zip_code" name="zip_code">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fax Number <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="fax" name="fax">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>

                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();

    function showCodeAfterSave()
    {
        $("#code").val("Sponsor Code i'll Be generated After Sponsor Saved ");
    }
    

    function getStateByCountry(id)
    {

        $.get("/sponser/sponser/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
            // $("#view_programme_details").html(data);
            // $("#view_programme_details").show();
        });
    }


    $(document).ready(function() {
        $("#form_sponser").validate({
            rules: {
                name: {
                    required: true
                },
                address: {
                    required: true
                },
                mobile_number: {
                    required: true
                },
                id_country: {
                    required: true
                },
                id_state: {
                    required: true
                },
                id_city: {
                    required: true
                },
                zip_code: {
                    required: true
                },
                status: {
                    required: true
                },
                address2: {
                    required: true
                },
                location: {
                    required: true
                },
                fax: {
                    required: true
                },
                email: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Enter Name</p>",
                },
                address: {
                    required: "<p class='error-text'>Enter Address</p>",
                },
                mobile_number: {
                    required: "<p class='error-text'>Enter Mobile Number</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                id_city: {
                    required: "<p class='error-text'>Select City</p>",
                },
                zip_code: {
                    required: "<p class='error-text'>Enter Zip Code</p>",
                },
                status: {
                    required: "<p class='error-text'>Status required</p>",
                },
                address2: {
                    required: "<p class='error-text'>Address 2 Required</p>",
                },
                location: {
                    required: "<p class='error-text'>Location Required</p>",
                },
                fax: {
                    required: "<p class='error-text'>Fax Required</p>",
                },
                email: {
                    required: "<p class='error-text'>Email Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>