<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Frequency_mode_model extends CI_Model
{
    function frequencyModeList()
    {
        $this->db->select('fm.*');
        $this->db->from('frequency_mode as fm');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getFrequencyMode($id)
    {
        $this->db->select('fm.*');
        $this->db->from('frequency_mode as fm');
        $this->db->where('fm.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewFrequencyMode($data)
    {
        $this->db->trans_start();
        $this->db->insert('frequency_mode', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editFrequencyMode($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('frequency_mode', $data);
        return TRUE;
    }
}

