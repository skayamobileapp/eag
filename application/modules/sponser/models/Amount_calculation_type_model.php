<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Amount_calculation_type_model extends CI_Model
{
    function amountCalculationTypeList()
    {
        $this->db->select('act.*');
        $this->db->from('amount_calculation_type as act');
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAmountCalculationType($id)
    {
        $this->db->select('act.*');
        $this->db->from('amount_calculation_type as act');
        $this->db->where('act.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewAmountCalculationType($data)
    {
        $this->db->trans_start();
        $this->db->insert('amount_calculation_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewAmountCalculationTypeDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('amount_calculation_type_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editAmountCalculationType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('amount_calculation_type', $data);
        return TRUE;
    }
}

