<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Petty_cash_entry_model extends CI_Model
{

    function pettyCashEntryListSearch($data)
    {
         $this->db->select('efe.*, fy.year as financial_year, fy.name as financial_name, d.name as department_name, d.code as department_code, bty.name as budget_year, st.ic_no, st.name as staff_name');
        $this->db->from('petty_cash_entry as efe');
        $this->db->join('financial_year as fy','efe.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','efe.id_budget_year = bty.id');
        $this->db->join('department_code as d','efe.department_code = d.code');
        $this->db->join('staff as st','efe.id_staff = st.id');
        if ($data['name']!='')
        {
            $likeCriteria = "(efe.description  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['department_code'] !='')
        {
            $this->db->where('efe.department_code', $data['department_code']);
        }
        if ($data['id_financial_year']!='')
        {
            $this->db->where('efe.id_financial_year', $data['id_financial_year']);
        }
        if ($data['id_budget_year']!='')
        {
            $this->db->where('efe.id_budget_year', $data['id_budget_year']);
        }
        if ($data['status'] !='')
        {
            $this->db->where('efe.status', $data['status']);
        }
        $this->db->order_by("efe.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($result);exit();     
         return $result;
    }

     function getPettyCashAllocationById($id)
    {
         $this->db->select('efe.*');
        $this->db->from('petty_cash_allocation as efe');
        $this->db->where('efe.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getPettyCashEntry($id)
    {
        $this->db->select('efe.*, fy.year as financial_year, fy.name as financial_name, d.name as department_name, d.code as department_code, bty.name as budget_year, efa.reference_number as petty_cash_allocation, efa.used_amount as petty_cash_allocation_used_amount, efa.balance_amount as petty_cash_allocation_balance_amount, st.ic_no, st.name as staff_name');
        $this->db->from('petty_cash_entry as efe');
        $this->db->join('petty_cash_allocation as efa','efe.id_petty_cash_allocation = efa.id');
        $this->db->join('financial_year as fy','efe.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','efe.id_budget_year = bty.id');
        $this->db->join('department_code as d','efe.department_code = d.code');
        $this->db->join('staff as st','efe.id_staff = st.id');
        $this->db->where('efe.id', $id);
        $query = $this->db->get();
        return $query->row();
    }


	function getAccountCodeList()
    {
        $this->db->select('*');
        $this->db->from('account_code');
        $this->db->where('level', '3');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getActivityCodeList()
    {
        $this->db->select('*');
        $this->db->from('activity_code');
        $this->db->where('level', '3');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getDepartmentCodeList()
    {
        $this->db->select('*');
        $this->db->from('department_code');
         $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getFundCodeList()
    {
        $this->db->select('*');
        $this->db->from('fund_code');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function financialYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('financial_year');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function budgetYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('budget_year');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStaffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStudentListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('student');
         $this->db->where('applicant_status', $status);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function addNewPettyCashEntry($data)
    {
         $this->db->trans_start();
        $this->db->insert('petty_cash_entry', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updatePettyCashEntry($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('petty_cash_entry', $data);
        return TRUE;
    }

    function updatePettyCashAllocation($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('petty_cash_allocation', $data);
        return TRUE;
    }

    function getPettyCashAllocation($id)
    {
        $this->db->select('efa.*, fy.year as financial_year, fy.name as financial_name, d.name as department_name, d.code as department_code, bty.name as budget_year');
        $this->db->from('petty_cash_allocation as efa');
        $this->db->join('financial_year as fy','efa.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','efa.id_budget_year = bty.id');
        $this->db->join('department_code as d','efa.department_code = d.code');
        $this->db->where('efa.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function pettyCashAllocationListByStatus($status)
    {
        $this->db->select('efa.*');
        $this->db->from('petty_cash_allocation as efa');
        $this->db->where('efa.status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    // function getPettyCashAllocation($data)
    // {
    //      $this->db->select('efe.*, fy.year as financial_year, fy.name as financial_name, d.name as department_name, d.code as department_code, bty.name as budget_year');
    //     $this->db->from('petty_cash_allocation as efe');
    //     $this->db->join('financial_year as fy','efe.id_financial_year = fy.id');
    //     $this->db->join('budget_year as bty','efe.id_budget_year = bty.id');
    //     $this->db->join('department_code as d','efe.department_code = d.code');
    //     $this->db->where('efe.id_financial_year', $data['id_financial_year']);
    //     $this->db->where('efe.id_budget_year', $data['id_budget_year']);
    //     $this->db->where('efe.type', $data['type']);
    //     $this->db->where('efe.status', '1');
    //     if(!empty($data['id_staff']))
    //     {
    //         $this->db->where('efe.id_staff', $data['id_staff']);
    //     }
    //     if(!empty($data['id_student']))
    //     {
    //         $this->db->where('efe.id_student', $data['id_student']);
    //     }
    //     $query = $this->db->get();
    //     return $query->row();
    // }

   

    function generatePettyCashEntryNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('*');
            $this->db->from('petty_cash_entry');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
            $generated_number = "PCE" .(sprintf("%'06d", $count)). "/" . $Year;
           // echo "<Pre>";print_r($generated_number);exit();
           return $generated_number;
    }
}