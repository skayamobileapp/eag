<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Etf_voucher_model extends CI_Model
{

    function getBankList()
    {
        $this->db->select('*');
        $this->db->from('bank_registration');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }


    function getEtfVoucherListSearch($data)
    {
        $this->db->select('ina.*, bnk.name as bank_name, bnk.code as bank_code');
        $this->db->from('etf_voucher as ina');
        $this->db->join('bank_registration as bnk','ina.id_bank = bnk.id');
        if ($data['name']!='')
        {
            $likeCriteria = "(ina.description  LIKE '%" . $data['name'] . "%' or ina.reference_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_bank'] !='')
        {
            $this->db->where('ina.id_bank', $data['id_bank']);
        }
        if ($data['status'] !='')
        {
            $this->db->where('ina.status', $data['status']);
        }
        $this->db->order_by("ina.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($data);exit();     
         return $result;
    }

    function generateEtfVoucherNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('pv.*');
            $this->db->from('etf_voucher as pv');
            $this->db->order_by("pv.id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

            $count= $result + 1;
            $generated_number = "ETF" .(sprintf("%'06d", $count)). "/" . $Year;
           // echo "<Pre>";print_r($generated_number);exit();
           return $generated_number;
    }

    function addEtfVoucher($data)
    {
        $this->db->trans_start();
        $this->db->insert('etf_voucher', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addEtfVoucherDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('etf_voucher_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updatePaymentVoucher($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('payment_voucher', $data);
        return TRUE;
    }

    function updateEtfVoucher($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('etf_voucher', $data);
        return TRUE;
    }

    function searchPaymentVoucher($data)
    {
        $this->db->select('ina.*, bnk.name as bank_name, bnk.code as bank_code');
        $this->db->from('payment_voucher as ina');
        $this->db->join('bank_registration as bnk','ina.id_bank = bnk.id');
        if ($data['reference_number']!='')
        {
            $likeCriteria = "(ina.description  LIKE '%" . $data['reference_number'] . "%' or ina.reference_number  LIKE '%" . $data['reference_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['from_date'] !='')
        {
            $likeCriteria = "(date(ina.payment_date)  >= '" . date('Y-m-d',strtotime($data['from_date'])) . "')";
            $this->db->where($likeCriteria);
        }
        if ($data['to_date'] !='')
        {
         // echo "<Pre>";print_r($data);exit();     
            $likeCriteria = "(date(ina.payment_date)  <= '" . date('Y-m-d',strtotime($data['to_date'])) . "')";
            $this->db->where($likeCriteria);
        }
        if ($data['voucher_type'] !='')
        {
            $this->db->where('ina.type', $data['voucher_type']);
        }
            $this->db->where('ina.is_sab', '0');
            $this->db->where('ina.status', '1');
        $this->db->order_by("ina.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($data);exit();     
         return $result;

    }

    function getEtfVoucher($id)
    {
        $this->db->select('ina.*, bnk.name as bank_name, bnk.code as bank_code');
        $this->db->from('etf_voucher as ina');
        $this->db->join('bank_registration as bnk','ina.id_bank = bnk.id');
        $this->db->where('ina.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getEtfVoucherDetails($id)
    {
        $this->db->select('ina.*, bnk.*');
        $this->db->from('etf_voucher_details as ina');
        $this->db->join('payment_voucher as bnk','ina.id_referal = bnk.id');
        $this->db->where('ina.id_etf_voucher', $id);
        $query = $this->db->get();
        return $query->result();
    }

     function getPaymentVoucher($id)
    {
        $this->db->select('ina.*');
        $this->db->from('payment_voucher as ina');
        $this->db->where('ina.id', $id);
         $query = $this->db->get();
         $result = $query->row();   
         return $result;
    }
}
