<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Emergency_fund_entry_model extends CI_Model
{

    function emergencyFundEntryListSearch($data)
    {
         $this->db->select('efe.*, fy.year as financial_year, fy.name as financial_name, d.name as department_name, d.code as department_code, bty.name as budget_year');
        $this->db->from('emergency_fund_entry as efe');
        $this->db->join('financial_year as fy','efe.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','efe.id_budget_year = bty.id');
        $this->db->join('department_code as d','efe.department_code = d.code');
        if ($data['name']!='')
        {
            $likeCriteria = "(efe.description  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['type']!='')
        {
            $this->db->where('efe.type', $data['type']);
        }
        if ($data['department_code'] !='')
        {
            $this->db->where('efe.department_code', $data['department_code']);
        }
        if ($data['id_financial_year']!='')
        {
            $this->db->where('efe.id_financial_year', $data['id_financial_year']);
        }
        if ($data['id_budget_year']!='')
        {
            $this->db->where('efe.id_budget_year', $data['id_budget_year']);
        }
        if ($data['status'] !='')
        {
            $this->db->where('efe.status', $data['status']);
        }
        $this->db->order_by("efe.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($result);exit();     
         return $result;
    }

    function getEmergencyFundEntry($id)
    {
        $this->db->select('efe.*, fy.year as financial_year, fy.name as financial_name, d.name as department_name, d.code as department_code, bty.name as budget_year, efa.description as emergency_fund_allocation, efa.department_code as emergency_fund_allocation_department_code');
        $this->db->from('emergency_fund_entry as efe');
        $this->db->join('emergency_fund_allocation as efa','efe.id_emergency_fund_allocation = efa.id');
        $this->db->join('financial_year as fy','efe.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','efe.id_budget_year = bty.id');
        $this->db->join('department_code as d','efe.department_code = d.code');
        $this->db->where('efe.id', $id);
        $query = $this->db->get();
        return $query->row();
    }


	function getAccountCodeList()
    {
        $this->db->select('*');
        $this->db->from('account_code');
        $this->db->where('level', '3');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getActivityCodeList()
    {
        $this->db->select('*');
        $this->db->from('activity_code');
        $this->db->where('level', '3');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getDepartmentCodeList()
    {
        $this->db->select('*');
        $this->db->from('department_code');
         $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getFundCodeList()
    {
        $this->db->select('*');
        $this->db->from('fund_code');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function financialYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('financial_year');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function budgetYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('budget_year');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStaffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStudentListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('student');
         $this->db->where('applicant_status', $status);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function addNewEmergencyFundEntry($data)
    {
         $this->db->trans_start();
        $this->db->insert('emergency_fund_entry', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updateEmergencyFundEntry($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('emergency_fund_entry', $data);
        return TRUE;
    }

    function updateEmergencyFundAllocation($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('emergency_fund_allocation', $data);
        return TRUE;
    }

    function getEmergencyFundAllocation($data)
    {
         $this->db->select('efe.*, fy.year as financial_year, fy.name as financial_name, d.name as department_name, d.code as department_code, bty.name as budget_year');
        $this->db->from('emergency_fund_allocation as efe');
        $this->db->join('financial_year as fy','efe.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','efe.id_budget_year = bty.id');
        $this->db->join('department_code as d','efe.department_code = d.code');
        $this->db->where('efe.id_financial_year', $data['id_financial_year']);
        $this->db->where('efe.id_budget_year', $data['id_budget_year']);
        $this->db->where('efe.type', $data['type']);
        $this->db->where('efe.status', '1');
        if(!empty($data['id_staff']))
        {
            $this->db->where('efe.id_staff', $data['id_staff']);
        }
        if(!empty($data['id_student']))
        {
            $this->db->where('efe.id_student', $data['id_student']);
        }
        $query = $this->db->get();
        return $query->row();
    }

    function getEmergencyFundAllocationById($id)
    {
         $this->db->select('efe.*');
        $this->db->from('emergency_fund_allocation as efe');
        $this->db->where('efe.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function generateEmergencyFundEntryNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('*');
            $this->db->from('emergency_fund_entry');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
            $generated_number = "EFE" .(sprintf("%'06d", $count)). "/" . $Year;
           // echo "<Pre>";print_r($generated_number);exit();
           return $generated_number;
    }
}