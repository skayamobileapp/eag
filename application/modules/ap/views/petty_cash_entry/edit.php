<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Petty Cash Entry</h3>
            </div>



    <div class="form-container">
            <h4 class="form-group-title">Petty Cash Entry Details</h4>



            <div class="row">



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $pettyCashEntry->financial_year;?>" readonly="readonly">
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $pettyCashEntry->budget_year;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fund Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $pettyCashEntry->fund_code;?>" readonly="readonly">
                    </div>
                </div>                    

               

            </div>





            <div class="row">

              



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $pettyCashEntry->department_code;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Activity Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $pettyCashEntry->activity_code;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $pettyCashEntry->account_code;?>" readonly="readonly">
                    </div>
                </div>



            </div>


            <div class="row">



              <div class="col-sm-4">
                    <div class="form-group">
                        <label>Staff <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" value="<?php echo $pettyCashEntry->ic_no . " - ". $pettyCashEntry->staff_name;?>" readonly="readonly">
                    </div>
                </div>               



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Paid Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" value="<?php echo $pettyCashEntry->paid_amount;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $pettyCashEntry->description;?>" readonly="readonly">
                    </div>
                </div>



            </div>

            <h4>Petty Cash Allocation</h4>

             <div class="row">


               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Petty Cash Allocation <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $pettyCashEntry->petty_cash_allocation;?>" readonly="readonly">
                    </div>
                </div>



            </div>

             <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" value="<?php
                        if($pettyCashEntry->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($pettyCashEntry->status == '1')
                        {
                            echo "Reimbursed";
                        }
                        elseif($pettyCashEntry->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>


                </div>
            


             <?php
            if($pettyCashEntry->status == '2')
            {
             ?>


                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $pettyCashEntry->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>


          </div>

      </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script type="text/javascript">
     $('select').select2();
</script>