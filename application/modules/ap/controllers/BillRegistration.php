<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class BillRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('bill_registration_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('bill_registration.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['bankList'] = $this->bill_registration_model->getBankList();
            $data['financialYearList'] = $this->bill_registration_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->bill_registration_model->budgetYearListByStatus('1');

            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['id_bank'] = $this->security->xss_clean($this->input->post('id_bank'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['billRegistrationList'] = $this->bill_registration_model->getBillRegistrationListSearch($formData);

            // echo "<Pre>";print_r($data['billRegistrationList']);exit;
            $this->global['pageTitle'] = 'FIMS : List Bill Registration';
            $this->loadViews("bill_registration/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('bill_registration.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            
            if($this->input->post())
            {

                

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                $generated_number = $this->bill_registration_model->generateBillRegistrationNumber();

                $type = $this->security->xss_clean($this->input->post('type'));
                $contact_person_one = $this->security->xss_clean($this->input->post('contact_person_one'));
                $contact_person_two = $this->security->xss_clean($this->input->post('contact_person_two'));
                $contact_person_three = $this->security->xss_clean($this->input->post('contact_person_three'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $bank_acc_no = $this->security->xss_clean($this->input->post('bank_acc_no'));
                $id_bank = $this->security->xss_clean($this->input->post('id_bank'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_from = $this->security->xss_clean($this->input->post('id_from'));
                // $total_amount = $this->security->xss_clean($this->input->post('total_amount'));
                $total_amount = $this->security->xss_clean($this->input->post('total_detail'));
                
                

                $ef_for = $this->security->xss_clean($this->input->post('ef_for'));
                $ef_for_id = $this->security->xss_clean($this->input->post('ef_for_id'));


                if($type != 'Emergency Fund')
                {
                    $ef_for = '0';
                    $ef_for_id = '0';
                }

                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $id_financial_year = $this->security->xss_clean($this->input->post('id_financial_year'));
                $id_budget_year = $this->security->xss_clean($this->input->post('id_budget_year'));

                $data = array(
                    'type'=> $type,
                    'id_financial_year'=> $id_financial_year,
                    'id_budget_year'=> $id_budget_year,
                    'reference_number'=> $generated_number,
                    'contact_person_one'=> $contact_person_one,
                    'contact_person_two'=> $contact_person_two,
                    'contact_person_three'=> $contact_person_three,
                    'email'=> $email,
                    'bank_acc_no'=> $bank_acc_no,
                    'id_bank'=> $id_bank,
                    'description'=> $description,
                    'ef_for'=> $ef_for,
                    'ef_for_id'=> $ef_for_id,
                    'id_staff'=> $id_staff,
                    'id_from'=> $id_from,
                    'created_by' => $user_id,
                    'total_amount' => $total_amount,
                    'status'=>'0'
                );
             $inserted_id = $this->bill_registration_model->addBillRegistration($data);

             if($inserted_id)
                {
                    $referal_update['ap_status'] = 'B0';
                    $referal_update['is_bill_registered'] = $inserted_id;

                    if($type == 'Registered Vendor')
                    {
                        $updated_referal = $this->bill_registration_model->updateGRNDetailsForBill($referal_update,$id_from);
                    }

                $total_bill_amount = 0;
                 for($i=0;$i<count($formData['id_detail']);$i++)
                 {
                    $id_detail = $formData['id_detail'][$i];




                    


                    if($id_detail > 0)
                    {


                        switch ($type)
                        {
                            case 'Registered Vendor':

                            $amount = $formData['amount'][$i];


                                $detail_data = $this->bill_registration_model->getAssetOrderById($id_detail);


                            $detailsData = array(
                                'id_bill_registration'=>$inserted_id,
                                'type'=>$type,
                                'cr_account' => $detail_data->cr_account,
                                'cr_activity' => $detail_data->cr_activity,
                                'cr_department' => $detail_data->cr_department,
                                'cr_fund' => $detail_data->cr_fund,
                                'dt_account' => $detail_data->dt_account,
                                'dt_activity' => $detail_data->dt_activity,
                                'dt_department' => $detail_data->dt_department,
                                'dt_fund' => $detail_data->dt_fund,
                                'id_referal'=>$id_detail,
                                'total_final'=>$amount,
                                'price' => $detail_data->amount_per_item,
                                'quantity' => $detail_data->received_qty,
                                'tax_price' => $detail_data->tax_per_each_item * $detail_data->received_qty,
                                'created_by'=>$user_id,
                                'status'=> 1
                            );

                        $inserted_detail_id = $this->bill_registration_model->addBillRegistrationDetail($detailsData);




                                break;

                            case 'Emergency Fund':

                            // $cr_account = $formData['cr_account'][$i];
                            // $cr_activity = $formData['cr_activity'][$i];
                            // $cr_department = $formData['cr_department'][$i];
                            // $cr_fund = $formData['cr_fund'][$i];
                            // $dt_account = $formData['dt_account'][$i];
                            // $dt_activity = $formData['dt_activity'][$i];
                            // $dt_department = $formData['dt_department'][$i];
                            // $dt_fund = $formData['dt_fund'][$i];
                            // $requested_amount = $formData['requested_amount'][$i];

                // echo "<Pre>"; print_r($id_detail);exit;
                        $detail_data = $this->bill_registration_model->getEmergencyFundsByEntryIdForBillRegistration($id_detail);

                // echo "<Pre>"; print_r($detail_data);exit;


                            $detailsData = array(
                                'id_bill_registration'=>$inserted_id,
                                'type'=>$type,
                                'cr_account' => $detail_data->cr_account,
                                'cr_activity' => $detail_data->cr_activity,
                                'cr_department' => $detail_data->cr_department,
                                'cr_fund' => $detail_data->cr_fund,
                                'dt_account' => $detail_data->dt_account,
                                'dt_activity' => $detail_data->dt_activity,
                                'dt_department' => $detail_data->dt_department,
                                'dt_fund' => $detail_data->dt_fund,
                                'id_referal'=>$id_detail,
                                'total_final'=>$detail_data->requested_amount,
                                'created_by'=>$user_id,
                                'status'=> 1
                            );

                // echo "<Pre>"; print_r($detailsData);exit;


                            $total_bill_amount = $total_bill_amount + $detail_data->requested_amount;
                                // 'price' => $detail_data->amount_per_item,
                                // 'quantity' => $detail_data->received_qty,
                                // 'tax_price' => $detail_data->tax_price,

                        $inserted_detail_id = $this->bill_registration_model->addBillRegistrationDetail($detailsData);

                        if($inserted_detail_id)
                        {
                                    $updated_referal = $this->bill_registration_model->updateEFEntry($referal_update,$id_detail);

                        }

                                
                                break;

                            case 'Petty Cash':

            

                        $detail_data = $this->bill_registration_model->getPettyCashByEntryIdForBillRegistration($id_detail);

                // echo "<Pre>"; print_r($detail_data);exit;


                             $detailsData = array(
                                'id_bill_registration'=>$inserted_id,
                                'type'=>$type,
                                'cr_account' => $detail_data->cr_account,
                                'cr_activity' => $detail_data->cr_activity,
                                'cr_department' => $detail_data->cr_department,
                                'cr_fund' => $detail_data->cr_fund,
                                'dt_account' => $detail_data->dt_account,
                                'dt_activity' => $detail_data->dt_activity,
                                'dt_department' => $detail_data->dt_department,
                                'dt_fund' => $detail_data->dt_fund,
                                'id_referal'=>$id_detail,
                                'total_final'=>$detail_data->amount,
                                'created_by'=>$user_id,
                                'status'=> 1
                            );
                            $total_bill_amount = $total_bill_amount + $detail_data->amount;

                // echo "<Pre>"; print_r($detailsData);exit;


                        $inserted_detail_id = $this->bill_registration_model->addBillRegistrationDetail($detailsData);

                        if($inserted_detail_id)
                        {

                                    $updated_referal = $this->bill_registration_model->updatePCEntry($referal_update,$id_detail);
                        }

                                
                                break;

                            default:
                                # code...
                                break;
                        }
                    
                        
                    // echo "<Pre>"; print_r($detailsData);exit;
                       
                    }
                }
            }
            if($type != 'Registered Vendor')
            {
                $bill_data['total_amount'] = $total_bill_amount;
                $updated_bill_amount = $this->bill_registration_model->updateBillRegistration($bill_data,$inserted_id);
            }
                    // echo "<Pre>"; print_r($inserted_detail_id);exit;
                redirect('/ap/billRegistration/list');
            }

            $data['bankList'] = $this->bill_registration_model->getBankList();
            $data['financialYearList'] = $this->bill_registration_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->bill_registration_model->budgetYearListByStatus('1');

            // echo "<Pre>";print_r($data);exit();


            $this->global['pageTitle'] = 'FIMS : Add Bill Registration';
            $this->loadViews("bill_registration/add", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('bill_registration.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/ap/billRegistration/list');
            }
            
            $data['staffList'] = $this->bill_registration_model->getStaffListByStatus('1');
            $data['studentList'] = $this->bill_registration_model->getStudentListByStatus('Approved');
            $data['billRegistration'] = $this->bill_registration_model->getBillRegistration($id);
            $data['billRegistrationDetails'] = $this->bill_registration_model->getBillRegistrationDetails($id);
            $data['financialYearList'] = $this->bill_registration_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->bill_registration_model->budgetYearListByStatus('1');
            
            $get['type'] = $data['billRegistration']->type;
            if($get['type'] == 'Registered Vendor')
            {
                $get['id'] = $data['billRegistration']->id_from;
            }else
            {
                $get['id'] = $data['billRegistration']->id;
            }
            $data['billData'] = $this->bill_registration_model->getBillData($get);
            // echo "<Pre>";print_r($get);exit();

            

            $this->global['pageTitle'] = 'FIMS : View Bill Registration';
            $this->loadViews("bill_registration/edit", $this->global, $data, NULL);
        }
    }


    function view($id = NULL)
    {

        if ($this->checkAccess('bill_registration.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/ap/billRegistration/approvalList');
            }

            if($this->input->post())
            {

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_from = $this->security->xss_clean($this->input->post('id_from'));




                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );
                 $result = $this->bill_registration_model->updateBillRegistration($data,$id);
                 if($result)
                 {
                    for($i=0;$i<count($formData['id_bill_detail']);$i++)
                    {
                        $id_bill_detail = $formData['id_bill_detail'][$i];
                        $id_referal = $formData['id_referal'][$i];

                        if($status == '1')
                        {
                            $referal_update['ap_status'] = 'B1';
                        }
                        elseif ($status == '2')
                        {
                            $referal_update['ap_status'] = 'B2';
                        }


                        if($id_referal > 0)
                        {

                                // $referal_update['status'] = $status;
                            switch ($type)
                            {
                                case 'Registered Vendor':
                                    $updated_referal = $this->bill_registration_model->updateGRNDetailsForBill($referal_update,$id_from);
                                    break;

                                case 'Emergency Fund':
                                    $updated_referal = $this->bill_registration_model->updateEFEntry($referal_update,$id_referal);
                                    break;

                                case 'Petty Cash':
                                    $updated_referal = $this->bill_registration_model->updatePCEntry($referal_update,$id_referal);
                                    break;

                                default:
                                    # code...
                                    break;
                            }
                        }
                    }
                 }

                redirect('/ap/billRegistration/approvalList');

            }
            
            
            $data['staffList'] = $this->bill_registration_model->getStaffListByStatus('1');
            $data['studentList'] = $this->bill_registration_model->getStudentListByStatus('Approved');
            $data['billRegistration'] = $this->bill_registration_model->getBillRegistration($id);
            $data['billRegistrationDetails'] = $this->bill_registration_model->getBillRegistrationDetails($id);
            $data['financialYearList'] = $this->bill_registration_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->bill_registration_model->budgetYearListByStatus('1');

            $get['type'] = $data['billRegistration']->type;
            if($get['type'] == 'Registered Vendor')
            {
                $get['id'] = $data['billRegistration']->id_from;
            }else
            {
                $get['id'] = $data['billRegistration']->id;
            }
            $data['billData'] = $this->bill_registration_model->getBillData($get);
            // echo "<Pre>";print_r($data);exit();

            

            $this->global['pageTitle'] = 'FIMS : Approve Bill Registration';
            $this->loadViews("bill_registration/view", $this->global, $data, NULL);
        }
    }



     function approvalList()
    {

        if ($this->checkAccess('bill_registration.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['bankList'] = $this->bill_registration_model->getBankList();
            $data['financialYearList'] = $this->bill_registration_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->bill_registration_model->budgetYearListByStatus('1');

            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['id_bank'] = $this->security->xss_clean($this->input->post('id_bank'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;

            $data['billRegistrationList'] = $this->bill_registration_model->getBillRegistrationListSearch($formData);

            // echo "<Pre>";print_r($data['billRegistrationList']);exit;
            $this->global['pageTitle'] = 'FIMS : List Investment Application';
            $this->loadViews("bill_registration/approval_list", $this->global, $data, NULL);
        }
    }



    function getBillDataByType()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $type = $tempData['type'];

        // $view_data = $this->getDataEntryTemplateForBillRegistration();
        
        // echo "<Pre>";print_r($type);exit();

        
        switch ($type)
        {
            case 'Registered Vendor':

                $table = $this->getBillRegistrationByVendor($tempData);
                echo $table;exit;
                break;

            case 'Emergency Fund':

            $table = " 

                <script type='text/javascript'>
                    $('select').select2();
                </script>

                <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Bill For <span class='error-text'>*</span></label>
                        <select name='ef_for' id='ef_for' class='form-control' onchange='getEmergencyFundByData()'>
                            <option value=''>Select</option>
                            <option value='Staff'>Staff</option>
                        </select>
                    </div>
                </div>

            ";
                            // <option value='Student'>Student</option>
            echo $table;exit;

            // $table = $this->getBillRegistrationByEmergencyFund();
                
                break;

            case 'Petty Cash':

            $table = $this->getPettyCashForBillRegistration();
            
                break;

            default:
                # code...
                break;
        }
    }

    function getBillRegistrationByVendor($data)
    {
         $bill_data = $this->bill_registration_model->getRegisteredVendorDataForBillRegistration($data);
                // echo "<Pre>";print_r($bill_data);exit();

        $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select GRN <span class='error-text'>*</span></label>
                <select name='id_from' id='id_from' class='form-control' onchange='getFromGRN()'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($bill_data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $bill_data[$i]->id;
                $reference_number = $bill_data[$i]->reference_number;
                $description = $bill_data[$i]->description;

                $table.="<option value=".$id.">".$reference_number. " - " . $description . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getFromGRN()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id_grn = $tempData['id_from'];
        $grn_data = $this->bill_registration_model->getGrn($id_grn);
        $grn_details = $this->bill_registration_model->getAssetOrderByGRNId($id_grn);
        // echo "<Pre>";print_r($grn_data);exit();



         // <div class='col-sm-4'>
         //            <div class='form-group'>
         //                <label>Total Amount <span class='error-text'>*</span></label>
         //                <input type='text' class='form-control' id='total_amount' name='total_amount' value='$grn_data->total_amount' readonly>
         //            </div>
         //        </div>
               


         //    </div>


         //    <div class='row'>




        $table = "


        <script type='text/javascript'>
                $('select').select2();
            </script>


            <h3>GRN </h3>


            <div class='row'>

                <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Po Number <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='po_number' name='po_number' value='$grn_data->po_number' readonly>
                    </div>
                </div>



                  <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>PR Number <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='pr_number' name='pr_number' value='$grn_data->pr_number' readonly>
                    </div>
                </div>



                <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>GRN Number <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='grn_number' name='grn_number' value='$grn_data->reference_number' readonly>
                    </div>
                </div>
               

            </div>


            <div class='row'>

                <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>GRN Date Number <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='grn_date' name='grn_date' value='$grn_data->date_time' readonly>
                    </div>
                </div>



                  <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Expire Date <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='grn_expire_date' name='grn_expire_date' value='$grn_data->expire_date' readonly>
                    </div>
                </div>



               


              <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>GRN Rating <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='contact_person_three' name='contact_person_three' value='$grn_data->rating' readonly>
                    </div>
                </div>
               

            </div>
        ";

        // echo $table;exit();


        $table .= "
        <h4>GRN Details</h4>
        <div class='custom-table'>
        <table class='table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Category</th>
                    <th>Sub-Category</th>
                    <th>Item</th>
                    <th>Qty Received</th>
                    <th>amount per item</th>
                    <th>Total Amount</th>
                    </tr>
                    </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($grn_details);$i++)
                    {
                        $id = $grn_details[$i]->id;
                        $category_name = $grn_details[$i]->category_name;
                        $category_code = $grn_details[$i]->category_code;
                        $sub_category_name = $grn_details[$i]->sub_category_name;
                        $sub_category_code = $grn_details[$i]->sub_category_code;
                        $item_name = $grn_details[$i]->item_name;
                        $item_code = $grn_details[$i]->item_code;
                        $received_qty = $grn_details[$i]->received_qty;
                        $amount_per_item = $grn_details[$i]->amount_per_item;
                        $amount = $grn_details[$i]->amount;

                        // $maturity_date = date("d-m-Y", strtotime($grn_details[$i]->maturity_date));

                        $j=$i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j .
                            <input type='hidden' name='id_detail[]' id='id_detail[]' value='$id' />
                            </td>
                            <td>$category_code - $category_name</td>
                            <td>$sub_category_code - $sub_category_name</td>
                            <td>$item_code - $item_name</td>
                            <td>$received_qty</td>
                            <td>$amount_per_item</td>
                            <td>$amount
                            <input type='hidden' name='amount[]' id='amount[]' value='$amount' />
                            </td>
                        </tr>
                        </tbody>";
                        $total_detail = $total_detail + $amount;
                    }

                     $table .= "

                    <tr>
                            <td colspan='6' style='text-align: right'><b>Total Amount :</b></td>
                            <td><input type='hidden' name='total_detail' id='total_detail' value='$total_detail' />$total_detail</td>
                        </tr>";

                    // <td>
                    //             <span onclick='getTempData($id)'>Edit</a>
                    //         <td>
        $table.= "</table>
                </div>";
        

        echo $table;exit();
    }

    function getEmergencyFundByData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $type = $tempData['type'];
        // echo "<Pre>";print_r($tempData);exit();

        switch ($type)
        {
            
            case 'Emergency Fund':

            if($tempData['ef_for'] == 'Staff')
            {
                $select_heading = "Select Staff ";
                
                $bill_data = $this->bill_registration_model->getEFStaffDataForBillRegistration($tempData);


            }
            elseif ($tempData['ef_for'] == 'Student')
            {
                $select_heading = "Select Student ";

                $bill_data = $this->bill_registration_model->getEFStudentDataForBillRegistration($tempData);
               
            }
        // echo "<Pre>";print_r($bill_data);exit();

            if(!empty($bill_data))
            {
                $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>
                <div class='row'>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>$select_heading <span class='error-text'>*</span></label>
                <select name='ef_for_id' id='ef_for_id' class='form-control' onchange='getEmergencyFundsByData()'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($bill_data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $bill_data[$i]->ef_for_id;
                $code = $bill_data[$i]->code;
                $full_name = $bill_data[$i]->full_name;

                $table.="<option value=".$id.">".$code. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                </div>
                ";
            }
            else
            {
                echo "";exit();
            }

            default:
                # code...
                break;
        }
        echo $table;   
    }

    function getEmergencyFundsByData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $type = $tempData['type'];
        // echo "<Pre>";print_r($bill_data);exit();

        if($tempData['type'] == 'Emergency Fund')
        {

            if($tempData['ef_for'] == 'Staff')
            {
                
                $bill_data = $this->bill_registration_model->getEmergencyFundsByStaffData($tempData);


            }
            elseif ($tempData['ef_for'] == 'Student')
            {

                $bill_data = $this->bill_registration_model->getEmergencyFundsByStudentData($tempData);
               
            }


        // echo "<Pre>";print_r($bill_data);exit();

        $table = "
        <h4>Emergency Fund Details For Bill Registration</h4>
        
        <div class='custom-table'>
        <table class='table'>
                <thead>
                 <tr>
                    <th>Sl. No</th>
                    <th>Reference Number</th>
                    <th>Description</th>
                    <th>Debit Gl Codes</th>
                    <th>Credit Gl Codes</th>
                    <th>Requested Amount</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($bill_data);$i++)
                    {

                        $id = $bill_data[$i]->id;
                        $cr_fund = $bill_data[$i]->cr_fund;
                        $cr_department = $bill_data[$i]->cr_department;
                        $cr_activity = $bill_data[$i]->cr_activity;
                        $cr_account = $bill_data[$i]->cr_account;
                        $dt_fund = $bill_data[$i]->dt_fund;
                        $dt_department = $bill_data[$i]->dt_department;
                        $dt_activity = $bill_data[$i]->dt_activity;
                        $dt_account = $bill_data[$i]->dt_account;
                        $reference_number = $bill_data[$i]->reference_number;
                        $description = $bill_data[$i]->description;
                        $requested_amount = $bill_data[$i]->requested_amount;

                        // $maturity_date = date("d-m-Y", strtotime($grn_details[$i]->maturity_date));

                        $j=$i+1;

                         // <input type='hidden' name='id_detail[]' id='id_detail[]' value='$id' />

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j .
                           
                            </td>
                            <td>
                                $reference_number
                            </td>
                            <td>
                                $description
                            </td>
                            <td>
                                $dt_fund - $dt_department - $dt_activity - $dt_account
                            </td>
                            <td>
                                $cr_fund - $cr_department - $cr_activity - $cr_account
                            </td>
                            <td>
                                $requested_amount
                            </td>
                            <td class='text-center'>
                            <input type='checkbox' id='id_detail[]' name='id_detail[]' class='check' value='".$id."'>
                            </td>
                        </tr>
                     </tbody>";
                    }
        $table.= "</table>
        </div>";
        

        echo $table;exit();
        }

    }


    function getBillRegistrationByEmergencyFund()
    {
         $bill_data = $this->bill_registration_model->getEFDataForBillRegistration();
        // echo "<Pre>";print_r($bill_data);exit();

        $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Emergency Fund <span class='error-text'>*</span></label>
                <select name='id_from' id='id_from' class='form-control' onchange='getFromEF()'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($bill_data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $bill_data[$i]->id;
                $reference_number = $bill_data[$i]->reference_number;
                $description = $bill_data[$i]->description;

                $table.="<option value=".$id.">".$reference_number. " - " . $description . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getPettyCashEntryForBillRegistration()
    {
         $bill_data = $this->bill_registration_model->getPCDataForBillRegistration();
                // echo "<Pre>";print_r($bill_data);exit();

        $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Petty Cash <span class='error-text'>*</span></label>
                <select name='id_from' id='id_from' class='form-control' onchange='getFromPC()'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($bill_data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $bill_data[$i]->id;
                $reference_number = $bill_data[$i]->reference_number;
                $description = $bill_data[$i]->description;

                $table.="<option value=".$id.">".$reference_number. " - " . $description . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getPettyCashForBillRegistration()
    {
        $bill_data = $this->bill_registration_model->getPCStaffDataForBillRegistration();
                // echo "<Pre>";print_r($bill_data);exit();

        $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Staff <span class='error-text'>*</span></label>
                <select name='id_staff' id='id_staff' class='form-control' onchange='getPCByStaff()'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($bill_data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id_staff = $bill_data[$i]->id_staff;
                $code = $bill_data[$i]->code;
                $full_name = $bill_data[$i]->full_name;

                $table.="<option value=".$id_staff.">".$code. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getPCByStaff()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $type = $tempData['type'];

        // echo "<Pre>";print_r($tempData);exit();

        if($tempData['type'] == 'Petty Cash')
        {
                
            $bill_data = $this->bill_registration_model->getPCByStaff($tempData);

        // echo "<Pre>";print_r($bill_data);exit();

             $table = "
        <h4>Petty Cash Details For Bill Registration</h4>
        
        <div class='custom-table'>
        <table class='table'>
                <thead>
                 <tr>
                    <th>Sl. No</th>
                    <th>Reference Number</th>
                    <th>Description</th>
                    <th>Debit Gl Codes</th>
                    <th>Credit Gl Codes</th>
                    <th>Paid Amount</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($bill_data);$i++)
                    {

                        $id = $bill_data[$i]->id;
                        $cr_fund = $bill_data[$i]->cr_fund;
                        $cr_department = $bill_data[$i]->cr_department;
                        $cr_activity = $bill_data[$i]->cr_activity;
                        $cr_account = $bill_data[$i]->cr_account;
                        $dt_fund = $bill_data[$i]->dt_fund;
                        $dt_department = $bill_data[$i]->dt_department;
                        $dt_activity = $bill_data[$i]->dt_activity;
                        $dt_account = $bill_data[$i]->dt_account;
                        $reference_number = $bill_data[$i]->reference_number;
                        $description = $bill_data[$i]->description;
                        $paid_amount = $bill_data[$i]->paid_amount;

                        // $maturity_date = date("d-m-Y", strtotime($grn_details[$i]->maturity_date));

                        $j=$i+1;

                         // <input type='hidden' name='id_detail[]' id='id_detail[]' value='$id' />

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j .
                           
                            </td>
                            <td>$reference_number</td>
                            <td>$description</td>
                            <td>
                            $dt_fund - $dt_department - $dt_activity - $dt_account

                            <input type='hidden' name='dt_fund[]' id='dt_fund[]' value='$dt_fund' />
                            <input type='hidden' name='dt_department[]' id='dt_department[]' value='$dt_department' />
                            <input type='hidden' name='dt_activity[]' id='dt_activity[]' value='$dt_activity' />
                            <input type='hidden' name='dt_account[]' id='dt_account[]' value='$dt_account' />

                            </td>
                            <td>
                            $cr_fund - $cr_department - $cr_activity - $cr_account

                            <input type='hidden' name='cr_fund[]' id='cr_fund[]' value='$cr_fund' />
                            <input type='hidden' name='cr_department[]' id='cr_department[]' value='$cr_department' />
                            <input type='hidden' name='cr_activity[]' id='cr_activity[]' value='$cr_activity' />
                            <input type='hidden' name='cr_account[]' id='cr_account[]' value='$cr_account' />

                            </td>
                            <td>
                            $paid_amount
                            <input type='hidden' name='paid_amount[]' id='paid_amount[]' value='$paid_amount' />

                            </td>
                            <td class='text-center'>
                            <input type='checkbox' id='id_detail[]' name='id_detail[]' class='check' value='".$id."'>
                            </td>
                        </tr>
                     </tbody>";
                    }
        $table.= "</table>
        </div>";
        
        

        echo $table;exit();
        }


    }

}
