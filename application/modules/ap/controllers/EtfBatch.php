<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class EtfBatch extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('etf_batch_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('etf_batch.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // $data['bankList'] = $this->etf_batch_model->getBankList();
            // $data['investmentInstitutionList'] = $this->etf_batch_model->investmentInstitutionListByStatus('1');

            $formData['id_bank'] = $this->security->xss_clean($this->input->post('id_bank'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';

 
            $data['searchParam'] = $formData;

            $data['etfBatchList'] = $this->etf_batch_model->getEtfBatchListSearch($formData);

            // echo "<Pre>";print_r($data['etfBatchList']);exit;
            $this->global['pageTitle'] = 'FIMS : List Etf Batch';
            $this->loadViews("etf_batch/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('etf_batch.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {

                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                $generated_number = $this->etf_batch_model->generateEtfBatchNumber();

                $bank_reference_number = $this->security->xss_clean($this->input->post('bank_reference_number'));
                $batch_id = $this->security->xss_clean($this->input->post('batch_id'));
                $description = $this->security->xss_clean($this->input->post('description'));


                $data = array(
                    'bank_reference_number'=> $bank_reference_number,
                    'description'=> $description,
                    'batch_id'=> $batch_id,
                    'reference_number'=> $generated_number,
                    'created_by' => $user_id,
                    'status'=>'1'
                );
             $inserted_id = $this->etf_batch_model->addEtfBatch($data);

             if($inserted_id)
                {
                    // $referal_update['ap_status'] = 'V0';
                    $referal_update['is_sab_group'] = $inserted_id;

                // echo "<Pre>"; print_r($formData);exit;

                $total_bill_amount = 0;
                 for($i=0;$i<count($formData['id_etf']);$i++)
                 {
                    $id_etf = $formData['id_etf'][$i];
                    // $total_amount = $formData['total_amount'][$i];

                    if($id_etf > 0)
                    {
                        $detail_data = $this->etf_batch_model->getEtfVoucher($id_etf);

                             $detailsData = array(
                                'id_etf_batch'=>$inserted_id,
                                'id_referal'=>$id_etf,
                                'amount'=>$detail_data->total_amount,
                                'created_by'=>$user_id,
                                'status'=> 1
                            );

                        $total_bill_amount = $total_bill_amount + $detail_data->total_amount;

                        $inserted_detail_id = $this->etf_batch_model->addEtfBatchDetail($detailsData);
                        if($inserted_detail_id)
                        {
                            $updated_referal = $this->etf_batch_model->updateEtfVoucher($referal_update,$id_etf);
                        }
                    }

                }
            }
            if($inserted_id)
            {
                $bill_data['total_amount'] = $total_bill_amount;
                $updated_bill_amount = $this->etf_batch_model->updateEtfBatch($bill_data,$inserted_id);
            }
                    // echo "<Pre>"; print_r($inserted_detail_id);exit;
                redirect('/ap/etfBatch/list');
            }            

            // echo "<Pre>";print_r($data);exit();


            $this->global['pageTitle'] = 'FIMS : Add Etf Batch';
            $this->loadViews("etf_batch/add", $this->global, NULL, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('etf_batch.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/ap/etfBatch/list');
            }
            
            $data['etfBatch'] = $this->etf_batch_model->getEtfBatch($id);
            $data['etfBatchDetails'] = $this->etf_batch_model->getEtfBatchDetails($id);
            // echo "<Pre>";print_r($data);exit();

            

            $this->global['pageTitle'] = 'FIMS : View Etf Batch';
            $this->loadViews("etf_batch/edit", $this->global, $data, NULL);
        }
    }


    function searchEtfVoucher()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit();

        // $tempData['from_date'] = date('Y-m-d',strtotime($tempData['from_date']));
        // $tempData['to_date'] = date('Y-m-d',strtotime($tempData['to_date']));


        $voucher_data = $this->etf_batch_model->searchEtfVoucher($tempData);
        // echo "<Pre>";print_r($voucher_data);exit();

        if(!empty($voucher_data))
         {
             $table = "
        <h4>ETF Vouchers For Etf Batch</h4>
        <div class='custom-table'>
        <table class='table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Etf Reference Number</th>
                    <th>Batch Id </th>
                    <th>Letter Reference Number</th>
                    <th>Payment Mode</th>
                    <th>Payment Date</th>
                    <th>Bank</th>
                    <th>Amount</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                    </tr>
                    </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($voucher_data);$i++)
                    {
                        $id = $voucher_data[$i]->id;
                        $reference_number = $voucher_data[$i]->reference_number;
                        $payment_mode = $voucher_data[$i]->payment_mode;
                        $letter_reference_number = $voucher_data[$i]->letter_reference_number;
                        $payment_date = $voucher_data[$i]->payment_date;
                        $bank_name = $voucher_data[$i]->bank_name;
                        $bank_code = $voucher_data[$i]->bank_code;
                        $total_amount = $voucher_data[$i]->total_amount;
                        $batch_id = $voucher_data[$i]->batch_id;



                        $j=$i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j .
                            </td>
                            <td>$reference_number</td>
                            <td>$batch_id</td>
                            <td>$letter_reference_number</td>
                            <td>$payment_mode</td>
                            <td>$payment_date</td>
                            <td>$bank_code - $bank_name</td>
                            <td>$total_amount</td>
                            
                            <td class='text-center'>

                            <input type='checkbox' id='id_etf[]' name='id_etf[]' class='check' value='".$id."'>
                        </tr>
                        </tbody>";
                    }

                    // <td>
                    //             <span onclick='getTempData($id)'>Edit</a>
                    //         <td>
        $table.= "</table>
                </div>";

         

         }
         else
         {
               $table = "
        <h4>No ETF Voucher Availble For Selected Search</h4>";

        }
        

        echo $table;exit();
    }
}
