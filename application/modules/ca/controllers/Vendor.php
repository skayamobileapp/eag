<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Vendor extends BaseController
{
    public function __construct()
    {
        parent::__construct();
                $this->load->model('vendor_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('vendor.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

 
            $data['vendorList'] = $this->vendor_model->vendorList();


            $this->global['pageTitle'] = 'FIMS : Vendor';
            //print_r($subjectDetails);exit;
            $this->loadViews("vendor/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('vendor.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // print_r(expression)

            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                $generated_number = $this->vendor_model->generateVendorNumber();

                $vname = $this->security->xss_clean($this->input->post('vname'));
                $vemail = $this->security->xss_clean($this->input->post('vemail'));
                $vphone = $this->security->xss_clean($this->input->post('vphone'));
                $vnric = $this->security->xss_clean($this->input->post('vnric'));
                $gender = $this->security->xss_clean($this->input->post('gender'));
                $vmobile = $this->security->xss_clean($this->input->post('vmobile'));
                $vaddress_one = $this->security->xss_clean($this->input->post('vaddress_one'));
                $vaddress_two = $this->security->xss_clean($this->input->post('vaddress_two'));
                $vcountry = $this->security->xss_clean($this->input->post('vcountry'));
                $vstate = $this->security->xss_clean($this->input->post('vstate'));
                $vzipcode = $this->security->xss_clean($this->input->post('vzipcode'));
                $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));


                $data = array(
                    'name' => $vname,
                    'code' => $generated_number,
                    'email' => $vemail,
                    'phone' => $vphone,
                    'nric' => $vnric,
                    'gender' => $gender,
                    'mobile' => $vmobile,
                    'address_one' => $vaddress_one,
                    'address_two' => $vaddress_two,
                    'country' => $vcountry,
                    'state' => $vstate,
                    'zipcode' => $vzipcode,
                    'date_of_birth' => date("Y-m-d", strtotime($date_of_birth)),
                );

                $inserted_id = $this->vendor_model->addNewVendor($data);

                $cname = $this->security->xss_clean($this->input->post('cname'));
                $cemail = $this->security->xss_clean($this->input->post('cemail'));
                $cphone = $this->security->xss_clean($this->input->post('cphone'));
                $tax_no = $this->security->xss_clean($this->input->post('tax_no'));
                $caddress_one = $this->security->xss_clean($this->input->post('caddress_one'));
                $caddress_two = $this->security->xss_clean($this->input->post('caddress_two'));
                $ccountry = $this->security->xss_clean($this->input->post('ccountry'));
                $cstate = $this->security->xss_clean($this->input->post('cstate'));
                $czipcode = $this->security->xss_clean($this->input->post('czipcode'));

                     $companyData = array(
                        'name' => $cname,
                        'email' => $cemail,
                        'phone' => $cphone,
                        'tax_no' => $tax_no,
                        'address_one' => $caddress_one,
                        'address_two' => $caddress_two,
                        'country' => $ccountry,
                        'state' => $cstate,
                        'zipcode' => $czipcode,
                        'id_vendor' => $inserted_id,
                    );
                
                $result = $this->vendor_model->addNewCompany($companyData);

                $id_session = $this->session->my_session_id;

                   $details = $this->vendor_model->getTempBank($id_session);
                 for($i=0;$i<count($details);$i++)
                 {
                    $id_bank = $details[$i]->id_bank;
                    $address = $details[$i]->address;
                    $country = $details[$i]->country;
                    $state = $details[$i]->state;
                    $city = $details[$i]->city;
                    $zipcode = $details[$i]->zipcode;
                    $branch_no = $details[$i]->branch_no;
                    $acc_no = $details[$i]->acc_no;


                     $detailsData = array(
                        'id_vendor' => $inserted_id,
                        'id_bank' => $id_bank,
                        'address' => $address,
                        'country' => $country,
                        'state' => $state,
                        'city' => $city,
                        'zipcode' => $zipcode,
                        'branch_no' => $branch_no,
                        'acc_no' => $acc_no,
                    );
                    $result = $this->vendor_model->addNewBankDetails($detailsData);
                    $this->vendor_model->deleteTempData($id_session);
                }

                    $details = $this->vendor_model->getTempBilling($id_session);
                 for($i=0;$i<count($details);$i++)
                 {
                    $name = $details[$i]->name;
                    $nric = $details[$i]->nric;
                    $email = $details[$i]->email;
                    $phone = $details[$i]->phone;

                     $detailsData = array(
                        'id_vendor' => $inserted_id,
                        'name' => $name,
                        'nric' => $nric,
                        'email' => $email,
                        'phone' => $phone,
                    );
                    $result = $this->vendor_model->addNewBillingDetails($detailsData);
                    $this->vendor_model->deleteTempData1($id_session);
                 }

             redirect('/procurement/vendor/list');

            }

            $data['countryList'] = $this->vendor_model->countryListByStatus('1');

            $this->global['pageTitle'] = 'FIMS : Add Vendor';
            $this->loadViews("vendor/add", $this->global, $data, NULL);

        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('vendor.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/procurement/vendor/list');
            }
            if($this->input->post())
            {
                $id_student = $id;
               $qualification_level = $this->security->xss_clean($this->input->post('qualification_level'));
                $degree_awarded = $this->security->xss_clean($this->input->post('degree_awarded'));
                $specialization = $this->security->xss_clean($this->input->post('specialization'));
                $class_degree = $this->security->xss_clean($this->input->post('class_degree'));
                $result = $this->security->xss_clean($this->input->post('result'));
                $year = $this->security->xss_clean($this->input->post('year'));
                $medium = $this->security->xss_clean($this->input->post('medium'));
                $college_country = $this->security->xss_clean($this->input->post('college_country'));
                $college_name = $this->security->xss_clean($this->input->post('college_name'));
                $certificate = $this->security->xss_clean($this->input->post('certificate'));
                $transcript = $this->security->xss_clean($this->input->post('transcript'));


                $data = array(
                    'id_student' => $id_student,
                    'qualification_level' => $qualification_level,
                    'degree_awarded' => $degree_awarded,
                    'specialization' => $specialization,
                    'class_degree' => $class_degree,
                    'result' => $result,
                    'year' => $year,
                    'medium' => $medium,
                    'college_country' => $college_country,
                    'college_name' => $college_name,
                    'certificate' => $certificate,
                    'transcript' => $transcript
                );
                if ($qualification_level != "") {
                    $result = $this->student_model->addExamDetails($data);
                }

                $id_student = $id;
               $test = $this->security->xss_clean($this->input->post('test'));
                $date = $this->security->xss_clean($this->input->post('date'));
                $score = $this->security->xss_clean($this->input->post('score'));
                $file = $this->security->xss_clean($this->input->post('file'));

                $data = array(
                    'id_student' => $id_student,
                    'test' => $test,
                    'date' => date("Y-m-d", strtotime($date)),
                    'score' => $score,
                    'file' => $file
                );
                if ($test != "") {
                    $result = $this->student_model->addProficiencyDetails($data);
                }

                $id_student = $id;
               $company_name = $this->security->xss_clean($this->input->post('company_name'));
                $company_address = $this->security->xss_clean($this->input->post('company_address'));
                $telephone = $this->security->xss_clean($this->input->post('telephone'));
                $fax_num = $this->security->xss_clean($this->input->post('fax_num'));
                $designation = $this->security->xss_clean($this->input->post('designation'));
                $position = $this->security->xss_clean($this->input->post('position'));
                $service_year = $this->security->xss_clean($this->input->post('service_year'));
                $industry = $this->security->xss_clean($this->input->post('industry'));
                $job_desc = $this->security->xss_clean($this->input->post('job_desc'));
                $employment_letter = $this->security->xss_clean($this->input->post('employment_letter'));

                $data = array(
                    'id_student' => $id_student,
                    'company_name' => $company_name,
                    'company_address' => $company_address,
                    'telephone' => $telephone,
                    'fax_num' => $fax_num,
                    'designation' => $designation,
                    'position' => $position,
                    'service_year' => $service_year,
                    'industry' => $industry,
                    'job_desc' => $job_desc,
                    'employment_letter' => $employment_letter
                );
                if ($company_name != "") {
                    $result = $this->student_model->addEmploymentDetails($data);
                }

                $id_student = $id;
               $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $id_type = $this->security->xss_clean($this->input->post('id_type'));
                $id_number = $this->security->xss_clean($this->input->post('id_number'));
                $passport_expiry_date = $this->security->xss_clean($this->input->post('passport_expiry_date'));
                $gender = $this->security->xss_clean($this->input->post('gender'));
                $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
                $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
                $religion = $this->security->xss_clean($this->input->post('religion'));
                $nationality = $this->security->xss_clean($this->input->post('nationality'));
                $nationality_type = $this->security->xss_clean($this->input->post('nationality_type'));
                $race = $this->security->xss_clean($this->input->post('race'));
                $email_id = $this->security->xss_clean($this->input->post('email_id'));
                $mail_address = $this->security->xss_clean($this->input->post('mail_address'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $postcode = $this->security->xss_clean($this->input->post('postcode'));
                $country = $this->security->xss_clean($this->input->post('country'));
                $state = $this->security->xss_clean($this->input->post('state'));
                $city = $this->security->xss_clean($this->input->post('city'));

                $data = array(
                    'id_student' => $id_student,
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'id_type' => $id_type,
                    'id_number' => $id_number,
                    'passport_expiry_date' => $passport_expiry_date,
                    'gender' => $gender,
                    'date_of_birth' => date("Y-m-d", strtotime($date_of_birth)),
                    'martial_status' => $martial_status,
                    'religion' => $religion,
                    'nationality' => $nationality,
                    'nationality_type' => $nationality_type,
                    'race' => $race,
                    'email_id' => $email_id,
                    'mail_address' => $mail_address,
                    'address' => $address,
                    'postcode' => $postcode,
                    'country' => $country,
                    'state' => $state,
                    'city' => $city
                );

                if ($first_name != "") {
                    $result = $this->student_model->addProfileDetails($data);
                }

                $id_student = $id;
               $malaysian_visa = $this->security->xss_clean($this->input->post('malaysian_visa'));
                $visa_expiry_date = $this->security->xss_clean($this->input->post('visa_expiry_date'));
                $visa_status = $this->security->xss_clean($this->input->post('visa_status'));

                $data = array(
                    'id_student' => $id_student,
                    'malaysian_visa' => $malaysian_visa,
                    'visa_expiry_date' => $visa_expiry_date,
                    'visa_status' => $visa_status
                );
                if ($malaysian_visa != "") {
                    $result = $this->student_model->addVisaDetails($data);
                }

                $id_student = $id;
               $doc_name = $this->security->xss_clean($this->input->post('doc_name'));
                $doc_file = $this->security->xss_clean($this->input->post('doc_file'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));

                $data = array(
                    'id_student' => $id_student,
                    'doc_name' => $doc_name,
                    'doc_file' => $doc_file,
                    'remarks' => $remarks
                );
                if ($malaysian_visa != "") {
                    $result = $this->student_model->addOtherDocuments($data);
                }
                    redirect($_SERVER['HTTP_REFERER']);

            }

            $data['studentDetails'] = $this->student_model->getStudentDetails($id);

            $data['examDetails'] = $this->student_model->getExamDetails($id);
            $data['proficiencyDetails'] = $this->student_model->getProficiencyDetails($id);
            $data['employmentDetails'] = $this->student_model->getEmploymentDetails($id);
            $data['profileDetails'] = $this->student_model->getProfileDetails($id);
            $data['visaDetails'] = $this->student_model->getVisaDetails($id);
            $data['otherDocuments'] = $this->student_model->getOtherDocuments($id);
            $data['countryList'] = $this->vendor_model->countryListByStatus('1');

            $this->global['pageTitle'] = 'FIMS : Edit Student';
            $this->loadViews("student/edit", $this->global, $data, NULL);
        }
    }

    function tempAddBank()
    {
        $id_session = $this->session->my_session_id;
        $id_bank = $this->security->xss_clean($this->input->post('id_bank'));
        $address = $this->security->xss_clean($this->input->post('address'));
        $country = $this->security->xss_clean($this->input->post('country'));
        $state = $this->security->xss_clean($this->input->post('state'));
        $city = $this->security->xss_clean($this->input->post('city'));
        $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
        $branch_no = $this->security->xss_clean($this->input->post('branch_no'));
        $acc_no = $this->security->xss_clean($this->input->post('acc_no'));

        $data = array(
               'id_session' => $id_session,
               'id_bank' => $id_bank,
               'address' => $address,
               'country' => $country,
               'state' => $state,
               'city' => $city,
               'zipcode' => $zipcode,
               'branch_no' => $branch_no,
               'acc_no' => $acc_no
            );

        $inserted_id = $this->vendor_model->addNewTempBank($data);

        $details = array(
                'id' => $inserted_id,
                'id_bank' => $id_bank,
                'branch_no' => $branch_no,
                'acc_no' => $acc_no,
                'address' => $address,
                'zipcode' => $zipcode,
            );
        $details = $this->vendor_model->getTempBank($id_session);

        if(!empty($details))
        {  
            $table = "
            <table  class='table' id='list-table'>
                <tr>
                    <th>Bank ID</th>
                    <th>Branch Number</th>
                    <th>Account Number</th>
                    <th>Address</th>
                    <th>Zipcode</th>
                </tr>";
                for($i=0;$i<count($details);$i++)
                {
                    $id_bank = $details[$i]->id_bank;
                    $branch_no = $details[$i]->branch_no;
                    $acc_no = $details[$i]->acc_no;
                    $address = $details[$i]->address;
                    $zipcode = $details[$i]->zipcode;
                    $id = $details[$i]->id;

                    $table .= "
                <tr>
                    <td>$id_bank</td>
                    <td>$branch_no</td>
                    <td>$acc_no</td>
                    <td>$address</td>
                    <td>$zipcode</td>
                </tr>";
                }
                        
            $table .= "
            </table>";
            echo $table;
        }
    }

    function tempAddBilling()
    {
        $id_session = $this->session->my_session_id;
        $name = $this->security->xss_clean($this->input->post('name'));
        $nric = $this->security->xss_clean($this->input->post('nric'));
        $email = $this->security->xss_clean($this->input->post('email'));
        $phone = $this->security->xss_clean($this->input->post('phone'));

        $data = array(
               'id_session' => $id_session,
               'name' => $name,
               'nric' => $nric,
               'email' => $email,
               'phone' => $phone
            );

        $inserted_id = $this->vendor_model->addNewTempBilling($data);

        $details = array(
                'id' => $inserted_id,
                'name' => $name,
                'nric' => $nric,
                'email' => $email,
                'address' => $phone,
            );
        $details = $this->vendor_model->getTempBilling($id_session);

        if(!empty($details))
        {  
            $table = "
            <table  class='table' id='list-table'>
                <tr>
                    <th>Name</th>
                    <th>NRIC</th>
                    <th>Email</th>
                    <th>Phone</th>
                </tr>";
                for($i=0;$i<count($details);$i++)
                {
                    $name = $details[$i]->name;
                    $nric = $details[$i]->nric;
                    $email = $details[$i]->email;
                    $phone = $details[$i]->phone;
                    $id = $details[$i]->id;

                    $table .= "
                <tr>
                    <td>$name</td>
                    <td>$nric</td>
                    <td>$email</td>
                    <td>$phone</td>
                </tr>";
                }
                        
            $table .= "
            </table>";
            echo $table;
        }
    }

    function deleteid()
    {
        $id = $this->input->get('id');


       $this->vendor_model->deleteTempBankDetails($id);

       redirect($_SERVER['HTTP_REFERER']);
    }

    function delete_english()
    {
        $id = $this->input->get('id');

       $this->student_model->deleteProficiencyDetails($id);

       redirect($_SERVER['HTTP_REFERER']);  
    }

    function delete_employment()
    {
        $id = $this->input->get('id');

       $this->student_model->deleteEmploymentDetails($id);

       redirect($_SERVER['HTTP_REFERER']);  
    }

    function delete_document()
    {
        $id = $this->input->get('id');

       $this->student_model->deleteOtherDocument($id);

       redirect($_SERVER['HTTP_REFERER']);  
    }

    function getStateByCountry($id_country)
    {
        $results = $this->vendor_model->getStateByCountryId($id_country);

        // echo "<Pre>"; print_r($programme_data);exit;
        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>
     ";

        $table.="
        <select name='vstate' id='vstate' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }

    function getStateByCountryCompany($id_country)
    {
        $results = $this->vendor_model->getStateByCountryId($id_country);

        // echo "<Pre>"; print_r($programme_data);exit;
        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>
     ";

        $table.="
        <select name='cstate' id='cstate' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;

    }

    function getStateByCountryBank($id_country)
    {
        $results = $this->vendor_model->getStateByCountryId($id_country);

        // echo "<Pre>"; print_r($programme_data);exit;
        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>
     ";

        $table.="
        <select name='state' id='state' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;

    }
}
