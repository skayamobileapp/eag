<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Purchase Order Entry</h3>
            </div>
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PR Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $poMaster->pr_number;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PR Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $poMaster->pr_description;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PR Entry Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="pr_entry_date" name="pr_entry_date" value="<?php echo date('d-m-Y',strtotime($poMaster->pr_entry_date));?>" readonly="readonly">
                    </div>
                </div>
            </div>


             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $poMaster->financial_year;?>" readonly="readonly">
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $poMaster->budget_year;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code<span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $poMaster->department_code . ' - ' . $poMaster->department_name;?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Vendor <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $poMaster->vendor_code . ' - ' . $poMaster->vendor_name; ?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PO Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $poMaster->po_number;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PO Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $poMaster->description;?>" readonly="readonly">
                    </div>
                </div>

            </div>           

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PO Entry Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="pr_entry_date" name="pr_entry_date" value="<?php echo date('d-m-Y',strtotime($poMaster->po_entry_date));?>" readonly="readonly">
                    </div>
                </div>

          

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $poMaster->type;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $poMaster->amount;?>" readonly="readonly">
                    </div>
                </div>

             </div>           

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($poMaster->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($poMaster->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($poMaster->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <hr>

            <h3>Purchase Order Details</h3>

        <div class="custom-table">
        <table class="table">
            <thead>
                 <tr>
                     <th>Sl. No</th>
                     <th>DEBIT GL CODE</th>
                      <th>CREDIT GL CODE</th>
                     <th>Category</th>
                     <th>Sub Category</th>
                     <th>Item</th>
                     <th>Tax</th>
                     <th>Qty</th>
                     <th>Price</th>
                     <th>Tax Amount</th>
                     <th>Final Total</th>
                 </tr>
            </thead>
            <tbody>
                 <?php 
                  $total = 0;
                 for($i=0;$i<count($poDetails);$i++)
                    { 
                    // echo "<Pre>";print_r($poDetails[$i]);exit();

                        ?>
                    <tr>
                    <td><?php echo $i+1;?></td>
                   <td><?php echo $poDetails[$i]->dt_fund . " - " . $poDetails[$i]->dt_department . " - " . $poDetails[$i]->dt_activity . " - " . $poDetails[$i]->dt_account;?></td>
                    <td><?php echo $poDetails[$i]->cr_fund . " - " . $poDetails[$i]->cr_department . " - " . $poDetails[$i]->cr_activity . " - " . $poDetails[$i]->cr_account;?></td>
                    <td><?php echo $poDetails[$i]->category_code . " - " . $poDetails[$i]->category_name; ?></td>
                    <td><?php echo $poDetails[$i]->sub_category_code . " - " . $poDetails[$i]->sub_category_name;?></td>
                    <td><?php echo $poDetails[$i]->item_code . " - " . $poDetails[$i]->item_name;?></td>
                    <td><?php echo $poDetails[$i]->tax_code . " - " . $poDetails[$i]->tax_name;?></td>
                    <td><?php echo $poDetails[$i]->quantity;?></td>
                    <td><?php echo $poDetails[$i]->price;?></td>
                    <td><?php echo $poDetails[$i]->tax_price;?></td>
                    <td><?php echo $poDetails[$i]->total_final;?></td>

                     </tr>
                  <?php 
                  $total = $total + $poDetails[$i]->total_final;
                }
                $total = number_format($total, 2, '.', ',');
                ?>

                <tr>
                    <td bgcolor="" colspan="9"></td>
                    <td bgcolor=""><b> Total : </b></td>
                    <td bgcolor=""><b><?php echo $total; ?></b></td>
                </tr>

            </tbody>
        </table>
        </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../approvalList" class="btn btn-link">Back</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>