<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Procurement Limit</h3>
        </div>
        <form id="form_grade" action="" method="post">
            <div class="row">
               
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Procurement Name <span class='error-text'>*</span></label>
                        <select name="name" id="name" class="form-control">
                            <option value="">Select</option>
                            <option value="PO"
                                <?php 
                                if ($procurementLimit->name == 'PO')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "PO";  ?>
                            </option>

                            <option value="Direct Purchaase"
                                <?php 
                                if ($procurementLimit->name == 'Direct Purchase')
                                {
                                    echo "selected=selected";
                                } ?>
                                >
                                        <?php echo "Direct Purchase";  ?>
                            </option>

                            <option value="Warrant"
                                <?php 
                                if ($procurementLimit->name == 'Warrant')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Warrant";  ?>
                            </option>

                            <option value="Tender"
                                <?php 
                                if ($procurementLimit->name == 'Tender')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Tender";  ?>
                            </option>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="from_limit" name="from_limit" value="<?php echo $procurementLimit->from_limit; ?>">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="to_limit" name="to_limit" value="<?php echo $procurementLimit->to_limit; ?>">
                    </div>
                </div>

            </div>

            <div class="row">
                
                 <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($procurementLimit->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($procurementLimit->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>               
                
            </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 from_limit: {
                    required: true
                },
                 to_limit: {
                    required: true
                },
                 status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                from_limit: {
                    required: "<p class='error-text'>Min. Limit Required</p>",
                },
                to_limit: {
                    required: "<p class='error-text'>Max. Limit Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Status Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>