<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View NON-PO Entry</h3>
            </div>
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PR Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->pr_number;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PR Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->pr_description;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PR Entry Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="pr_entry_date" name="pr_entry_date" value="<?php echo $nonPoMaster->pr_entry_date;?>" readonly="readonly">
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>NON-PO Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->nonpo_number;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>NON-PO Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->description;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PO Entry Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="pr_entry_date" name="pr_entry_date" value="<?php echo $nonPoMaster->nonpo_entry_date;?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->financial_year;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Vendor <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->vendor_code . ' - ' . $nonPoMaster->vendor_name; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->department_code . ' - ' . $nonPoMaster->department_name;?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->type;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $nonPoMaster->amount;?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <hr>

            <h3>NON-PO Details</h3>

        <table width="100%">
            <thead>
                 <tr>
                     <th>Sl. No</th>
                     <th>CR Fund</th>
                     <th>CR Department</th>
                     <th>CR Activity</th>
                     <th>CR Account</th>
                     <th>Dbt Fund</th>
                     <th>Dbt Department</th>
                     <th>Dbt Activity</th>
                     <th>Dbt Account</th>
                     <th>Category</th>
                     <th>Sub Category</th>
                     <th>Item</th>
                     <th>Tax</th>
                     <th>Qty</th>
                     <th>Price</th>
                     <th>Tax Amount</th>
                     <th>Final Total</th>
                 </tr>
            </thead>
            <tbody>
                 <?php 
                  $total = 0;
                 for($i=0;$i<count($nonPoDetails);$i++)
                    { 
                    // echo "<Pre>";print_r($nonPoDetails[$i]);exit();

                        ?>
                    <tr>
                    <td><?php echo $i+1;?></td>
                    <td><?php echo $nonPoDetails[$i]->cr_account;?></td>
                    <td><?php echo $nonPoDetails[$i]->cr_activity;?></td>
                    <td><?php echo $nonPoDetails[$i]->cr_department;?></td>
                    <td><?php echo $nonPoDetails[$i]->cr_fund;?></td>
                    <td><?php echo $nonPoDetails[$i]->dt_account;?></td>
                    <td><?php echo $nonPoDetails[$i]->dt_activity;?></td>
                    <td><?php echo $nonPoDetails[$i]->dt_department;?></td>
                    <td><?php echo $nonPoDetails[$i]->dt_fund;?></td>
                    <td><?php echo $nonPoDetails[$i]->category_code . " - " . $nonPoDetails[$i]->category_name; ?></td>
                    <td><?php echo $nonPoDetails[$i]->sub_category_code . " - " . $nonPoDetails[$i]->sub_category_name;?></td>
                    <td><?php echo $nonPoDetails[$i]->item_code . " - " . $nonPoDetails[$i]->item_name;?></td>
                    <td><?php echo $nonPoDetails[$i]->tax_code . " - " . $nonPoDetails[$i]->tax_name;?></td>
                    <td><?php echo $nonPoDetails[$i]->quantity;?></td>
                    <td><?php echo $nonPoDetails[$i]->price;?></td>
                    <td><?php echo $nonPoDetails[$i]->tax_price;?></td>
                    <td><?php echo $nonPoDetails[$i]->total_final;?></td>

                     </tr>
                  <?php 
                  $total = $total + $nonPoDetails[$i]->total_final;
                }
                $total = number_format($total, 2, '.', ',');
                ?>

                <tr>
                    <td bgcolor="" colspan="15"></td>
                    <td bgcolor=""><b> Total : </b></td>
                    <td bgcolor=""><b><?php echo $total; ?></b></td>
                </tr>

            </tbody>
        </table>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../approvalList" class="btn btn-link">Back</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>