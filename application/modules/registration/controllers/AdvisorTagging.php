<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AdvisorTagging extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('advisor_tagging_model');
        $this->isLoggedIn();
    }

    function add()
    {
        if ($this->checkAccess('advisor_tagging.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $formData = $this->input->post();
                
                $id_advisor = $this->security->xss_clean($this->input->post('id_advisor_for_tagging'));



                for($i=0;$i<count($formData['id_student']);$i++)
                {
                    $id_student = $formData['id_student'][$i];

                    if($id_student > 0 && $id_advisor > 0)
                    {
                            $data = array(
                                'id_advisor'=>$id_advisor,
                                'updated_by'=>$id_user
                            );
                    // echo "<Pre>"; print_r($this->input->post());exit;
                    // echo "<Pre>"; print_r($detailsData);exit;
                        $updated_student_data = $this->advisor_tagging_model->updateStudent($data,$id_student);

                            if($updated_student_data)
                            {
                                $data_advisor = array(
                                'id_advisor'=>$id_advisor,
                                'id_student'=>$id_student,
                                'created_by'=>$id_user
                                );
                                
                                $added_advisor_data = $this->advisor_tagging_model->addAdvisorTagging($data_advisor);
                            
                            }
                    }
                }
                
                redirect($_SERVER['HTTP_REFERER']);
            }


            $data['intakeList'] = $this->advisor_tagging_model->intakeListByStatus('1');
            $data['programList'] = $this->advisor_tagging_model->programListByStatus('1');
            $data['semesterList'] = $this->advisor_tagging_model->semesterListByStatus('1');
            $data['staffList'] = $this->advisor_tagging_model->staffListByStatus('1');
            $data['qualificationList'] = $this->advisor_tagging_model->qualificationListByStatus('1');

            // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Campus Management System : Advisor Taagging';
            $this->loadViews("advisor_tagging/add", $this->global, $data, NULL);
        }
    }

    function searchStudents()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $staffList = $this->advisor_tagging_model->staffListByStatus('1');
        
        $student_data = $this->advisor_tagging_model->studentSearch($tempData);

        // echo "<Pre>";print_r($student_data);exit();
        if(!empty($student_data))
        {


         $table = "

         <script type='text/javascript'>
             $('select').select2();
         </script>

         <h4>Advisor Tagging For Students</h4>


         <div class='row'>
            <div class='col-sm-4'>
                <div class='form-group'>
                <label>Select Advisor </label>
                <select name='id_advisor_for_tagging' id='id_advisor_for_tagging' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($staffList);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $staffList[$i]->id;
            $ic_no = $staffList[$i]->ic_no;
            $name = $staffList[$i]->name;
            $table.="<option value=".$id.">".$ic_no . " - " . $name .
                    "</option>";

            }
            $table .="
                </select>
                </div>
              </div>

            </div>
            ";


         $table .= "
         <br>
         <h4> Select Students For Advisor Tagging</h4>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Student NRIC</th>
                    <th>Program</th>
                    <th>Intake</th>
                    <th>Qualification</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th style='text-align: center;'>Advisor</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $full_name = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;
                $email_id = $student_data[$i]->email_id;
                $phone = $student_data[$i]->phone;
                $program_code = $student_data[$i]->program_code;
                $program_name = $student_data[$i]->program_name;
                $intake_year = $student_data[$i]->intake_year;
                $intake_name = $student_data[$i]->intake_name;
                $qualification_code = $student_data[$i]->qualification_code;
                $qualification_name = $student_data[$i]->qualification_name;
                $ic_no = $student_data[$i]->ic_no;
                $advisor_name = $student_data[$i]->advisor_name;
                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$full_name</td>                        
                    <td>$nric</td>                           
                    <td>$program_code - $program_name</td>                           
                    <td>$intake_year - $intake_name</td>                           
                    <td>$qualification_name</td>                           
                    <td>$email_id</td>                      
                    <td>$phone</td>                      
                    <td style='text-align: center;'>$ic_no - $advisor_name</td>                  
                    
                    <td class='text-center'>
                        <input type='checkbox' id='id_student[]' name='id_student[]' class='check' value='".$id."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Data Found For Your Search</h4>";
        }
        echo $table;exit;
    }
}