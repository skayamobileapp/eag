<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Credit_transfer_model extends CI_Model
{
    function getSemesterListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('semester as a');
        $this->db->where('a.status', $status);
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();      
         return $result;
    }

    function courseListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('course as a');
        $this->db->where('a.status', $status);
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();      
         return $result;
    }

    function gradeListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('grade as a');
        $this->db->where('a.status', $status);
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();      
         return $result;
    }

    function equivalentCourseListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('course as a');
        $this->db->where('a.status', $status);
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();      
         return $result;
    }

    function creditTransferListSearch($data)
    {
        // $date = 
        $this->db->select('a.*, s.code as semester_code, s.name as semester_name, stu.nric, stu.full_name, p.code as program_code, p.name as program_name');
        $this->db->from('credit_transfer as a');
        $this->db->join('semester as s', 'a.id_semester = s.id');
        $this->db->join('student as stu', 'a.id_student = stu.id');
        $this->db->join('programme as p', 'stu.id_program = p.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(application_id  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_semester'] != '')
        {
            $this->db->where('a.id_semester', $data['id_semester']);
        }
        if ($data['application_type'] != '')
        {
            $this->db->where('a.application_type', $data['application_type']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('a.status', $data['status']);
        }
         $query = $this->db->get();
         $result = $query->result();  

         // $list = array();
         // foreach ($result as $value)
         // {
         //    $data = $this->getExamCenterNLocationByCenterId($value->id_exam_center);
         //    $value->exam_center = $data['exam_center'];
         //    $value->location = $data['location'];
            
         //    array_push($list, $value);
         // }
         return $result;
    }

    function getCreditTransfer($id)
    {
        $this->db->select('*');
        $this->db->from('credit_transfer');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addCreditTransfer($data)
    {
        $this->db->trans_start();
        $this->db->insert('credit_transfer', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editCreditTransfer($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('credit_transfer', $data);
        return TRUE;
    }

    function saveTempDetailData($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_credit_transfer_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempCreditTransferDetailsBySessionId($id_session)
    {
        $this->db->select('tctd.*, co.name as course_name, co.code as course_code, c.name as e_course_name, c.code as e_course_code, g.name as grade_code, g.description as grade_name');
        $this->db->from('temp_credit_transfer_details as tctd');
        $this->db->join('grade as g', 'tctd.id_grade = g.id');
        $this->db->join('course as c', 'tctd.id_equivalent_course = c.id');
        $this->db->join('course as co', 'tctd.id_course = co.id','left');
        $this->db->where('tctd.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteTempCreditTransfer($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_credit_transfer_details');
        return TRUE;
    }


    function deleteTempCreditTransferBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_credit_transfer_details');
        return TRUE;
    }

    function getCourseCreditHours($id)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('id', $id);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->row();

         if($result)
         {
            return $result->credit_hours;
         }
        return 0;
    }




    function examCenterLocationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function studentListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('applicant_status', 'Approved');
        $this->db->order_by("full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function moveDetailDataFromTempToMain($id_credit_transfer)
    {
        $id_session = $this->session->my_session_id;
        $details = $this->getTempCreditTransferDetailsBySessionIdForMove($id_session);
        foreach ($details as $detail)
        {
            $detail->id_credit_transfer = $id_credit_transfer;
            unset($detail->id_session);
            unset($detail->id);

            $added = $this->addCreditTransferDetails($detail);
            # code...
        }
        
        $added = $this->deleteTempCreditTransferBySession($id_session);
    }

    function addCreditTransferDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('credit_transfer_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function getTempCreditTransferDetailsBySessionIdForMove($id_session)
    {
        $this->db->select('tctd.*');
        $this->db->from('temp_credit_transfer_details as tctd');
        $this->db->where('tctd.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    

    function getExamCenterNLocationByCenterId($id)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $data['exam_center'] = $query->row()->name;



        $id_location = $query->row()->id_location;

        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->where('id', $id_location);
        $query = $this->db->get();
        $data['location'] = $query->row()->name;

        return $data;
    }

    function generateCreditTransferNumber()
    {

        $year = date('y');
        $Year = date('Y');
            $this->db->select('*');
            $this->db->from('credit_transfer');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;

           $generated_number = "CT" .(sprintf("%'06d", $count)). "/" . $Year;
           // echo "<Pre>";print_r($generated_number);exit();
           return $generated_number;
    }

    function getCreditTransferDetailsByIdCreditTransfer($id_credit_transfer)
    {
        $this->db->select('tctd.*, co.name as course_name, co.code as course_code, c.name as e_course_name, c.code as e_course_code, g.name as grade_code, g.description as grade_name');
        $this->db->from('credit_transfer_details as tctd');
        $this->db->join('grade as g', 'tctd.id_grade = g.id');
        $this->db->join('course as c', 'tctd.id_equivalent_course = c.id');
        $this->db->join('course as co', 'tctd.id_course = co.id','left');
        $this->db->where('tctd.id_credit_transfer', $id_credit_transfer);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteDetailData($id)
    {
         $this->db->where('id', $id);
        $this->db->delete('credit_transfer_details');
        return TRUE;
    }







    function getFeeStructureActivityType($type,$trigger,$id_program)
    {
        $this->db->select('s.*');
        $this->db->from('fee_structure_activity as s');
        $this->db->join('activity_details as a', 's.id_activity = a.id');
        $this->db->where('a.name', $type);
        $this->db->where('s.trigger', $trigger);
        $this->db->where('s.id_program', $id_program);
        $this->db->where('s.status', 1);
        $this->db->order_by('s.id', 'DESC');
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function generateMainInvoice($data,$id_credit_transfer)
    {
        $user_id = $this->session->userId;

        // echo "<Pre>";print_r($id_credit_transfer);exit();

        $id_student = $data['id_student'];
        $add = $data['add'];

        $student_data = $this->getStudent($id_student);

        $credit_transfer = $this->getCreditTransfer($id_credit_transfer);

        $nationality = $student_data->nationality;
        $id_program = $student_data->id_program;
        $id_intake = $student_data->id_intake;


        $application_type = $credit_transfer->application_type;

        // echo "<Pre>";print_r($application_type);exit();



        if($add == 1)
        {
            $fee_structure_data = $this->getFeeStructureActivityType($application_type,'Application Level',$id_program);
        }
        elseif($add == 0)
        {
            $fee_structure_data = $this->getFeeStructureActivityType($application_type,'Approval Level',$id_program);
        }


        if($nationality == 'Malaysian')
        {
            $currency = 'MYR';
            $invoice_amount = $fee_structure_data->amount_local;
        }
        elseif($nationality == 'Other')
        {
            $currency = 'USD';
            $invoice_amount = $fee_structure_data->amount_international;
        }

        // echo "<Pre>";print_r($invoice_amount);exit();


        $invoice_number = $this->generateMainInvoiceNumber();


        $invoice['invoice_number'] = $invoice_number;
        $invoice['type'] = 'Student';
        $invoice['remarks'] = 'Student '.$application_type;
        $invoice['id_application'] = '0';
        $invoice['id_program'] = $id_program;
        $invoice['id_intake'] = $id_intake;
        $invoice['id_student'] = $id_student;
        $invoice['id_student'] = $id_student;
        $invoice['currency'] = $currency;
        $invoice['total_amount'] = $invoice_amount;
        $invoice['invoice_total'] = $invoice_amount;
        $invoice['balance_amount'] = $invoice_amount;
        $invoice['paid_amount'] = '0';
        $invoice['status'] = '1';
        $invoice['created_by'] = $user_id;


        // echo "<Pre>";print_r($invoice);exit();

        // $fee_structure_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme);

        
        // $update = $this->editStudentData($id_program_scheme,$id_program,$id_intake,$id_student);
        
        $inserted_id = $this->addNewMainInvoice($invoice);

        if($inserted_id)
        {
            $data = array(
                    'id_main_invoice' => $inserted_id,
                    'id_fee_item' => $fee_structure_data->id_fee_setup,
                    'amount' => $invoice_amount,
                    'status' => 1,
                    'quantity' => 1,
                    'price' => $invoice_amount,
                    'id_reference' => $id_credit_transfer,
                    'description' => $application_type,
                    'created_by' => $user_id
                );

            $this->addNewMainInvoiceDetails($data);
        }
        return TRUE;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        // return $insert_id;
    }

    function getStudent($id_student)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }
}