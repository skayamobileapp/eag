<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_registration_model extends CI_Model
{
    function examRegistrationList($applicantList)
    {
        $this->db->select('DISTINCT(a.id_student) as id_student, a.id as id, in.name as intake, p.name as program, std.full_name, std.nric, std.email_id, in.id as id_intake, p.id as id_programme, sem.code as semester_code, sem.name as semester_name');
        $this->db->from('exam_register as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->join('student as std', 'a.id_student = std.id');
        $this->db->join('semester as sem', 'a.id_semester = sem.id');

        if($applicantList['first_name']) {
            $likeCriteria = "(std.full_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['email_id']) {
            $likeCriteria = "(std.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['nric']) {
            $likeCriteria = "(std.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_program']) {
            $likeCriteria = "(std.id_program  LIKE '%" . $applicantList['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_intake']) {
            $likeCriteria = "(std.id_intake  LIKE '%" . $applicantList['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }

    function examCenterListSearch($search)
    {
        // $date = 
        $this->db->select('*');
        $this->db->from('exam_center');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getPerogramLandscape($id_intake)
    {
        $this->db->select('a.id, a.pre_requisite, in.name as intakeName, c.name as courseName, pl.name as plName, pl.min_total_cr_hrs');
        $this->db->from('add_course_to_program_landscape as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('course as c', 'a.id_course = c.id');
        $this->db->join('programme_landscape as pl', 'a.id_program_landscape = pl.id');
        $this->db->where('a.id_intake', $id_intake);
        $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function getCourseFromCourseRegistr($id_intake,$id_programme,$id_student)
    {
        $status = '0';
        $this->db->select('cr.id, c.id as id_course, in.name as intake_name, c.name as course_name');
        $this->db->from('course_registration as cr');
        $this->db->join('intake as in', 'cr.id_intake = in.id');
        $this->db->join('course as c', 'cr.id_course = c.id');
        $this->db->where('cr.id_intake', $id_intake);
        $this->db->where('cr.id_student', $id_student);
         $this->db->where('cr.id_programme', $id_programme);
         $this->db->where('cr.is_exam_registered', $status);
        $query = $this->db->get();
          $result = $query->result();
         return $result; 
    }


     function getExamRegistrerDetails($id_intake,$id_programme,$id_student)
    {
        $this->db->select('a.*,c.*');
        $this->db->from('exam_registration as a');
        $this->db->join('exam_center as e', 'e.id = in.id_exam_center');
        $this->db->where('a.id_intake', $id_intake);
        $this->db->where('a.id_student', $id_student);
         $this->db->where('a.id_programme', $id_programme);
        $query = $this->db->get();
          $result = $query->result();
         return $result; 
    }


    function getCourseRegistrationList($id){
        $this->db->select('*');
        $this->db->from('course_registration');
        $this->db->where('id', $id);
        $query = $this->db->get();
         return $query->row();
    }

    function studentList($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addExamRegistration($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editCoureRegistration($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('course_registration', $data);
        return TRUE;
    }

    function addCourseList($array, $insert_id){
        foreach ($array as $row){
          $data = ['id_course_programme'=>$row];
          $this->db->insert('courses_from_programme_landscape', $data);
        }
        $data = ['id_course_registration'=>$insert_id];
        $this->db->where_in('id_course_programme', $array);
        $this->db->update('courses_from_programme_landscape', $data);
        return TRUE;
    }

    function getCoursesByProgramNIntakeNStudent($id_intake,$id_programme,$id_student,$id)
    {
         // print_r($id_intake);exit();
        $this->db->select('cou.name, cou.name_in_malay, cou.code, acpl.pre_requisite, pl.min_total_cr_hrs, ec.name as exam_center_name, ec.city, ec.address, a.is_bulk_withdraw, ee.name as event, ee.from_dt, ee.from_tm');
        $this->db->from('exam_registration as a');
        $this->db->join('course as cou', 'a.id_course = cou.id');
        $this->db->join('add_course_to_program_landscape as acpl', 'acpl.id_course = cou.id');
        $this->db->join('programme_landscape as pl', 'acpl.id_program_landscape = pl.id');
        $this->db->join('exam_event as ee', 'a.id_exam_center = ee.id');
        $this->db->join('exam_center as ec', 'ee.id_exam_center = ec.id');
        $this->db->where('pl.id_programme', $id_programme);
        $this->db->where('pl.id_intake', $id_intake);
        $this->db->where('a.id_programme', $id_programme);
        $this->db->where('a.id_intake', $id_intake);
        $this->db->where('a.id_exam', $id);
        $this->db->where('a.id_student', $id_student);
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function semesterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('semester');
         $this->db->where('status', $status);
         $this->db->order_by("name", "ASC");
        $query = $this->db->get();
          $result = $query->result();
         return $result; 
    }

    function examEventListByLocation($id_location)
    {
        $this->db->select('DISTINCT(a.id) as id, a.*');
        $this->db->from('exam_event as a');
        $this->db->join('exam_center_location as ec', 'a.id_location = ec.id');
        $this->db->where('a.id_location', $id_location);
        $this->db->where('a.status', '1');
        // $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function examLocationListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('exam_center_location as a');
        $this->db->where('a.status', $status);
        // $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getExamRegistration($id)
    {
        $this->db->select('a.*, ec.name as location');
        $this->db->from('exam_register as a');
        $this->db->join('exam_center_location as ec', 'a.id_location = ec.id');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result; 
    }

    function addNewExamRegister($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_register', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;      
    }
}