<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Bulk Withdraw</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Bulk Withdraw Details</h4>


            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Programme <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" onchange="getIntakes()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label style="display: none;" id="display_intake">Intake <span class='error-text'>*</span></label>
                        <span id='view_intake' ></span>
                    </div>
                </div>
               
                <div class="col-sm-4">
                    <div class="form-group">
                        <label style="display: none;" id="display_student">Course <span class='error-text'>*</span></label>
                        <span id='view_student' ></span>
                    </div>
                </div>

            </div>

        




            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reason For Withdraw <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reason" name="reason">
                    </div>
                </div>
            </div>


        </div>


        <div class="form-container" id="view_search_students" style="display: none">
          <h4 class="form-group-title">Search Students</h4>

          <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Student Name</label>
                          <input type="text" class="form-control" id="name" name="name">
                      </div>
                  </div>


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>NRIC</label>
                          <input type="text" class="form-control" id="nric" name="nric">
                      </div>
                  </div>


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Email</label>
                          <input type="text" class="form-control" id="email_id" name="email_id">
                      </div>
                  </div>

              </div>

              <br>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" onclick="displayStudentsByIdCourseRegisteredLandscape()">Search</button>
                  </div>

          </div>




        <div class="form-container" id="view_visible" style="display: none;">
            <h4 class="form-group-title">Registered Course For Withdraw Details</h4>


            <div class="custom-table">
                  <div id="view"></div>
            </div>

        </div>






        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();
    
    function getIntakes()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/registration/bulkWithdraw/getIntakes',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_intake").html(result);
                $("#display_intake").show();

                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });
    }


    function getCourseLandscapeByProgNIntake()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        // alert(tempPR['id_programme']);
            $.ajax(
            {
               url: '/registration/bulkWithdraw/getCourseLandscapeByProgNIntake',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#display_student").show();
                $("#view_student").html(result);

                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });
    }



    function displayStudentData()
    {
        var id_intake = $("#id_intake").val();
        var id_programme = $("#id_programme").val();
        var id_course_registered_landscape = $("#id_course_registered_landscape").val();
        // alert(id_student);

        if(id_intake != '' && id_programme != '' && id_course_registered_landscape != '')
        {
            $("#view_search_students").show();
        }   
    }


    

    function displayStudentsByIdCourseRegisteredLandscape()
    {
        var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_course_registered_landscape'] = $("#id_course_registered_landscape").val();
        tempPR['name'] = $("#name").val();
        tempPR['nric'] = $("#nric").val();
        tempPR['email_id'] = $("#email_id").val();

        // alert(id_student);

        if(id_course_registered_landscape != '')
        {

            $.ajax(
            {
               url: '/registration/bulkWithdraw/displayStudentsByIdCourseRegisteredLandscape',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_visible").show();
                $("#view").html(result);
               }
            });

        }   
    }


    function getStudentByProgNIntake()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        // alert(tempPR['id_programme']);
            $.ajax(
            {
               url: '/registration/bulkWithdraw/getStudentByProgNIntake',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#display_student").show();
                $("#view_student").html(result);

                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });
    }

    function displaydata()
    {

        var id_intake = $("#id_intake").val();
        var id_programme = $("#id_programme").val();
        var id_student = $("#id_student").val();
        // alert(id_student);

            $.ajax(
            {
               url: '/registration/bulkWithdraw/displaydata',
                type: 'POST',
               data:
               {
                'id_intake': id_intake,
                'id_programme': id_programme,
                'id_student': id_student
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if (id_programme != '' && id_intake != '')
                {
                    $("#view").html(result);
                    $("#view_visible").show();
                }
               }
                });        
    }




    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                id_programme: {
                    required: true
                },
                 id_intake: {
                    required: true
                },
                id_student: {
                    required: true
                },
                 reason: {
                    required: true
                },
                 id_course_registered_landscape: {
                    required: true
                }
            },
            messages: {
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                reason: {
                    required: "<p class='error-text'>Withdraw Reason Required</p>",
                },
                id_course_registered_landscape: {
                    required: "<p class='error-text'>Select Course</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>