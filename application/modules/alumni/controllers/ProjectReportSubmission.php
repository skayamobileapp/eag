<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProjectReportSubmission extends BaseController
{
    public function __construct()
    {
        // $test = new BaseController();
        // $test->isStudentLoggedIn();
        parent::__construct();
        $this->load->model('project_report_submission_model');
        $this->load->model('edit_profile_model');
        $this->isAlumniLoggedIn();
    }

    function list()
    {       
        $id = $this->session->id_alumni;
        // $check['id_student'] = $this->session->id_student;
        // $check['id_intake'] = $this->session->id_intake;
        // $check['id_program'] = $this->session->id_program;
        // $check['id_qualification'] = $this->session->id_qualification;
        
        $data['studentDetails'] = $this->project_report_submission_model->getStudentByStudentId($id);
        $data['projectReportList'] = $this->project_report_submission_model->getProjectReportListByStudentId($id);


        //$data['maxLimit'] = $this->project_report_submission_model->getMaxLimit();
        // $data['check_limit'] = $this->project_report_submission_model->checkStudentProjectReport($check);


                 // echo "<Pre>";print_r($data);exit();

        $this->global['pageTitle'] = 'Alumni Portal : List Project Report Submision';
        $this->loadViews("project_report_submission/list", $this->global, $data, NULL);
    }
    
    function add()
    {

        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;

        if($this->input->post())
        {
            $duration = $this->security->xss_clean($this->input->post('duration'));
            $description = $this->security->xss_clean($this->input->post('description'));
            $name = $this->security->xss_clean($this->input->post('name'));
            $from_dt = $this->security->xss_clean($this->input->post('from_dt'));
            $to_dt = $this->security->xss_clean($this->input->post('to_dt'));


            // $generated_number = $this->project_report_submission_model->generateProjectReportNumber();

            $m = "+" . $duration . " months";

            $data = array(
                'duration' => $duration,
                // 'application_number' => $generated_number,
                'description' => $description,
                'name' => $name,
                'from_dt' => date('Y-m-d', strtotime($from_dt)),
                'to_dt' => date('Y-m-d', strtotime($to_dt)),
                // 'to_dt' => strtotime(date("Y-m-d", strtotime($from_dt)) . $m),
                'id_qualification' => $id_qualification,
                'id_intake' => $id_intake,
                'id_program' => $id_program,
                'id_student' => $id_student,
                'status' => 0
            );

            // $check_limit = $this->project_report_submission_model->checkStudentProjectReport($data);
            // if($check_limit == 1)
            // {
            //      echo "<Pre>";print_r("Max Application Limit Reached For Active & Pending Project Report Submision");exit();
            // }            
                 // echo "<Pre>";print_r($check_limit);exit();
            $insert_id = $this->project_report_submission_model->addProjectReport($data);
            redirect('/student/projectReportSubmission/list');
        }

        $data['companyTypeList'] = $this->project_report_submission_model->companyTypeListByStatus('1');

        $this->global['pageTitle'] = 'Alumni Portal : Add Project Report Submision';
        $this->loadViews("project_report_submission/add", $this->global, $data, NULL);
    }


    function view($id = NULL)
    {
        $id_student = $this->session->id_alumni;

        if ($id == null)
        {
            redirect('/student/project_reportApplication/list');
        }
        $data['studentDetails'] = $this->project_report_submission_model->getStudentByStudentId($id_student);
        $data['projectReportSubmission'] = $this->project_report_submission_model->getProjectReport($id);

            // echo "<Pre>"; print_r($data);exit;

        $this->global['pageTitle'] = 'Alumni Portal : View Project Report Submision';
        $this->loadViews("project_report_submission/view", $this->global, $data, NULL);
    }





    function getCompanyByCompanyType($id_company_type)
    {
        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;

            $results = $this->project_report_submission_model->getCompanyByCompanyTypeId($id_company_type,$id_program);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>


            <select name='id_company' id='id_company' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $registration_no = $results[$i]->registration_no;
            $table.="<option value=".$id.">".$registration_no  ." - " . $name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }





























    function courseWithdraw()
    {
        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_programme = $this->session->id_program;
        $user_id = $this->session->userId;

        if($this->input->post())
        {
            // $id_course = $this->security->xss_clean($this->input->post('id_course'));
            $reason = $this->security->xss_clean($this->input->post('reason'));
            $id_exam_register = $this->security->xss_clean($this->input->post('id_exam_register'));
            // $reason = $this->security->xss_clean($this->input->post('reason'));

                $exam_registration_data = $this->project_report_submission_model->getExamRegistration($id_exam_register);
            // echo "<Pre>";print_r($exam_registration_data);exit();

                 $data = array(
                        'id_exam_register' => $id_exam_register,
                        'id_exam_center' => $exam_registration_data->id_exam_center,
                        'id_course' => $exam_registration_data->id_course,
                        'id_semester' => $exam_registration_data->id_semester,
                        'id_student' => $exam_registration_data->id_student,
                        'id_intake' => $exam_registration_data->id_intake,
                        'id_programme' => $exam_registration_data->id_programme,
                        'reason' => $reason,
                        'status' => '0',
                        'created_by' => $user_id
                    );
                     // echo "<Pre>";print_r($data);exit();
                $insert_id = $this->project_report_submission_model->addBulkWithdraw($data);
                if ($insert_id)
                {
                    $update = array(
                        'is_bulk_withdraw' => $insert_id
                    );
                    $updated = $this->project_report_submission_model->updateExamRegistration($update,$id_exam_register);                       
                }
            redirect($_SERVER['HTTP_REFERER']);
        }

        $data['getStudentData'] = $this->edit_profile_model->getStudentData($id_student);
        $data['courseRegistrationList'] = $this->project_report_submission_model->getCourseFromExamRegister($id_intake,$id_programme,$id_student);
        $data['bulkWithdrawList'] = $this->project_report_submission_model->bulkWithdrawList($id_student);
        
                 // echo "<Pre>";print_r($data['courseRegistrationList']);exit();

        $this->global['pageTitle'] = 'Campus Management System : Add Student To Exam Center';
        $this->loadViews("project_report_application/bulk_withdraw", $this->global, $data, NULL);
    }

    function diffDates()
    {
        // // Declare two dates 
        // $start_date = strtotime("2018-06-08"); 
        // $end_date = strtotime("2018-09-01"); 
          
        // // Get the difference and divide into  
        // // total no. seconds 60/60/24 to get  
        // // number of days 
        // echo "Difference between two dates: "
        //     . ($end_date - $start_date)/60/60/24;exit();
    }
}

