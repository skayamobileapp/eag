<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Campus Management</title>
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/main.css">
  <style>
    /* LOGIN CSS STARTS HERE */
.login-wrapper {
  background: url(../assets/img/unnamed_one.jpg) no-repeat left top transparent;
  background-size: cover;
}
.login-new-container {
  background-color: #fff;
  min-height: 100vh;
  margin-left: -15px;
  padding: 1rem 3rem;
}
.login-new-container h1 {
  color: #3639a4;
  text-transform: uppercase;
  font-family: "rubikbold";
}
.login-new-container h3 {
  color: #3639a4;
  margin-top: 3rem;
  margin-bottom: 3rem;
}
.login-new-container .btn-primary {
  margin-bottom: 2rem;
  height: 40px;
  margin-top: 2rem;
}
.d-flex {
  display: flex;
}
.align-items-center {
  align-items: center;
}
.ml-auto {
  margin-left: auto;
}

</style>
</head>

<body>
    <div class="login-wrapper">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-8 col-md-5 col-lg-4">
              <div class="login-new-container d-flex align-items-center">
                <div>
                  <h1>&emsp;<img style="align-content: center;" src='/assets/images/logo.png' /></h1>
                  <h3 style="text-align: center;color: #b91f1c;">Welcome to <br/>Agile University Partner University Login</h3>
                    <form action="<?php echo base_url(); ?>partnerUniversityLogin/partnerUniLogin" method="post">
                      <div class="form-group">
                        <label>Login ID</label>
                        <input type="text" class="form-control" placeholder="Login ID" name="email" required>
                      </div>
                      <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" placeholder="Password" name="password" required>
                      </div>
                      <button type="submit" class="btn btn-primary btn-block">Login</button>
                     <!--  <div class="login-links">
                         <p><a href="#">Forgot password?</a></p> 
                        <hr />
                        <p><a href="/applicantLogin/applicantRegistration">Applicant Registration</a></p>
                      </div> -->
                    </form>                                                      
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>  
    <script src="<?php echo BASE_PATH; ?>js/jquery-1.12.4.min.js"></script>
    <script src="<?php echo BASE_PATH; ?>js/bootstrap.min.js"></script>
</body>

</html>