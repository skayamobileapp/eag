<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>School Management | Applicant Login</title>
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/main.css">
</head>

<body>
  <div class="login-wrapper">
    <div class="container">
      <div class="login-container" > 
        <div class="text-center">
          <a href="/"><img src="<?php echo BASE_PATH; ?>assets/img/logo-2.png" /></a>     
        </div>
        <h3 class="login-title">Applicant Login</h3>
        <div>
          <?php
          $this->load->helper('form');
          $error = $this->session->flashdata('error');
          if ($error) {
          ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <?php echo $error; ?>
            </div>
          <?php }
          $success = $this->session->flashdata('success');
          $entered_url = $this->session->flashdata('entered_url');
          // print_r($success);exit();
    
          if ($success)
          {
          ?>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $success; ?>
              </div>
          <?php
          }
          ?>
        </div>
        <div>

          <form action="<?php echo base_url(); ?>applicantLogin/applicantLogin" method="post">
            <div class="form-group">
              <label>Email Address</label>
              <input type="email" class="form-control" placeholder="Email" name="email" required>
            </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" class="form-control" placeholder="Password" name="password" required>
            </div>
            <button type="submit" class="btn btn-primary btn-block">Login</button>
            <div class="login-links">
              <!-- <p><a href="/applicantLogin/applicantRegistration">Applicant Registration</a></p> -->
              <p><a href="#">Forgot password?</a></p>
              
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="helpinfo-block">
        <h4>New Registration? <a href="/applicantLogin/applicantRegistration">Click Here</a></h4>
        <h4>For international applicants :</h4>
        <ul>
          <li>Please use your passport number for registration and use your registered email for login.</li>
          <li>Please download and read Guidelines For The Acceptance Of International Students Who Have Not Met The English Language Requirement For Enrolling As A Student In Malaysia. <a href="http://online.usas.edu.my/postgraduate/v2/English_Requiremt_2-21-Dec-2017.pdf" target="_blank">Click Here</a></li>
        </ul>
        <h4>For any enquiries, please contact <a href="mailto:admission.usas@eag.com.my">admission.usas@eag.com.my</a></h4>
    </div>    
  </div>

  <script src="<?php echo BASE_PATH; ?>assets/js/jquery-1.12.4.min.js"></script>
  <script src="<?php echo BASE_PATH; ?>assets/js/bootstrap.min.js"></script>
</body>

</html>